<?php

require_once __DIR__ . '/constants.php';
require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use xc\Controllers\FrontController;

$timeout = getenv('ADMIN_PHP_TIMEOUT') ?: 300;
set_time_limit($timeout);

function app()
{
    static $app = null;
    if (!$app) {
        $app = new FrontController(Request::createFromGlobals(), __DIR__, ENVIRONMENT);
    }
    return $app;
}

try {
    app()->handleRequest();
} catch (Exception $e) {
    app()->debugException($e);
    echo $e->getMessage();
}
