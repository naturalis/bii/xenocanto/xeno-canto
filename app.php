<?php

require_once __DIR__ . '/constants.php';
require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use xc\Controllers\FrontController;

$timeout = getenv('APP_PHP_TIMEOUT') ?: 120;
set_time_limit($timeout);

function app()
{
    static $app = null;
    if (!$app) {
        $app = new FrontController(Request::createFromGlobals(), __DIR__, ENVIRONMENT);
    }
    return $app;
}

try {
    app()->handleRequest();
} catch (Exception $e) {
    app()->debugException($e);
    echo $e->getMessage();
}
