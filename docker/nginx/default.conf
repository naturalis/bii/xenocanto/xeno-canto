limit_req_zone $binary_remote_addr zone=rate_limited:10m rate=10r/s;
limit_req_zone $binary_remote_addr zone=api_rate_limited:10m rate=1r/s;
limit_conn_zone $binary_remote_addr zone=connection_limited:10m;

server {
    listen 80 default_server;

    server_name _;

    access_log /proc/self/fd/1 json_combined;
    error_log /proc/self/fd/2 warn;

    root /var/www/;
    index app.php;

	# Maintenance: add temporary .maintenance file to root
	# docker-compose exec nginx sh; touch/rm .maintenance
	if (-f $document_root/.maintenance) {
		return 503;
	}

	location ~ favicon.ico {
        alias /static/img/favicon.png;
    }

	# Serve files in these directories directly
    location ~ /(static|graphics|sitemaps|ranges|sounds|docs)/.* {
        expires max;
        add_header Cache-Control "public, immutable";
        try_files $uri =404;
    }

	# Deny access to tasks, tools and vendor directories
    location ~ /(tasks|tools|vendor) {
        deny all;
        return 404;
    }

    # Deny access to dotfiles
    location ~ (^|/)\. {
        return 403;
    }

	# Dedicated rate limiting and CORS header for api
	location ~ api {
		limit_req zone=api_rate_limited burst=10 delay=2;
		limit_req_status 429;

        try_files $uri /app.php?$query_string;
	}

	# Rate limited and restrict number of connections and download rate for embedded sounds
	location ~ embed {
		limit_req zone=rate_limited burst=40 delay=5;
		limit_req_status 430;
        limit_conn connection_limited 100;
        limit_rate 500k;

        try_files $uri /app.php?$query_string;

    }

    location ~ app.php$ {
        fastcgi_split_path_info ^(.+?\.php)(|/.*)$;
        fastcgi_index app.php;
        fastcgi_read_timeout 120;
        fastcgi_param PHP_TIMEOUT 120;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param QUERY_STRING $query_string;
        fastcgi_intercept_errors on;
        include fastcgi_params;
        fastcgi_pass php-fpm:9000;
    }

    location ~ admin.php$ {
        fastcgi_split_path_info ^(.+?\.php)(|/.*)$;
        fastcgi_index admin.php;
        fastcgi_read_timeout 300;
        fastcgi_param PHP_TIMEOUT 300;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param QUERY_STRING $query_string;
        fastcgi_intercept_errors on;
        include fastcgi_params;
        fastcgi_pass php-fpm:9000;
    }

	# Default rate limiting
    location / {
		limit_req zone=rate_limited burst=50 nodelay;

        try_files $uri /app.php?$query_string;
   }

	# Longer timeouts, no rate limiting for admin pages and uploads
	location ~ /(admin|upload) {
        alias /var/www;

		try_files $uri /admin.php?$query_string;
	}

    # Return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    location ~ \.yml$ {
        return 404;
    }

	# Error pages
	error_page 503 @maintenance;
	error_page 430 @embed;

	location @maintenance {
		rewrite ^(.*)$ /static/maintenance/notice.html break;
	}

	location @embed {
		rewrite ^(.*)$ /static/maintenance/embed.html break;
	}

    client_max_body_size 128M;

    proxy_buffers 16 16k;
    proxy_buffer_size 16k;

    fastcgi_buffers 16 16k;
    fastcgi_buffer_size 32k;

    gzip on;
    gzip_proxied any;
    gzip_static on;
    gzip_http_version 1.0;
    gzip_vary on;
    gzip_comp_level 6;
    gzip_types
        application/javascript
        application/json
        application/x-javascript
        application/xhtml+xml
        application/xml
        application/xml+rss
        image/svg+xml
        image/x-icon
        text/css
        text/javascript
        text/plain
        text/xml;
    gzip_buffers 16 16k;
    gzip_min_length 512;
}
