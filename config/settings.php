<?php

$db = [
    'host'     => getenv('MYSQL_HOST'),
    'database' => getenv('MYSQL_DATABASE'),
    'user'     => getenv('MYSQL_USER'),
    'password' => getenv('MYSQL_PASSWORD'),
];

$settings = [
    'debug' => [
        'db'    => $db,
        'debug' => true,
    ],
    'test' => [
        'db'    => $db,
        'debug' => true,
    ],
    'prod'  => [
        'db'    => $db,
        'debug' => false,
    ],
    'task'  => [
        'db'             => $db,
        'debug'          => true,
        'useMockSession' => true,
    ],
];
