<?php

namespace xc;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class XCLogger
{

    private $logger;

    public function __construct($channel = 'XC')
    {
        $this->logger = new Logger($channel);
        $level = strtolower(ENVIRONMENT) == 'prod' ? Logger::INFO : Logger::DEBUG;
        $stream = new StreamHandler('php://stdout', $level);
        $this->logger->pushHandler($stream);
    }

    public function logInfo($message, $context = [])
    {
        $this->logger->info($message, $context);
    }

    public function logDebug($message, $context = [])
    {
        $this->logger->debug($message, $context);
    }

    public function logWarn($message, $context = [])
    {
        $this->logger->warning($message, $context);
    }

    public function logError($message, $context = [], $sendMail = false)
    {
        $this->logger->error($message, $context);
        if ($sendMail) {
            if (!empty($context)) {
                $message .= "\n\n" . json_encode($context);
            }
            (new XCMail(WEBMASTER_EMAIL, '[xeno-canto] Error', $message))->send();
        }
    }

}
