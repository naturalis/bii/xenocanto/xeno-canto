<?php

namespace xc;

abstract class Map
{

    protected $m_canvasId;

    protected $m_markerSpecs;

    protected $m_cluster;

    protected $m_kmlURL;

    protected $m_processed;

    protected $m_markers;

    protected $m_results;

    protected $m_tag;

    protected $m_markerLimit = 0;

    protected $m_exceedsMarkerLimit = false;

    public function __construct($mapCanvasId, $dbResult = null, $cluster = false)
    {
        $this->m_canvasId = $mapCanvasId;
        $this->m_markerSpecs = [];
        $this->m_cluster = $cluster;
        $this->m_results = [];
        $this->m_processed = false;
        $this->m_markers = [];
        $this->m_tag = str_replace('.', '-', IMAGE_VERSION);

        if ($dbResult) {
            $this->addResults($dbResult);
        }
    }

    protected function addResults($dbResult)
    {
        if (!empty($dbResult)) {
            $this->m_results[] = $dbResult;
        }
    }

    public function setMarkerLimit($max)
    {
        $this->m_markerLimit = intval($max);
    }

    public function getMarkerLimit()
    {
        return $this->m_markerLimit;
    }

    public function exceedsMarkerLimit()
    {
        return $this->m_exceedsMarkerLimit;
    }

    public function getJS($autoInit = true)
    {
        $this->processResults();

        // and one for the markers
        $jsMarkers = $this->getMarkersJSON();
        // create a js variable that holds an array of MarkerImages
        $jsMarkerTypes = $this->getSpecsJSON();

        $clusterTF = 'false';
        if ($this->m_cluster) {
            $clusterTF = 'true';
        }

        $doinit = '';
        if ($autoInit) {
            $doinit = '
                var xcmap = null;
                jQuery(document).ready(function() {
                    xcmap = xc.initMap();
                });
                ';
        }

        $headerAdditions = '
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=en&amp;key=' . GOOGLE_MAPS_API_KEY . '"></script>
            <script type="text/javascript" src="/static/js/markerclusterer_packed.js"></script>
            <script type="text/javascript" src="/static/js/xc-map.js?' . $this->m_tag . '"></script>';
        $headerAdditions .= <<<EOT
                <script type="text/javascript">
                xc.initialMarkers = {
                    specs: $jsMarkerTypes,
                    markers: $jsMarkers
                }
                
                xc.initMap = function() {
                    try {
                        var map = xc.createMap("$this->m_canvasId", "$this->m_kmlURL", $clusterTF)
                        map.setMarkers(xc.initialMarkers.markers, xc.initialMarkers.specs, true);
                
                        return map;
                    } catch (e) {
                        return null;
                    }
                };
                
                $doinit
                </script>
                EOT;

        return $headerAdditions;
    }

    protected function processResults()
    {
        if ($this->m_processed) {
            return;
        }

        foreach ($this->m_results as $result) {
            if ($result->num_rows) {
                $result->data_seek(0);
            }

            $nrMarkers = 0;
            while ($row = $result->fetch_object()) {
                $marker = $this->createMarker($row);
                if ($marker) {
                    $specID = array_search($marker->spec, $this->m_markerSpecs, true);
                    if ($specID === false) {
                        // store a list of unique marker types so that we can use
                        // them to display a legend later on.
                        $this->m_markerSpecs[] = $marker->spec;
                        $specID = count($this->m_markerSpecs) - 1;
                        $nrMarkers = 0;
                    }
                    if ($this->m_markerLimit == 0 || $nrMarkers < $this->m_markerLimit) {
                        $marker->specID = $specID;
                        $this->m_markers[] = $marker;
                        $nrMarkers++;
                    } elseif (!$this->m_exceedsMarkerLimit) {
                        $this->m_exceedsMarkerLimit = true;
                    }
                }
            }
            if ($result->num_rows) {
                $result->data_seek(0);
            }
        }

        $this->m_processed = true;
    }

    abstract protected function createMarker($db_row);

    public function getMarkersJSON()
    {
        return json_encode($this->getMarkers());
    }

    public function getMarkers()
    {
        $this->processResults();

        $markers = [];
        foreach ($this->m_markers as $marker) {
            if ($marker && !$marker->isRestricted()) {
                $markers[] = $marker->asArray();
            }
        }

        return $markers;
    }

    public function getSpecsJSON()
    {
        return json_encode($this->getSpecs());
    }

    public function getSpecs()
    {
        $this->processResults();

        return $this->m_markerSpecs;
    }

    public function getLegendHtml()
    {
        // make sure that we've processed all of the database rows before
        // getting the legend so that the marker spec values are filled
        $this->processResults();

        return $this->getLegendVfunc();
    }

    protected function getLegendVfunc()
    {
        if (empty($this->m_markerSpecs)) {
            return '';
        }

        $html = '<ul>';

        foreach ($this->m_markerSpecs as $spec) {
            $html .= "<li><span class=\"marker-image\"><img class=\"icon\" src=$spec->url/></span> <span class=\"marker-text\">$spec->legendText</span></li>";
        }

        $html .= '</ul>';

        return $html;
    }

    public function hasRestrictedRecordings()
    {
        $this->processResults();

        foreach ($this->m_markers as $marker) {
            if ($marker && $marker->isRestricted()) {
                return true;
            }
        }

        return false;
    }
}