<?php

namespace xc;

class WorldArea
{

    private static $areas;

    public $branch;

    public $birdsoundsColumn;

    public $desc;

    public function __construct($branch, $birdsoundsColumn, $desc)
    {
        $this->branch           = $branch;
        $this->birdsoundsColumn = $birdsoundsColumn;
        $this->desc             = $desc;
    }

    public static function all()
    {
        return self::$areas;
    }

    public static function lookup($branch)
    {
        foreach (self::$areas as $area) {
            if ($area->branch === $branch) {
                return $area;
            }
        }
        return null;
    }

    public static function init()
    {
        self::$areas = [
            // country 'branch' value, birdsounds column name, description
            // FIXME: this should really get standardized...
            new WorldArea('africa', 'africa', _('Africa')),
            new WorldArea('america', 'neotropics', _('Americas')),
            new WorldArea('asia', 'asia', _('Asia')),
            new WorldArea('australia', 'australia', _('Australasia')),
            new WorldArea('europe', 'europe', _('Europe')),
        ];
    }
}

WorldArea::init();
