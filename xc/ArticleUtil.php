<?php

namespace xc;

use Samwilson\PhpFlickr\PhpFlickr as PhpFlickr;
use Symfony\Component\HttpFoundation\Request;

class ArticleUtil
{
    public static function generateBlog($blog)
    {
        $text = '';
        $paragraphs = preg_split("/(\r?\n){2,}/", $blog);

        for ($i = 0; $i < count($paragraphs); $i++) {
            if (preg_match('/\w/', $paragraphs[$i])) {
                $texts = self::createContent($paragraphs[$i]);
                $text .= HtmlUtil::formatUserText($texts[0]);
                if ($texts[1]) {
                    $text .= $texts[1];
                }
            }
        }

        return $text;
    }

    public static function createContent($para)
    {
        $orig_para = $para;
        $column = null;
        $parsed_para = null;

        if (preg_match('/^ROW/', $orig_para)) {
            $str = str_replace('ROW(', '', $orig_para);
            $str = str_replace("\n", '', $str);
            [$query, $limit] = explode(',', trim(rtrim(str_replace(')', '', $str))));
            if ($query) {
                $column = self::makeRowSonos($query, $limit);
            }
        } elseif (preg_match('/^MAP/', $orig_para)) {
            $str = str_replace('MAP(', '', $orig_para);
            $str = str_replace("\n", '', $str);
            $q = trim(rtrim(str_replace(')', '', $str)));
            if (count(explode(',', $q)) == 2) {
                [$query, $zoom] = explode(',', $q);
                if ($query) {
                    if (!$zoom) {
                        $zoom = 3;
                    }
                    [$markers, $centerx, $centery] = self::makeStaticMap($query);
                    $column = "<div><img src=\"//maps.google.com/staticmap?center=$centerx,$centery&maptype=terrain&zoom=$zoom&size=400x250&markers=$markers&key=" . GOOGLE_MAPS_API_KEY . "\"><p>
                    A map with recording localities for query <b>$query</b></p></div>";
                }
            }
        } elseif (preg_match('/^ATLAS/', $orig_para)) {
            $str = str_replace('ATLAS(', '', $orig_para);
            $str = str_replace("\n", '', $str);
            $q = trim(rtrim(str_replace(')', '', $str)));
            if (count(explode(',', $q)) == 2) {
                [$query, $atlas] = explode(',', $q);
                $atlas = trim($atlas);
                if ($query) {
                    $img = self::makeAtlas($query, $atlas);
                    $column = "<div>$img
                    <p>A country-based biodiversity map for <b>$query</b></p></div>";
                }
            }
        } elseif (preg_match('/^FLICKR/', $orig_para)) {
            $str = str_replace('FLICKR(', '', $orig_para);
            $str = str_replace("\n", '', $str);
            $q = trim(rtrim(str_replace(')', '', $str)));
            if (count(explode(',', $q)) == 2) {
                [$query, $nr] = explode(',', $q);
                if ($query) {
                    $flickr = self::flickr($query, 'small', $nr);
                    $column = "<div>$flickr<p>Flickr images for <b>$query</b></p></div>";
                }
            }
        } elseif (preg_match('/^\[\[(.*)\]\]/', $orig_para, $matches)) {
            if (preg_match_all('/\bXC[0-9]{1,6}\b/i', $matches[1], $matches)) {
                $xcnrs = [];
                foreach ($matches[0] as $xcnr) {
                    $xcnr = str_ireplace('XC', '', $xcnr);
                    $xcnrs[] = $xcnr;
                }
                $xcnrs = array_unique($xcnrs);
            }
            /*
            // FIXME: do this
            for ($i = 0; $i < count($xcnrs) -1; $i++) {
            if (!is_numeric($xcnrs[$i])) {
              $notjustxcnrs = 1;
            }
            }
             */

            if (isset($xcnrs)) {
                $column = self::createSonoColumn($xcnrs);
                $orig_para = null;
            }
        } else {
            $xcnrs = [];
            $matches = [];
            if (preg_match_all('/\bXC[0-9]{1,6}\b/i', $orig_para, $matches)) {
                foreach ($matches[0] as $xcnr) {
                    $xcnr = str_ireplace('XC', '', $xcnr);
                    $xcnrs[] = $xcnr;
                }
            }

            $xcnrs = array_unique($xcnrs);
            $parsed_para = $orig_para;

            if (count($xcnrs)) {
                $column = self::createSonoColumn($xcnrs);
            }
        }

        return [$parsed_para, $column];
    }

    public static function makeRowSonos($query, $limit)
    {
        $q = new Query($query);
        $q->setPageSize($limit);
        $res = $q->execute(1);
        $xcnrs = [];
        while ($row = $res->fetch_object()) {
            if ($row->snd_nr) {
                $xcnrs[] = $row->snd_nr;
            }
        }

        $text = '';
        if (count($xcnrs)) {
            $text .= self::createSonoColumn($xcnrs);
        }
        return $text;
    }

    public static function createSonoColumn($sndnrs)
    {
        $sonos = '<table>
            <tr>';
        $j = 0;
        foreach ($sndnrs as $sndnr) {
            if ($sndnr) {
                $j++;
                $sonos .= '<td valign=top>' . self::makeSonoCode($sndnr) . '</td>';
                if ($j % 3 == 0) {
                    $sonos .= '</tr><tr>';
                }
            }
        }
        $sonos .= '</tr></table>';
        return $sonos;
    }

    public static function makeSonoCode($sndnr)
    {
        $rec = Recording::load($sndnr);
        if (!$rec) {
            return "<div class='xc-audio'>Recording XC$sndnr does not exist in the database</div>";
        }
        return $rec->player();
    }

    public static function makeStaticMap($query)
    {
        $q = new Query($query);
        $reslocall = $q->getLocations(100);
        $lats = [];
        $longs = [];
        $markers = '';
        while ($row = $reslocall->fetch_object()) {
            if ($row->latitude != 0) {
                // reduce precision because there's an upper limit on the URL length
                // of the static map
                $lat = round($row->latitude, 2);
                $long = round($row->longitude, 2);
                $markers .= "$lat,$long|";
                $lats[] = $lat;
                $longs[] = $long;
            }
        }

        $centerx = ((max($lats) - min($lats)) / 2) + min($lats);
        $centery = ((max($longs) - min($longs)) / 2) + min($longs);
        return [$markers, $centerx, $centery];
    }

    public static function makeAtlas($query, $atlas)
    {
        $cntcode = [
            'AFGHANISTAN' => 'AF',
            'ALBANIA' => 'AL',
            'ALGERIA' => 'DZ',
            'AMERICAN SAMOA' => 'AS',
            'ANDORRA' => 'AD',
            'ANGOLA' => 'AO',
            'ANGUILLA' => 'AI',
            'ANTARCTICA' => 'AQ',
            'ANTIGUA AND BARBUDA' => 'AG',
            'ARGENTINA' => 'AR',
            'ARMENIA' => 'AM',
            'ARUBA' => 'AW',
            'AUSTRALIA' => 'AU',
            'AUSTRIA' => 'AT',
            'AZERBAIJAN' => 'AZ',
            'BAHAMAS' => 'BS',
            'BAHRAIN' => 'BH',
            'BANGLADESH' => 'BD',
            'BARBADOS' => 'BB',
            'BELARUS' => 'BY',
            'BELGIUM' => 'BE',
            'BELIZE' => 'BZ',
            'BENIN' => 'BJ',
            'BERMUDA' => 'BM',
            'BHUTAN' => 'BT',
            'BOLIVIA' => 'BO',
            'BOSNIA AND HERZEGOVINA' => 'BA',
            'BOTSWANA' => 'BW',
            'BOUVET ISLAND' => 'BV',
            'BRAZIL' => 'BR',
            'BRITISH INDIAN OCEAN TERRITORY' => 'IO',
            'BRUNEI' => 'BN',
            'BULGARIA' => 'BG',
            'BURKINA' => 'BF',
            'BURUNDI' => 'BI',
            'CAMBODIA' => 'KH',
            'CAMEROON' => 'CM',
            'CANADA' => 'CA',
            'CAPOEVERDE' => 'CV',
            'CAYMAN ISLANDS' => 'KY',
            'CAR' => 'CF',
            'CHAD' => 'TD',
            'CHILE' => 'CL',
            'CHINA' => 'CN',
            'CHRISTMAS ISLAND' => 'CX',
            'COCOS (KEELING) ISLANDS' => 'CC',
            'COLOMBIA' => 'CO',
            'COMOROS' => 'KM',
            'CONGO' => 'CG',
            'ZAIRE' => 'CD',
            'COOK ISLANDS' => 'CK',
            'COSTA_RICA' => 'CR',
            'IVORYCOAST' => 'CI',
            'CROATIA' => 'HR',
            'CUBA' => 'CU',
            'CYPRUS' => 'CY',
            'CZECH REPUBLIC' => 'CZ',
            'DENMARK' => 'DK',
            'DJIBOUTI' => 'DJ',
            'DOMINICA' => 'DM',
            'DOMINICAN REPUBLIC' => 'DO',
            'ECUADOR' => 'EC',
            'EGYPT' => 'EG',
            'EL_SALVADOR' => 'SV',
            'EQGUINEA' => 'GQ',
            'ERITREA' => 'ER',
            'ESTONIA' => 'EE',
            'ETHIOPIA' => 'ET',
            'FALKLAND ISLANDS' => 'FK',
            'FAROE ISLANDS' => 'FO',
            'FIJI' => 'FJ',
            'FINLAND' => 'FI',
            'FRANCE' => 'FR',
            'FRENCH_GUIANA' => 'GF',
            'FRENCH POLYNESIA' => 'PF',
            'FRENCH SOUTHERN TERRITORIES' => 'TF',
            'GABON' => 'GA',
            'THEGAMBIA' => 'GM',
            'GEORGIA' => 'GE',
            'GERMANY' => 'DE',
            'GHANA' => 'GH',
            'GIBRALTAR' => 'GI',
            'GREECE' => 'GR',
            'GREENLAND' => 'GL',
            'GRENADA' => 'GD',
            'GUADELOUPE' => 'GP',
            'GUAM' => 'GU',
            'GUATEMALA' => 'GT',
            'GUERNSEY' => 'GG',
            'GUINEA' => 'GN',
            'BISSAU' => 'GW',
            'GUYANA' => 'GY',
            'HAITI' => 'HT',
            'HONDURAS' => 'HN',
            'HONG KONG' => 'HK',
            'HUNGARY' => 'HU',
            'ICELAND' => 'IS',
            'INDIA' => 'IN',
            'INDONESIA' => 'ID',
            'IRAN' => 'IR',
            'IRAQ' => 'IQ',
            'IRELAND' => 'IE',
            'ISLE OF MAN' => 'IM',
            'ISRAEL' => 'IL',
            'ITALY' => 'IT',
            'JAMAICA' => 'JM',
            'JAPAN' => 'JP',
            'JERSEY' => 'JE',
            'JORDAN' => 'JO',
            'KAZAKHSTAN' => 'KZ',
            'KENYA' => 'KE',
            'KIRIBATI' => 'KI',
            'SOUTH KOREA' => 'KP',
            'NORTH KOREA' => 'KR',
            'KUWAIT' => 'KW',
            'KYRGYZSTAN' => 'KG',
            'LAOS' => 'LA',
            'LATVIA' => 'LV',
            'LEBANON' => 'LB',
            'LESOTHO' => 'LS',
            'LIBERIA' => 'LR',
            'LIBYA' => 'LY',
            'LIECHTENSTEIN' => 'LI',
            'LITHUANIA' => 'LT',
            'LUXEMBOURG' => 'LU',
            'MACAO' => 'MO',
            'MACEDONIA' => 'MK',
            'MADAGASCAR' => 'MG',
            'MALAWI' => 'MW',
            'MALAYSIA' => 'MY',
            'MALDIVES' => 'MV',
            'MALI' => 'ML',
            'MALTA' => 'MT',
            'MARSHALL ISLANDS' => 'MH',
            'MARTINIQUE' => 'MQ',
            'MAURITANIA' => 'MR',
            'MAURITIUS' => 'MU',
            'MAYOTTE' => 'YT',
            'MEXICO' => 'MX',
            'MICRONESIA' => 'FM',
            'MOLDOVA' => 'MD',
            'MONACO' => 'MC',
            'MONGOLIA' => 'MN',
            'MONTENEGRO' => 'ME',
            'MONTSERRAT' => 'MS',
            'MOROCCO' => 'MA',
            'MOZAMBIQUE' => 'MZ',
            'MYANMAR' => 'MM',
            'NAMIBIA' => 'NA',
            'NAURU' => 'NR',
            'NEPAL' => 'NP',
            'NETHERLANDS' => 'NL',
            'NETHERLANDS ANTILLES' => 'AN',
            'NEW CALEDONIA' => 'NC',
            'NEW ZEALAND' => 'NZ',
            'NICARAGUA' => 'NI',
            'NIGER' => 'NE',
            'NIGERIA' => 'NG',
            'NIUE' => 'NU',
            'NORFOLK ISLAND' => 'NF',
            'NORTHERN MARIANA ISLANDS' => 'MP',
            'NORWAY' => 'NO',
            'OMAN' => 'OM',
            'PAKISTAN' => 'PK',
            'PALAU' => 'PW',
            'PALESTINIAN TERRITORY' => 'PS',
            'PANAMA' => 'PA',
            'PAPUA NEW GUINEA' => 'PG',
            'PARAGUAY' => 'PY',
            'PERU' => 'PE',
            'PHILIPPINES' => 'PH',
            'PITCAIRN' => 'PN',
            'POLAND' => 'PL',
            'PORTUGAL' => 'PT',
            'PUERTO RICO' => 'PR',
            'QATAR' => 'QA',
            'REUNION' => 'RE',
            'ROMANIA' => 'RO',
            'RUSSIA' => 'RU',
            'RWANDA' => 'RW',
            'SAINT BARTHÉLEMY' => 'BL',
            'SAINT HELENA' => 'SH',
            'SAINT KITTS AND NEVIS' => 'KN',
            'SAINT LUCIA' => 'LC',
            'SAINT MARTIN' => 'MF',
            'SAINT PIERRE AND MIQUELON' => 'PM',
            'SAINT VINCENT AND THE GRENADINES' => 'VC',
            'SAMOA' => 'WS',
            'SAN MARINO' => 'SM',
            'STOME' => 'ST',
            'SAUDI ARABIA' => 'SA',
            'SENEGAL' => 'SN',
            'SERBIA' => 'RS',
            'SEYCHELLES' => 'SC',
            'SIERRALEONE' => 'SL',
            'SINGAPORE' => 'SG',
            'SLOVAKIA' => 'SK',
            'SLOVENIA' => 'SI',
            'SOLOMON ISLANDS' => 'SB',
            'SOMALIA' => 'SO',
            'SAFRICA' => 'ZA',
            'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS' => 'GS',
            'SPAIN' => 'ES',
            'SRI LANKA' => 'LK',
            'SUDAN' => 'SD',
            'SURINAME' => 'SR',
            'SVALBARD AND JAN MAYEN' => 'SJ',
            'SWAZILAND' => 'SZ',
            'SWEDEN' => 'SE',
            'SWITZERLAND' => 'CH',
            'SYRIA' => 'SY',
            'TAIWAN' => 'TW',
            'TAJIKISTAN' => 'TJ',
            'TANZANIA' => 'TZ',
            'THAILAND' => 'TH',
            'TIMOR-LESTE' => 'TL',
            'TOGO' => 'TG',
            'TOKELAU' => 'TK',
            'TONGA' => 'TO',
            'TRINIDAD AND TOBAGO' => 'TT',
            'TUNISIA' => 'TN',
            'TURKEY' => 'TR',
            'TURKMENISTAN' => 'TM',
            'TURKS AND CAICOS ISLANDS' => 'TC',
            'TUVALU' => 'TV',
            'UGANDA' => 'UG',
            'UKRAINE' => 'UA',
            'UNITED ARAB EMIRATES' => 'AE',
            'UNITED KINGDOM' => 'GB',
            'UNITED STATES' => 'US',
            'UNITED STATES MINOR OUTLYING ISLANDS' => 'UM',
            'URUGUAY' => 'UY',
            'UZBEKISTAN' => 'UZ',
            'VANUATU' => 'VU',
            'VENEZUELA' => 'VE',
            'VIETNAM' => 'VN',
            'VIRGIN ISLANDS, BRITISH' => 'VG',
            'VIRGIN ISLANDS, U.S.' => 'VI',
            'WALLIS AND FUTUNA' => 'WF',
            'WESTERN SAHARA' => 'EH',
            'YEMEN' => 'YE',
            'ZAMBIA' => 'ZM',
            'ZIMBABWE' => 'ZW',
        ];

        $cnts = [];
        foreach (array_keys($cntcode) as $cnt) {
            $cnts[] = $cnt;
        }

        // only support world maps for now
        $atlas = 'world';

        $cntc = $cntcode['ANTARCTICA'];
        $cntd = ['0'];

        $q = new Query($query);
        $res = $q->execute();

        $data = [];
        while ($row = $res->fetch_array()) {
            $i = $row['country'];
            if (in_array(strtoupper($i), $cnts)) {
                if (isset($data[$i])) {
                    $data[$i]++;
                } else {
                    $data[$i] = 1;
                }
            }
        }
        foreach (array_keys($data) as $cnt) {
            $cntc .= $cntcode[strtoupper($cnt)];
            $cntd[] = $data[$cnt];
        }

        return '<img src=//chart.apis.google.com/chart?cht=t&chs=440x220&chd=s:' . self::encode($cntd) . "&chco=ffffff,edf0d4,13390a&chld=$cntc&chtm=$atlas&chf=bg,s,EAF7FE>";
    }

    public static function encode($data)
    {
        // first normalize data between 0 and 61
        $m = 1000000000;
        $M = -100000000000;
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i] < $m) {
                $m = $data[$i];
            }
            if ($data[$i] > $M) {
                $M = $data[$i];
            }
        }

        for ($i = 0; $i < count($data); $i++) {
            $data[$i] = round(($data[$i] - $m) / ($M - $m) * 61);
        }

        $encoded = '';
        $simple_encoding = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for ($i = 0; $i < count($data); $i++) {
            $c = $data[$i];
            $encoded .= substr($simple_encoding, $c, 1);
        }

        return $encoded;
    }

    public static function flickr($search, $size, $nr)
    {
        if ($size == '') {
            $size = 'Square';
        }
        if ($nr == '') {
            $nr = 5;
        }
        if ($nr > 6) {
            $nr = 6;
        }
        $f = new phpFlickr('6a4213da1df5326f39cdb519a8e2cbdd');
        $photos = $f->photos_search([
            'text' => "$search",
            'per_page' => "$nr",
            'sort' => 'relevance',
        ]);
        $text = '';
        foreach ((array)$photos['photo'] as $photo) {
            $text .= "<a href='//www.flickr.com/photos/" . $photo['owner'] . '/' . $photo['id'] . "' target=_blank>";
            $text .= "<img border='0' alt='$photo[title]' src=" . $f->buildPhotoURL($photo, "$size") . '> ';
            $text .= '</a>';
        }
        return $text;
    }

    public static function featureDeleteForm($blognr, $method = 'get')
    {
        return "<form id='delete-form' action='" . Library::getUrl('feature-delete', ['blognr' => $blognr]) . "' method='$method'>
            <input type='submit' id='delete-button' class='delete' value='Delete this Article'/>
            </form>";
    }

    public static function featureEditForm($blognr = null)
    {
        if (!User::current()) {
            return '';
        }

        $html = $text = $title = '';
        $blognr = intval($blognr);
        $user = User::current();
        $author = $user->userName();
        $public = 0;

        $allowed = true;
        if ($blognr) {
            $sql = "SELECT * FROM blogs WHERE blognr = $blognr";
            $res = Library::query($sql);
            $row = $res->fetch_object();

            // first test to see if the surfer is indeed the owner of the blog
            if ($row->dir != $user->userId() && !$user->isAdmin()) {
                $allowed = false;
            } else {
                $text = Library::sanitize($row->blog);
                $title = Library::sanitize($row->title);
                $public = $row->public;
                $author = Library::sanitize($row->author);
            }
        } else {
            // Would have been better to pass the request, but overkill to create a separate method
            $request = Request::createFromGlobals();
            $text = Library::sanitize($request->request->get('blog'));
            $author = Library::sanitize($request->request->get('author')) ?: $user->userName();
            $title = Library::sanitize($request->request->get('title'));
        }

        if (!$allowed) {
            $html .= HtmlUtil::printErrorMessages(_('Insufficient privileges'),
                [_('You do not have permission to edit this article')]);
        } else {
            $publicSelect = "
                <select name='public'>
                <option value='0' " . ($public == 0 ? 'selected' : '') . '>' . _('Draft') . "</option>
                <option value='1' " . ($public == 1 ? 'selected' : '') . '>' . _('Published') . "</option>";
            if ($user->isAdmin() || isset($row->dir) && $row->dir == $user->userId() && $public == 2) {
                $publicSelect .= "<option value='2' " . ($public == 2 ? 'selected' : '') . '>' . _('Featured on front page') . '</option>';
            }
            $publicSelect .= '</select>';

            $html .= "
                <form method='post'>
                <input type='hidden' name='blognr' value='$blognr'>
                <input type='hidden' name='submitted' value='1'>
                <p>
                <input name='title' type='text' value='$title' placeholder='" . htmlspecialchars(_('Article Title'),
                    ENT_QUOTES) . "'>
                </p>
                <p>
                <input name='author' type='text' value='$author' placeholder='" . htmlspecialchars(_('Author Name'),
                    ENT_QUOTES) . "'>
                </p>
                $publicSelect
                 <p class='important'><b>" . _('Note') . '</b>: ' . sprintf(_('You can format your text using the %s text formatting syntax.'),
                    "<a target='_blank' href='" . Library::getUrl('markdown') . "'>Markdown</a>") . "</p>
                <p>
                <textarea name='blog' placeholder='" . htmlspecialchars(_('Article Text'), ENT_QUOTES) . "'>$text</textarea>
                </p>
                <p>
                <input type='submit' value='" . htmlspecialchars(_('Save'), ENT_QUOTES) . "'>
                </p>
                </form>";
        }
        return $html;
    }
}