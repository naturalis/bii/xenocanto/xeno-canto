<?php

namespace xc;

abstract class UploadStep
{
    public const LOCATION = 1;

    public const DETAILS = 2;

    public const VERIFY = 3;

    public const RESULTS = 4;

    // Semi-hard code which groups should display temperature field in upload screen
    public static $groupsUsingTemperature = [2, 4];

    protected $groups = [];

    protected $categories = [];

    protected $properties = [];

    protected $m_step;

    protected $m_data;

    public function __construct($step, &$data)
    {
        $this->m_step = $step;
        $this->m_data = $data;
    }

    public static function validateCountryString($country)
    {
        // Check for a valid country
        if (empty($country)) {
            return false;
        }

        $countries = HtmlUtil::getCountryList();
        foreach ($countries as $candidate) {
            if (html_entity_decode($country) === $candidate) {
                return true;
            }
        }

        return false;
    }

    public static function jsVariables()
    {
        return "
            <script type='text/javascript'>
                var locationApiUrl = '" . Library::getUrl('api-location-search') . "';
                var completionLocationUrl = '" . Library::getUrl('api-completion-locations') . "';
                var none = '" . _('none') . "';
                var completionSspUrl = '" . Library::getUrl('api-completion-subspecies') . "';
            </script>";
    }

    public static function getCountryByCode($code)
    {
        return self::countries()[$code] ?? null;
    }

    protected static function countries()
    {
        $res = Library::query('SELECT country, country_code FROM world_country_list ORDER BY country');
        if ($res) {
            while ($row = $res->fetch_object()) {
                $list[$row->country_code] = $row->country;
            }
        }
        return $list ?? [];
    }

    public static function validateDateString($datestr)
    {
        $valid = true;
        $dateParts = explode('-', $datestr);
        $dateRequirements = _("Dates must be entered in the format 'YYYY-MM-DD' (or '?' if unknown).");
        if (count($dateParts) != 3) {
            if ($datestr == '') {
                Library::notifyError(htmlspecialchars(_("Date was not specified.")) . " $dateRequirements");
                $valid = false;
            } elseif ($datestr !== '?') {
                Library::notifyError(
                    htmlspecialchars(sprintf(_("'%s' is not a valid date."), $datestr)) . " $dateRequirements"
                );
                $valid = false;
            }
        } else {
            foreach ($dateParts as &$v) {
                // for validation, just consider an unknown day or month as a 1
                if ($v == '??' || $v == '00') {
                    $v = 1;
                }
            }
            if (!checkdate(intval($dateParts[1]), intval($dateParts[2]), intval($dateParts[0]))) {
                Library::notifyError(htmlspecialchars(sprintf(_("'%s' is not a valid date."), $datestr)) . " $dateRequirements");
                $valid = false;
            } elseif (strtotime($datestr) > time()) {
                Library::notifyError(_("You can't upload recordings from the future... yet."));
                $valid = false;
            }
        }

        return $valid;
    }

    public static function validateTimeString($timestr)
    {
        $valid = true;
        $timeRequirements = _(
            "If the exact time is unknown, please use '?'; If only an approximate time is known (e.g. 'morning'), please mention that in the remarks."
        );
        // check for valid time of day
        if (empty($timestr)) {
            Library::notifyError(_('Time was not specified') . ". $timeRequirements");
            $valid = false;
        } else {
            $timeFormatError = false;
            if ($timestr != '?') {
                $parts = explode(':', $timestr);
                if (count($parts) != 2) {
                    $timeFormatError = true;
                } elseif (!is_numeric($parts[0]) || !is_numeric($parts[1]) || strlen($parts[1]) != 2) {
                    $timeFormatError = true;
                } else {
                    $hour = intval($parts[0]);
                    $min = intval($parts[1]);
                    if (($hour < 0 || $hour > 23) || ($min < 0 || $min > 59)) {
                        $timeFormatError = true;
                    }
                }
            }
            if ($timeFormatError) {
                Library::notifyError(
                    htmlspecialchars(
                        sprintf(_("Invalid time specified: '%s'"), $timestr)
                    ) . " $timeRequirements"
                );
                $valid = false;
            }
        }

        return $valid;
    }

    public static function inStudioInputId()
    {
        $res = Library::query('select id from sound_properties where property = "studio recording"');
        return $res->fetch_object()->id ?? null;
    }

    public function pageId()
    {
        return '';
    }

    abstract public function validate($request);

    protected function setFormProperties()
    {
        if (!$this->categories || !$this->properties) {
            $res = Library::query(
                "select t4.id as group_id, t4.name as group_name, t3.category, 
                    t2.id as property_id, t2.property, t3.multiple 
                from group_sound_properties as t1
                left join sound_properties as t2 on t1.property_id = t2.id
                left join sound_property_categories as t3 on t2.category_id = t3.id
                left join groups as t4 on t1.group_id = t4.id
                order by t4.id, t3.sort_order, t1.sort_order"
            );
            while ($row = $res->fetch_object()) {
                $this->categories[$row->group_id][$row->category] = $row->multiple;
                $this->properties[$row->group_id][$row->category][$row->property_id] = $row->property;
                $this->groups[$row->group_id] = $row->group_name;
            }
        }
    }

    protected function isSoundscape()
    {
        if (isset($this->m_data->recordingIdType)) {
            return $this->m_data->recordingIdType == 'envrec';
        }
        return $this->m_data->speciesNr == Species::soundscapeSpeciesNumber();
    }

}
