<?php

namespace xc;

class License
{

    private static $licenses;

    // id that is stored in the database
    public $id;

    // url describing the license text
    public $url;

    // attributes of this license, used for displaying icons, etc.  This could
    // theoretically be parsed from the $id, but I'd rather make it more
    // explicit in case a new license type gets added in the future that doesn't
    // follow the same naming system
    public $attrs;

    public $name;

    public function __construct($id, $url, $name, $attrs)
    {
        $this->id = $id;
        $this->url = $url;
        $this->name = $name;
        $this->attrs = $attrs;
    }

    public static function init()
    {
        self::$licenses = [
            new License(
                'CC-by-nc-nd-2.5',
                '//creativecommons.org/licenses/by-nc-nd/2.5/',
                'Creative Commons Attribution-NonCommercial-NoDerivs 2.5',
                ['by', 'nc', 'nd']
            ),
            new License(
                'CC-by-nc-sa-3.0',
                '//creativecommons.org/licenses/by-nc-sa/3.0/',
                'Creative Commons Attribution-NonCommercial-ShareAlike 3.0',
                ['by', 'nc', 'sa']
            ),
            new License(
                'CC-by-nc-nd-3.0',
                '//creativecommons.org/licenses/by-nc-nd/3.0/',
                'Creative Commons Attribution-NonCommercial-NoDerivs 3.0',
                ['by', 'nc', 'nd']
            ),
            new License(
                'CC-by-sa-3.0',
                '//creativecommons.org/licenses/by-sa/3.0/',
                'Creative Commons Attribution-ShareAlike 3.0',
                ['by', 'sa']
            ),
            new License(
                'CC-by-nc-sa-3.0-us',
                '//creativecommons.org/licenses/by-nc-sa/3.0/us/',
                'Creative Commons Attribution-NonCommercial-ShareAlike 3.0 United States',
                ['by', 'nc', 'sa']
            ),

            new License(
                'CC-by-nc-sa-4.0',
                '//creativecommons.org/licenses/by-nc-sa/4.0/',
                'Creative Commons Attribution-NonCommercial-ShareAlike 4.0',
                ['by', 'nc', 'sa']
            ),
            new License(
                'CC-by-nc-nd-4.0',
                '//creativecommons.org/licenses/by-nc-nd/4.0/',
                'Creative Commons Attribution-NonCommercial-NoDerivs 4.0',
                ['by', 'nc', 'nd']
            ),
            new License(
                'CC-by-sa-4.0',
                '//creativecommons.org/licenses/by-sa/4.0/',
                'Creative Commons Attribution-ShareAlike 4.0',
                ['by', 'sa']
            ),

            new License(
                'CC-by-nc-4.0',
                '//creativecommons.org/licenses/by-nc/4.0/',
                'Creative Commons Attribution-NonCommercial 4.0',
                ['by', 'nc']
            ),
            new License(
                'CC-by-4.0',
                '//creativecommons.org/licenses/by/4.0/',
                'Creative Commons Attribution 4.0',
                ['by']
            ),
            new License(
                'CC0',
                '//creativecommons.org/publicdomain/zero/1.0/',
                'Creative Commons Public Domain Dedication 1.0',
                ['pd']
            ),
        ];
    }

    public static function all()
    {
        return self::$licenses;
    }

    public static function uploadOptions()
    {
        return [
            'CC-by-nc-sa-4.0',
            'CC-by-nc-nd-4.0',
            'CC-by-sa-4.0',
            'CC-by-nc-4.0',
            'CC-by-4.0',
            'CC0',
        ];
    }

    public static function thirdParty()
    {
        return [
            -1 => 'Select an option...',
            1 => 'Yes, I allow XC to share metadata under CC BY-NC',
            0 => "No, please don't share metadata",
        ];
    }

    public static function lookupById($id)
    {
        for ($i = 0; $i < count(self::$licenses); $i++) {
            if (self::$licenses[$i]->id === $id) {
                return self::$licenses[$i];
            }
        }

        return null;
    }

    public static function lookupByName($name)
    {
        for ($i = 0; $i < count(self::$licenses); $i++) {
            if (self::$licenses[$i]->name === $name) {
                return self::$licenses[$i];
            }
        }

        return null;
    }

    public static function lookupByAttributes($attrs)
    {
        $results = [];
        $attrs = array_map('strtolower', array_map('trim', $attrs));
        sort($attrs);

        for ($i = 0; $i < count(self::$licenses); $i++) {
            $testAttrs = self::$licenses[$i]->attrs;
            sort($testAttrs);

            if (array_values($attrs) == array_values($testAttrs)) {
                $results[] = self::$licenses[$i];
            }
        }

        return $results;
    }
}

License::init();
