<?php

namespace xc;

class NewsComment
{
    private $m_row;

    public function __construct($db_row)
    {
        $this->m_row = $db_row;
    }

    public function id()
    {
        return $this->m_row->nr;
    }

    public function text()
    {
        return HtmlUtil::formatUserTextLimited($this->m_row->text);
    }

    public function authorWithLink()
    {
        if (!empty($this->m_row->userid)) {
            return "<a href='" . Library::getUrl('recordist', ['id' => $this->m_row->userid]) . "'>{$this->m_row->name}</a>";
        }
        return $this->author();
    }

    public function author()
    {
        return $this->m_row->name;
    }

    public function date()
    {
        return HtmlUtil::formatDate('%B %e, %Y', $this->m_row->date);
    }

}
