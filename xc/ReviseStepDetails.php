<?php

namespace xc;

class ReviseStepDetails extends UploadStepDetailsBase
{

    public function content($request)
    {
        $output = $this->showUploadDetailsForm($request, _('Update'), true);
        $output .= $this->script();

        return $output;
    }
}
