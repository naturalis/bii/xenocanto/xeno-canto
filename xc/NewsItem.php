<?php

namespace xc;

class NewsItem
{
    private $m_text;

    private $m_id;

    private $m_comments;

    private $m_date;

    public function __construct($db_row)
    {
        $this->m_text = $db_row->text;
        $this->m_id = $db_row->news_nr;
        $this->m_date = $db_row->date;
        $this->m_comments = null;
    }

    public static function load($id)
    {
        $nr = intval($id);
        $sql = "SELECT * FROM news_world WHERE news_nr = $nr";
        $res = Library::query($sql);
        $row = $res->fetch_object();

        return $row ? new NewsItem($row) : null;
    }

    public static function loadRecent($nr = 0)
    {
        $items = [];
        if (empty($nr)) {
            $nr = 4;
        }

        $sql = "SELECT * FROM news_world ORDER BY news_nr DESC LIMIT $nr";
        $res = Library::query($sql);
        while ($row = $res->fetch_object()) {
            $items[] = new NewsItem($row);
        }

        return $items;
    }

    //=====================
    // make the news for the front page
    //=====================

    public static function loadForYear($year)
    {
        $year = intval($year);
        if (empty($year)) {
            return;
        }

        $items = [];
        $sql = "SELECT * FROM news_world WHERE YEAR(date) = $year ORDER BY news_nr DESC";
        $res = Library::query($sql);
        while ($row = $res->fetch_object()) {
            $items[] = new NewsItem($row);
        }

        return $items;
    }

    public function toHtml()
    {
        $date = HtmlUtil::formatDate('%B %e, %Y', $this->m_date);
        $articleContent = $this->text();

        $news = "
            <article>
            <footer>$date
            ";

        $user = User::current();
        if ($user && $user->isAdmin()) {
            $editUrl = Library::getUrl('admin-news-edit', ['id' => $this->id()]);
            $deleteUrl = Library::getUrl('admin-news-delete', ['id' => $this->id()]);
            $news .= "
                <a href='$editUrl'><img class='icon' width='16' height='16' src='/static/img/edit-toolbar.png'/>Edit</a>
                <a href='$deleteUrl'><img class='icon' width='16' height='16' src='/static/img/delete-toolbar.png'/>Delete</a>";
        }

        $news .= "
            </footer>
            <p>
            $articleContent
            </p>
            <section class='comments'>";

        $comments = $this->comments();
        $nr_replies = count($comments);
        foreach ($comments as $comment) {
            $author = $user && $user->isAdmin() ? $comment->authorWithLink() : $comment->author();
            $dateStr = sprintf(_('%s on %s'), $author, $comment->date());
            $content = $comment->text();
            $deleteLink = '';
            if ($user && $user->isAdmin()) {
                $deleteLink = "<img class='icon' src='/static/img/admin-16.png' title='xeno-canto administrator'/> <a href='" . Library::getUrl('admin-delete-comment',
                        ['t' => 'discuss_news_world', 'id' => $comment->id()]
                    ) . "'>(delete this comment)</a>";
            }
            $news .= "
                <article>
                <div>$content $deleteLink
                </div>
                <footer>$dateStr</footer>
                </article>
                ";
        }
        if ($nr_replies) {
            $news .= "
                <p><b>$nr_replies</b> replies ::
                ";
        }// if nr_replies>0
        $news .= "
            <img width='14' height='14' src='/static/img/discuss-light.png'> <a href='" . Library::getUrl('news_discussion',
                ['news_nr' => $this->id()]
            ) . "'>" . _('Your reply') . '</a>
            </p>
            </section>
            </article>
            ';

        return $news;
    }

    public function text()
    {
        return HtmlUtil::formatUserTextLimited($this->m_text);
    }

    public function id()
    {
        return $this->m_id;
    }

    public function comments()
    {
        if (is_null($this->m_comments)) {
            $sql = "SELECT * FROM discuss_news_world WHERE news_nr = '$this->m_id' ORDER BY nr ASC";
            $res = Library::query($sql);
            $this->m_comments = [];
            while ($row = $res->fetch_object()) {
                $this->m_comments[] = new NewsComment($row);
            }
        }

        return $this->m_comments;
    }
}
