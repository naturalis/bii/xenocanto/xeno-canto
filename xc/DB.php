<?php

namespace xc;

use mysqli;
use Symfony\Component\HttpFoundation\Request;

class DB
{
    private $db_connection;

    private $logger;

    public function __construct($settings)
    {
        $this->db_connection = new mysqli(
            $settings->dbHost(), $settings->dbUser(), $settings->dbPassword(), $settings->dbDatabase()
        );
        $this->db_connection->set_charset('utf8');

        if (PROFILE_QUERIES) {
            $this->logger = new XCLogger('XC Profiler');
        }
    }

    public function query($sql)
    {
        $start = microtime(true);
        $result = $this->db_connection->query($sql) or Library::logger()->logError(
            $sql . ":\n" . $this->db_connection->error
        );
        $time = microtime(true) - $start;

        if (PROFILE_QUERIES && $time > intval(PROFILE_THRESHOLD)) {
            $request = Request::createFromGlobals();
            $message = json_encode([
                'ip' => $request->getClientIp(),
                'time' => round($time, 3),
                'uri' => $request->getRequestUri(),
                'query' => trim(preg_replace('!\s+!', ' ', $sql)),
            ]);
            $this->logger->logInfo($message);
        }
        return $result;
    }

    public function getConnection()
    {
        return $this->db_connection;
    }

    public function insertId()
    {
        return $this->db_connection->insert_id;
    }

}
