<?php

namespace xc\Models;

class Model
{
    public static function query($sql): \mysqli_result|bool
    {
        return self::db()->query($sql);
    }

    public static function db(): \xc\DB
    {
        return app()->db();
    }

    public static function mysqli(): \mysqli
    {
        return self::db()->getConnection();
    }
}