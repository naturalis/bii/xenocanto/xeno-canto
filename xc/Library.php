<?php

namespace xc;

require_once './constants.php';
require_once './xc/Models/Model.php';

use xc\Models\Model;

class Library
{
    private static $logger;

    public static function rateRecording($sndNr, $quality, $userId = null)
    {
        if (!$userId) {
            $userId = User::current()->userId();
        }

        if (isset($quality) && isset($sndNr)) {
            if ($quality >= 1 && $quality <= 5) {
                // this is experimental for now, a table with per-user ratings
                Library::query("INSERT INTO birdsounds_user_ratings (snd_nr, userid, rating, date)
                    VALUES($sndNr, '$userId', $quality, NOW())
                    ON DUPLICATE KEY UPDATE rating = VALUES(rating), date=NOW()"
                );

                $sql = "UPDATE birdsounds SET quality = '$quality' WHERE snd_nr = '$sndNr'";
                $res = Library::query($sql);

                return (bool)$res;
            }
        }
        return false;
    }

    public static function query($sql)
    {
        return Model::query($sql);
    }

    public static function db()
    {
        return Model::db();
    }

    public static function nextSessionCounter()
    {
        $i = app()->session()->get('counter', 0);
        $i++;
        app()->session()->set('counter', $i);

        return $i;
    }

    public static function localNameLanguage()
    {
        $lang = app()->determineBestLanguage();
        return $lang ? $lang->localNameLanguage : 'english';
    }

    public static function acceptableErrors($n)
    {
        $target = 0.99;
        $nominal = (1.0 - $target) * $n;
        $padding = sqrt($target * $n);

        return min(ceil($nominal + $padding), $n);
    }

    public static function addUrlParamsToCurrentPage($request, $params)
    {
        $url = $request->getBaseUrl();
        $vars = $request->query->all();
        foreach ($params as $key => $val) {
            $vars[$key] = $val;
        }
        return $url . '?' . http_build_query($vars);
    }

    public static function getUrl($route, $args = [], $absolute = false)
    {
        return app()->getUrl($route, $args, $absolute);
    }

    public static function resolveForumMystery($xcid)
    {
        $res = Library::query('
            SELECT topic_nr FROM forum_world WHERE type = ' . ThreadType::MYSTERY . " AND snd_nr = $xcid"
        );
        $topic = $res->fetch_object();
        Library::query('
            UPDATE forum_world SET type = ' . ThreadType::MYSTERY_RESOLVED . ' WHERE topic_nr = ' . $topic->topic_nr
        );
        Library::query('DELETE FROM forum_topic_thread_types WHERE topic_nr = ' . $topic->topic_nr);
        (new XCRedis())->clearForumCache();
    }

    public static function notifyError($text)
    {
        app()->session()->getFlashBag()->add('error', $text);
    }

    public static function notifyWarning($text)
    {
        app()->session()->getFlashBag()->add('warning', $text);
    }

    public static function notifySuccess($text)
    {
        app()->session()->getFlashBag()->add('success', $text);
    }

    public static function notifyInfo($text)
    {
        app()->session()->getFlashBag()->add('info', $text);
    }

    public static function sanitize($var)
    {
        if (is_array($var)) {
            foreach ($var as $k => $v) {
                $var[$k] = self::sanitize($v);
            }
        } else {
            $var = htmlspecialchars($var ?? '', ENT_QUOTES, null, false);
        }
        return $var;
    }

    public static function adminMailAddresses()
    {
        return array_merge([WEBMASTER_EMAIL], array_map('trim', explode(',', ADMINS_EMAIL)));
    }

    public static function obfuscateEmail($address, $link)
    {
        return '
            <script>
            document.write(
                "<n uers=\"' . str_rot13('mailto:' . $address) . '\" ery=\"absbyybj\">' . str_rot13($link) . '</n>" .
                replace(/[a-zA-Z]/g, 
                    function(c) {
                        return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                    }
                )
            )
            </script>
        ';
    }

    public static function strip($var, $escape = true, $allowTags = false)
    {
        $var = !$allowTags ? strip_tags($var) : HtmlUtil::sanitizeHtml($var, HtmlUtil::htmlSanitizerConfigDefault());
        return $escape ? self::escape($var) : $var;
    }

    public static function escape($var)
    {
        if (is_array($var)) {
            foreach ($var as $k => $v) {
                $var[$k] = self::escape($v);
            }
        } else {
            $var = self::mysqli()->real_escape_string($var ?? '');
        }
        return $var;
    }

    public static function mysqli()
    {
        return Model::mysqli();
    }

    public static function defaultNumPerPage()
    {
        $user = User::current();
        return $user ? $user->numResultsPerPage() : 30;
    }

    public static function defaultResultsView()
    {
        $user = User::current();
        return $user ? $user->viewPreference() : ViewType::DETAILED;
    }

    public static function logger()
    {
        if (!self::$logger) {
            self::$logger = new XCLogger('XC');
        }
        return self::$logger;
    }
}
