<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Library;
use xc\User;
use xc\XCMail;

class NewsItem extends Controller
{
    public function handlePost($news_nr)
    {
        if (!$news_nr) {
            return $this->notFound();
        }

        $incomplete = false;
        $text = $this->request->request->get('text');
        $sanitizedText = HtmlUtil::formatUserTextLimited($text);

        if (!User::current()) {
            $this->notifyError(_('You must be logged in to post a comment'));
            $incomplete = true;
        } elseif (!User::current()->isVerified()) {
            $this->notifyError(_('You must verify your email address before posting a comment.') . ' ' .
                sprintf(_("To re-send a verification code, please visit <a href='%s'>your account page</a>."),
                    $this->getUrl('mypage')
                )
            );
            $incomplete = true;
        } elseif (empty($sanitizedText)) {
            $this->notifyError(_('You cannot post an empty comment.'));
            $incomplete = true;
        }

        if (!$incomplete) {
            $user = User::current();

            $name = $user->userName();
            $dir = $user->userId();
            $date = date('Y-m-d');
            $time = date('H:i');
            $text = $this->escape($text);
            $sql = "
                INSERT INTO discuss_news_world 
                (`news_nr`, `text`, `name`, `date`, `time`, `datetime`, `userid`)
                VALUES 
                ($news_nr, '$text', '$name', '$date', '$time', NOW(), '$dir')";
            if (!$this->query($sql)) {
                $this->notifyError('Database error: Unable to save reply');
            } else {
                $text = "$name responded to a news item:\n\n" . $text . "\n\n" . $this->getUrl('news_discussion',
                        ['news_nr' => $news_nr], true
                    );

                (new XCMail(Library::adminMailAddresses(), '[xeno-canto] News item front page', $text))->send();
                $this->notifySuccess(_('Comment posted'));
                return $this->seeOther($this->getUrl('news_discussion', ['news_nr' => $news_nr]));
            }
        }

        return $this->renderContent($news_nr, $text);
    }

    protected function renderContent($news_nr, $replyText = null)
    {
        $title = sprintf(_('News discussion %d'), $news_nr);
        $item = \xc\NewsItem::load($news_nr);

        if (!$item) {
            return $this->notFound();
        }

        $output = "
            <div id='news-discussion'>
            <h1>News Item $news_nr</h1>" . $item->toHtml();

        if (User::current()) {
            $output .= '
                <h2>' . _('Reply') . "</h2>
                <p class='important'>" . sprintf(_('You can format your text using the %s text formatting syntax.'
                ), "<a target='_blank' href='" . $this->getUrl('markdown') . "'>Markdown</a>"
                ) .  ' ' . _("Images and embedded sonograms are not allowed in comments.") ."</p>
                <p>
                <form action='' method='post'>
                <input type='hidden' name='news_nr' value='$news_nr'/>
                <input type='hidden' name='submit' value='1'/>
                <textarea autofocus name='text' placeholder='" . htmlspecialchars(_('Your reply'), ENT_QUOTES) . "...'>$replyText</textarea>
                <input type='submit' value='" . htmlspecialchars(_('Submit'), ENT_QUOTES) . "'/>
                </form>
                ";
        }
        $output .= '</div>';

        return $this->template->render($output, ['title' => $title]);
    }

    public function handleRequest($news_nr)
    {
        return $this->renderContent($news_nr);
    }

}
