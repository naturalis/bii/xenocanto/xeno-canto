<?php

namespace xc\Controllers;

use xc\AudioUtil;
use xc\Library;
use xc\Recording;
use xc\RecordingFormData;
use xc\ReviseStepDetails;
use xc\Species;
use xc\ThreadType;
use xc\UploadStep;
use xc\UploadStepLocation;
use xc\User;
use xc\XCRedis;

class ReviseRecording extends UploadBase
{
    public function start($xcid)
    {
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $i = Library::nextSessionCounter();
        $allowed = false;

        $user = User::current();
        if ($user->isAdmin() || $user->isUploader($rec)) {
            $allowed = true;
        }

        if (!$allowed) {
            return $this->unauthorized(_('You do not have permission to edit this recording'));
        }

        // we passed all tests above, so we're ready to start a new revising session
        $formData = RecordingFormData::fromRecording($rec, false);
        $_SESSION[$this->formDataVar($i)] = $formData;
        $_SESSION[$this->redirectUrlVar($i)] = $this->request->headers->get('Referer');
        return $this->proceed(UploadStep::LOCATION, $i);
    }

    protected function formDataVar($i)
    {
        return "revise-form-data-$i";
    }

    protected function redirectUrlVar($i)
    {
        return "revise-redirect-url-$i";
    }

    protected function proceed($step, $id)
    {
        $url = $this->getUrl('revise-step', ['i' => $id, 'step' => $step]);
        return $this->seeOther($url);
    }

    public function revise($i, $nextStep)
    {
        $title = _('Revise Recording');
        $data = $this->formData($i);

        // by this point, we better have some form data to edit...
        if (!$data) {
            return $this->badRequest('Expired edit session');
        }

        $output = '<h1>' . sprintf(_('Edit File Metadata for %s'), "XC{$data->id}") . '</h1>';
        $step = null;
        switch ($nextStep) {
            case UploadStep::LOCATION:
                $step = new UploadStepLocation($data);
                break;

            case UploadStep::DETAILS:
                $step = new ReviseStepDetails($data);
                break;

            case UploadStep::RESULTS:
                $url = $_SESSION[$this->redirectUrlVar($i)];
                if (empty($url)) {
                    $url = $this->getUrl('recording', ['xcid' => $data->id]);
                }
                unset($_SESSION[$this->formDataVar($i)]);
                unset($_SESSION[$this->redirectUrlVar($i)]);
                return $this->seeOther($url);
        }

        $output .= $step->content($this->request);
        return $this->template->render($output, ['title' => $title, 'bodyId' => $step->pageId()]);
    }

    protected function formData($i)
    {
        // this needs to be a reference, and we need to pass it by reference to the
        // various validate functions so that the session variable gets updated in-place
        // with the submitted for data.
        $data = &$_SESSION[$this->formDataVar($i)];
        return $data;
    }

    public function handlePost($i, $lastStep)
    {
        $formData = $this->formData($i);

        switch ($lastStep) {
            case UploadStep::LOCATION:
                $step = new UploadStepLocation($formData);
                if (!$step->validate($this->request)) {
                    return $this->proceed(UploadStep::LOCATION, $i);
                } else {
                    return $this->proceed(UploadStep::DETAILS, $i);
                }

            // no break
            case UploadStep::DETAILS:
                $step = new ReviseStepDetails($formData);
                if (!$step->validate($this->request)) {
                    return $this->proceed(UploadStep::DETAILS, $i);
                } else {
                    if ($this->request->request->get('back')) {
                        return $this->proceed(UploadStep::LOCATION, $i);
                    } else {
                        return $this->commitData($formData, $i);
                    }
                }
        }
        return $this->badRequest();
    }

    protected function commitData($formData, $i)
    {
        $d = self::prepareSubmittedValuesForUpload($formData);

        $res = $this->query("SELECT species_nr, discussed, group_id FROM birdsounds WHERE snd_nr={$d->id}");
        $row = $res->fetch_object();

        $updateStatus = '';
        $updateSonos = $row->group_id != $d->groupId;

        // if the user is revising a recording to change the species to something
        // other than 'identity unknown', automatically mark it as resolved
        if ($row && ($row->discussed == ThreadType::MYSTERY) && ($d->speciesNr != $row->species_nr) && ($d->speciesNr != Species::mysterySpeciesNumber(
                ))) {
            $updateStatus = ', discussed = ' . ThreadType::MYSTERY_RESOLVED;
            // also update the status of the forum thread...
            Library::resolveForumMystery($d->id);

            $redis = new XCRedis();
            $redis->clearForumCache();
            $redis->clearMysteryCache();
        }

        $sqlUpdate = "UPDATE birdsounds SET
            genus='$d->genus',
            species='$d->species',
            ssp='$d->ssp',
            eng_name='$d->commonName',
            family='$d->family',
            songtype='$d->soundTypeExtra',
            quality=$d->quality,
            recordist='$d->recordist',
            date='$d->recordingDate',
            time='$d->time',
            country='$d->country',
            location='$d->location',
            longitude=$d->lng,
            latitude=$d->lat,
            elevation='$d->elevation',
            remarks='$d->remarks',
            back_extra='$d->bgExtra',
            species_nr='$d->speciesNr',
            order_nr='$d->speciesOrderNr',
                      
            group_id='$d->groupId',
            collection_date=$d->collectionDate,
            temperature=$d->temperature,
            collection_specimen='$d->specimen',
            device='$d->device',
            microphone='$d->microphone',
            automatic=$d->automatic,
            observed=$d->animalSeen,
            playback=$d->playbackUsed,
                     
            neotropics='$d->americas',
            africa='$d->africa',
            asia='$d->asia',
            europe='$d->europe',
            australia='$d->australia',
                      
            license='$d->license',
            form_data='" . self::setHistoryData($d) . "'
            
            $updateStatus
            WHERE snd_nr=$d->id
            ;";

        if (!$this->query($sqlUpdate)) {
            Library::logger()->logError("Cannot update XC-$d->id: '$sqlUpdate': " . Library::mysqli()->error);
        }
        if (!self::updateBackgroundSpecies($d->id, $d->bgSpecies) || !self::updateSoundProperties($d->id,
                $d->soundProperties
            )) {
            Library::logger()->logError("Cannot update background species or sound properties for XC-$d->id");
        }
        if (!AudioUtil::writeId3($d->id)) {
            Library::logger()->logWarn("Could not write id3 tags for XC-$d->id");
        }
        if ($updateSonos && !AudioUtil::createMp3($d->id)) {
            Library::logger()->logError("Could not create mp3(s) for XC-$d->id");
        }


        return $this->proceed(UploadStep::RESULTS, $i);
    }
}
