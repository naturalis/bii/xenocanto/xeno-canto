<?php

namespace xc\Controllers;

use xc\User;
use xc\XCMail;

class VerifyEmail extends Controller
{
    public function resend()
    {
        $user = User::current();
        if (!$user) {
            return $this->unauthorized();
        }
        if (!$user->isVerified()) {
            $dir = $user->userId();
            $res = $this->query("SELECT username, email, verificationCode FROM users WHERE dir = '$dir'");
            $row = $res->fetch_object();
            if ($row) {
                $verificationUrl = $this->getUrl('verify-email', [
                    'userid' => $dir,
                    'code' => $row->verificationCode
                ],true);
                $message = self::verificationMessage($this->sanitize($row->username), $verificationUrl);
                (new XCMail($row->email, 'Verification code for xeno-canto.org', $message))->send();

                $this->notifySuccess("The verification code has been sent to $row->email");
            } else {
                $this->notifyError(_("Couldn't send verification code"));
            }
        } else {
            $this->notifyError(_("This email address has already been verified"));
        }

        return $this->seeOther($this->getUrl('index'));
    }

    public static function verificationMessage($userName, $verificationCode)
    {
       return sprintf("
Dear %s,

Please visit the following URL in your browser to verify
your email address:

%s

Best regards,

The xeno-canto.org team", $userName, $verificationCode);
    }

    public function handleRequest($userid, $code)
    {
        $title = _('Verify Email Address');
        $body = "<h1>$title</h1>";

        $userid = $this->escape($userid);
        $res = $this->query("SELECT dir, email, verified, verificationCode FROM users WHERE dir = '$userid'");
        if (!$res || $res->num_rows == 0) {
            return $this->badRequest("User doesn't exist");
        } else {
            $row = $res->fetch_object();
            if ($row->verified) {
                $body .= '<p>' . _('This email address has already been verified.') . '</p>';
            } elseif ($row->verificationCode !== $code) {
                $body .= "<div class='error'>" . _('Verification code does not match') . '</div>';
            } else {
                $this->query("UPDATE users SET verified = 1 WHERE dir = '$userid'");
                $body .= '<p>' . _('Your email address is now verified') . '</p>';
                if (User::current()) {
                    User::current()->reloadFromDatabase();
                }
            }
        }

        return $this->template->render($body, ['title' => $title]);
    }
}
