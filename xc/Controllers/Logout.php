<?php

namespace xc\Controllers;

use xc\Library;
use xc\User;

class Logout extends Controller
{
    public function handleRequest()
    {
        $user = User::current();
        if ($user) {
            $user->logout();
            $this->notifySuccess(_('You have been logged out.'));
        }
        app()->session()->clear();

        return $this->seeOther($this->getUrl('index'));
    }
}
