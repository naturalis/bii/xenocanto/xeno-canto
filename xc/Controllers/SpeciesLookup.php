<?php

namespace xc\Controllers;

use xc\Library;
use xc\Species;

class SpeciesLookup extends Controller
{
    public function handleRequest()
    {
        $q = strip_tags($this->request->query->get('query'));

        if (!empty($q)) {
            $spnr = Species::getSpeciesNumberForString($q);
            if ($spnr) {
                $sp = Species::load($spnr);
                if ($sp) {
                    return $this->seeOther($sp->profileURL());
                }
            }
            return $this->seeOther($this->getUrl('browse', ['query' => $q]));
        }

        return $this->seeOther($this->getUrl('index'));
    }
}
