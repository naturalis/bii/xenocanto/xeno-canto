<?php

namespace xc\Controllers;

use xc\ForumPost;
use xc\ForumThread;
use xc\HtmlUtil;
use xc\Library;
use xc\Recording;
use xc\StaticMap;
use xc\ThreadType;
use xc\User;
use xc\XCMail;
use xc\XCRedis;

class ForumTopic extends Controller
{
    private $thread;

    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public static function postSystemReply($topicNr, $replyText, $sender = 'System')
    {
        if (empty($topicNr) || empty($replyText)) {
            Library::logger()->logError("[System forum post] No topic or reply text specified");
            return false;
        }

        $sender = Library::escape($sender);
        $date = date('Y-m-d');
        $tm = date('H:i');
        $reply = Library::escape($replyText);

        $sql = "
            INSERT INTO discussion_forum_world 
            (`name`, `text`, `date`, `time`, `branch`, `dir`, `topic_nr`, `datetime`)
            VALUES 
            ('$sender', '$reply', '$date', '$tm', '', '" . SYSTEM_USERID . "', $topicNr, NOW())";
        if (Library::query($sql)) {
            // Update number of replies
            Library::query("UPDATE `forum_world` SET `replies` = (`replies` + 1) WHERE `topic_nr` = $topicNr");

            // Send email only to topic starter
            $message = ForumPost::formatNotificationEmail($topicNr, $replyText);
            $res = Library::query("
                SELECT users.email
                FROM forum_world AS forum
                LEFT JOIN users ON forum.userid = users.dir
                WHERE forum.topic_nr = $topicNr"
            );
            $row = $res->fetch_object();
            (new XCMail($row->email, "[xeno-canto] Automatic comment on forum topic $topicNr", $message))->send();

            (new XCRedis())->clearForumCache();
            return true;
        }

        return false;
    }

    public function handlePost($topic_nr)
    {
        $replyText = $this->request->request->get('text');
        $isPreview = $this->request->request->get('preview', false);
        if (!$topic_nr) {
            return $this->notFound();
        }

        if ($this->request->request->get('update')) {
            return $this->POSTUpdate($topic_nr, $this->request->request->get('type'),
                $this->request->request->get('branch'), $this->request->request->get('group_id')
            );
        } elseif ($this->request->request->get('admin-delete')) {
            return $this->POSTDelete($topic_nr);
        }

        if ($this->request->request->get('resolved')) {
            return $this->POSTResolve($topic_nr);
        } else {
            return $this->POSTReply($topic_nr, $replyText, $isPreview);
        }
    }

    protected function POSTUpdate($topic_nr, $type, $branch, $groupId)
    {
        $user = User::current();
        if (!$user) {
            return $this->unauthorized();
        }

        $branch = $this->escape($branch);
        $topic_nr = intval($topic_nr);
        $type = intval($type);
        $groupId = intval($groupId);

        $this->query("
            UPDATE forum_world SET type = $type, branch = '$branch', group_id = $groupId WHERE topic_nr = $topic_nr"
        );
        $this->query("DELETE FROM forum_topic_thread_types WHERE topic_nr = $topic_nr");
        $res = $this->query("SELECT snd_nr FROM forum_world WHERE topic_nr = $topic_nr");
        $row = $res->fetch_object();
        if ($row->snd_nr) {
            $this->query("UPDATE birdsounds SET discussed = $type WHERE snd_nr = $row->snd_nr");
        }

        $this->redis->clearForumCache();
        return $this->seeForumTopic($topic_nr);
    }

    private function seeForumTopic($topic_nr, $anchor = null)
    {
        $url = $this->getUrl('discuss_forum', ['topic_nr' => $topic_nr]);
        if ($anchor) {
            $url .= "#$anchor";
        }
        return $this->seeOther($url);
    }

    private function POSTDelete($topic_nr)
    {
        $user = User::current();
        if (!($user && $user->isAdmin())) {
            return $this->unauthorized();
        }

        if ($this->request->request->get('confirm-delete') == 'yes') {
            $this->query("DELETE FROM forum_world WHERE topic_nr = $topic_nr");
            $this->query("DELETE FROM discussion_forum_world WHERE topic_nr = $topic_nr");
            $this->query("DELETE FROM forum_topic_thread_types WHERE topic_nr = $topic_nr");
            $this->redis->clearForumCache();
            return $this->seeOther($this->getUrl('forum'));
        }

        $res = $this->query("SELECT subject FROM forum_world WHERE topic_nr = $topic_nr");
        $row = $res->fetch_object();
        $subj = $row->subject;

        $output = "<h1>Delete Topic</h1>
            <p>Do you really want to delete forum thread $topic_nr: <em>$subj</em>?</p>
            <form method='post'>
            <input type='hidden' name='topic_nr' value='$topic_nr'>
            <input type='hidden' name='confirm-delete' value='yes'>
            <input type='submit' name='admin-delete' value='Delete topic'/>
            </form>";

        return $this->template->render($output, ['title' => "Delete Forum thread $topic_nr?"]);
    }

    protected function POSTResolve($topic_nr)
    {
        $user = User::current();
        if (!$user) {
            return $this->unauthorized();
        }
        $this->query("INSERT IGNORE INTO forum_topic_thread_types VALUES ($topic_nr, " . ThreadType::REPORT_RESOLVED . ')');
        $this->notifySuccess(
            _("Thank you for notifying the admins that this recording's identification discussion has been resolved!")
        );
        $this->redis->clearForumCache();
        return $this->seeForumTopic($topic_nr);
    }

    private function POSTReply($topic_nr, $replyText, $isPreview)
    {
        $replyFailed = false;

        if (User::current()) {
            if (!User::current()->isVerified()) {
                $replyFailed = true;
                $this->notifyError(_('You must verify your email address before posting a comment.') . ' ' .
                    sprintf(_("To re-send a verification code, please visit <a href='%s'>your account page</a>."), $this->getUrl('mypage')
                    )
                );
            }
            if (!$this->request->request->get('topic_nr')) {
                $replyFailed = true;
                $this->notifyError('Internal error: No topic specified');
            }

            if (empty(HtmlUtil::formatUserText($replyText))) {
                $replyFailed = true;
                $this->notifyError(_('You cannot post an empty comment.'));
            } elseif (strlen($replyText) > 65500) {
                $replyFailed = true;
                $this->notifyError(_('Your reply text is too long.'));
            }

            if (!$replyFailed) {
                if (!$isPreview) {
                    if ($this->processReply($topic_nr, $replyText)) {
                        $this->redis->clearForumCache();
                        return $this->seeForumTopic($topic_nr, 'last');
                    } else {
                        $this->notifyError('Database error: Unable to save reply');
                    }
                }
            }
        } else {
            $this->notifyError(_('You must be logged in to post a comment'));
        }

        return $this->renderContent($topic_nr, $replyText);
    }

    private function processReply($topic_nr, $replyText)
    {
        $dir = User::current()->userId();

        // attempt to prevent duplicate submissions
        $res = $this->query("SELECT dir, text from discussion_forum_world WHERE topic_nr = $topic_nr ORDER BY mes_nr DESC LIMIT 1");
        $row = $res->fetch_object();
        if ($row && ($row->text == $replyText) && ($row->dir == $dir)) {
            return true;
        }

        $name = $this->escape(User::current()->userName());
        $date = date('Y-m-d');
        $tm = date('H:i');
        $etext = $this->escape($replyText);

        $sql = "
            INSERT INTO discussion_forum_world 
            (`name`, `text`, `date`, `time`, `branch`, `dir`, `topic_nr`, `datetime`)
            VALUES 
            ('$name', '$etext', '$date', '$tm', '', '$dir', $topic_nr, NOW())";
        if ($this->query($sql)) {
            // Update number of replies (XC-46)
            $this->query("UPDATE `forum_world` SET `replies` = (`replies` + 1) WHERE `topic_nr` = $topic_nr");

            $this->notifyTopicParticipants($topic_nr, $replyText);
            $this->notifySuccess(_('Comment posted'));
            return true;
        }
        return false;
    }

    private function notifyTopicParticipants($nr, $text)
    {
        if (!User::current()) {
            return false;
        }

        $nr = intval($nr);
        $userid = User::current()->userId();
        $res = $this->query("SELECT users.email, users.dir, users.username, subject, type
            FROM forum_world AS forum
            LEFT JOIN users ON forum.userid = users.dir
            WHERE forum.topic_nr = $nr"
        );
        $row = $res->fetch_object();
        if (!$row) {
            return false;
        }

        $addresses = [];
        // always notify the topic originator (unless this is the topic originator
        // posting a comment)
        if ($row->dir != $userid) {
            $addresses[$row->email] = $row->username;
        }

        $subject = $row->subject;
        $commenter = User::current()->userName();

        // get a list of all topic participants and their email addresses
        $sql = "
            (SELECT  users.email, users.username
            FROM discussion_forum_world AS cmt
            LEFT JOIN users USING(dir)
            WHERE cmt.topic_nr = $nr AND dir != '$userid')

            UNION
            
            (SELECT users.email, users.username
            FROM forum_world AS forum
            INNER JOIN birdsounds USING(snd_nr)
            INNER JOIN users USING(dir)
            WHERE forum.topic_nr = $nr AND dir != '$userid')";

        $res = $this->query($sql);
        while ($row = $res->fetch_object()) {
            $addresses[$row->email] = $row->username;
        }

        if (!empty($addresses)) {
            $wrappedText = wordwrap($text, 60);
            foreach ($addresses as $email => $username) {
                $messageBody = <<<EOT
Dear $username,

xeno-canto user $commenter has posted the following comment on
a forum topic that you have participated in:

Topic: $subject

$wrappedText
EOT;
                $message = ForumPost::formatNotificationEmail($nr, $messageBody);
                (new XCMail($email, "[xeno-canto] New comment on forum topic $nr", $message))->send();
            }
        }
    }

    protected function renderContent($topic_nr, $replyText = null)
    {
        $topic_nr = intval($topic_nr);
        $sql = "SELECT subject, snd_nr FROM forum_world WHERE topic_nr = '$topic_nr'";
        $res = $this->query($sql);
        $row = $res->fetch_object();
        if (!$row) {
            return $this->notFound();
        }

        $topic = $this->sanitize($row->subject);
        $player = $map = '';

        if (!empty($row->snd_nr)) {
            $recording = Recording::load($row->snd_nr);

            if ($recording) {
                $userHasAccess = !$recording->restrictedForUser(User::current());
                $player = $recording->player([
                    'showLocation' => $userHasAccess,
                    'showDate' => true,
                    'linkFields' => $userHasAccess,
                ]);
                if ($recording->hasCoordinates() && $userHasAccess) {
                    $staticMap = new StaticMap($recording->latitude(), $recording->longitude());
                    $staticMap->setSize(300, 300);
                    $map = "<div id='map-canvas'>{$staticMap->getMapImage()}</div>";
                }
            } else {
                $player = "<p class='important'>" . sprintf(_('Recording %s was not found.  It may have been deleted.'),
                        "<strong>XC$row->snd_nr</strong>"
                    ) . '</p>';
            }
        }

        // Create the thread before the actions form is generated, as we need info on the number of replies
        $this->thread = new ForumThread($topic_nr);

        $output = "<ul class='breadcrumbs'>
            <li><a href='" . $this->getUrl('forum') . "'>" . _('Forum') . "</a></li>
            <li class='current'>$topic</li>
            </ul>" . //add a drop down menu to admin the thread type
            $this->renderActionsForm($topic_nr) . "
            <div>
            <section class='column forum-thread'>";

        $output .= $this->thread->generateHTML();

        if (User::current()) {
            $output .= "
                <!-- reply to this forum -->
                <a name='reply'></a>
                <h2>" . _('Reply to this topic') . '</h2>';

            if (!empty(HtmlUtil::formatUserText($replyText))) {
                $output .= "<p class='important'>" . _('This is only a preview, your comment has not been saved yet.'
                    ) . "</p>
                    <article class='comment-preview'> " . ForumPost::formatBodyText($replyText) . '
                    </article>
                    ';
            }

            $output .= "
                <form class='new-message' action='#reply' method='post'>
                <input type='hidden' name='topic_nr' value='$topic_nr'>
                <input type='hidden' name='submit' value='1'>
                <p>" . sprintf(_('You can format your text using the %s text formatting syntax.'),
                    "<a target='_blank' href='" . $this->getUrl('markdown') . "'>Markdown</a>"
                ) . ' ' . _('Typing the catalogue number for a recording (for example XC20464) will cause a player for that recording to be appended to your comment.'
                ) . "</p>
                <p>
                <p>
                <textarea class='message-text' name='text' placeholder='" . $this->sanitize(_('Message Text')) . "'>" . $this->sanitize($this->request->request->get('text')) . "</textarea>
                </p>
                <input type='submit' name='preview' value='" . $this->sanitize(_('Preview')) . "'>
                <input type='submit' name='submit' value='" . $this->sanitize(_('Submit')) . "'>
                </form>
                ";
        } else {
            $output .= '
                <p>' . sprintf(_("<a href='%s'>Log in</a> to reply to this topic."), $this->getUrl('login')) . '
                </p>';
        }

        $output .= "
            </section>
            <section id='forum-thread-recording' class='column'>
            $player
            $map
            </section>
            </div>";

        return $this->template->render($output, ['title' => "$topic :: topic $topic_nr"]);
    }

    private function renderActionsForm($topic_nr)
    {
        $user = User::current();
        $menu = '';

        // Better safe than sorry
        if (!$this->thread) {
            $this->thread = new ForumThread($topic_nr);
        }

        if ($user) {
            $q = "
                SELECT t1.`type`, t1.`branch`, t1.`userid`, t1.`group_id`, t2.`type_id` AS resolved
                FROM forum_world AS t1
                LEFT JOIN forum_topic_thread_types AS t2 ON t1.topic_nr = t2.topic_nr
                WHERE t1.topic_nr = '$topic_nr'";
            $res = $this->query($q);
            $row = $res->fetch_object();

            $menu = "
                <form method='post'>
                <input type='hidden' name='topic_nr' value='$topic_nr'>\n";

            if ($user->isAdmin()) {
                $typeSelect = HtmlUtil::threadTypeSelect('admin', 'type', $row->type
                    ) . HtmlUtil::regionSelect('branch', $row->branch);

                $groupSelect = "
                    <select name='group_id'>
                    <option value='0'>" . _("Any group") . "</option>\n";

                foreach ($this->groups() as $id => $name) {
                    $selected = $id == $row->group_id ? 'selected' : '';
                    $groupSelect .= "<option value='$id' $selected>" . ucfirst(_($name)) . "</option>";
                }
                $groupSelect .= "</select>";

                $menu .= "
                    <img class='icon' src='/static/img/admin-16.png'/> " . $typeSelect . $groupSelect . "
                    <input type='submit' name='update' value='Update'>
                    <input type='submit' name='admin-delete' value='Delete thread'/>";
            }

            // Report id/mystery as solved
            if ($row->type == ThreadType::ID_QUESTIONED || ($row->type == ThreadType::MYSTERY && $this->thread->nrReplies(
                    ) >= 1)) {
                $disabled = $row->resolved == ThreadType::REPORT_RESOLVED ? 'disabled' : '';
                $menu .= "<p><input type='submit' name='resolved' $disabled value='Report as resolved'/></p>\n";
            }

            $menu .= '</form>';
        }

        return $menu;
    }

    public function handleRequest($topic_nr)
    {
        if (!$topic_nr) {
            return $this->notFound();
        }

        return $this->renderContent($topic_nr);
    }
}
