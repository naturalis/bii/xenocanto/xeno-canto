<?php

namespace xc\Controllers;

//require_once('vendor/recaptcha/recaptchalib.php');


use xc\Library;
use xc\User;
use xc\ViewType;
use xc\XCMail;

use function curl_exec;
use function curl_init;
use function curl_setopt;
use function json_decode;


class Register extends Controller
{
    public function handleRequest()
    {
        if (User::current()) {
            return $this->seeOther($this->getUrl('index'));
        }

        return $this->renderForm();
    }

    protected function renderForm(
        $name = null, $email = null, $recaptchaError = null)
    {
        $recaptcha = "";
        if (RECAPTCHA_SITE_KEY) {
            $recaptcha = '<div class="g-recaptcha" data-sitekey="' . RECAPTCHA_SITE_KEY . '"></div>
                 <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>';
        }


        $output = "<div id='login-form'>
            <h1>" . _('Register') . '</h1>

            <p>' . _('To register as a member, please fill out the form') . '.</p>
            <p>' . _("If you don't see an <a href='https://support.google.com/recaptcha' target='_blank'>\"I'm not a robot\" checkbox</a> in the form, you may need to adjust privacy plugins in your browser."
            ) . "</p>

            <form class='register' method='post'>
            <p>
            <input autofocus type='text' name='name' value='" . $this->sanitize($name) . "' placeholder='" . _('Name') . "'>
            </p>
            <p>
            <input type='email' name='email' value='" . $this->sanitize($email) . "' placeholder='" . _('Email Address') . "'>
            </p>
            <p>
            <input type='password' name='password' maxlength='" . MAX_LENGTH_PASSWORD. "' placeholder='" . _('Password') . "'>
            </p>
            <p>
            <input type='password' name='password2' maxlength='" . MAX_LENGTH_PASSWORD. "' placeholder='" . _('Type password again') . "'>
            </p>
            <p>$recaptcha</p>
            <input type='submit' value='" . _('Register') . "'>
            </form>
            <p>
            <a href='" . $this->getUrl('FAQ') . "#register'>" . _('Why become a member?') . '</a>
            </p>
            </div>
            ';

        return $this->template->render($output, ['title' => _('Register'), 'bodyId' => 'register']);
    }

    public function handlePost()
    {
        $incomplete = false;
        $recaptchaError = '';

        $userid = $this->getRandomDir();

        $name = trim($this->request->request->get('name'));
        $email = $this->request->request->get('email');
        $password = $this->request->request->get('password');
        $password2 = $this->request->request->get('password2');

        if (empty($this->strip($name))) {
            $this->notifyError(_('Name is required'));
            $incomplete = true;
        }
        if (empty($email)) {
            $this->notifyError(_('Email is required'));
            $incomplete = true;
        }
        if (empty($password)) {
            $this->notifyError(_('Password is required'));
            $incomplete = true;
        }
        if (empty($password2)) {
            $this->notifyError(_('Password Verification is required'));
            $incomplete = true;
        }

        if (!$incomplete) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $incomplete = true;
                $this->notifyError(_('That is not a valid email address. Please try again.'));
            }
            // check if password has been typed twice correctly.
            if ($password != $password2) {
                $incomplete = true;
                $this->notifyError(_('The two passwords do not match. Please try again.'));
            }
            if (strlen($password) < MIN_LENGTH_PASSWORD) {
                $this->notifyError(sprintf(_('Your password must be at least %d characters long.'), MIN_LENGTH_PASSWORD));
                $incomplete = true;
            }
            if (!preg_match("((?=.*[A-Z])(?=.*[a-z])(?=.*\d).{7,50})", $password)) {
                $this->notifyError(_('Your password must contain at least one uppercase letter, one lowercase letter and one number.'));
                $incomplete = true;
            }

            if (RECAPTCHA_SECRET_KEY && RECAPTCHA_VERIFY_URL) {
                $resp = $this->verifyRecaptcha(RECAPTCHA_SECRET_KEY, $this->request->request->get('g-recaptcha-response'),
                    $this->request->getClientIp()
                );
                if (!$resp->success) {
                    $this->notifyError(_('Your verification code was incorrect. Please try again.'));
                    $incomplete = true;
                    $recaptchaError = isset($resp->{'error-codes'}) && is_array($resp->{'error-codes'}) ?
                        implode('; ', $resp->{'error-codes'}) : '';
                }
            }

            // Test for unique email
            $sql = "SELECT username FROM users WHERE email = '$email'";
            $result = $this->query($sql);
            if ($result && $result->num_rows > 0) {
                $this->notifyError(_('This email address is already in use.'));
                $incomplete = true;
            }

            // Test for unique user name
            $name = $this->strip($name);
            $sql = "SELECT username FROM users WHERE username = '$name'";
            $result = $this->query($sql);
            if ($result && $result->num_rows > 0) {
                $this->notifyError(_('This user name is already in use.'));
                $incomplete = true;
            }

            // register the new member
            if (!$incomplete) {
                $salt = $this->getRandomSalt();
                $hashedPassword = User::hashPassword($password, $salt);

                $view = ViewType::DETAILED;
                $vc = User::otp();

                // set the current session language as the user's default
                // language, otherwise default to english
                $lang = app()->language() ? app()->language()->code : 'en';

                $sql = "
                        INSERT INTO users 
                        (`username`, `email`, `password`, `salt`, `dir`, `language`, `num_per_page`, 
                         `view`, `notification`, `downsample`, `blurb`, `region`, `license`, `joindate`,
                         `verified`, `verificationCode`, `contactform`, `third_party_license`)
                        VALUES 
                        ('$name', '$email', '$hashedPassword', '$salt', '$userid', '$lang', 30, 
                         $view, DEFAULT, 0, '', '', DEFAULT, NOW(), 0, '$vc', DEFAULT, -1)";
                $result = $this->query($sql);

                if ($result && Library::mysqli()->affected_rows == 1) {
                    User::loginWithPassword($email, $password);
                    $verificationUrl = $this->getUrl('verify-email', ['userid' => $userid, 'code' => $vc], true);
                    $uploadUrl = $this->getUrl('upload', [], true);
                    $forumUrl = $this->getUrl('forum', [], true);
                    $faqUrl = $this->getUrl('FAQ', [], true);

                    $message = <<<EOT
Dear $name,

Thank you for registering with xeno-canto.org!

Your registration details are:

login: $email

Please keep this for future reference.

Now you've registered, you can participate in the mystery
section and enlarge the database of wildlife song recordings by
uploading your own.  

The copyright of the uploads remains yours, but you share them 
under certain conditions that are described in 
Creative Commons licenses. Be sure to read them!
 
To start uploading a recording, go to

    $uploadUrl

In order to participate in the forum section, you'll need to
verify your email address by visiting the following URL:

    $verificationUrl

If you have any questions, please search the FAQ

    $faqUrl

or use the forum

    $forumUrl

Best regards,

The xeno-canto.org team.
EOT;

                    (new XCMail($email, 'Registration details for xeno-canto.org', $message))->send();

                    $this->notifySuccess(_('You have successfully registered your account. Welcome!'));
                    return $this->seeOther($this->getUrl('mypage'));
                } else {
                    $this->notifyError(_('A database error occurred, please contact support.'));
                }
            }
        }

        app()->session()->remove('user');

        return $this->renderForm($name, $email, $recaptchaError);
    }

    private function getRandomDir()
    {
        $length = 10;

        $str = '';
        // build string
        for ($i = 0; $i < $length; $i++) {
            $str .= chr(rand(65, 90));  // A-Z
        }

        return $str;
    }

// to make a bit of salt to add to the hashing of passwords. only made once for each user
// upon registration

    private function verifyRecaptcha($secret, $response, $remoteip)
    {
        $post = [
            'secret' => $secret,
            'response' => $response,
            'remoteip' => $remoteip,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, RECAPTCHA_VERIFY_URL);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        return json_decode(curl_exec($curl));
    }

    private function getRandomSalt()
    {
        $length = 5;
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= strtolower(chr(rand(65, 90)));
        }
        return $str;
    }

}
