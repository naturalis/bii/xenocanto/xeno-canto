<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Recording;
use xc\Species;

class RecordingHistory extends LoggedInController
{
    public function show($xcid)
    {
        $rec = Recording::load($xcid, false);

        if (!$rec) {
            return $this->notFound();
        }

        //$rec->setFormData();
        $revStartDate = mktime(22, 43, 30, 1, 24, 2014);

        $html = "
            <h1>Revision history for <a href='{$rec->URL()}'>XC{$rec->xcid()}</a></h1>
            
            <table class='results'>
                <thead>
                    <tr>
                        <th>Timestamp</th>
                        <th>Common name</th>
                        <th>Genus</th>
                        <th>Species</th>
                        <th>Ssp</th>
                        <th>Song type</th>
                        <th>Recordist</th>
                        <th>Lon</th>
                        <th>Lat</th>
                        <th>Remarks</th>
                        <th>License</th>
                        <th>Form</th>
                    </tr>
                </thead>
                <tbody>";

        $uploadDate = HtmlUtil::formatDate('%Y-%m-%d %T', $rec->uploadDate());
        $lastTimestamp = "&mdash;<sup><a href='#note'>1</a></sup>";
        if ($rec->uploadDate() < $revStartDate) {
            $html .= "
            <tr>
            <td>
            $uploadDate
            </td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            <td>&mdash;</td>
            </tr>";
        } else {
            $lastTimestamp = $uploadDate;
        }

        $res = $this->query("
            SELECT H.*, NULL as species_nr, NULL as family 
            FROM birdsounds_history AS H 
            WHERE snd_nr = {$rec->xcid()}
            ORDER BY `timestamp` ASC"
        );
        $lastRec = null;
        while ($row = $res->fetch_object()) {
            $r = new Recording($row);
            $html .= $this->row($r, $lastTimestamp, $lastRec);
            $lastTimestamp = $row->timestamp;
            $lastRec = $r;
        }
        $html .= $this->row($rec, $lastTimestamp, $lastRec);
        $html .= '
            </tbody>
            </table>';

        if ($rec->uploadDate() < $revStartDate) {
            $html .= "
            <p><sup><a name='note'>1</a></sup>Since this recording was
            uploaded before we started tracking revisions, the date of this
            change and any previous revisions are unknown
            </p>";
        }

        $res = $this->query("SELECT * FROM ai_accepted_log WHERE snd_nr = {$rec->xcid()} ORDER BY `created` ASC");
        if ($res->num_rows > 0) {
            $html .= "
                <h1>Sound recognition log</h1>
                
                <table class='results'>
                <thead>
                    <tr>
                        <th>Timestamp</th>
                        <th>Species</th>
                        <th>Type</th>
                        <th>Set by</th>
                        <th>Model version</th>
                    </tr>
                </thead>
                <tbody>";

            while ($row = $res->fetch_object()) {
                $species = Species::load($row->species_nr);

                $html .= "<tr>
                    <td>$row->created</td>
                    <td>{$species->htmlDisplayName()}</td>
                    <td>{$row->type}ground</td>
                    <td>" . $this->sanitize($row->username) . "</td>
                    <td>$row->model_version</td>
                    </tr>";
            }

            $html .= "</tbody>
                </table>";
        }

        return $this->template->render($html,
            ['title' => "Revision History for XC{$rec->xcid()}", 'bodyId' => 'rec-history']
        );
    }

    private function row($rec, $timestamp, $prev)
    {
        return "
            <tr>
            <td>$timestamp</td>
            <td {$this->diff($prev, $rec, 'commonName')}>{$rec->commonName()}</td>
            <td {$this->diff($prev, $rec, 'genus')}>{$rec->genus()}</td>
            <td {$this->diff($prev, $rec, 'speciesName')}>{$rec->speciesName()}</td>
            <td {$this->diff($prev, $rec, 'subspecies')}>{$rec->subspecies()}</td>
            <td {$this->diff($prev, $rec, 'soundTypeHistory')}>{$rec->soundTypeHistory()}</td>
            <td {$this->diff($prev, $rec, 'recordist')}>{$rec->recordist()}</td>
            <td {$this->diff($prev, $rec, 'longitude')}>{$rec->longitude()}</td>
            <td {$this->diff($prev, $rec, 'latitude')}>{$rec->latitude()}</td>
            <td {$this->diff($prev, $rec, 'remarks')}>" . HtmlUtil::formatUserText($rec->remarks()) . " </td>
            <td {$this->diff($prev, $rec, 'license')}>{$rec->license()}</td>
            <td>{$this->printTooltip($prev, $rec, $timestamp)}</td>
            </tr>";
    }

    private function diff($first, $second, $prop)
    {
        if (!($first && $second)) {
            return '';
        }
        $oldval = call_user_func([$first, $prop]);
        $newval = call_user_func([$second, $prop]);
        if ($oldval === $newval) {
            return '';
        } elseif (!$oldval) {
            return "class='diff-added'";
        } elseif (!$newval) {
            return "class='diff-removed'";
        } else {
            return "class='diff-changed'";
        }
    }

    private function printTooltip($prev, $rec, $timestamp)
    {
        $tooltip = '';
        $form = (array)json_decode($rec->formData());
        $prevForm = $prev instanceof Recording ? (array)json_decode($prev->formData()) : [];

        if (!empty($form)) {
            $tooltip = '<table class="key-value">';
            foreach ($form as $field => $value) {
                $diff = !empty($prevForm) && $value != $prevForm[$field];
                if (is_array($value)) {
                    $value = json_encode($value);
                } elseif ($value == 'null') {
                    $value = null;
                }
                $value = stripslashes(str_replace(["\\r\\n", "\\r", "\\n"], '<br>', htmlentities($value, ENT_QUOTES)));
                $tooltip .= "
                    <tr>
                        <td>$field:</td>
                        <td>" . ($diff ? '<u>' : '') . $value . ($diff ? '</u>' : '') . "</td>
                    </tr>";
            }
            $tooltip .= "</table>";
        }
        return $tooltip ? "<span class='tooltip' data-qtip-header='$timestamp' data-qtip-content='$tooltip'>show</span>" : '&mdash;';
    }

}
