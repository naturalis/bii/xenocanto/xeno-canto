<?php

namespace xc\Controllers;

use xc\Language;
use xc\Library;

class Credits extends Controller
{
    public function handleRequest()
    {
        $translatorsfilecontents = file_get_contents(
            'locales/contributors.json'
        );
        $translators = json_decode($translatorsfilecontents, true);
        $translatorshtml = "<ul class='simple'>";
        if ($translators) {
            if ($translators['response']['code'] == 200) {
                foreach ($translators['list'] as $translator) {
                    $project = $translator['projects'][0];
                    $languageStrings = [];
                    if (array_key_exists('languages', $project)) {
                        $languages = $project['languages'];
                        foreach ($languages as $code) {
                            $language = Language::lookup($code);
                            if ($language) {
                                $languageStrings[] = $language->name;
                            } else {
                                $languageStrings[] = $code;
                            }
                        }
                    }
                    $languageList = implode(', ', $languageStrings);
                    if ($languageList) {
                        $languageList = " ($languageList)";
                    }
                    $translatorshtml .= '<li>' . $translator['name'] . "$languageList</li>";
                }
            }
        }
        $translatorshtml .= '</ul>';

        $pageTitle = _('Colophon and Credits');
        $output = "
            <h1>$pageTitle</h1>
            <section>
            <h1>" . _('Website') . '</h1>

            <h2>' . _('Team xeno-canto') . '</h2>
            <ul class="simple">
            <li>Bob Planqué (founder, Xeno-canto Foundation)</li>
            <li>Willem-Pier Vellinga (founder, Xeno-canto Foundation)</li>
            <li>Sander Pieterse (Xeno-canto Foundation)</li>
            <li>Jonathon Jongsma (programming)</li>
            <li>Rolf de By (maps)</li>
            </ul>

            <h2>' . _('Concepts, programming and implementation') . '</h2>
            <ul class="simple">
            <li>Bob Planqué (2005- )</li>
            <li>Willem-Pier Vellinga (2005- )</li>
            <li>Jonathon Jongsma (2012-2018)</li>
            <li>Ruud Altenburg (2018- )</li>
            </ul>
            
            <br>
            <h1>' . _('Partner Organizations') . "</h1>
            <table id='partners'>
            <tr>
            <td>
            <img src='/static/img/naturalis_logo.png' alt='Naturalis Biodiversity Center'/>
            </td>
            <td>" . _(
            "All xeno-canto websites are supported long term by the <a href='http://www.naturalis.nl' target='_blank'>Naturalis Biodiversity Center</a>"
        ) . "
            </td>
            <tr>
            <td><img src='/static/img/university_of_leeds_logo_large.gif' alt='University of Leeds'/></td>
            <td>" . _(
            "www.xeno-canto.org/Asia is a cooperation with <a href='http://www.asianbirdsounds.com' target='_blank'>Asianbirdsounds.com</a>, initiated and initially funded by the <a href='http://www.leeds.ac.uk' target='_blank'>University of Leeds</a>"
        ) . "
            </td>
            </tr>
            <tr>
            <td>
            <img src='/static/img/abc_logo_large.png' alt='African Bird Club'/>
            </td>
            <td>" . _(
            "www.xeno-canto.org/Africa is a cooperation with the <a href='http://www.africanbirdclub.org' target='_blank'>African Bird Club</a>"
        ) . "
            </td>
            </tr>
            <tr>
            <td>
            <img src='/static/img/vantienhovenstichting_logo_large.png' alt='Van Tienhoven Foundation for International Nature Protection'/>
            </td>
            <td>" . _(
            "The <a href='http://www.vantienhovenfoundation.com' target='_blank'>Van Tienhoven Foundation for International Nature Protection</a> provided generous funding to digitize a large collection of tapes to be shared through this website"
        ) . "
            </td>
            </tr>
            <tr>
            <td>
            <img src='/static/img/oxford_medium.png' alt='Edward Grey Institute'/>
            </td>
            <td>" . _(
            'Xeno-canto will provide open access to the sound collection of Nathalie Seddon and Joe Tobias at the Edward Grey institute of the University of Oxford.'
        ) . "
            </td>
            </tr>
            <tr>
            <td>
            <img src='/static/img/iavh-logo-100.png' alt='Instituto Humboldt'/>
            </td>
            <td>" . _(
            "Xeno-canto will provide open access to the recordings of the Colección de Sonidos Ambientales from the <a href='http://www.humboldt.org.co'>Humboldt Institute</a> in Colombia"
        ) . '
            </td>
            </tr>
            </table>
            </section>
            
            <section>
            <h1>' . _('Classification') . '</h1>
 
            <h2>' . _('Birds') . "</h2>
            <ul class='simple'>
            <li>Gill, F & D Donsker (Eds). 2021. IOC World Bird List (v11.2).</li>
            <li>Retrieved from <a href='https://worldbirdnames.org/master_ioc_list_v11.2.xlsx'>www.worldbirdnames.org</a>.</li>
            </ul>
            
            <h2>" . _('Grasshoppers') . "</h2>
            <ul class='simple'>
            <li>Cigliano, M. M., Braun, H., Eades, D. C., & Otte, D. 2021. Orthoptera Species File. In O. Bánki, Y. Roskov, M. Döring, G. Ower, L. Vandepitte, D. Hobern, D. Remsen, P. Schalk, R. E. DeWalt, M. Keping, J. Miller, T. Orrell, R. Aalbu, R. Adlard, E. M. Adriaenssens, C. Aedo, E. Aescht, N. Akkari, P. Alfenas-Zerbini, et al., Catalogue of Life Checklist (Aug 2021).</li>
            <li>Retrieved as a DarwinCore Archive from the <a href='https://www.catalogueoflife.org'>Catalogue of Life</a>, <a href='https://www.checklistbank.org/dataset/2368/taxon/8NKFC'>edition COL22.1</a>.</li>
            </ul>

            <h2>" . _('Bats') . ' ' . _('and') . ' ' . _('Land mammals') . "</h2>
            <ul class='simple'>
            <li>Mammal Diversity Database. 2023. Mammal Diversity Database (Version 1.11).</li>
            <li>Retrieved from <a href='https://doi.org/10.5281/zenodo.7830771'>Zenodo</a>.</li>
            </ul>
           
            <h2>" . _('Frogs') . "</h2>
            <ul class='simple'>
            <li>Frost, Darrel R. 2024. Amphibian Species of the World: an Online Reference. Version 6.2 (Date of access). Electronic Database accessible at <a href='https://amphibiansoftheworld.amnh.org/'>https://amphibiansoftheworld.amnh.org</a>. American Museum of Natural History, New York, USA. doi.org/10.5531/db.vz.0001.</li>
            <li>For some taxa, for which this source does not give common names and Amphibiaweb does, the common names on <a href='https://amphibiaweb.org/'>Amphibiaweb</a> have been used.</li>
            </ul>
            </section>

            <h1>" . _('Acknowledgements') . '</h1>

            <h2>' . _('The xeno-canto community') . '</h2>
            <p>' . _(
            'Xeno-canto has benefited enormously from the generous help of many people. First of all, of course, all the recordists who share their recordings on xeno-canto. Many other people have contributed by suggestions and submitting bug reports on the forum! Still others have shared their expertise by identifying mystery recordings or by pointing out errors in the database. Bloggers, webmasters and scientists all over the world have helped spread the word on XC.  Thanks a lot & keep going!'
        ) . '
            </p>
            
            <h2>' . _('Birds') . "</h2> 
            <p>
            XC thank David Donsker for commenting on the validity of all subspecies entered before 2013.
            </p>
            <h2>" . _('Grasshoppers') . "</h2> 
            <p>
            The Grasshopper collection on XC started out with a NLBIF programme \"<a href='https://www.nlbif.nl/van-acheta-tot-zoemertje-sprinkhaangeluiden-op-xeno-canto-en-gbif/'>Van Acheta tot Zoemertje: sprinkhaangeluiden op xeno-canto en GBIF</a>\".  
            This was a cooperation with Baudewijn Odé, Roy Kleukers, EIS-Kenniscentrum Insecten and the Orthopterists' Society.
            In a NLBIF follow-up programme,\"Geluidsherkenning Europese sprinkhanen\", a number of large collections, by ao. Baudewijn Odé, Klaus-Gerhard Heller, Sigfrid Ingrisch and Dragan Chobanov have been made available through the site.
            This was a cooperation with Baudewijn Odé, Naturalis Biodiversity Center and the IUCN. At this stage, XC had great help from Marius Faiss and Vincent Kalkman.
            </p>
            <h2>" . _('Bats') . "</h2>
            The Bat collection on XC started as a result of a NLBIF programme \"<a href='https://www.nlbif.nl/bat-sounds-on-xeno-canto-gbif'>Bat sounds on Xeno-canto GBIF</a>\". A collaboration with Gloriana Chevarri and Marcelo Araya-Salas from the University of Costa Rica.
            </p>
            <h2>" . _('Frogs') . "</h2>
            <p>
            The Frog collection on XC started with the Hoogmoed collection, as part of the NLBIF programme \"<a href='https://www.nlbif.nl/digitalisering-collectie-hoogmoed/'>Digitalisering collectie Hoogmoed</a>\".
            XC thank Darrel Frost for his great and hands-on help in getting the taxonomy for the Frogs branch here into proper shape.
            Dick de Graaff is thanked for his work in getting the sounds and metadata of the Hoogmoed collection in shape.
            </p>
            <h2>" . _('Programming advice in the early days') . '</h2>
            <ul class="simple">
            <li>' . _('Everyone at the Bristol Machine Learning Lab') . ':
            <ul>
            <li>Rob Eggington</li>
            <li>Susanne Hoche</li>
            <li>James Marshall</li>
            <li>Simon Price</li>
            <li>Simon Rawles</li>
            </ul></li>
            <li>Chris Parrish</li>
            </ul>
            <h2>' . _('Translators') . "</h2>
            $translatorshtml
            </section>

            <section>
            <h1>" . _('Much valued stuff from other places') . "</h1>

            <h2><a id='rangemaps'></a>" . _('Birdlife International and NatureServe Species ranges') . "</h2>
            <p>The basis for the species maps is:</p>
            <p><a href='http://www.birdlife.org/datazone/info/spcdownload'><b>Bird species distribution maps of the world, version 1.0 (BirdLife International and NatureServe (2011))</b></a>.</p>
            <p>Xeno-canto actively curates and maintains these maps since 2013: the maps reflect the IOC taxonomy that Xeno-canto has adopted (showing splits, lumps, new taxa...); they reflect comments that the Xeno-canto community has reported and continues to report, and they are continuously updated on the basis of data presented in various citizen science portals and books and papers published in the literature. Comments on our maps can be sent " . Library::obfuscateEmail(
            'maps@xeno-canto.org',
            _('by email')
        ) . '.</p>

            <h2>' . _('Gnome Icons') . '</h2>
            <p>' . _(
            "Icons used throughout the site are © <a href='http://gnome.org/'>The Gnome Project</a> and are licenced under the <a href='//creativecommons.org/licenses/by-sa/3.0/'>Creative Commons Attribution-Share Alike 3.0 License</a>."
        ) . '</p>
            </section>

            <section>
            <h1>' . _('Storage') . '</h1>

            <p>' . _(
            'The recordings in xeno-canto are stored on two virtual machines dedicated for xeno-canto with >5TB storage, a web and database server are running on an OpenStack cloud within a datacenter in the Netherlands. The volume storage makes sure that 3 copies of the data are always available. Should a disk fail, a copy will be created on a different disk. Daily backups are made on a separate volume storage cluster, which is also distributed to a second datacenter.'
        ) . '</p>
            </section>
            ';
        return $this->template->render($output, ['title' => $pageTitle, 'bodyId' => 'credits']);
    }
}
