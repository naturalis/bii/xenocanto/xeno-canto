<?php

namespace xc\Controllers\Admin;

class ServerInfo extends Controller
{

    public function show()
    {
        ob_start();
        phpinfo();
        $phpinfo = ob_get_contents();
        ob_get_clean();
        $data   = base64_encode($phpinfo);
        $output = "<h1>PHP info</h1>
            <iframe style='width: 100%' scrolling='no' frameborder=0 onload='this.height = this.contentWindow.document.body.scrollHeight + \"px\"' src='data:text/html;charset=UTF-8;base64,$data'></iframe>";
        return $this->template->render($output);
    }
}
