<?php

namespace xc\Controllers\Admin;


use xc\Recording;



class UpdateRecordingStatus extends Controller
{
    public function handlePost()
    {
        $xcid = $this->request->request->get('xcid');
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        if ($rec->updateStatus($this->request->request->get('status'))) {
            $this->notifySuccess('Updated recording status');
        } else {
            $this->notifyError('Unable to update recording status');
        }

        return $this->seeOther($this->request->headers->get('Referer'));
    }
}
