<?php

namespace xc\Controllers\Admin;

use xc\ResultsTable;
use xc\User;

class Masquerade extends Controller
{
    public function switchUser()
    {
        $id = $this->request->request->get('u');
        $user = User::load($id);
        if ($user) {
            $user->setMasquerade(true);
            app()->session()->set('user', $user);
            $this->notifySuccess("Masquerading as {$user->userName()}");
            return $this->seeOther($this->getUrl('index'));
        }

        $this->notifyError("Couldn't find that user");
        return $this->seeOther($this->getUrl('admin-masquerade'));
    }

    public function switch($id, $referer = false)
    {
        // Coming from admin-masquerade-direct; return to page from which it was invoked
        if (!$referer) {
            $referer = $this->request->headers->get('Referer');
            // Coming from admin-masquerade-redirect; referer is provided as route in second argument
        } else {
            $referer = $this->getUrl($referer);
        }

        $user = User::load($id);
        if ($user) {
            $user->setMasquerade(true);
            app()->session()->set('user', $user);
            $this->notifySuccess("Masquerading as {$user->userName()}");
        } else {
            $this->notifyError("Couldn't find that user");
        }
        return $this->seeOther($referer);
    }

    public function handleRequest()
    {
        $output = '
            <h1>Masquerade as another User</h2>
            
            <p>This page allows you to easily log in as another user to debug problems
            that they report with their accounts</p>' .

            ResultsTable::userTable($this->request, 'admin-masquerade', 'admin-masquerade-redirect',
                ['referer' => 'index']
            );

        return $this->template->render($output, ['title' => 'Masquerade as another user']);
    }
}
