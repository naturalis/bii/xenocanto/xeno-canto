<?php

namespace xc\Controllers\Admin;

use xc\Query;
use xc\User;

class DisableUser extends Controller
{
    public function confirm($id)
    {
        $user = User::load($id);
        if (!$user) {
            return $this->notFound();
        }

        $body = "<h1>Disable User Account?</h1>
            <p>This will move the user account for <b>{$user->userName()}</b> into the <tt>spammers</tt> database table, which will disable the account but retain the user information for future use.</p>";
        if (!$this->canDisableUser($user)) {
            $body .= "<div class='error'>
                Cannot disable <b>{$user->userName()}</b> since they have uploaded
                recordings to xeno-canto.</div>";
        } else {
            $body .= "
                <form method='post'/>
                <input type='submit' value='Delete'/>
                </form>";
        }


        return $this->template->render($body, ['title' => 'Disable User Account']);
    }

    private function canDisableUser($user)
    {
        // check if this user has any recordings
        $q = new Query("dir:{$user->userId()}");
        return ($q->numRecordings() == 0);
    }

    public function disable($id)
    {
        $user = User::load($id);
        if (!$user) {
            return $this->notFound();
        }

        if (!$this->canDisableUser($user)) {
            return $this->badRequest('Unable to disable user who has uploaded recordings');
        }

        $this->query("
            INSERT INTO spammers 
                SELECT username, email, dir, blurb, joindate, verified FROM users WHERE dir = '$id'"
        );
        $this->query("DELETE FROM users WHERE dir = '$id'");
        $this->query("DELETE FROM user_cookies WHERE userid = '$id'");
        $this->notifySuccess('Disabled user account');

        return $this->seeOther($this->getUrl('index'));
    }
}
