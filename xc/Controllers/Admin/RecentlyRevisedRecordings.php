<?php

namespace xc\Controllers\Admin;

use xc\HtmlUtil;
use xc\Recording;

class RecentlyRevisedRecordings extends Controller
{
    public function handleRequest()
    {
        $perpage = 30;
        $page = $this->request->query->get('pg', 1);
        $offset = ($page - 1) * $perpage;
        $limitClause = "LIMIT $offset, $perpage";

        $res = $this->query('SELECT COUNT(*) AS N FROM (SELECT snd_nr FROM birdsounds_history GROUP BY snd_nr) H');
        $row = $res->fetch_object();
        if ($row) {
            $nrows = $row->N;
        }

        $pagenav = HtmlUtil::pageNumberNavigationWidget($this->request, ceil(($nrows ?? 0) / $perpage), $page);

        $output = '
            <h1>Recently Revised Recordings</h1>
            <p>A list showing the most-recently-revised files. Showing revisions <b>' . ($offset + 1) . ' - ' . ($offset + $perpage) . "</b></p>
            $pagenav
            ";
        $output .= "<table class='results'>
            <thead>
            <tr>
            <th>Catalog Number</th>
            <th>Species</th>
            <th>Recordist</th>
            <th>Revision date</th>
            <th>History</th>
            </tr>
            </thead>
            ";
        $res = $this->query("
            SELECT timestamp, snd_nr 
            FROM (
                select MAX(timestamp) as timestamp, snd_nr 
                from birdsounds_history 
                GROUP BY snd_nr 
                ORDER BY MAX(historyid) DESC $limitClause
            ) H 
            ORDER BY timestamp DESC"
        );
        while ($row = $res->fetch_object()) {
            $rec = Recording::load($row->snd_nr);
            $output .= "
            <tr>
                <td><a href='" . $rec->URL() . "'>XC$row->snd_nr</a></td>
                <td>{$rec->htmlDisplayName()}</td>
                <td>{$rec->recordist()}</td>
                <td>$row->timestamp</td>
                <td><a href='" . $this->getUrl('recording-history', ['xcid' => $row->snd_nr]) . "'>view revisions</a></td>
           </tr>";
        }
        $output .= "</table>
        
        $pagenav";

        return $this->template->render($output, ['title' => 'Recently Revised Recordings']);
    }

}
