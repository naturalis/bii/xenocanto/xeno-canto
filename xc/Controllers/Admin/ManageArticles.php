<?php

namespace xc\Controllers\Admin;

use xc\ArticleUtil;

class ManageArticles extends Controller
{
    public function handlePost()
    {
        if ($this->request->request->get('rebuild-features') == 'Rebuild') {
            $sql = null;

            if ($this->request->request->get('feature-selection') === 'all') {
                $sql = 'SELECT blognr, title, blog, author FROM blogs';
            } else {
                $id = intval($this->request->request->get('feature-selection'));
                $sql = "SELECT blognr, title, blog, author FROM blogs WHERE blognr = $id";
            }

            $res = $this->query($sql);
            if (!$res) {
                $this->notifyError('Unable to retrieve data about the specified articles');
            } elseif ($res->num_rows == 0) {
                $this->notifyError('No articles matched your query');
            } else {
                while ($row = $res->fetch_object()) {
                    $generatedHtml = ArticleUtil::generateBlog($row->blog);
                    $escaped = $this->escape($generatedHtml);
                    $res2 = $this->query("UPDATE blogs SET blog_as_html='$escaped' WHERE blognr = $row->blognr");
                    if (!$res2) {
                        $this->notifyError("Error while rebuilding article #$row->blognr: $row->title");
                    }
                }
                $this->notifySuccess('Rebuilt articles');
            }
        }
        return $this->seeOther($this->getUrl('admin-articles'));
    }

    public function handleRequest()
    {
        $output = "
            <h1>Manage Articles</h1>
            <h2>Rebuild Articles</h2>
            <form method='post'>
            " . $this->featuresSelectHtml() . "
            <input type='submit' name='rebuild-features' value='Rebuild' />
            </form>
            ";

        return $this->template->render($output, ['title' => 'Manage Articles']);
    }

    private function featuresSelectHtml()
    {
        $html = "
            <select name='feature-selection' id='feature-selection'>
            <option value='all'>All Articles</option>
        ";
        $res = $this->query('SELECT * FROM blogs ORDER BY blognr DESC');
        while ($row = $res->fetch_object()) {
            $html .= "<option value='$row->blognr'>$row->blognr. '$row->title' by $row->author</option>
            ";
        }
        $html .= '</select>';

        return $html;
    }
}
