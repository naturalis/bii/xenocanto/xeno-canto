<?php

namespace xc\Controllers\Admin;

use xc\Library;
use xc\XCRedis;

class ClearCaches extends Controller
{
    public function clear()
    {
        $redis = new XCRedis();
        $redis->clearAllCache();

        return $this->template->render(
            'All cached page contents cleared!',
            ['title' => "Clear Redis cache"]
        );
    }
}
