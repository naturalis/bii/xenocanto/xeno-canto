<?php

namespace xc\Controllers\Admin;

use xc\Library;

class ManageNews extends Controller
{
    public function saveItem($id = 0)
    {
        $referer = $this->request->request->get('referer', $this->getUrl('index'));
        $text = $this->request->request->get('text');
        if (!$text) {
            $this->notifyError('You cannot post an empty news item');
        } else {
            $text = $this->escape($text);
            if (!$id) {
                $sql = "INSERT INTO news_world (`date`, `text`, `branch`) VALUES (CURDATE(),'$text','world')";
                $successMessage = 'Added news item';
            } else {
                $sql = "UPDATE news_world SET text = '$text' WHERE news_nr = $id";
                $successMessage = "Updated news item $id";
            }
            $res = $this->query($sql);
            if (!$res) {
                $this->notifyError("Unable to save news item to the database. ($sql) " . Library::mysqli()->error);
            } else {
                $this->notifySuccess($successMessage);
                return $this->seeOther($referer);
            }
        }
        return $this->showContent($id, $text);
    }

    private function showContent($id = null, $text = '')
    {
        $desc = 'Add a News Item';
        if ($id) {
            $desc = "Editing News Item $id";
        }

        $text = htmlspecialchars($text);
        $referer = $this->request->headers->get('Referer');

        $output = "
            <h1>Manage News</h1>
            <p>$desc</p>
            <form method='post' action='" . $this->getUrl('admin-news-post', ['id' => $id]) . "'>
            <textarea name='text' style='width:600px;height:200px' placeholder='news text...'>$text</textarea>
            <p>
            <input type='submit' name='submit' value='Save' />
            <input type='hidden' name='referer' value='$referer'/>
            </p>
            </form>
            ";

        return $this->template->render($output, ['title' => 'Manage News']);
    }

    public function newItem()
    {
        return $this->showContent();
    }

    public function editItem($id)
    {
        $res = $this->query("SELECT text from news_world WHERE news_nr = $id");
        $row = $res ? $res->fetch_object() : null;

        if (!$row) {
            return $this->notFound();
        }
        return $this->showContent($id, $row->text);
    }

    public function confirmDelete($id)
    {
        $res = $this->query("SELECT text from news_world WHERE news_nr = $id");
        $row = $res ? $res->fetch_object() : null;

        if (!$row) {
            return $this->notFound();
        }

        $output = "<h1>Delete News Item $id?</h1>
            <p>Are you sure you want to delete this news item?</p>
            <form method='post'>
            <textarea name='text' style='width:600px;height:200px;opacity:0.4;' disabled'>$row->text</textarea>
            <p><input type='submit' name='delete' value='Delete'/></p>
            </form>";

        return $this->template->render($output, ['title' => 'Delete News']);
    }

    public function delete($id)
    {
        $this->query("DELETE FROM news_world WHERE news_nr = $id");
        $this->query("DELETE FROM discuss_news_world WHERE news_nr = $id");
        $this->notifySuccess("Deleted News Item $id");
        return $this->seeOther($this->getUrl('index'));
    }

}
