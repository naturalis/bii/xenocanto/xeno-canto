<?php

namespace xc\Controllers\Admin;

class Index extends Controller
{
    public function handleRequest()
    {
        $output = "
            <h1>Validator Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-resolved') . "'>Resolved mystery and questioned recordings</a></li>
            </ul>
            <h1>Content Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-articles') . "'>Manage Articles</a></li>
            <li><a href='" . $this->getUrl('admin-spotlights') . "'>Manage Spotlights</a></li>
            <li><a href='" . $this->getUrl('admin-tips') . "'>Manage 'New Stuff' Tips</a></li>
            <li><a href='" . $this->getUrl('admin-news') . "'>Manage News</a></li>
            <li><a href='" . $this->getUrl('admin-locations-export') . "'>Download Text File With Locations</a></li>
            <li><a href='" . $this->getUrl('admin-restricted-species') . "'>Manage Restricted Species</a></li>
            </ul>
            <h1>Recording Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-replace-recording') . "'>Replace a Recording File</a></li>
            <li><a href='" . $this->getUrl('admin-modify-species-set') . "'>Modify Recordings</a></li>
            <li><a href='" . $this->getUrl('admin-delete-recording') . "'>Delete Recordings</a></li>
            <li><a href='" . $this->getUrl('admin-sono') . "'>Generate Sonograms</a></li>
            <li><a href='" . $this->getUrl('admin-id3') . "'>Redo ID3 Tags</a></li>
            <li><a href='" . $this->getUrl('admin-batch-index') . "'>Batch Upload</a></li>
            <li><a href='" . $this->getUrl('admin-sets') . "'>Manage Sets</a></li>
            </ul>
            <h1>AI Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-ai-accepted') . "'>Accepted AI Suggestions</a></li>
            <li><a href='" . $this->getUrl('admin-ai-unresolved') . "'>Unresolved Mystery Recordings</a></li>
            <li><a href='" . $this->getUrl('admin-ai-matched') . "'>Matching AI Suggestions</a></li>
            <li><a href='" . $this->getUrl('admin-ai-non-matched') . "'>Non-matching AI Suggestions</a></li>
            <li><a href='" . $this->getUrl('admin-ai-non-matched-species') . "'>Non-matching Recordings by Foreground Species</a></li>
            <li><a href='" . $this->getUrl('admin-ai-csv-suggestions') . "'>CSV Export of Recordings with AI Suggestions</a></li>            
            <li><a href='" . $this->getUrl('admin-ai-csv-no-suggestions') . "'>CSV Export of Recordings without AI Suggestions</a></li>
            <li><a href='" . $this->getUrl('admin-ai') . "'>Run Sound Recognition</a></li>
            </ul>
            <h1>Classification Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-classification-index') . "'>XML Classification Update</a></li>            
            <li><a href='" . $this->getUrl('admin-dwca-index') . "'>DwCA Classification Update</a></li>            
            <li><a href='" . $this->getUrl('admin-mdd-index') . "'>MDD Classification Update</a></li>            
            </ul>
            <h1>Sync From Minio</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-range-maps-index') . "'>Copy Range Maps</a></li>
            <li><a href='" . $this->getUrl('admin-documents-index') . "'>Copy Files To Docs</a> 
                (-> <a href='" . $this->getUrl('admin-documents-list') . "'>List Of Uploaded Files</a>)</li>
            </ul>
            <h1>User Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-restricted-users') . "'>A list of restricted users</a></li>
            <li><a href='" . $this->getUrl('admin-potential-spammers') . "'>A list of potential spammers</a></li>
            <li><a href='" . $this->getUrl('admin-masquerade', ['q' => 'A']) . "'>Temporarily log in as another user</a></li>
            <li><a href='" . $this->getUrl('admin-mail') . "'>Send an email to groups of users</a></li>
            <li><a href='" . $this->getUrl('admin-user-permissions') . "'>Manage user permissions</a></li>
            </ul>
            <h1>Translations</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-translations') . "'>Upload/download translations</a></li>
            </ul>
            <h1>QA Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-splits') . "'>A list of recordings from potential splits</a> (warning: long query)</li>
            <li><a href='" . $this->getUrl('admin-ssp-issues') . "'>A list of recordings with potential subspecies issues</a> (warning: long query)</li>
            <li><a href='" . $this->getUrl('admin-fileinfo') . "'>A list of files with non-standard media info</a></li>
            <li><a href='" . $this->getUrl('admin-recently-deleted') . "'>A list of deleted files</a></li>
            <li><a href='" . $this->getUrl('admin-recently-revised') . "'>A list of revised recordings</a></li>
            <li><a href='" . $this->getUrl('admin-sharing-stats') . "'>Metadata sharing options for users</a></li>
            </ul>
            <h1>Server Tools</h1>
            <ul>
            <li><a href='" . $this->getUrl('admin-server-info') . "'>Server info</a></li>
            <li><a href='" . $this->getUrl('admin-clear-cache') . "'>Clear Redis cache</a></li>
            </ul>
            <h1>Experimental Pages</h1>
            <ul>
            <li><a href='" . $this->getUrl('gpslist') . "'>gpslist</a></li>
            </ul>
            ";

        return $this->template->render($output);
    }
}
