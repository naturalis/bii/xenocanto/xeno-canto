<?php

namespace xc\Controllers\Admin;

use xc\Library;
use xc\Set;
use xc\User;

class Sets extends Controller
{
    public function showSetList()
    {
        $title = _('Sets');
        $sets  = Set::loadAll(true);
        $body  = "
            <h1>$title</h1>
            <ul class='simple'>";
        foreach ($sets as $set) {
            $class = '';
            if ($set->visibility() != Set::VISIBILITY_PUBLIC) {
                $class = 'class="draft"';
            }
            $user = User::load($set->userId());
            if (!$user) {
                Library::logger()->logWarn("set belongs to a user who doesn't exist");
                continue;
            }
            $body .= "<li $class><a href='{$set->url()}'>{$set->name()}</a> &mdash; <a href='{$user->getProfileURL()}'>{$user->userName()}</a> &mdash; {$set->numRecordings()} recordings
                </li>";
        }

        $body .= '</ul>';
        return $this->template->render($body, ['title' => $title]);
    }
}
