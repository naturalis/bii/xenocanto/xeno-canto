<?php

namespace xc\Controllers\Admin;

use xc\SoundRecognition;
use xc\SoundRecognitionUtil;

class RunSoundRecognition extends Controller
{
    // this is posted via AJAX, so no need to display anything...
    public function run()
    {
        $xcid = intval($this->request->request->get('xcid', 0));
        if (!SoundRecognitionUtil::getIdentification($xcid)) {
            return json_encode(['error' => "sound recognition failed!"]);
        }
        // Post reply to forum if ID yielded results
        $summary = SoundRecognitionUtil::getIdentifiedSpecies($xcid);
        if (!empty($summary)) {
            SoundRecognitionUtil::postReplyToForum($xcid, $summary);
            return json_encode(['message' => count($summary) . " species recognised"]);
        } else {
            return json_encode(['message' => "no species recognised"]);
        }
    }

    public function showForm()
    {
        $apiUrl = $this->getUrl('api-recording-search');
        $script = <<<EOT
            <script type='text/javascript'>
            jQuery(document).ready(function() 
            {
                var n = 0;
            
                function runAnalysis(url, xcid)
                {
                    var d = jQuery.Deferred();
                    var id = "xc-" + n++;
                    var status = jQuery("<p id='" + id + "'>Running sound analysis for XC" + xcid + "... <span class='result'></span></p>");
                    jQuery("#status-log").append(status);
                    jQuery.post(url, {xcid: xcid})
                        .done (function(data) {
                            var resultElt = jQuery("#" + id + " > .result");
                            data = JSON.parse(data);
                            var message = data.error != null ? "<span class='warning'>" + data.error + "</span>" : data.message;
                            resultElt.html(message);
                        }).fail(function(xhr, status, error) {
                             var resultElt = jQuery("#" + id + " > .result");
                            resultElt.html("<span class='warning'>" + xhr.responseText + "</span>");
                        }).always(function() {
                            d.resolve();
                        });
                    return d;
                }
            
                jQuery("#form-ai-id").submit(function()
                    {
                        var xcid = jQuery(this).find("input[name=xcid]").val();
                        if (xcid) {
                            runAnalysis(jQuery(this).attr('action'), xcid);
                        }
                        return false;
                    });
            
                jQuery("#form-ai-query").submit(function()
                    {
                        var query = jQuery(this).find("input[name=query]").val();
                        var url = jQuery(this).attr('action');
                        if (query) {
                            jQuery.get("$apiUrl", {query: query},
                                function(data) {
                                    if (!data.error && data.recordings.length) {
                                        // issue these requests sequentially
                                        var deferred = jQuery.Deferred();
                                        var next = deferred;
                                        var x;
                                        for (x = 0; x < data.recordings.length; ++x) {
                                            (function (url, xcid) {
                                                next = next.pipe(function() {
                                                    return runAnalysis(url, xcid).done(function() { 
                                                        console.log("done"); 
                                                    });
                                                });
                                            } (url, data.recordings[x].id));
                                        }
                                        // kick off the pipeline
                                        deferred.resolve();
                                    }
                                }, 'json');
                        }
                        return false;
                    });
            });
            </script>
            EOT;

        $output = "
            <h1>Run Sound Analysis</h1>

            <form id='form-ai-id' action='" . $this->getUrl('admin-ai-post') . "' method='post'>
            <p>Run sound analysis for a single recording</p>
            <p><input type='text' name='xcid' placeholder='xcid' />
            <input type='submit' name='generate' value='Run' /></p>
            </form>
            <form id='form-ai-query' action='" . $this->getUrl('admin-ai-post') . "' method='post'>
            <p><label>Run all recordings matching a query (<b>careful!</b>)</label></p>
            <p><input type='text' name='query' placeholder='query' />
            <input type='submit' name='generate' value='Run' /></p>
            </form>
            <div id='status-log'></div>
            $script
            ";

        return $this->template->render($output, ['title' => 'Run Sound Analysis']);
    }
}
