<?php

namespace xc\Controllers\Admin;

use xc\HtmlUtil;
use xc\Query;
use xc\ResultsTable;
use xc\Species;

class AiResults extends Controller
{
    private $title;

    private $sql;

    private $resultText;

    public function handleMatched()
    {
        $this->title = _('Matching AI Predictions');
        $this->resultText = '%s recordings (excluding mysteries and soundscapes) matching AI predictions.';
        $this->sql = "
            SELECT %s
            FROM ai_results_summaries AS t1
            LEFT JOIN birdsounds ON t1.snd_nr = birdsounds.snd_nr
            LEFT JOIN audio_info ON birdsounds.snd_nr = audio_info.snd_nr
            WHERE birdsounds.species_nr != '" . Species::soundscapeSpeciesNumber() . "' 
            AND birdsounds.species_nr != '" . Species::mysterySpeciesNumber() . "'
            AND (birdsounds.species_nr IN 
            (
                SELECT t3.species_nr
                FROM ai_results_summaries AS t3
                WHERE birdsounds.snd_nr = t3.snd_nr        
            )
            OR t1.species_nr IN 
            (
                SELECT GROUP_CONCAT(t4.species_nr)
                FROM ai_results_summaries AS t4
                LEFT JOIN birdsounds_background AS t5 ON t4.snd_nr = t5.snd_nr AND t4.species_nr = t5.species_nr 
                WHERE birdsounds.snd_nr = t5.snd_nr
                GROUP BY t4.snd_nr         
            ))
            GROUP BY t1.snd_nr";

        return $this->handleRecordings();
    }

    public function handleRecordings()
    {
        $nrPerPage = $this->defaultNumPerPage();
        $output = '';

        $resultsTable = '';

        $dir = $this->request->query->get('dir');
        $order = $this->request->query->get('order', 'xc');
        $view = $this->request->query->get('view', $this->defaultResultsView());
        $pageNumber = $this->pageNumber();

        $select = Query::birdsoundsSelectFields() . ', audio_info.length, audio_info.smp AS sample_rate, 
           birdsounds.eng_name as localName';

        $q = new Query('');
        $q->setRawSqlClause($this->sql, $select);
        $q->setOrder($order, $dir);

//        $a = $q->getQueryString();

        // Need to manually get the number of rows, as we're using a GROUP BY
        $sqlCount = sprintf($this->sql, 'COUNT(*) OVER()') . ' LIMIT 1';
        $nrHits = $this->query($sqlCount)->fetch_row()[0] ?? 0;

        $nrPages = ceil($nrHits / $nrPerPage);
        $res = $q->execute($pageNumber);

        $links = ResultsTable::makeViewLinks($this->request, $view);
        $resultsTable .= "
            <p>
            <div class=\"results-format\">" . _('Results format') . ": $links</div>
            </p>
            ";
        $resultsTable .= HtmlUtil::pageNumberNavigationWidget($this->request, $nrPages, $pageNumber);
        $resultsTable .= ResultsTable::resultsTableForView($this->request, $res, $view);
        $resultsTable .= HtmlUtil::pageNumberNavigationWidget($this->request, $nrPages, $pageNumber);

        $resultsSummary = '
            <p class="important">'.
            sprintf("There are regular search strings to search for all recordings with a successful AI identification
            (<a href='%s'><tt>ai:yes</tt></a>) and for recordings without (<a href='%s'><tt>ai:no</tt></a>).",
                $this->getUrl('browse', ['query' => 'ai:yes']), $this->getUrl('browse', ['query' => 'ai:no'])) . '
            </p>
            <p>' .
            sprintf($this->resultText,"<strong>$nrHits</strong>") . '
            </p>';

        $output .= "
            <h1>$this->title</h1>
            $resultsSummary
            $resultsTable";

        return $this->template->render($output, ['title' => $this->title, 'bodyId' => 'species-profile']);
    }

    public function handleAccepted()
    {
        $this->title = _('Accepted AI Predictions');
        $this->resultText = '%s recordings with accepted AI predictions (foreground and background).';
        $this->sql = "
            SELECT %s
            FROM ai_accepted_log AS t1
            LEFT JOIN birdsounds ON t1.snd_nr = birdsounds.snd_nr
            LEFT JOIN audio_info ON birdsounds.snd_nr = audio_info.snd_nr
            WHERE birdsounds.snd_nr IS NOT NULL
            GROUP BY t1.snd_nr";

        return $this->handleRecordings();
    }

    public function handleNonMatchedSpecies()
    {
        /*
        Needs a better query! This produces non sensical results+
        */

        $this->title = _('Non-matching AI Predicted Species');
        $this->resultText = '%s AI predictions not matching with the uploaders identification as foreground or background species, broken down by foreground species.';
        $this->sql = "
            SELECT birdsounds.snd_nr, birdsounds.species_nr, birdsounds.genus, birdsounds.species, 
                   COUNT(DISTINCT birdsounds.snd_nr) AS nr_incorrect, GROUP_CONCAT(DISTINCT birdsounds.snd_nr) as snd_nrs
            FROM ai_results_summaries AS t1
            LEFT JOIN birdsounds ON t1.snd_nr = birdsounds.snd_nr
            LEFT JOIN audio_info ON birdsounds.snd_nr = audio_info.snd_nr
            WHERE birdsounds.species_nr != '" . Species::soundscapeSpeciesNumber() . "' 
            AND birdsounds.species_nr != '" . Species::mysterySpeciesNumber() . "'
            AND birdsounds.species_nr NOT IN 
            (
                SELECT t3.species_nr
                FROM ai_results_summaries AS t3
                WHERE birdsounds.snd_nr = t3.snd_nr        
            )
            AND birdsounds.species_nr NOT IN 
            (
                SELECT GROUP_CONCAT(t4.species_nr)
                FROM ai_results_summaries AS t4
                LEFT JOIN birdsounds_background AS t5 ON t4.snd_nr = t5.snd_nr AND t4.species_nr = t5.species_nr 
                WHERE birdsounds.snd_nr = t5.snd_nr
                GROUP BY t4.snd_nr           
            )
            GROUP BY birdsounds.species_nr
            ORDER BY COUNT(DISTINCT birdsounds.snd_nr) DESC, genus, species";

        $res = $this->query($this->sql);

        $table = "<table class='results' style='width: 50%'>
            <thead>
            <tr>
                <th>Species</th>
                <th>Number of non-matches</th>
            </tr>
            </thead>
            ";

        $rows = '';
        $nrHits = 0;
        while ($row = $res->fetch_object()) {
            $species = Species::load($row->species_nr);
            $nrHits += $row->nr_incorrect;
            $resultUrl = $this->getUrl('admin-ai-non-matched', ['species_nr' => $row->species_nr]);

            $rows .= "
                <tr>
                    <td>{$species->htmlDisplayName()}</td>
                    <td><a href='$resultUrl'>$row->nr_incorrect</a></td>
                </tr>";
        }

        $output = "
            <h1>$this->title</h1>
            <p>" . sprintf($this->resultText,"<strong>$nrHits</strong>") . "</p>" .
            $table . $rows . '
            </table>';

        return $this->template->render($output, ['title' => $this->title]);
    }

    public function handleUnresolved()
    {
        $this->title = _('Unresolved Mystery Recordings');
        $this->resultText = '%s unresolved mystery recordings having AI predictions.';
        $this->sql = "
            SELECT %s
            FROM birdsounds
            LEFT JOIN audio_info ON birdsounds.snd_nr = audio_info.snd_nr
            LEFT JOIN ai_results_summaries ON birdsounds.snd_nr = ai_results_summaries.snd_nr
            WHERE birdsounds.species_nr = 'mysmys'
            AND ai_results_summaries.id IS NOT NULL
            GROUP BY birdsounds.snd_nr";

        return $this->handleRecordings();
    }

    public function handleNonMatched()
    {
        $speciesNr = $this->request->query->get('species_nr');

        $this->title = _('Non-matching AI Predictions');
        $this->resultText = '%s recordings (excluding mysteries and soundscapes) in which the ID does not match the AI predictions.';
        $this->sql = "
            SELECT %s
            FROM ai_results_summaries AS t1
            LEFT JOIN birdsounds ON t1.snd_nr = birdsounds.snd_nr
            LEFT JOIN audio_info ON birdsounds.snd_nr = audio_info.snd_nr
            WHERE birdsounds.species_nr != '" . Species::soundscapeSpeciesNumber() . "' 
            AND birdsounds.species_nr != '" . Species::mysterySpeciesNumber() . "'
            AND birdsounds.species_nr NOT IN 
            (
                SELECT t3.species_nr
                FROM ai_results_summaries AS t3
                WHERE birdsounds.snd_nr = t3.snd_nr        
            )
            AND t1.species_nr NOT IN 
            (
                SELECT GROUP_CONCAT(t4.species_nr)
                FROM ai_results_summaries AS t4
                LEFT JOIN birdsounds_background AS t5 ON t4.snd_nr = t5.snd_nr AND t4.species_nr = t5.species_nr 
                WHERE birdsounds.snd_nr = t5.snd_nr
                GROUP BY t4.snd_nr        
            ) " .
            ($speciesNr ? " AND birdsounds.species_nr = '" . $this->escape($speciesNr) . "'" : "") . "
            GROUP BY t1.snd_nr";

        return $this->handleRecordings();
    }
}
