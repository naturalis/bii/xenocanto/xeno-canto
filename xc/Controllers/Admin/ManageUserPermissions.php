<?php

namespace xc\Controllers\Admin;

use xc\Library;
use xc\ResultsTable;
use xc\User;

class ManageUserPermissions extends Controller
{
    public function handlePost()
    {
        if ($this->request->request->get('id')) {
            $user = User::load($this->request->request->get('id'));
            if ($user) {
                $largeFiles = intval($this->request->request->get('large-files'));
                $canConfirm = intval($this->request->request->get('can-confirm'));
                $disableRating = intval($this->request->request->get('disable-rating'));
                $downloadRestricted = intval($this->request->request->get('download-restricted'));
                $sql = "INSERT INTO permissions (userid, canconfirm, largefiles, disablerating, downloadrestricted) VALUES('{$user->userId()}', $canConfirm, $largeFiles, $disableRating, $downloadRestricted) ON DUPLICATE KEY UPDATE canconfirm=VALUES(canconfirm), largefiles=VALUES(largefiles), disablerating=VALUES(disablerating), downloadrestricted=VALUES(downloadrestricted)";
                $res = $this->query($sql);
                if (!$res) {
                    $this->notifyError('Error saving permissions: ' . Library::mysqli()->error);
                } else {
                    $this->notifySuccess("Updated permissions for {$user->userName()}");
                }
                return $this->seeOther("{$this->request->getBaseUrl()}?id={$user->userId()}");
            }
        }

        $this->notifyError("Couldn't find that user");
        return $this->seeOther($this->request->getBaseUrl());
    }

    public function handleRequest()
    {
        $u = $this->request->query->get('id');
        if ($u) {
            return $this->showUserDetailsPage($u);
        }

        return $this->showPrivilegedUsers();
    }

    protected function showUserDetailsPage($userid)
    {
        $user = User::load($userid);
        if (!$user) {
            return $this->notFound();
        }

        $title = "Manage permissions for {$user->userName()}";
        $output = "<ul class='breadcrumbs'>
            <li><a href='" . $this->getUrl('admin-index') . "'>Admin</a></li>
            <li><a href='" . $this->getUrl('admin-user-permissions') . "'>Manage User Permissions</a></li>
            <li class='current'>{$user->userName()}</li>
            </ul>";

        $isAdminChecked = '';
        $largeFilesChecked = '';
        $canConfirmChecked = '';
        $disableRatingChecked = '';
        $downloadRestrictedChecked = '';

        if ($user->isAdmin()) {
            $isAdminChecked = 'checked';
        }

        if ($user->canConfirm()) {
            $canConfirmChecked = 'checked';
        }

        if ($user->canUploadLargeFiles()) {
            $largeFilesChecked = 'checked';
        }

        if (!$user->canRateRecordings()) {
            $disableRatingChecked = 'checked';
        }

        if ($user->canDownloadRestrictedRecordings()) {
            $downloadRestrictedChecked = 'checked';
        }

        $output .= "<h1>$title</h1>
            <form method='post'>
            <input type='hidden' name='id' value='$userid'/>
            <p><input type='checkbox' disabled name='is-admin' $isAdminChecked value=1 id='is-admin' /> <label for='is-admin'>Administrator</label></p>
            <p><input type='checkbox' name='can-confirm' $canConfirmChecked value=1 id='can-confirm' /> <label for='can-confirm'>Can confirm recordings</label></p>
            <p><input type='checkbox' name='large-files' $largeFilesChecked value=1 id='large-files' /> <label for='large-files'>Can upload large files</label></p>
            <p><input type='checkbox' name='download-restricted' $downloadRestrictedChecked value=1 id='download-restricted' /> <label for='download-restricted'>Can download restricted recordings</label></p>
            <p><input type='checkbox' name='disable-rating' $disableRatingChecked value=1 id='disable-rating' /> <label for='disable-rating'><strong>Not</strong> allowed to rate recordings</label></p>
            <p><input type='submit' name='foo' value='Update' /></p>

            </form>
            ";

        return $this->template->render($output, ['title' => $title]);
    }

    protected function showPrivilegedUsers()
    {
        $title = 'Manage User Permissions';
        $newUserUrl = "On this page you can modify the permissions of a select group of users, or <a href='" . $this->getUrl('admin-user-permissions-new-user',
                ['q' => 'A']
            ) . "'>select a new user</a>.";

        $output = "<ul class='breadcrumbs'>
            <li><a href='" . $this->getUrl('admin-index') . "'>Admin</a></li>
            <li class='current'>Manage User Permissions</li>
            </ul>

            <h1>$title</h1>
            <p>$newUserUrl</p>
            <h2>Privileged users</h2>
            <p>The following users have admin privileges</p>
            <ul>
            ";

        $res = $this->query('SELECT * FROM users INNER JOIN permissions ON users.dir=permissions.userid WHERE admin ORDER BY username ASC'
        );
        while ($row = $res->fetch_object()) {
            $user = new User($row);
            $output .= "<li><a href='" . $this->getUrl('admin-user-permissions', ['id' => $user->userId()]
                ) . "'>{$user->userName()}</a></li>";
        }
        $output .= '</ul>
            <p>The following users can confirm recordings</p>
            <ul>
            ';

        $res = $this->query('
            SELECT * FROM users INNER JOIN permissions ON users.dir = permissions.userid WHERE canconfirm ORDER BY username ASC'
        );
        while ($row = $res->fetch_object()) {
            $user = new User($row);
            $output .= "<li><a href='" . $this->getUrl('admin-user-permissions', ['id' => $user->userId()]
                ) . "'>{$user->userName()}</a></li>";
        }
        $output .= '</ul>
            <p>The following users can upload large files</p>
            <ul>
            ';

        $res = $this->query('
            SELECT * FROM users INNER JOIN permissions ON users.dir = permissions.userid WHERE largefiles ORDER BY username ASC'
        );
        while ($row = $res->fetch_object()) {
            $user = new User($row);
            $output .= "<li><a href='" . $this->getUrl('admin-user-permissions', ['id' => $user->userId()]
                ) . "'>{$user->userName()}</a></li>";
        }
        $output .= '</ul>';

        $output .= '</ul>
            <p>The following users are allowed to download restricted recordings</p>
            <ul>
            ';

        $res = $this->query('
            SELECT * FROM users INNER JOIN permissions ON users.dir = permissions.userid WHERE downloadrestricted ORDER BY username ASC'
        );
        while ($row = $res->fetch_object()) {
            $user = new User($row);
            $output .= "<li><a href='" . $this->getUrl('admin-user-permissions', ['id' => $user->userId()]
                ) . "'>{$user->userName()}</a></li>";
        }
        $output .= '</ul>';

        $output .= '</ul>
            <p>The following users are <strong>not</strong> allowed to rate recordings</p>
            <ul>
            ';

        $res = $this->query('
            SELECT * FROM users INNER JOIN permissions ON users.dir = permissions.userid WHERE disablerating ORDER BY username ASC'
        );
        while ($row = $res->fetch_object()) {
            $user = new User($row);
            $output .= "<li><a href='" . $this->getUrl('admin-user-permissions', ['id' => $user->userId()]
                ) . "'>{$user->userName()}</a></li>";
        }
        $output .= '</ul>';


        return $this->template->render($output, ['title' => $title]);
    }

    public function selectNewUser()
    {
        $title = 'Select a User';
        $output = "<ul class='breadcrumbs'>
            <li><a href='" . $this->getUrl('admin-index') . "'>Admin</a></li>
            <li><a href='" . $this->getUrl('admin-user-permissions') . "'>Manage User Permissions</a></li>
            <li class='current'>Select a User</li>
            </ul>

            <h1>$title</h1>" . ResultsTable::userTable($this->request, 'admin-user-permissions-new-user',
                'admin-user-permissions'
            );

        return $this->template->render($output, ['title' => $title]);
    }
}
