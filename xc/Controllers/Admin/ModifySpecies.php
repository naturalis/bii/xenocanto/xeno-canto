<?php

namespace xc\Controllers\Admin;

use xc\Library;
use xc\Set;
use xc\Species;
use xc\User;

class ModifySpecies extends Controller
{
    private $set;

    private $fromSpecies;

    private $toSpecies;

    private $nrForeUpdated = 0;

    private $nrBackUpdated = 0;

    public function __construct($request)
    {
        parent::__construct($request);
    }

    public function selectSet()
    {
        $output = "<h2>Modify Species</h2>
            <p style='width: 600px;'>This page lets you modify the species name for a batch of recordings. The recordings should be grouped
            in a (disposible) set. Please make absolutely sure that the set contains just those recordings that need modification
            and that you select the correct set, as there is no option to revert the change!</p>
            <p style='margin-bottom: 15px;'><b>Step 1 (of 4)</b>: select a set:</p>
            <ul class='simple'>";

        $userid = User::current()->userId();
        $sets = Set::loadUserSets($userid, true);

        if ($sets) {
            foreach ($sets as $set) {
                $output .= "<li><a href='{$set->url()}'>{$set->name()}</a> &mdash; " . sprintf(ngettext('%d recording',
                            '%d recordings', intval($set->numRecordings())
                        ), $set->numRecordings()
                    ) . "
                    <a href='" . $this->getUrl('admin-modify-species-from', ['set' => $set->id()]
                    ) . "'><img src='/static/img/next.png' title='" . $this->sanitize(_('Select Set')
                    ) . "' class='icon'/></a>" . '</li>';
            }
        } else {
            $output .= "<li class='unspecified'>" . _('None') . '</li>';
        }

        $output .= '</ul>';

        return $this->template->render($output);
    }


    public function selectFromSpecies($setId)
    {
        $this->setSet($setId);

        if (empty($this->set)) {
            return $this->badRequest();
        }

        $output = "<h2>Modify Species</h2>
            <p style='margin-bottom: 15px;'><b>Step 2 (of 4)</b>: select the foreground or background
            species in the set <b>{$this->set->name()}</b> to modify:</p>
            <ul>";

        $species = $this->set->speciesInSet($setId);

        foreach ($species as $sp) {
            $url = $this->getUrl('admin-modify-species-to', [
                    'set' => $this->set->id(),
                    'fromSpecies' => $sp->speciesNumber(),
                ]);
            $output .= "<li><a href='$url'>{$sp->scientificName()}</a></li>";
        }

        return $this->template->render($output . '</ul>');
    }

    private function setSet($setId)
    {
        $this->set = Set::load($setId);
        return $this->set;
    }

    public function selectToSpecies($setId, $fromSpecies)
    {
        $this->setSet($setId);
        $this->setFromSpecies($fromSpecies);

        if (empty($this->set) || empty($this->fromSpecies)) {
            return $this->badRequest();
        }

        $output = "<h2>Modify Species</h2>
            <p style='margin-bottom: 15px;'><b>Step 3 (of 4)</b>: select a species to change
            <b>{$this->fromSpecies->scientificName()}</b> to:</p>
            <ul>";

        $sql = "
            SELECT species_nr, genus, species
            FROM taxonomy
            WHERE species_nr != '{$this->fromSpecies->speciesNumber()}' 
                AND species_nr != '' AND group_id = {$this->fromSpecies->groupId()}
            ORDER BY genus, species";
        $res = $this->query($sql);
        while ($row = $res->fetch_object()) {
            $url = $this->getUrl('admin-modify-species-confirm', [
                    'set' => $this->set->id(),
                    'fromSpecies' => $this->fromSpecies->speciesNumber(),
                    'toSpecies' => $row->species_nr,
                ]);
            $output .= "<li><a href='$url'>$row->genus $row->species</a></li>";
        }

        return $this->template->render($output . '</ul>');
    }

    private function setFromSpecies($speciesNr)
    {
        $this->fromSpecies = Species::load($speciesNr, false);
        return $this->fromSpecies;
    }

    public function confirm($setId, $fromSpecies, $toSpecies)
    {
        $this->setSet($setId);
        $this->setFromSpecies($fromSpecies);
        $this->setToSpecies($toSpecies);

        if (empty($this->set) || empty($this->fromSpecies) || empty($this->toSpecies)) {
            return $this->badRequest();
        }

        $url = $this->getUrl('admin-modify-species-save', [
                'set' => $this->set->id(),
                'fromSpecies' => $this->fromSpecies->speciesNumber(),
                'toSpecies' => $this->toSpecies->speciesNumber(),
            ]);
        $output = "<h2>Modify Species</h2>
            <p style='width: 600px; margin-bottom: 15px;'><b>Step 4 (of 4)</b>: please confirm
            that you want to change the foreground and background species in the set
            <b>{$this->set->name()}</b> from <b>{$this->fromSpecies->scientificName()}</b> to
            <b>{$this->toSpecies->scientificName()}</b>.</p>
            <p><a href='$url'>Do it!</a></p>";

        return $this->template->render($output);
    }

    private function setToSpecies($speciesNr)
    {
        $this->toSpecies = Species::load($speciesNr, false);
        return $this->toSpecies;
    }

    public function save($setId, $fromSpecies, $toSpecies)
    {
        $this->setSet($setId);
        $this->setFromSpecies($fromSpecies);
        $this->setToSpecies($toSpecies);

        if (empty($this->set) || empty($this->fromSpecies) || empty($this->toSpecies)) {
            return $this->badRequest();
        }

        $this->updateHistory();
        $this->updateRecordings();

        $successMessage = "Successfully modified $this->nrForeUpdated foreground and
            $this->nrBackUpdated background recordings in the set <b>{$this->set->name()}</b>.
            The species name was changed from  <b>{$this->fromSpecies->scientificName()}</b> to
            <b>{$this->toSpecies->scientificName()}</b>.";

        $this->notifySuccess($successMessage);
        return $this->seeOther($this->getUrl('admin-modify-species-set'));
    }

    private function updateHistory()
    {
        $update = "
            INSERT INTO birdsounds_history (
                SELECT null, now(), t1.snd_nr, t1.genus, t1.species, t1.ssp, t1.eng_name,
                    t1.songtype, t1.recordist, t1.longitude, t1.latitude, t1.remarks, t1.license, null
                FROM birdsounds AS t1
                LEFT JOIN datasets_recordings AS t2 ON t1.snd_nr = t2.snd_nr
                WHERE
                    t2.datasetid = {$this->set->id()} AND
                    (t1.species_nr = '{$this->fromSpecies->speciesNumber()}' OR 
                    t1.species_nr like '%{$this->fromSpecies->speciesNumber()}%')
             )";
        app()->db()->query($update);
        return Library::mysqli()->affected_rows;
    }

    private function updateRecordings()
    {
        $genus = $this->escape($this->toSpecies->genus());
        $species = $this->escape($this->toSpecies->species());
        $ssp = $this->escape($this->toSpecies->subspecies());
        $commonName = $this->escape($this->toSpecies->commonName());
        $family = $this->escape($this->toSpecies->family());

        $sql = "
            UPDATE birdsounds AS t1
            LEFT JOIN datasets_recordings AS t2 ON t1.snd_nr = t2.snd_nr
            SET
                t1.genus = '$genus',
                t1.species = '$species',
                t1.ssp = '$ssp',
                t1.eng_name = '$commonName',
                t1.species_nr = '{$this->toSpecies->speciesNumber()}',
                t1.family = '$family'
            WHERE t1.species_nr = '{$this->fromSpecies->speciesNumber()}' AND t2.datasetid = {$this->set->id()}";
        app()->db()->query($sql);
        $this->nrForeUpdated = Library::mysqli()->affected_rows;

        $sql = "
            UPDATE birdsounds_background AS t1
            LEFT JOIN datasets_recordings AS t2 ON t1.snd_nr = t2.snd_nr
            SET
                t1.species_nr = '{$this->toSpecies->speciesNumber()}',
                t1.scientific = '$genus $species',
                t1.english = '$commonName',
                t1.family = '$family'
            WHERE t1.species_nr = '{$this->fromSpecies->speciesNumber()}' 
                AND t2.datasetid = {$this->set->id()}";
        app()->db()->query($sql);
        $this->nrBackUpdated = Library::mysqli()->affected_rows;
    }
}
