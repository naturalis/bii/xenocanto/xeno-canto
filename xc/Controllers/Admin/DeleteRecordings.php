<?php

namespace xc\Controllers\Admin;

use xc\Query;
use xc\Recording;
use xc\SonoSize;
use xc\SonoType;
use xc\User;

class DeleteRecordings extends Controller
{
    public function delete()
    {
        $xcid = intval($this->request->request->get('xcid', 0));

        // This clones the functionality in RecordingEdit::delete(); not pretty,
        // but  it's rather impossible to abstract this into a single method
        $rec = Recording::load($xcid);
        if (!$rec) {
            return json_encode(['error' => 'recording already deleted']);
        }

        $reason = $this->escape($this->request->request->get('reason'));
        $deleter = User::current()->userId();
        $path = $rec->filePath();

        $sql = "
            INSERT INTO birdsounds_trash (
                genus,
                species,
                ssp,
                eng_name,
                family,
                length_snd,
                volume_snd,
                speed_snd,
                pitch_snd,
                number_notes_snd,
                variable_snd,
                songtype,
                song_classification,
                quality,
                recordist,
                olddate,
                `date`,
                `time`,
                country,
                location,
                longitude,
                latitude,
                elevation,
                remarks,
                back_extra,
                path,
                species_nr,
                order_nr,
                dir,
                neotropics,
                africa,
                asia,
                europe,
                australia,
                `datetime`,
                discussed,
                license,
                snd_nr,
                group_id,
                temperature,
                collection_specimen,
                collection_date,
                observed,
                playback,
                automatic, 
                device,
                microphone,
                form_data,
                delete_date,
                delete_user,
                delete_reason
            )
            (SELECT
                genus,
                species,
                ssp,
                eng_name,
                family,
                length_snd,
                volume_snd,
                speed_snd,
                pitch_snd,
                number_notes_snd,
                variable_snd,
                songtype,
                song_classification,
                quality,
                recordist,
                olddate,
                `date`,
                `time`,
                country,
                location,
                longitude,
                latitude,
                elevation,
                remarks,
                back_extra,
                path,
                species_nr,
                order_nr,
                dir,
                neotropics,
                africa,
                asia,
                europe,
                australia,
                `datetime`,
                discussed,
                license,
                snd_nr,
                group_id,
                temperature,
                collection_specimen,
                collection_date,
                observed,
                playback,
                automatic, 
                device,
                microphone,
                form_data,
                NOW(),
                '$deleter',
                '$reason'
            FROM birdsounds 
            WHERE snd_nr = $xcid
        )";

        if ($this->query($sql)) {
            $sql = "DELETE from birdsounds where snd_nr = $xcid";
            $this->query($sql);
            $sql = "DELETE from audio_info where snd_nr = $xcid";
            $this->query($sql);
            if (!empty($path) && is_file($path)) {
                $fname = basename($path);
                $destdir = app()->soundsTrashDir();
                if (!is_dir($destdir)) {
                    mkdir($destdir, 0755, true);
                }

                $dest = "$destdir/{$rec->recordistID()}_$fname";
                $res = rename($path, $dest);
                if (!$res) {
                    return json_encode(['error' => "recording deleted, but unable to move recording to trash"]);
                }
            }
            foreach ([SonoSize::SMALL, SonoSize::MEDIUM, SonoSize::LARGE, SonoSize::FULL] as $size) {
                foreach ([SonoType::SPECTROGRAM, SonoType::OSCILLOGRAM] as $type) {
                    @unlink($rec->sonoPath($size, $type));
                }
            }
            return json_encode(['error' => null]);
        } else {
            return json_encode(['error' => 'unable to delete recording from the database']);
        }
    }

    public function showForm()
    {
        $output = "
            <h1>Delete Recordings</h1>
            
            <p>This section offers the option to delete recordings based on a query. The process is identical to 
            deleting a single record, so recordings are not deleted permanently.<br><br></p>

            <form id='form-delete-rec-query' action='" . $this->getUrl('admin-delete-recording-verify') . "' method='post'>
            <p><input type='text' name='reason' placeholder='reason' size='40'/></p>
            <p><input type='text' name='query' placeholder='query' size='40' /></p>
            <p><input type='submit' name='delete' value='Delete' /></p>
            </form>";

        return $this->template->render($output, ['title' => 'Delete Recordings']);
    }

    public function verify()
    {
        $reason = $this->sanitize($this->request->request->get('reason'));
        $query = $this->sanitize($this->request->request->get('query'));

        $q = new Query($query);
        $result = $q->execute();
        $total = $result->num_rows;
        $resultUrl = $this->getUrl('browse', [
            'query' => $this->request->request->get('query'),
            'dir' => 1,
            'order' => 'xc'
        ]);

        $apiUrl = $this->getUrl('api-recording-search');
        $cacheUrl = $this->getUrl('admin-clear-cache');

        $script = <<<EOT
            <script type='text/javascript'>
            jQuery(document).ready(function() 
            {
                var n = 0;
            
                function deleteRecording(url, xcid, reason)
                {
                    var d = jQuery.Deferred();
                    var id = "xc-" + n++;
                    var status = jQuery("<p id='" + id + "'>Deleting recording XC" + xcid + "... <span class='result'></span></p>");
                    console.log("starting " + xcid);
                    jQuery("#status-log").append(status);
                    jQuery.post(url, {xcid: xcid, reason: reason})
                        .done (function(data) {
                            var resultElt = jQuery("#" + id + " > .result");
                            data = JSON.parse(data);
                            var message = data.error != null ? "<span class='warning'>" + data.error + "</span>" : 'deleted';
                            resultElt.html(message);
                        }).fail(function(xhr, status, error) {
                            var resultElt = jQuery("#" + id + " > .result");
                            resultElt.html("<span class='warning'>" + xhr.responseText + "</span>");
                        }).always(function() {
                            d.resolve();
                        });
                    return d;
                }
            
                jQuery("#form-delete-rec-query").submit(function()
                    {
                        var query = jQuery(this).find("input[name=query]").val();
                        var reason = jQuery(this).find("input[name=reason]").val();
                        var url = jQuery(this).attr('action');
                        if (query) {
                            jQuery.get("$apiUrl", {query: query, order: 'xc'},
                                function(data) {
                                    if (!data.error && data.recordings.length) {
                                        // issue these requests sequentially
                                        var deferred = jQuery.Deferred();
                                        var next = deferred;
                                        var x;
                                        for (x = 0; x < data.recordings.length; ++x) {
                                            (function (url, xcid) {
                                                console.log("chaining request for " + xcid);
                                                next = next.pipe(function() {
                                                    return deleteRecording(url, xcid, reason).done(function() { 
                                                        console.log("processed"); 
                                                    });
                                                });
                                            } (url, data.recordings[x].id));
                                        }
                                        // kick off the pipeline
                                        deferred.resolve();
                                    }
                                }, 'json');
                        }
                        return false;
                    });
            });
            </script>
            EOT;

        $output = "
            <h1>Delete Recordings</h1>
            <p class='important'>Your query will delete $total recordings. ";

        if ($total > 0) {
            $output .= "Note that <i>it is not possible to restore the recordings in batch</i>, 
            so make sure you have <a href='$resultUrl' target='_blank'>verified the query</a>.</p>
            <p>When ready, please manually <a href='$cacheUrl'>clear all caches</a>.</p>
            <br>
            <form id='form-delete-rec-query' action='" . $this->getUrl('admin-delete-recording-delete') . "' method='post'>
            <p><input type='text' name='reason' placeholder='reason' value='$reason' size='40' disabled /></p>
            <p><input type='text' name='query' placeholder='query' value='$query' size='40' disabled /></p>
            <p><input type='submit' name='delete' value='Delete' /></p>
            </form>
            <div id='status-log'></div>
            $script
            ";
        } else {
            $output .= '<a href="#" onclick="window.history.back(); return false;">Go back</a></p>.';
        }

        return $this->template->render($output, ['title' => 'Delete Recordings']);
    }
}
