<?php

namespace xc\Controllers\api;

use xc\Controllers\ApiMethod;
use xc\Library;

class LocationSpeciesList extends ApiMethod
{
    public function get()
    {
        $loc = Library::escape($this->request->query->get('location'));
        if (!$loc) {
            return $this->missingParameter('location');
        }

        $fgSpecies = [];
        $bgSpecies = [];

        $this->speciesListForLocation($loc, $fgSpecies, $bgSpecies);

        return $this->respond(
            [array_values($fgSpecies), array_values($bgSpecies)]
        );
    }

    private function speciesListForLocation($location, &$fgSpecies, &$bgSpecies = null)
    {
        $loc = Library::escape($location);
        $localName = $this->localNameLanguage();

        $sql = "
            SELECT t1.genus, t1.species, t2.$localName as common_name, t1.species_nr, t1.discussed 
            FROM birdsounds AS t1
            LEFT JOIN taxonomy_multilingual AS t2 ON t1.species_nr = t2.species_nr
            WHERE t1.location = '$loc'
            GROUP BY t1.species_nr 
            ORDER BY t1.order_nr";
        $res = $this->query($sql);
        $fgSpecies = [];

        while ($row = $res->fetch_object()) {
            $fgSpecies[$row->species_nr] = [
                'nr' => $row->species_nr,
                'scientificName' => "$row->genus $row->species",
                'commonName' => $row->common_name,
                'uncertain' => ($row->discussed == 1 || $row->discussed == 2),
            ];
        }

        if (!is_null($bgSpecies)) {
            $bgSpecies = [];
            $sql = "
                SELECT t1.species_nr, t3.genus, t3.species, t3.$localName as common_name
                FROM birdsounds_background AS t1
                LEFT JOIN birdsounds AS t2 ON t1.snd_nr = t2.snd_nr
                LEFT JOIN taxonomy_multilingual AS t3 ON t1.species_nr = t3.species_nr
                WHERE t2.location = '$loc'
                GROUP BY t1.species_nr 
                ORDER BY t2.order_nr";
            $res = $this->query($sql);
            while ($row = $res->fetch_object()) {
                if (!array_key_exists($row->species_nr, $fgSpecies)) {
                    $bgSpecies[$row->species_nr] = [
                        'nr' => $row->species_nr,
                        'scientificName' => "$row->genus $row->species",
                        'commonName' => $row->common_name,
                    ];
                }
            }
        }
    }
}
