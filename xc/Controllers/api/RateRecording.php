<?php

namespace xc\Controllers\api;

use xc\Controllers\AuthenticatedApiMethod;
use xc\Library;
use xc\User;

class RateRecording extends AuthenticatedApiMethod
{
    public function post()
    {
        $sndNr = intval($this->request->request->get('snd_nr'));
        if (empty($sndNr)) {
            return $this->missingParameter('snd_nr');
        }

        if (!User::current()->canRateRecording($sndNr)) {
            return $this->forbidden();
        }

        $quality = intval($this->request->request->get('quality'));

        $res = Library::rateRecording($sndNr, $quality);

        $payload = [
            'success' => ($res != false),
            'snd_nr' => $sndNr,
            'quality' => $quality,
        ];

        return $this->respond($payload);
    }
}
