<?php

namespace xc\Controllers\api;

use xc\Controllers\CompletionServiceApiMethod;
use xc\Library;

class CompletionServiceLocations extends CompletionServiceApiMethod
{
    protected function query($term, &$suggestions, &$data)
    {
        $coordClause = $this->request->query->get('coords') ? 'AND latitude IS NOT NULL AND longitude IS NOT NULL' : '';

        $location = Library::escape($term);
        $sql = "
            SELECT location, longitude, latitude, country 
            FROM birdsounds 
            WHERE MATCH (location) AGAINST ('$location*' IN BOOLEAN MODE) $coordClause
            GROUP BY location";
        $res = Library::query($sql);

        while ($row = $res->fetch_object()) {
            $suggestions[] = "$row->location";
            $data[] = [
                'country' => $row->country,
                'lat' => $row->latitude,
                'lng' => $row->longitude,
            ];
        }
    }
}
