<?php

namespace xc\Controllers\api;

use xc\Controllers\ApiMethod;
use xc\Library;
use xc\MultiSpeciesMap;
use xc\Query;

class RegionResults extends ApiMethod
{
    public function get()
    {
        $latmax = Library::strip($this->request->query->get('yn'));
        $latmin = Library::strip($this->request->query->get('ys'));
        $lonmin = Library::strip($this->request->query->get('xw'));
        $lonmax = Library::strip($this->request->query->get('xe'));
        $filter = Library::escape($this->request->query->get('query'));

        $q = new Query("box:$latmin,$lonmin,$latmax,$lonmax $filter");
        $res = $q->execute(Query::NO_PAGING, 2000);
        $numSpecies = $q->numSpecies();
        $numRecordings = $q->numRecordings();
        $summary = [
            'species' => $numSpecies,
            'recordings' => $numRecordings,
        ];

        // this is a little bit strange.  We don't actually want to display a map, so
        // we'll pass an arbitrary map id.  We're simply using this to generate JSON for
        // markers
        $map = new MultiSpeciesMap('map-canvas', $res);

        return $this->respond(
            ['specs' => $map->getSpecs(), 'markers' => $map->getMarkers(), 'summary' => $summary]
        );
    }
}
