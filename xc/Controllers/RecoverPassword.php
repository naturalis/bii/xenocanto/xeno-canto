<?php

namespace xc\Controllers;

use xc\Library;
use xc\User;
use xc\XCMail;
use xc\XCRedis;

class RecoverPassword extends Controller
{
    public const MAX_ATTEMPTS = 5;

    public const ATTEMPT_TIMEOUT = 3600;

    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis('password-reset-' . $this->request->getClientIp());
    }

    public function handlePost()
    {
        $email = $this->request->request->get('email');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->notifyError(_('Invalid email address'));
        } else {
            $escapedEmail = $this->escape($email);

            $sql = "SELECT username, dir FROM users WHERE email = '$escapedEmail'";
            $res = $this->query($sql);
            if ($res && $res->num_rows > 0) {
                $row = $res->fetch_object();
                $code = User::getPasswordResetCode();
                $sql = "INSERT INTO user_password_reset_codes (dir, reset_code) VALUES ('" . $row->dir . "', '$code')";
                $res = $this->query($sql);
                if ($res) {
                    $message = wordwrap("
A password change has been requested for $email. Use this link to set a new password:

https://xeno-canto.org/auth/reset/$code

If you did not request a password change, you can ignore this email.

Best regards,

The xeno-canto team", 60);

                    (new XCMail($email, '[xeno-canto] Password Reset', $message))->send();
                }
            }
            // Do not present a message when the email address has not been registered (user enumeration)
            $this->notifySuccess(sprintf(_('We have sent password reset instructions to %s.'), "<b>$email</b>"));
            return $this->seeOther($this->getUrl('login'));
        }

        return $this->content($email);
    }

    protected function content($email = '')
    {
        $title = _('Recover Password');
        $emailValue = htmlspecialchars($email, ENT_QUOTES);
        $output = "
            <div id='login-form'>
            <h1>$title</h1>
            <p>" . _('Have you forgotten your password? Then fill in your email address below, and we will send instructions on how to create a new one.') . "
            </p>
            <form method='post'>
            <input type='hidden' name='submitted' value='1'>
            <p>
            <input type='text' name='email' value='$emailValue' placeholder='" . htmlspecialchars(_('Email address'),
                    ENT_QUOTES) . "'>
            </p>
            <p>
            <input type='submit' value='" . htmlspecialchars(_('Submit'), ENT_QUOTES) . "'>
            </p>
            </form>
            </div>";

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'recover-password']);
    }

    public function handleRequest()
    {
        if ($this->resetAttempts() >= self::MAX_ATTEMPTS) {
            return $this->tooManyAttempts();
        }
        return $this->content();
    }

    private function resetAttempts()
    {
        return $this->redis->get() ?? 0;
    }

    private function tooManyAttempts()
    {
        $title = _('Password Recovery');
        $output = "
            <h1>$title</h1>
            <p>" . $this->tooManyAttemptsMessage() . '</p>';

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'recover-password']);
    }

    public function tooManyAttemptsMessage(): string
    {
        return _('You have exceeded the maximum number of password resets.') . ' ' .
            sprintf(_('You have been locked out of further attempts for %s minutes. %s.'),
            (self::ATTEMPT_TIMEOUT / 60),
            Library::obfuscateEmail(CONTACT_EMAIL . '?subject=Password%20recovery%20xeno-canto',
                _('Contact us via email')));
    }

    public function resetPassword($code)
    {
        $this->updateResetAttempts();
        if ($this->resetAttempts() > 2 && $this->resetAttempts() < self::MAX_ATTEMPTS) {
            $this->notifyWarning(sprintf(_('You have %d more password reset attempts.'),
                (self::MAX_ATTEMPTS - $this->resetAttempts())));
        } elseif ($this->resetAttempts() >= self::MAX_ATTEMPTS) {
            $this->notifyError($this->tooManyAttemptsMessage());
            return $this->seeOther($this->getUrl('index'));
        }

        // User is not supposed to be logged in
        if (User::current()) {
            $this->notifyError(_('You are already logged in, please use the regular option to change your password.'));
            return $this->seeOther($this->getUrl('mypage', ['p' => 'password']));
        }
        if (User::loginWithResetCode($code)) {
            $this->notifySuccess(_('You have been logged in for a single session. Please update your password now.'));
            return $this->seeOther($this->getUrl('mypage', ['p' => 'password', 'reset' => $code]));
        }
        $this->notifyError(_('Incorrect password reset code entered, please carefully check your email.'));
        return $this->seeOther($this->getUrl('index'));
    }

    private function updateResetAttempts(): void
    {
        if (!$this->redis->get()) {
            $this->redis->set(1, self::ATTEMPT_TIMEOUT);
        } else {
            $this->redis->incr();
        }
    }
}