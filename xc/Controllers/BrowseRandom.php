<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Query;
use xc\ResultsTable;
use xc\ViewType;

class BrowseRandom extends Controller
{
    public function handleRequest()
    {
        $title = _('Random Recordings');
        $nrPerPage = $this->defaultNumPerPage();

        // generate a set of random IDS.  we do this here, because the alternative
        // ("order by rand()" in sql) is insanely slow for large databases
        $randomIds = [];

        // generate 3x the number of IDs we need because some of the ids may end up
        // being deleted from the database
        $res = $this->query('SELECT MAX(snd_nr) AS maxid FROM birdsounds');
        $row = $res->fetch_object();
        $maxId = $row->maxid;
        for ($i = 0; $i < ($nrPerPage * 3); $i++) {
            $id = rand(1, $maxId);
            $randomIds[] = $id;
        }
        $randomIds = array_unique($randomIds);

        // compute the query, find the result
        $localName = $this->localNameLanguage();
        $idsString = implode(', ', $randomIds);
        $sql = "
            SELECT * FROM (
                SELECT " . Query::birdsoundsSelectFields() . ", audio_info.length, M.$localName as localName
                FROM birdsounds
                INNER JOIN audio_info USING(snd_nr)
                INNER JOIN taxonomy_multilingual M USING (species_nr)
                WHERE snd_nr IN ($idsString)
            ) T 
            ORDER BY RAND() 
            LIMIT $nrPerPage";
        $res = $this->query($sql);
        $numHits = $res->num_rows;

        $output = "
            <h1>$title</h1>
            <p>" . _('Reload the page for a new set of random recordings.') . '</p>';

        if ($numHits) {
            $output .= ResultsTable::resultsTableForView(
                $this->request, $res, ViewType::SONOGRAMS, ['showSortSelect' => false]
            );
        } else {
            $output .= HtmlUtil::printErrorMessages(_('Internal Error'), [_('Unable to query recordings'),]);
        }

        return $this->template->render(
            $output, ['title' => $title, 'bodyId' => 'random']
        );
    }
}
