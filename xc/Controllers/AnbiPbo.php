<?php

namespace xc\Controllers;

class AnbiPbo extends Controller
{
    public function handleRequest()
    {
        $output = "
            <h1>Required information supporting ANBI-PBO status</h1>
            <ul>
            <li>Name: Stichting Xeno-canto voor natuurgeluiden. (\"Xeno-canto Foundation\")</li>
            <li>RSIN (Legal Entities & Partnerships Identification Number): 819962946</li>
            <li>KvK number: 27327175</li>
            <li>Visiting address: Laan van Nieuw Oost Indië 177, 2593BN 's-Gravenhage</li>
            <li>Mail address: Laan van Nieuw Oost Indië 177, 2593BN 's-Gravenhage</li>
            <li>Purpose: Increase accessibility of sounds of nature; performing research into sounds of nature, 
                especially geographical and temporal variation</li>
            <li><a href='/docs/XC_Beleidsplan_2018.pdf'>Policy plan (2018)</a></li>
            <li><a href='/docs/XC_Beleidsplan_2024-2028.pdf'>Policy plan (2024-2028)</a></li>
            <li>Board:</li>
            <ul>
                <li>Chairman: Willem-Pier Vellinga</li>
                <li>Treasurer: Bob Planqué</li>
                <li>Secretary: Sander Pieterse</li>
            </ul>
            <li>The board is non-renumerated</li>
            <li><a href='/docs/XC_Jaarverslag_2017.pdf'>Report of activities (2017)</a></li>
            <li><a href='/docs/XC_jaarverslag_2018.pdf'>Report of activities (2018)</a></li>
            <li><a href='/docs/XC_jaarverslag_2019.pdf'>Report of activities (2019)</a></li>
            <li><a href='/docs/XC_jaarverslag_2020.pdf'>Report of activities (2020)</a></li>
            <li><a href='/docs/XC_jaarverslag_2021.pdf'>Report of activities (2021)</a></li>
            <li><a href='/docs/XC_jaarverslag_2022.pdf'>Report of activities (2022)</a></li>
            <li><a href='/docs/XC_jaarverslag_2023.pdf'>Report of activities (2023)</a></li>
            <li><a href='/docs/jaarrekening_XC_2017 WP_SP_BP.pdf'>Financial statement (2017)</a></li>
            <li><a href='/docs/jaarrekening_XC_2018.pdf'>Financial statement (2018)</a></li>
            <li><a href='/docs/jaarrekening_XC_2019.pdf'>Financial statement (2019)</a></li>
            <li><a href='/docs/jaarrekening_XC_2020.pdf'>Financial statement (2020)</a></li>
            <li><a href='/docs/jaarrekening_XC_2021.pdf'>Financial statement (2021)</a></li>
            <li><a href='/docs/jaarrekening_XC_2022.pdf'>Financial statement (2022)</a></li>
            <li><a href='/docs/jaarrekening_XC_2023.pdf'>Financial statement (2023)</a></li>
            </ul>";
        return $this->template->render($output, ['title' => _('ANBI-PBO')]);
    }
}
