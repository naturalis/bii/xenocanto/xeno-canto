<?php

/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Library;
use xc\Query;
use xc\ResultsTable;
use xc\Set;
use xc\User;

class SetActions
{
    private $set;

    public function __construct($set)
    {
        $this->set = $set;
    }

    public function actions($rec)
    {
        $user = User::current();
        if ($user && ($user->isAdmin() || ($user->userId() == $this->set->userId()))) {
            return "<a href='" . Library::getUrl(
                    'set-remove-recording', ['id' => $this->set->id(), 'xcid' => $rec->xcid()]
                ) . "' title='" . _(
                    'Remove from set'
                ) . "'><img class='icon' src='/static/img/bookmark-remove.png' /></a>";
        }
        return '';
    }
}

class SetView extends Controller
{
    public function showSet($id)
    {
        $set = Set::load(intval($id));
        if (!$set) {
            return $this->notFound();
        }

        $user = User::current();
        $allowed = $canEdit = false;
        if ($set->visibility() != Set::VISIBILITY_PRIVATE) {
            $allowed = true;
        }

        if ($user) {
            if ($user->isAdmin() || ($user->userId() == $set->userId())) {
                $allowed = $canEdit = true;
            }
        }

        if (!$allowed) {
            return $this->unauthorized();
        }

        $pagenumber = $this->pageNumber();
        $order = $this->request->query->get('order');
        $direction = $this->request->query->get('dir');

        $num_per_page = $this->defaultNumPerPage();

        $setUser = User::load($set->userId());
        $actions = '';
        if ($canEdit) {
            $actions = "
                <a href='" . $this->getUrl('set-edit', ['id' => $id]
                ) . "'><img src='/static/img/edit.png' title='" . $this->sanitize(
                    _('Edit Set Details')
                ) . "' class='icon'/></a>" . "<a href='" . $this->getUrl(
                    'set-delete', ['id' => $id]
                ) . "'><img src='/static/img/delete.png' title=" . $this->sanitize(
                    _('Delete Set')
                ) . "'' class='icon'/></a>";
        }

        $body = "<header>
            <h1>{$set->name()} $actions</h1>
            <div>" . sprintf(
                _('Curated by %s'),
                "<a href='{$setUser->getProfileURL()}'>{$setUser->userName()}</a>"
            ) . '</div>';

        if ($set->description()) {
            $body .= "
            <div id='set-description'>" . HtmlUtil::formatUserTextLimited($set->description()) . '
            </div>';
        }

        $filter = $this->request->query->get('filter');
        $qstring = "$filter set:{$set->id()}";
        $q = new Query($qstring);
        $q->setOrder($order, $direction);
        $total = $q->numRecordings();
        $nSpecies = $q->numSpecies();
        $no_pages = ceil($total / $num_per_page);
        $res = $q->execute($pagenumber);

        $body .= "
            <form method='get' class='hflex'>
            <input class='flex-main' type='text' name='filter' value='" . $this->sanitize(
                $filter
            ) . "' placeholder='" . $this->sanitize(_('Filter recordings')) . "...' />
            <input type='submit' value='" . $this->sanitize(_('Filter')) . "'/>
            </form>
            <p>" . sprintf(
                ngettext(
                    '%d recording',
                    '%d recordings',
                    $total
                ),
                $total
            ) . ', ' . sprintf(
                ngettext(
                    '%d species',
                    '%d species',
                    $nSpecies
                ),
                $nSpecies
            ) . '</p>
                </header>';

        $body .= HtmlUtil::pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
        $view = $this->request->query->get('view', $this->defaultResultsView());
        $body .= "<div class='results-format'>" . _('Results format') . ': ' . ResultsTable::makeViewLinks(
                $this->request,
                $view
            ) . '</div>';

        $body .= ResultsTable::resultsTableForView(
            $this->request, $res, $view, ['actionsCallback' => [new SetActions($set), 'actions']]
        );
        $body .= HtmlUtil::pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
        $body .= "
            <script type='text/javascript'>
            jQuery(document).ready(function() {
                jQuery('div#set-description').expander({
                    slicePoint: 200,
                    expandText: '" . _('more') . " &raquo;',
                    userCollapseText: '&laquo; " . _('less') . "'
                });
            });
            </script>";

        return $this->template->render(
            $body, ['title' => $set->name(), 'bodyId' => 'set']
        );
    }

    public function redirect()
    {
        $id = $this->request->query->get('set');
        if ($id) {
            return $this->movedPermanently($this->getUrl('set', ['id' => $id]));
        }
        return $this->notFound();
    }
}
