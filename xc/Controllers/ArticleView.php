<?php

namespace xc\Controllers;

use xc\ForumThread;
use xc\HtmlUtil;
use xc\Library;
use xc\User;
use xc\XCMail;

class ArticleView extends Controller
{
    public function handlePost($blogNr)
    {
        $comment = $this->request->request->get('comment-text');
        $sanitizedComment = HtmlUtil::sanitizeHtml($comment, HtmlUtil::htmlSanitizerConfigDefault());
        $incomplete = false;

        $user = User::current();
        if (!$user) {
            $this->notifyError(_('You must be logged in to post a comment.'));
            $incomplete = true;
        } elseif (!$user->isVerified()) {
            $this->notifyError(_('You must verify your email address before posting a comment.') . ' ' . sprintf(_("To re-send a verification code, please visit <a href='%s'>your account page</a>."),
                    $this->getUrl('mypage')));
            $incomplete = true;
        } elseif (empty($sanitizedComment)) {
            $this->notifyError(_('You cannot post an empty comment.'));
            $incomplete = true;
        }

        if (!$incomplete) {
            $name = $this->escape($user->userName());
            $date = date('Y-m-d');
            $time = date('H:i');
            $ecomment = $this->escape($comment);
            $userdir = User::current()->userId();
            $sql = "
                INSERT INTO feature_discussions 
                (`name`, `text`, `date`, `time`, `dir`, `blognr`, `datetime`)
                VALUES 
                ('$name', '$ecomment', '$date', '$time', '$userdir', '$blogNr', NOW())";
            $this->query($sql);

            $subject = "[xeno-canto] New article comment on $blogNr";
            $link = $this->getUrl('feature-view', ['blognr' => $blogNr]);
            $body = "From: $name\n\nLink: $link\n\nText: $comment";

            (new XCMail(Library::adminMailAddresses(), $subject, $body))->send();

            $res = $this->query("SELECT title, email, username FROM blogs INNER JOIN users USING(dir) WHERE blognr = $blogNr");
            $row = $res->fetch_object();
            $featurename = $this->sanitize($row->title);

            $authoremail = $row->email;
            $commenter = $user->userName();

            $subject = '[xeno-canto] New comment on article';
            $message = wordwrap(sprintf("%s has posted a new comment on your article '%s'.  To view all comments, visit the following URL:",
                $commenter, $featurename), 60);
            $wrappedComment = wordwrap($comment, 60);
            (new XCMail($authoremail, "[xeno-canto] $subject", "$message

                $link

                $wrappedComment"))->send();

            return $this->seeOther($link);
        }

        return $this->renderContent($blogNr);
    }

    protected function renderContent($blogNr)
    {
        $row = null;
        $blogNr = intval($blogNr);
        if ($blogNr) {
            $res = $this->query("
                SELECT title, blog_as_html, dir, public, published AS published, 
                    date_last_update AS updated, U.username, U.blurb, 
                    nrecordings, ncomments
                FROM blogs 
                INNER JOIN users U USING(dir) 
                LEFT JOIN rec_summary_stats S ON S.userid = U.dir AND S.group_id = 0
                WHERE blognr = $blogNr
                GROUP BY dir");
            $row = $res->fetch_object();
        }

        if (!($row && $this->userCanView($row))) {
            return $this->notFound();
        }

        $date = HtmlUtil::formatDate('%B %e, %Y', strtotime($row->published));
        $updateDate = null;
        if ($row->updated > $row->published) {
            $updateDate .= '<em>' . sprintf(_('This article was last updated on %s at %s'),
                    HtmlUtil::formatDate('%B %e, %Y', strtotime($row->updated)),
                    date('H:i', strtotime($row->updated))) . '</em>';
        }

        $articleTitle = $this->sanitize($row->title);
        $body = "
            <ul class='breadcrumbs'>
            <li><a href='" . $this->getUrl('features') . "'>" . _('Articles') . "</a></li>
            <li class='current'>$articleTitle</li>
            </ul>
            ";

        $editLink = '';
        if ($this->userCanEdit($row)) {
            $editLink = "<p><a href='" . $this->getUrl('feature-edit',
                    ['blognr' => $blogNr]) . "'>" . _('Edit this article') . '</a></p>';
        }
        $body .= "<section class='column forum-thread'>
            <article itemscope itemtype='//schema.org/Article'>
            <h1 itemprop='name'>$articleTitle</h1>
            <h2>" . sprintf(_('By %s'),
                "<span itemprop='author' itemscope itemtype='//schema.org/Person'><span itemprop='name'>" . $this->sanitize($row->username) . "</span></span>") . "</h2>
            <p>$date</p>
            <div itemprop='articleBody'>$row->blog_as_html</div>
            $updateDate
            $editLink
            </article>";

        //add discussion if there is anything on record
        $body .= '
            <p>' . _('How was this made?') . " <a href='" . $this->getUrl('feature-preview',
                ['blognr' => $blogNr]) . "'>" . _('View the text') . "</a></p>
            <a name='comments'></a>
            ";

        $thread = new ForumThread($blogNr, 'feature_discussions', 'blognr');
        $body .= $thread->generateHTML();
        $body .= '
            <h2>' . _('Add your comments') . "</h2>
            <form class='new-message' action='#last' method='post'>";
        if (User::current()) {
            $body .= "
                <input type='hidden' name='blognr' value='$blogNr'>
                <p>" . sprintf(_('You can format your text using the %s text formatting syntax.'),
                    "<a target='_blank' href='" . $this->getUrl('markdown') . "'>Markdown</a>") . "</p>
                <p>
                <p>
                <textarea class='message-text' name='comment-text' placeholder='" . htmlspecialchars(_('Comment Text'),
                    ENT_QUOTES) . "'>{$this->request->request->get('comment-text')}</textarea>
                </p>
                <input type='submit' value='" . htmlspecialchars(_('Comment'), ENT_QUOTES) . "'>
                ";
        } else {
            $body .= '
                <p>' . sprintf(_("<a href='%s'>Log in</a> to reply to this topic."), $this->getUrl('login')) . '
                </p>';
        }
        $body .= "
            </form>
            </section>
            <section class='column author-info'>
            " . $this->userInfoBox($row) . '
            </section>';

        return $this->template->render($body, ['title' => _('Article'), 'bodyId' => 'features']);
    }

    private function userCanView($row)
    {
        $user = User::current();
        return ($row->public || ($user && ($user->isAdmin() || $row->dir === $user->userId())));
    }

    private function userCanEdit($row)
    {
        $user = User::current();
        return ($user && ($user->isAdmin() || ($row->dir === $user->userId())));
    }

    private function userInfoBox($row)
    {
        // this doesn't have full information, but enough to use parts of the User
        // API.  yeah... ugly.
        $u = new User($row);
        $avatarText = '';
        if ($u->hasAvatar()) {
            $avatarText = "<a href='{$u->getAvatar()}' class='fancybox'><img class='avatar' src='{$u->getThumbnail()}'/></a>";
        } else {
            $avatarText = "<img class='avatar' src='{$u->getDefaultThumbnail()}'/>";
        }

        return '
        <h1>' . _('About the author') . "</h1>
        <div class='userbox'>
          <div class='avatar-container'>
            $avatarText
          </div>
          <h1>
          <a href='{$u->getProfileURL()}' title='public profile'>{$u->userName()}</a>
          </h1>
          <ul>
          <li><img class='icon' src='/static/img/discuss-light.png' title='Number of forum posts'/>$row->ncomments</li>
          <li><img class='icon' src='/static/img/audio.png' title='Number of recordings' />$row->nrecordings</li>
          </ul>
        </div>
        <p>" . HtmlUtil::formatUserText($u->blurb()) . '</p>
        ';
    }

    public function handleRequest($blogNr)
    {
        return $this->renderContent($blogNr);
    }
}
