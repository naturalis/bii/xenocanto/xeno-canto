<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Library;

class BrowseTaxonomy extends Controller
{
    private $hideBranches;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->setHideBranches();
    }

    private function setHideBranches()
    {
        // Do not hide by default
        if (in_array($this->request->query->get('hide'), ['y', 'n'])) {
            $this->hideBranches = $this->request->query->get('hide');
            app()->session()->set('hide', $this->hideBranches);
        } elseif (in_array(app()->session()->get('hide'), ['y', 'n'])) {
            $this->hideBranches = app()->session()->get('hide');
        } else {
            $this->hideBranches = 'n';
        }
    }

    public function handleRequest()
    {
        $title = _('Browse by Taxonomy');
        $output = "<h1>$title</h1>";

        $group = strip_tags($this->request->query->get('grp'));
        $order = strip_tags($this->request->query->get('ord') ?? $this->request->query->get('o'));
        $family = strip_tags($this->request->query->get('fam') ?? $this->request->query->get('f'));
        $genus = strip_tags($this->request->query->get('gen') ?? $this->request->query->get('g'));

        if (!empty($genus)) {
            $g = $this->escape($genus);
            $localName = $this->localNameLanguage();
            $sql = "
                SELECT t1.order, t1.family, t1.family_english, t1.genus, t1.species, t1.eng_name, t1.species_nr, t1.recordings, 
                       t2.$localName AS localName, t1.extinct, t1.authority, t1.group_id, t3.name AS group_name
                FROM taxonomy t1
                LEFT JOIN taxonomy_multilingual t2 ON t1.species_nr = t2.species_nr
                LEFT JOIN groups AS t3 ON t1.group_id = t3.id
                WHERE t1.genus = '$g' 
                ORDER BY CASE WHEN t1.group_id = 1 THEN t1.IOC_order_nr ELSE t1.species END ASC";
            $result = $this->query($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= HtmlUtil::printErrorMessages(_('Error!'), [sprintf(_("No results for genus '%s'"), $genus)]);
            } else {
                $output .= $this->getBackLink() . $this->hideBranchesInput() . "
                    <ul class='tree'>
                    <li>" . $this->getGroupLink($row) . "</a>
                    <ul>
                    <li>" . $this->getOrderLink($row) . '
                    <ul>
                    <li>' . $this->getFamilyLink($row) . '
                    <ul>
                    <li>' . $this->getGenusLink($row) . $this->printSpeciesList($result) . '
                    </li>
                    </ul>
                    </li>
                    </ul>
                    </li>
                    </ul>
                    </li>
                    </ul>';
            }
        } elseif (!empty($family)) {
            $f = $this->escape($family);
            $sql = "
                SELECT t1.order, t1.family, t1.family_english, t1.genus, SUM(t1.recordings) AS recordings, 
                    (COUNT(t1.species_nr)-SUM(t1.extinct)) AS nextant, t1.group_id, t2.name AS group_name
                FROM taxonomy AS t1
                LEFT JOIN groups AS t2 ON t1.group_id = t2.id
                WHERE t1.family = '$f' 
                GROUP BY t1.genus 
                ORDER BY CASE WHEN t1.group_id = 1 THEN t1.IOC_order_nr ELSE t1.genus END ASC";
            $result = $this->query($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= HtmlUtil::printErrorMessages(_('Error!'),
                    [sprintf(_("No results for family '%s'"), $family)]);
            } else {
                $output .= $this->getBackLink() . $this->hideBranchesInput() . "
                    <ul class='tree'>
                    <li>" . $this->getGroupLink($row) . "
                    <ul>
                    <li>" . $this->getOrderLink($row) . '
                    <ul>
                    <li>' . $this->getFamilyLink($row) . $this->printGenusList($result) . '</li>
                    </ul>
                    </li>
                    </ul>
                    </li>
                    </ul>';
            }
        } elseif (!empty($order)) {
            $o = $this->escape($order);
            $sql = "
                SELECT t1.order, t1.family, t1.family_english, SUM(t1.recordings) as recordings, 
                    (COUNT(t1.species_nr)-SUM(t1.extinct)) as nextant, t1.group_id, t2.name AS group_name
                FROM taxonomy AS t1
                LEFT JOIN groups AS t2 ON t1.group_id = t2.id
                WHERE t1.order = '$o' 
                GROUP BY t1.family 
                ORDER BY CASE WHEN t1.group_id = 1 THEN t1.IOC_order_nr ELSE t1.family END ASC";
            $result = $this->query($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= HtmlUtil::printErrorMessages(_('Error!'), [sprintf(_("No results for order '%s'"), $order)]);
            } else {
                $output .= $this->getBackLink() . $this->hideBranchesInput() . "
                    <ul class='tree'>
                    <li>" . $this->getGroupLink($row) . "</a>
                    <ul>
                    <li>" . $this->getOrderLink($row) . $this->printFamilyList($result) . '</li>
                    </ul>
                    </li>
                    </ul>';
            }
        } elseif (!empty($group)) {
            $g = $this->escape($group);
            $sql = "
                SELECT t1.order, SUM(recordings) as recordings, t1.group_id, t2.name AS group_name,
                    (COUNT(species_nr)-SUM(extinct)) as nextant
                FROM taxonomy AS t1
                LEFT JOIN groups AS t2 ON t1.group_id = t2.id
                WHERE t2.name = '$g'
                GROUP BY t1.order 
                ORDER BY " . ($g == 'birds' ? 't1.IOC_order_nr' : 't1.order');
            $result = $this->query($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= HtmlUtil::printErrorMessages(_('Error!'), [sprintf(_('No results for group %s'), $group)]);
            } else {
                $output .= $this->printOrderList($result);
            }
        } else {
            $sql = "
                SELECT t2.id AS group_id, t2.name AS group_name, SUM(recordings) AS recordings, 
                    (COUNT(species_nr)-SUM(extinct)) AS nextant 
                FROM taxonomy t1
                LEFT JOIN groups AS t2 ON t1.group_id = t2.id
                GROUP BY t1.group_id 
                ORDER BY t2.id ASC";
            $result = $this->query($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= HtmlUtil::printErrorMessages(_('Error!'), [_('Internal Database Error')]);
            } else {
                $output .= $this->printGroupList($result);
            }
        }

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'taxonomy']);
    }

    private function getBackLink()
    {
        return "<a href='" . $this->getUrl('browse-taxonomy') . "'>" . _('List of all groups') . "</a>";
    }

    private function hideBranchesInput()
    {
        $checked = $this->hideBranches() ? 'checked' : '';
        $value = !$this->hideBranches() ? 'y' : 'n';

        $output = "
            <form id='hide-branches' method='get'>
            <input type='checkbox' id='hide' name='hide' value='$value' $checked>
            <label for='hide'>" . _('Hide taxa without recordings') . "</label><br>";

        foreach ($this->request->query->all() as $param => $value) {
            if ($param != 'hide') {
                $output .= "<input type='hidden' name='$param' value='$value'>";
            }
        }

        $output .= "
            <script>
            var checkbox = jQuery('#hide');
            var form = jQuery('#hide-branches');
            checkbox.click(function() {
                if (checkbox.val() == 'n') {
                    jQuery('<input>').attr('type','hidden').attr('name','hide').attr('value','n').appendTo(form);
                }
                form.submit();
            });
            </script>";

        return $output . '</form>';
    }

    private function hideBranches()
    {
        return $this->hideBranches == 'y';
    }

    private function getGroupLink($row)
    {
        return "<a class='common-name' href='" . $this->getUrl('browse-taxonomy',
                ['grp' => $row->group_name]) . "'>" . strtoupper($row->group_name) . "</a>";
    }

    private function getOrderLink($row)
    {
        if ($this->hideBranches()) {
            $sql = "SELECT SUM(recordings) AS children FROM taxonomy WHERE `order` = '$row->order'";
            if ($this->query($sql)->fetch_object()->children == 0) {
                return "<span class='common-name'>$row->order</span>";
            }
        }

        return "<a href='" . $this->getUrl('browse-taxonomy', ['ord' => $row->order]) . "'>$row->order</a>";
    }

    private function getFamilyLink($row)
    {
        if ($this->hideBranches()) {
            $sql = "SELECT SUM(recordings) AS children FROM taxonomy WHERE family = '$row->family'";
            if ($this->query($sql)->fetch_object()->children == 0) {
                return "<span class='common-name'>$row->family</span>" . ($row->family_english ? " ($row->family_english)" : '');
            }
        }

        return "<a href='" . $this->getUrl('browse-taxonomy',
                ['fam' => $row->family]) . "'>$row->family</a>" . ($row->family_english ? " ($row->family_english)" : '');
    }

    private function getGenusLink($row)
    {
        $sql = "SELECT SUM(recordings) AS children FROM taxonomy WHERE genus = '$row->genus'";
        $nrSpecies = $this->query($sql)->fetch_object()->children;

        if ($this->hideBranches() && $nrSpecies == 0) {
            return "<span class='common-name'>$row->genus</span>";
        }

        $link = "<a href='" . $this->getUrl('browse-taxonomy', ['gen' => $row->genus]) . "'>$row->genus</a>";
        return $nrSpecies == 0 ? $link : $link . " (<a href='" . $this->getUrl('genus',
                ['genus' => $row->genus]) . "'>" . _('genus overview') . '</a>)';
    }

    private function printSpeciesList($res)
    {
        $res->data_seek(0);

        $text = '<ul>';
        while ($row = $res->fetch_object()) {
            if (!$this->hideBranches() || $row->recordings > 0) {
                $class = '';
                if ($row->extinct) {
                    $class = "class='extinct'";
                }
                $text .= "
                    <li $class>" . $this->makeSpeciesLink($row) . $this->printSubspeciesList($row->species_nr) . '</li>';
            }
        }
        $text .= '</ul>';

        return $text;
    }

    private function makeSpeciesLink($row): string
    {
        $cname = $row->localName;
        if (!$cname) {
            $cname = $row->eng_name;
        }
        $extinct = '';
        if ($row->extinct) {
            $extinct = ' ' . HtmlUtil::extinctSymbol();
        }
        $href = $this->getUrl('species', ['genus' => $row->genus, 'species' => $row->species]);
        if ($cname) {
            $link = "<a href='$href'><span class='common-name'>$cname</span></a> &middot; <span class='sci-name'>$row->genus $row->species</span>";
        } else {
            $link = "<a href='$href'><span class='sci-name'>$row->genus $row->species</span></a> ";
        }
        return $link . "$extinct &middot; <span class='authority'>$row->authority</span> <span class='recording-count'>$row->recordings</span>";
    }

    private function printSubspeciesList($species_nr)
    {
        $res = $this->query("SELECT S.ssp, S.author, extinct, COUNT(B.ssp) as nrecs FROM taxonomy_ssp S LEFT JOIN birdsounds B USING(species_nr, ssp) WHERE species_nr='$species_nr' GROUP BY S.ssp");
        if (!$res) {
            Library::logger()->logWarn(Library::mysqli()->error);
        }
        if ($res->num_rows == 0) {
            return '';
        }

        $html = '<ul>';
        while ($row = $res->fetch_object()) {
            if (!$this->hideBranches() || $row->nrecs > 0) {
                $class = 'ssp';
                $extinct = '';
                if ($row->extinct == 1) {
                    $extinct = ' ' . HtmlUtil::extinctSymbol();
                    $class .= ' extinct';
                }
                $html .= "<li class='$class'><strong>$row->ssp</strong>$extinct &middot; <span class='authority'>$row->author</span> <span class='recording-count'>$row->nrecs</span></li>";
            }
        }
        $html .= '</ul>';

        return $html;
    }

    private function printGenusList($res)
    {
        $res->data_seek(0);

        $text = "<ul>";
        while ($row = $res->fetch_object()) {
            if (!$this->hideBranches() || $row->recordings > 0) {
                $class = '';
                $extinct = '';
                if ($row->nextant == 0) {
                    $extinct = ' ' . HtmlUtil::extinctSymbol();
                    $class = "class='extinct'";
                }
                $text .= "<li $class>" . $this->getGenusLink($row) . $extinct . " <span class='recording-count'>$row->recordings</span></li>";
            }
        }
        $text .= '</ul>';

        return $text;
    }

    private function printFamilyList($res)
    {
        $res->data_seek(0);

        $text = "<ul>";
        while ($row = $res->fetch_object()) {
            if (!$this->hideBranches() || $row->recordings > 0) {
                $class = '';
                if ($row->nextant == 0) {
                    $class = "class='extinct'";
                }
                $text .= "
                    <li $class>" . $this->getFamilyLink($row) . " <span class='recording-count'>$row->recordings</span></li>";
            }
        }
        $text .= '</ul>';

        return $text;
    }

    private function printOrderList($res)
    {
        $res->data_seek(0);
        $i = 0;

        $text = $this->getBackLink() . $this->hideBranchesInput();
        while ($row = $res->fetch_object()) {
            // Ignore the 'mystery' order, which is blank
            if (!empty($row->order)) {
                if ($i == 0) {
                    $text .= "
                        <ul class='tree'>
                            <li>" . $this->getGroupLink($row) . "</li>
                        <ul>";
                    $i++;
                }
                if (!$this->hideBranches() || $row->recordings > 0) {
                    $class = '';
                    if ($row->nextant == 0) {
                        $class = "class='extinct'";
                    }
                    $text .= "<li $class>" . $this->getOrderLink($row);
                    if (isset($row->recordings)) {
                        $text .= " <span class='recording-count'>$row->recordings</span>";
                    }
                    $text .= '</li>';
                }
            }
        }
        $text .= '</ul>
            </ul>';

        return $text;
    }

    private function printGroupList($res)
    {
        $res->data_seek(0);

        $text = "<ul class='tree'>";
        while ($row = $res->fetch_object()) {
            // Ignore the 'mystery' order, which is blank
            if (!empty($row->group_name)) {
                $class = '';
                if ($row->nextant == 0) {
                    $class = "class='extinct'";
                }
                $text .= "<li $class>" . $this->getGroupLink($row);
                if (isset($row->recordings)) {
                    $text .= " <span class='recording-count'>$row->recordings</span>";
                }
                $text .= '</li>';
            }
        }
        $text .= '</ul>';

        return $text;
    }

    private function setGroupName($row)
    {
        return "<span class='common-name'>" . strtoupper($this->groupName($row->group_id)) . "</span>";
    }
}
