<?php

namespace xc\Controllers;

use xc\Library;

class Redirect extends Controller
{
    public function moved($route)
    {
        $url = $this->getUrl($route, $this->request->query->all());
        return $this->movedPermanently($url);
    }
}
