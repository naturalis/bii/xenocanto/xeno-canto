<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Library;
use xc\Marker;
use xc\Species;

class LocationMap extends Controller
{
    private $location;
    private $fgSpecies = [];
    private $bgSpecies = [];

    public function handleRequest()
    {
        $this->location = $this->sanitize($this->request->query->get('loc'));

        if (!$this->location) {
            return $this->notFound();
        }

        $lat = floatval($this->request->query->get('lat'));
        $long = floatval($this->request->query->get('long'));
        $hasCoordinates = $lat && $long;
        $jsLoc = json_encode($this->location);
        $icon = Marker::$icons[0];
        $apiUrl = 'https://maps.googleapis.com/maps/api/js?language=en&amp;key=' . GOOGLE_MAPS_API_KEY;

        $script = <<<EOT
            <script type="text/javascript" src="$apiUrl"></script>
            <script type="text/javascript">
            jQuery(document).ready(function() {
            var pos = new google.maps.LatLng($lat, $long)
            var mapOptions = {
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                zoom: 8,
                center: pos
            };
            var gmap = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var markerOptions = {
                position: pos,
                map: gmap,
                title: $jsLoc,
                icon: "$icon",
                animation: google.maps.Animation.DROP
            }
            var marker = new google.maps.Marker(markerOptions);
            });
            </script>
            EOT;

        $output = "<h1>$this->location</h1>
            <div id='flex-container'>
            <section id='map-column' class='column'>";

        if ($hasCoordinates) {
            $output .= "<div id='map-canvas'></div>
            <p>The marker shows the location of <b>$this->location</b>, at ($lat, $long)</p>
            <p>Zoom in and out with the buttons. Click on the map and drag it around.</p>";
        } else {
            $output .= '<p>No coordinates were given for this location</p>';
        }

        $output .= "</section>
            <section class='column'> 
            <div class='group-select'>" .
                HtmlUtil::groupSelect($this->groupId(), $this->groups(), $this->request) .
            "</div>
            <div class='species-list'>";

        $searchExtension = $this->groupId() > 0 ? ' grp:"' . $this->groupName($this->groupId()) . '"' : '';

        if (!empty($this->setForegroundSpecies())) {
            $searchUrl = $this->getUrl('browse', ['query' => "loc:\"=$this->location\"" . $searchExtension]);
            $output .= "
                <p>Species <a href='$searchUrl'>recorded</a> at <em>$this->location</em>:</p>
                <ul class='simple'>";

            $currentGroupId = 0;
            foreach ($this->fgSpecies as $sp) {
                if ($this->groupId() == 0 && $currentGroupId != $sp['groupId']) {
                    $output .= "<li><h2>" . ucfirst($this->groupName($sp['groupId'])) . '</h2></li>';
                    $currentGroupId = $sp['groupId'];
                }
                $species = new Species($sp['genus'], $sp['species'], $sp['commonName'], $sp['nr']);
                $url = $this->getUrl('browse', ['query' => "sp:{$sp['nr']} loc:\"=$this->location\""]);
                $output .= '<li>' . $species->htmlDisplayName(true, $url) . '</li>';
            }
            $output .= '</ul>';
        }

        if (!empty($this->setBackgroundSpecies())) {
            $output .= '<p><br>And also the following species only in the background:</p>
                <ul class="simple">
                ';
            foreach ($this->bgSpecies as $sp) {
                $species = new Species($sp['genus'], $sp['species'], $sp['commonName'], $sp['nr']);
                $url = $this->getUrl(
                    'browse',
                    ['query' => 'also:"=' . $sp['genus'] . ' ' . $sp['species'] . '" loc:"=' . $this->location . '"']
                );
                $output .= '<li>' . $species->htmlDisplayName(true, $url) . '</li>';
            }
            $output .= '</ul>';
        }

        if ($hasCoordinates) {
            $query_near = 'box:' . ($lat - 1 / 2) . ',' . ($long - 1 / 2) . ',' . ($lat + 1 / 2) . ',' . ($long + 1 / 2);
            $output .= "<p><br>
            <a href='" . $this->getUrl('browse', ['query' => $query_near . $searchExtension]) . "'>Search</a> for recordings made near to this location.</p>";
            $output .= $script;
        }
        $output .= '
            </section>
            </div>
            </div>';

        return $this->template->render($output, ['title' => $this->location, 'bodyId' => 'location-map']);
    }

    private function setForegroundSpecies()
    {
        $localName = $this->localNameLanguage();
        $sql = "
            SELECT t1.genus, t1.species, t1.eng_name, t2.$localName as common_name, t1.species_nr, t1.discussed, t1.group_id 
            FROM birdsounds AS t1
            LEFT JOIN taxonomy_multilingual AS t2 ON t1.species_nr = t2.species_nr
            WHERE t1.location = '" . Library::escape($this->location) . "'";
        if ($this->groupId() > 0) {
            $sql .= ' AND t1.group_id = ' . $this->groupId();
        }
        $sql .= '
            GROUP BY t1.species_nr 
            ORDER BY t1.group_id, t1.order_nr';
        $res = $this->query($sql);
        while ($row = $res->fetch_object()) {
            $this->fgSpecies[$row->species_nr] = [
                'nr' => $row->species_nr,
                'genus' => $row->genus,
                'species' => $row->species,
                'commonName' => $row->common_name ?: $row->eng_name,
                'groupId' => $row->group_id
            ];
        }
        return $this->fgSpecies;
    }

    private function setBackgroundSpecies()
    {
        $localName = $this->localNameLanguage();
        $sql = "
            SELECT t1.species_nr, t2.genus, t2.species, t2.eng_name
            FROM birdsounds_background AS t1
            LEFT JOIN birdsounds AS t2 ON t1.snd_nr = t2.snd_nr
            WHERE t2.location = '" . Library::escape($this->location) . "'";
        if ($this->groupId() > 0) {
            $sql .= ' AND t2.group_id = ' . $this->groupId();
        }
        $sql .= '
            GROUP BY t2.species_nr 
            ORDER BY t2.group_id, t2.order_nr';
        $res = $this->query($sql);
        while ($row = $res->fetch_object()) {
            if (!array_key_exists($row->species_nr, $this->fgSpecies)) {
                // Restructured query as it was way too slow with taxonomy_multilingual joined as well
                // Only fetch name if language is not English
                $commonName = $row->eng_name;
                if ($localName != 'english') {
                    $species = Species::load($row->species_nr);
                    $commonName = $species->commonName();
                }
                $this->bgSpecies[$row->species_nr] = [
                    'nr' => $row->species_nr,
                    'genus' => $row->genus,
                    'species' => $row->species,
                    'commonName' => $commonName,
                ];
            }
        }
        return $this->bgSpecies;
    }
}
