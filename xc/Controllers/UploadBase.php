<?php

namespace xc\Controllers;

use xc\AudioUtil;
use xc\ForumPost;
use xc\Library;
use xc\RecordingData;
use xc\SoundRecognitionUtil;
use xc\Species;
use xc\ThreadType;
use xc\UploadStep;
use xc\XCRedis;

class UploadBase extends LoggedInController
{
    // presumably accepts a POSTed array
    public static function finishUpload($formData, $userId)
    {
        $d = self::prepareSubmittedValuesForUpload($formData);
        $redis = new XCRedis();

        if ($d->discussionStatus != ThreadType::MYSTERY) {
            // check to see if this user can upload uncomfirmed recordings for this
            // country. If the user has more than one sigma deviation from the
            // expected accuracy for this country (currently 99%), then all of their
            // recordings will be set to ID_UNCOMFIRMED and require confirmation by
            // another user/admin
            // Mystery recordings and unconfirmed recordings don't count toward the
            // total for this calculation.
            $sql = '
                SELECT 
                    SUM(IF(discussed!=' . ThreadType::MYSTERY . ',1,0)) as N, 
                    SUM(IF(discussed=' . ThreadType::ID_QUESTIONED . ' OR discussed=' . ThreadType::ID_UNCONFIRMED . ", 1, 0)) AS NQ
                FROM birdsounds 
                WHERE dir='$userId' AND country='{$d->country}'";
            $res = Library::query($sql);
            $row = $res->fetch_object();
            if ($row) {
                $n = $row->N;
                $acceptableErrors = Library::acceptableErrors($n);
                if ($row->NQ > $acceptableErrors) {
                    $d->discussionStatus = ThreadType::ID_UNCONFIRMED;
                }
            }
        }

        $sql_insert = "
            INSERT INTO birdsounds (
                 `genus`, `species`, `ssp`, `eng_name`, `family`, 
                `length_snd`, `volume_snd`, `speed_snd`, `pitch_snd`, `number_notes_snd`, `variable_snd`,
                `songtype`, `song_classification`, `quality`, `recordist`, 
                `olddate`, `date`, `time`,
                `country`, `location`, `longitude`, `latitude`, `elevation`,
                `remarks`, 
                `back_extra`,
                `path`, `species_nr`, `order_nr`, `dir`,
                `neotropics`, `africa`, `asia`, `europe`, `australia`,
                `datetime`, `modified`,
                `discussed`,
                `license`,
                `snd_nr`, 
                `group_id`,
                `temperature`,
                `collection_specimen`, `collection_date`,
                `observed`, 
                `playback`,
                `automatic`,
                `device`, `microphone`, `form_data`               
            ) VALUES (
                '$d->genus', '$d->species',  '$d->ssp', '$d->commonName', '$d->family',
                -1, -1, -1, -1, -1, -1,
                '$d->soundTypeExtra', 0, $d->quality, '$d->recordist',
                '$d->recordingDate', '$d->recordingDate', '$d->time',
                '$d->country', '$d->location', $d->lng, $d->lat, '$d->elevation',
                '$d->remarks',
                '$d->bgExtra',
                '$d->filename', '$d->speciesNr', '$d->speciesOrderNr', '$userId',
                '$d->americas', '$d->africa', '$d->asia', '$d->europe', '$d->australia',
                '$d->uploadDate', '$d->uploadDate',
                '$d->discussionStatus',
                '$d->license',
                null,
                $d->groupId,
                $d->temperature,  
                '$d->specimen', $d->collectionDate,   
                $d->animalSeen, 
                $d->playbackUsed, 
                $d->automatic, 
               '$d->device', '$d->microphone', '" . self::setHistoryData($d) . "'
            );";

        $stagingFile = $formData->filename;

        if (!$stagingFile || !is_file($stagingFile)) {
            Library::notifyError(_('Unable to publish file (1)'));
            Library::logger()->logError("Missing filename'\n\tform data: " . var_export($formData, true
                ) . "\n\tprepared data: " . var_export($d, true
                )
            );
            return false;
        } elseif (Library::query($sql_insert)) {
            $res = Library::query('select LAST_INSERT_ID() as soundnr');
            if (!$res) {
                return false;
            }
            $row = $res->fetch_object();
            $xcid = $row->soundnr;

            // Abort if background species or sound properties can't be set
            if (!self::updateBackgroundSpecies($xcid, $d->bgSpecies) || !self::updateSoundProperties($xcid,
                    $d->soundProperties
                )) {
                Library::query("DELETE FROM birdsounds WHERE snd_nr = $xcid");
                return false;
            }

            $uploadDir = app()->soundsUploadDir($userId);
            if (!(is_dir($uploadDir) || mkdir($uploadDir, 0755, true))) {
                Library::notifyError(_('Unable to publish file (2)'));
                Library::logger()->logError("Unable to create upload directory '$uploadDir'");
                return false;
            } else {
                // prepending the XC number allows us to identify the file
                // more easily after downloading, and also guarantees that
                // the filename is unique and doesn't overwrite an existing
                // file with the same filename from the same user.  Don't
                // use $d->filename here because that variable is
                // sql-escaped
                // Ruud 09-08-22: just get rid of those weird file names and only accept
                // A-Z and 0-9 as valid characters.
                $file = basename($stagingFile);
                $fn = str_replace(['\\', '/', ':', '*', '?', '"', "'", '<', '>', '|', ' ', '#'], '-',
                    pathinfo($file, PATHINFO_FILENAME));
                $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $xcfname = "XC$xcid-$fn.$ext";
                $publishedFile = "$uploadDir/$xcfname";

                if (!copy($stagingFile, $publishedFile)) {
                    Library::notifyError(_('Unable to publish file (3)'));
                    Library::logger(
                    )->logError("Unable to rename '$stagingFile' to '$publishedFile'\n\tform data: " . var_export($formData
                        ) . "\n\tprepared data: " . var_export($d)
                    );
                    if ($xcid) {
                        Library::query("DELETE FROM birdsounds WHERE snd_nr = $xcid");
                    }

                    return false;
                }

                $xcfname = Library::escape($xcfname);
                // fix the file path in the DB now that we've named it
                Library::query("UPDATE birdsounds SET path = '$xcfname' WHERE snd_nr = $xcid");

                if ($d->quality != 0) {
                    Library::rateRecording($xcid, $d->quality, $userId);
                }

                if (!AudioUtil::updateAudioProperties($xcid)) {
                    Library::logger()->logError('Could not update audio properties for ' . $xcfname);
                }

                // Tags are only written for mp3 files
                AudioUtil::writeId3($xcid);

                // Sono generation has its own logging case of errors
                AudioUtil::generateSonos($xcid);

                // Creation of full length sono is combined with generation of mp3 derivatives
                if (!AudioUtil::scheduleFullLengthSono($xcid)) {
                    Library::logger(
                    )->logError('Could not schedule full length sonogram creation and mp3 derivatives for ' . $xcfname
                    );
                }

                // Schedule sound recognition for birds from Europe (condition set in prepareFormData)
                if ($d->runSoundRecognition && !SoundRecognitionUtil::schedule($xcid)) {
                    Library::logger()->logError('Could not schedule sound recognition for ' . $xcfname);
                }

                if ($d->isMystery) {
                    $subject = "ID Unknown from $d->country (XC$xcid)";
                    ForumPost::create($d->discussionStatus, $subject, $formData->remarks, $xcid);
                    $redis->clearMysteryCache();
                }
                $redis->clearRecordingCache();
            }
        } else {
            Library::notifyError('Database write error');
            Library::logger()->logError("Database write error for sql statement '$sql_insert': " . Library::mysqli(
                )->error
            );
            return false;
        }

        return $xcid;
    }

    public static function prepareSubmittedValuesForUpload($formData)
    {
        $d = new RecordingData();

        // get species info from database
        $species_nr = $formData->speciesNr;
        $sp = Species::load($species_nr, false);

        $d->uploadDate = date('Y-m-d h:i:s');
        $d->recordingDate = str_replace('?', '0', $formData->recordingDate);

        $d->discussionStatus = ThreadType::NONE;
        if ($sp->isMystery()) {
            $d->discussionStatus = ThreadType::MYSTERY;
        }

        // figure out which 'branch' this recording applies to
        $d->americas = 0;
        $d->africa = 0;
        $d->asia = 0;
        $d->europe = 0;
        $d->australia = 0;

        $l = $formData->lat;
        if (empty($l) || !is_numeric($l)) {
            $d->lat = 'null';
        } else {
            $d->lat = floatval($l);
        }
        $l = $formData->lng;
        if (empty($l) || !is_numeric($l)) {
            $d->lng = 'null';
        } else {
            $d->lng = floatval($l);
        }
        $d->country = Library::strip($formData->country);
        $res = Library::query("SELECT branch FROM world_country_list WHERE country = '$d->country'");
        $row = $res->fetch_object();
        if ($row) {
            $branch = $row->branch;
            // special case for carribean island territories
            if ($branch == 'europe' && $d->lat > 0 && $d->lng > -100.0 && $d->lng < -50.0) {
                $branch = 'america';
            }
            // special case for asian island territories
            if ($branch == 'europe' && $d->lat < 0 && ($d->lng < -110.0 || $d->lng > 120.0)) {
                $branch = 'australia';
            }
            // special case for atlantic islands (tristan da cunha, etc)
            if ($branch == 'europe' && $d->lat < -30 && ($d->lng < 15.0 || $d->lng > -25.0)) {
                $branch = 'world';
            }
            // special case for falkland islands, etc
            if ($branch == 'europe' && $d->lat < -50 && ($d->lng < -50.0 || $d->lng > -65.0)) {
                $branch = 'america';
            }
            // special case for african island territories (e.g. reunion island)
            if ($branch == 'europe' && $d->lat < 0 && $d->lng > 35.0 && $d->lng < 70.0) {
                $branch = 'africa';
            }
            // special case for european Russia. Arbitrarily use 60 longitude
            if ($d->country == 'Russian Federation' && $d->lng < 60.0) {
                $branch = 'europe';
            }

            if ($branch == 'asia') {
                $d->asia = 'Y';
            }
            if ($branch == 'africa') {
                $d->africa = 'Y';
            }
            if ($branch == 'america') {
                $d->americas = 'Y';
            }
            if ($branch == 'europe') {
                $d->europe = 'Y';
            }
            if ($branch == 'australia') {
                $d->australia = 'Y';
            }

            // special-case
            if ($d->country == 'Papua New Guinea') {
                $d->australia = 'Y';
                $d->asia = 'Y';
            }
        }

        $d->remarks = Library::escape($formData->remarks);
        // These two are not stored in remarks anymore
        $d->animalSeen = $formData->animalSeen == 'yes' ? 1 : ($formData->animalSeen == 'no' ? 0 : -1);
        $d->playbackUsed = $formData->playbackUsed == 'yes' ? 1 : ($formData->playbackUsed == 'no' ? 0 : -1);

        $stagingFile = $formData->filename;
        $filename = basename($stagingFile);

        // make sure all user-entered data is sanitized
        $d->genus = Library::escape($sp->genus());
        $d->species = Library::escape($sp->species());
        $d->isMystery = $sp->isMystery();
        $d->family = Library::escape($sp->family());
        $d->speciesOrderNr = $sp->order_nr();
        $d->commonName = Library::strip($sp->commonName());
        $d->ssp = Library::strip($formData->ssp);
        $d->soundTypeExtra = Library::strip($formData->soundTypeExtra);
        $d->quality = intval($formData->quality);
        $d->recordist = Library::strip($formData->recordist);
        $d->time = Library::strip($formData->time);
        $d->location = Library::strip($formData->location);
        $d->elevation = Library::strip($formData->elevation);

        $bgData = self::populateBackgroundSpecies($formData->bgSpecies);
        $d->bgSpecies = $bgData['species'];
        $d->bgExtra = Library::escape($bgData['extra']);

        $d->filename = Library::escape($filename);
        $d->speciesNr = Library::strip($species_nr);
        $d->license = Library::strip($formData->license);
        $d->id = intval($formData->id);
        $d->groupId = intval($formData->groupId);

        // Temperature hard-coded for grasshoppers atm
        if ($formData->temperature != '' && in_array($d->groupId, UploadStep::$groupsUsingTemperature)) {
            $d->temperature = floatval(str_replace(',', '.', $formData->temperature));
        } else {
            $d->temperature = 'null';
        }
        $d->device = Library::strip($formData->device);
        $d->microphone = Library::strip($formData->microphone);
        $d->specimen = Library::strip($formData->specimen);
        $d->automatic = $formData->automatic == 'yes' ? 1 : ($formData->automatic == 'no' ? 0 : -1);

        // Either a valid date between quotes or null
        // Test if recording method is studio recording, otherwise clear (XC-670)
        $collectionDate = (string)str_replace('?', '0', $formData->collectionDate);
        $d->collectionDate = ($collectionDate != '' && in_array(UploadStep::inStudioInputId(),
                $formData->soundProperties[$d->groupId]
            )) ? "'$collectionDate'" : 'null';

        $d->soundProperties = $formData->soundProperties[$d->groupId] ?? [];
        $d->runSoundRecognition = $d->europe == 'Y' && $d->groupId == 1;

        return $d;
    }

    public static function populateBackgroundSpecies($bgText)
    {
        $backgroundSpecies = $bgNrs = $bgExtra = [];

        if (!empty($bgText)) {
            $names = array_map('trim', explode(',', $bgText));
            foreach ($names as $name) {
                $spnr = Species::getSpeciesNumberForString($name);
                if (empty($spnr)) {
                    $bgExtra[] = $name;
                } else {
                    $bgNrs[] = $spnr;
                }
            }

            foreach ($bgNrs as $spNr) {
                $bg = [];
                $s = Species::load($spNr, false);
                if (!$s) {
                    continue;
                }
                $bg['species_nr'] = $spNr;
                $bg['scientific'] = $s->scientificName();
                $bg['english'] = $s->commonName();
                $bg['family'] = $s->family();

                $backgroundSpecies[] = (object)$bg;
            }
        }

        return [
            'species' => $backgroundSpecies,
            'extra' => implode(', ', $bgExtra)
        ];
    }

    protected static function setHistoryData($d)
    {
        // Remove upload date, otherwise even updates without changes are logged
        unset($d->uploadDate);
        return Library::escape(json_encode($d));
    }

    protected static function updateBackgroundSpecies($sndNr, $backgroundSpecies)
    {
        $sndNr = intval($sndNr);
        if (!$sndNr) {
            return false;
        }

        Library::query("DELETE FROM birdsounds_background WHERE snd_nr = $sndNr");
        foreach ($backgroundSpecies as $sp) {
            $scientific = Library::escape($sp->scientific);
            $english = Library::escape($sp->english);
            $family = Library::escape($sp->family);

            $insert = "
                INSERT INTO `birdsounds_background` 
                (`snd_nr`, `species_nr`, `scientific`, `english`, `family`)
                VALUES 
                ($sndNr, '$sp->species_nr', '$scientific', '$english', '$family')";
            if (!Library::query($insert)) {
                Library::notifyError('Database write error background species');
                Library::logger()->logError("Database write error for sql statement '$insert': " . Library::mysqli(
                    )->error
                );
                return false;
            }
        }

        (new XCRedis())->clearHourlyStatsCache();
        return true;
    }

    protected static function updateSoundProperties($sndNr, $soundProperties)
    {
        $sndNr = intval($sndNr);
        if (!$sndNr) {
            return false;
        }

        Library::query("DELETE FROM birdsounds_properties WHERE snd_nr = $sndNr");
        foreach ($soundProperties as $propertyId) {
            $res = Library::query('SELECT category_id, property FROM sound_properties WHERE id = ' . $propertyId);
            $row = $res->fetch_object();
            $property = Library::escape($row->property);

            $insert = "
                INSERT INTO `birdsounds_properties` 
                (`snd_nr`, `property_id`, `category_id`, `property`)
                VALUES 
                ($sndNr, $propertyId, $row->category_id, '$property')";
            if (!Library::query($insert)) {
                Library::notifyError('Database write error sound properties');
                Library::logger()->logError("Database write error for sql statement '$insert': " . Library::mysqli(
                    )->error
                );
                return false;
            }
        }

        return true;
    }
}
