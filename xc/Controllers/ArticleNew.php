<?php

namespace xc\Controllers;

use xc\ArticleUtil;
use xc\HtmlUtil;
use xc\Library;
use xc\User;
use xc\XCRedis;

class ArticleNew extends LoggedInController
{
    public function handleRequest()
    {
        $text = $this->request->request->get('blog');
        $sanitizedText = HtmlUtil::sanitizeHtml($text, HtmlUtil::htmlSanitizerConfigDefault());
        $author = $this->escape($this->request->request->get('author'));
        $title = $this->escape($this->request->request->get('title'));
        $dir = User::current()->userId();
        $public = intval($this->request->request->get('public'));
        $submitted = intval($this->request->request->get('submitted')) == 1;

        if (!$this->userCanPost()) {
            Library::logger()->logWarn('User ' . User::current()->userName() . " attempted to create a new article without meeting requirements: [{$this->request->request->get('title')}]");
            $this->notifyError($this->postingRequirementsMessage());
            return $this->seeOther($this->getUrl('features'));
        }

        if ($submitted && (empty($title) || empty($author) || empty($sanitizedText))) {
            $this->notifyError(_('All fields are required.'));
        } elseif ($submitted) {
            $this->query("
                INSERT INTO blogs 
                (`title`, `blog`, `blog_as_html`, `author`, `region`, `date_last_update`, `dir`, `public`, `published`)
                VALUES 
                ('$title', '$text', '', '$author', 'world', NOW(), '$dir', $public, NOW()) ");
            $res = $this->query('SELECT LAST_INSERT_ID() AS maxnr FROM blogs');
            $row = $res->fetch_object();
            $blogNr = $row->maxnr;
            if (!$blogNr) {
                $this->notifyError(_('Unable to create article'));
                return $this->seeOther($this->getUrl('features'));
            }

            $generatedText = ArticleUtil::generateBlog($this->request->request->get('blog'));
            $escaped = $this->escape($generatedText);
            $this->query("UPDATE blogs SET blog_as_html = '$escaped' WHERE blognr = $blogNr");

            (new XCRedis())->clearArticleCache();
            return $this->seeOther($this->getUrl('feature-edit', ['blognr' => $blogNr]));
        }

        $output = '
            <h1>' . _('Create a new Article') . "</h1>
            <section class='edit-feature'>
            <section class='tip'>
            <p>
            " . sprintf(_("Read the <a href='%s'>tutorial</a> for an overview of all the possibilities."),
                $this->getUrl('feature-view', ['blognr' => 5])) . '</p>
                </section>';
        $output .= ArticleUtil::featureEditForm();
        $output .= '</section>';

        return $this->template->render($output, ['title' => _('New Article'),]);
    }

    protected function userCanPost()
    {
        return (User::current()->isVerified() && (User::current()->age() > $this->requiredAge()));
    }

    protected function requiredAge()
    {
        return 24;
    }

    protected function postingRequirementsMessage()
    {
        return sprintf(ngettext('In order to reduce spam, new users must verify their email address and wait at least %s hour before writing an article.',
                'In order to reduce spam, new users must verify their email address and wait at least %s hours before writing an article.',
                $this->requiredAge()),
                $this->requiredAge()) . ' ' . _('We apologize for the inconvenience.') . ' ' . sprintf(_("To re-send a verification code, please visit <a href='%s'>your account page</a>."),
                $this->getUrl('mypage'));
    }
}
