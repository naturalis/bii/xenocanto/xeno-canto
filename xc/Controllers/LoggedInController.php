<?php

namespace xc\Controllers;

use xc\Library;
use xc\User;

class LoggedInController extends Controller
{
    public function authCheck()
    {
        if (User::current()) {
            return null;
        }

        $title  = _('Login Required');
        $prompt = "
            <h1>$title</h1>
            <p>" .
                  _('Oops! You have to be logged in to use this feature.') . '
            </p>
            <p>' .
                  sprintf(
                      _(
                          "If you are registered you can <a href='%s'>log in here</a>. Or else
                <a href='%s'>please register here</a>; it only takes a minute."
                      ),
                      $this->getUrl('login'),
                      $this->getUrl('register')
                  ) . '
                </p>';

        return $this->template->render($prompt, ['title' => $title]);
    }
}
