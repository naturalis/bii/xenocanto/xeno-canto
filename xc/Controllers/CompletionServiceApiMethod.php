<?php

namespace xc\Controllers;

abstract class CompletionServiceApiMethod extends ApiMethod
{

    public function get()
    {
        $suggestions = [];
        $data = [];

        $required_params = array_merge(['query'], $this->additionalRequiredParameters());
        foreach ($required_params as $param) {
            if (!array_key_exists($param, $this->request->query->all())) {
                return $this->missingParameter($param);
            }
        }

        $term = $this->request->query->get('query');

        $this->query($term, $suggestions, $data);

        $payload = [
            'query' => $term,
            'suggestions' => $suggestions,
            'data' => $data,
        ];

        return $this->respond($payload);
    }

    protected function additionalRequiredParameters()
    {
        return [];
    }

    abstract protected function query($term, &$suggestions, &$data);
}
