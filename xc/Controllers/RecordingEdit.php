<?php

namespace xc\Controllers;

use xc\AudioUtil;
use xc\DeletedRecording;
use xc\Library;
use xc\Recording;
use xc\SonoSize;
use xc\SonoType;
use xc\SoundRecognitionUtil;
use xc\Species;
use xc\ThreadType;
use xc\User;
use xc\XCMail;
use xc\XCRedis;

class RecordingEdit extends LoggedInController
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public function delete($xcid)
    {
        $xcid = $this->escape($xcid);
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $user = User::current();
        if (!($user->isAdmin() || $user->isUploader($rec))) {
            return $this->unauthorized();
        }

        $reason = $this->escape($this->request->request->get('delete-reason'));

        $deleter = User::current()->userId();
        Library::logger()->logWarn("Deleting recording XC$xcid from database:\n{$rec->debugString()}");
        $path = $rec->filePath();
        $sql = "
            INSERT INTO birdsounds_trash (
                genus,
                species,
                ssp,
                eng_name,
                family,
                length_snd,
                volume_snd,
                speed_snd,
                pitch_snd,
                number_notes_snd,
                variable_snd,
                songtype,
                song_classification,
                quality,
                recordist,
                olddate,
                `date`,
                `time`,
                country,
                location,
                longitude,
                latitude,
                elevation,
                remarks,
                back_extra,
                path,
                species_nr,
                order_nr,
                dir,
                neotropics,
                africa,
                asia,
                europe,
                australia,
                `datetime`,
                discussed,
                license,
                snd_nr,
                group_id,
                temperature,
                collection_specimen,
                collection_date,
                observed,
                playback,
                automatic, 
                device,
                microphone,
                form_data,
                delete_date,
                delete_user,
                delete_reason
            )
            (SELECT
                genus,
                species,
                ssp,
                eng_name,
                family,
                length_snd,
                volume_snd,
                speed_snd,
                pitch_snd,
                number_notes_snd,
                variable_snd,
                songtype,
                song_classification,
                quality,
                recordist,
                olddate,
                `date`,
                `time`,
                country,
                location,
                longitude,
                latitude,
                elevation,
                remarks,
                back_extra,
                path,
                species_nr,
                order_nr,
                dir,
                neotropics,
                africa,
                asia,
                europe,
                australia,
                `datetime`,
                discussed,
                license,
                snd_nr,
                group_id,
                temperature,
                collection_specimen,
                collection_date,
                observed,
                playback,
                automatic, 
                device,
                microphone,
                form_data,
                NOW(),
                '$deleter',
                '$reason'
            FROM birdsounds 
            WHERE snd_nr = $xcid
        )";

        if ($this->query($sql)) {
            $sql = "DELETE from birdsounds where snd_nr=$xcid";
            $this->query($sql);
            $sql = "DELETE from audio_info where snd_nr=$xcid";
            $this->query($sql);
            if (!empty($path) && is_file($path)) {
                $fname = basename($path);
                $destdir = app()->soundsTrashDir();
                if (!is_dir($destdir)) {
                    mkdir($destdir, 0755, true);
                }

                $dest = "$destdir/{$rec->recordistID()}_$fname";
                $res = rename($path, $dest);
                if (!$res) {
                    Library::logger()->logError("Unable to rename file '$path' to '$dest'");
                }
            }

            foreach ([SonoSize::SMALL, SonoSize::MEDIUM, SonoSize::LARGE, SonoSize::FULL] as $size) {
                foreach ([SonoType::SPECTROGRAM, SonoType::OSCILLOGRAM] as $type) {
                    $res = @unlink($rec->sonoPath($size, $type));
                    if (!$res) {
                        Library::logger()->logWarn("Unable to delete sono/oscillo for XC$xcid");
                    }
                }
            }
            $this->notifySuccess(sprintf(_('Deleted recording XC%d'), $xcid));
            $this->redis->clearRecordingCache();
        } else {
            $this->notifyError('Unable to delete recording.  Please report this error to ' . CONTACT_EMAIL . '.');
        }

        $referer = $this->request->request->get('redirect', User::current()->getProfileURL());
        return $this->seeOther($referer);
    }

    public function confirmDelete($xcid)
    {
        $xcid = $this->escape($xcid);
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $user = User::current();
        if (!($user->isAdmin() || $user->isUploader($rec))) {
            (new XCMail(WEBMASTER_EMAIL, 'Unauthorized delete',
                "Unauthorized user attempting to delete recording XC$xcid from database:\n{$rec->debugString()}"))->send();
            return $this->unauthorized();
        }
        $title = _('Delete a recording');
        $output = "<h1>$title</h1>
            <p class='important'>" . _('<b>NOTE</b>: although it is possible to delete your recordings from the collection, we <i>strongly</i> discourage it. Please only delete a recording if it is of very poor quality, has errors, or has copyright issues. It is important that the collection is stable so that people can reference recordings without them disappearing. In some cases we may restore a deleted recording to the collection under the terms of its original license if we feel that it is valuable (the Creative Commons licenses under which the recordings are shared permit this).') . '
            </p>
            <p>' . sprintf(_('Are you sure you want to delete recording %s from the collection?'),
                "<a href='{$rec->URL()}' target='_blank'>XC$xcid</a>") . "
                </p>
                <table class='key-value'>
                <tr>
                <td>" . _('Common name') . "</td>
                <td>{$rec->commonName()}</td>
                </tr>
                <tr>
                <td>" . _('Scientific name') . "</td>
                <td><span class='sci-name'>{$rec->scientificName()}</span></td>
                </tr>
                <tr>
                <td>" . _('Sound Type') . "</td>
                <td>{$rec->soundType()}</td>
                </tr>
                <tr>
                <td>" . _('Date') . "</td>
                <td>{$rec->date()}</td>
                </tr>
                <tr>
                <td>" . _('Location') . "</td>
                <td>{$rec->location()}, {$rec->country()}</td>
                </td>
                </tr>
                </table>
                <form method='post'>
                <p>" . _('We would appreciate it if you would specify a reason for deleting this recording') . "</p>
                <p>
                <textarea name='delete-reason'></textarea>
                </p>
                <input type='hidden' name='redirect' value='{$this->request->headers->get('Referer')}'>
                <input type='submit' name='delete' value='" . htmlspecialchars(_('Delete'), ENT_QUOTES) . "'></td>
                </form>
                ";

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'delete-recording']);
    }

    public function restore($xcid)
    {
        if (!(User::current()->isAdmin())) {
            return $this->unauthorized();
        }

        $xcid = $this->escape($xcid);
        $rec = DeletedRecording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        // First check if sound is still present in the trash; if so move file back
        $dest = $rec->filePath();
        $fname = basename($dest);
        $sourceDir = app()->soundsTrashDir();
        $source = "$sourceDir/{$rec->recordistID()}_$fname";

        // Looks like older files may not have the recordist id prefix; test without if file cannot be found
        if (!file_exists($source)) {
            $source = "$sourceDir/$fname";
        }

        if (file_exists($source)) {
            // Move file back to use dir
            $res = @rename($source, $rec->filePath());
            if ($res) {
                // Restore audio info for the file (has been deleted from audio_info table)
                AudioUtil::updateAudioProperties($xcid);
                // Create sonos
                AudioUtil::generateSonos($xcid);
                AudioUtil::generateFullLengthSonogram($xcid);
                // Move data back to birdsounds and delete entry from trash table
                $sql = "
                INSERT INTO birdsounds (
                    genus,
                    species,
                    ssp,
                    eng_name,
                    family,
                    length_snd,
                    volume_snd,
                    speed_snd,
                    pitch_snd,
                    number_notes_snd,
                    variable_snd,
                    songtype,
                    song_classification,
                    quality,
                    recordist,
                    olddate,
                    `date`,
                    `time`,
                    country,
                    location,
                    longitude,
                    latitude,
                    elevation,
                    remarks,
                    back_extra,
                    path,
                    species_nr,
                    order_nr,
                    dir,
                    neotropics,
                    africa,
                    asia,
                    europe,
                    australia,
                    `datetime`,
                    discussed,
                    license,
                    snd_nr,
                    group_id,
                    temperature,
                    collection_specimen,
                    collection_date,
                    observed,
                    playback,
                    automatic, 
                    device,
                    microphone,
                    form_data,
                    modified
                )
                (SELECT
                    genus,
                    species,
                    ssp,
                    eng_name,
                    family,
                    length_snd,
                    volume_snd,
                    speed_snd,
                    pitch_snd,
                    number_notes_snd,
                    variable_snd,
                    songtype,
                    song_classification,
                    quality,
                    recordist,
                    olddate,
                    `date`,
                    `time`,
                    country,
                    location,
                    longitude,
                    latitude,
                    elevation,
                    remarks,
                    back_extra,
                    path,
                    species_nr,
                    order_nr,
                    dir,
                    neotropics,
                    africa,
                    asia,
                    europe,
                    australia,
                    `datetime`,
                    discussed,
                    license,
                    snd_nr,
                    group_id,
                    temperature,
                    collection_specimen,
                    collection_date,
                    observed,
                    playback,
                    automatic, 
                    device,
                    microphone,
                    form_data,
                    NOW()
                FROM birdsounds_trash 
                WHERE snd_nr = $xcid
                )";
                $this->query($sql);
                $sql = "DELETE from birdsounds_trash where snd_nr = $xcid";
                $this->query($sql);
                $this->redis->clearRecordingCache();
            } else {
                $this->notifyError('Unable to move sound file to user directory, check permissions');
                Library::logger()->logWarn("Unable to rename file '$source' to '$dest'");
            }
        } else {
            $this->notifyError('Unable to locate sound file to restore');
            Library::logger()->logError("Unable to locate $source to restore");
        }

        return $this->seeOther($this->getUrl('recording', ['xcid' => $xcid]));
    }

    public function confirm($xcid)
    {
        $xcid = $this->escape($xcid);
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $user = User::current();
        if ($user->canConfirm()) {
            $res = $this->query('UPDATE birdsounds SET discussed=' . ThreadType::NONE . " WHERE snd_nr=$xcid");
            if (!$res) {
                $this->notifyError(_('Internal Database Error'));
            } else {
                $this->notifySuccess(sprintf(_('Thank you for confirming recording %s'), "XC$xcid"));
            }

            $this->query("
                INSERT INTO sounds_confirm 
                (`snd_nr`, `userid`, `datetime`, `species_nr`)
                VALUES
                ($xcid, '" . $user->userId() . "', NOW(), '" . $rec->speciesNumber() . "')");
        } else {
            $this->notifyWarning('Unable to confirm');
        }

        return $this->seeOther($this->getUrl('recording', ['xcid' => $xcid]));
    }

    public function confirmSpeciesChange()
    {
        $xcid = htmlentities($this->request->request->get('xcid'));
        $rec = Recording::load($xcid);
        $user = User::current();

        if ($user && !($user->isAdmin() || $user->isUploader($rec))) {
            return $this->unauthorized();
        }

        $from = htmlentities($this->request->request->get('from'));
        $to = htmlentities($this->request->request->get('to'));
        $type = htmlentities($this->request->request->get('type'));

        $toSpecies = Species::load($to);
        $fromSpecies = Species::load($from);

        if (!$rec || !$toSpecies || !$fromSpecies || !in_array($type, ['fore', 'back'])) {
            $this->notFound();
        }

        if ($type == 'fore') {
            $text = sprintf(_('Are you sure you want to change the foreground species from %s to %s?'),
                $fromSpecies->htmlDisplayName(false), $toSpecies->htmlDisplayName(false));
        } else {
            $text = sprintf(_('Are you sure you want to add %s as a background species?'),
                $toSpecies->htmlDisplayName(false));
        }

        $output = "<h1>" . ($type == 'fore' ? _('Change Foreground Species') : _('Add Background Species')) . "</h1>
            <p>$text</p>
            <p>
                <form action='" . $this->getUrl('recording-species-change') . "' method='post'>
                <input type='hidden' name='xcid' value='$xcid'>
                <input type='hidden' name='to' value='$to'>
                <input type='hidden' name='from' value='$from'>
                <input type='hidden' name='type' value='$type'>
            </p>
            <p style='margin-top: 20px;'>
                <input type='submit' name='change' value='" . ($type == 'fore' ? _('Change') : _('Add')) . " '/>
                <input type='submit' name='cancel' value='" . _('Cancel') . " '/>
            </p>
            </form>";

        return $this->template->render($output, ['title' => 'Change Species']);
    }

    public function speciesChange()
    {
        $xcid = $this->escape($this->request->request->get('xcid'));
        $rec = Recording::load($xcid);
        $user = User::current();

        if ($user && !($user->isAdmin() || $user->isUploader($rec))) {
            return $this->unauthorized();
        }

        $from = $this->escape($this->request->request->get('from'));
        $to = $this->escape($this->request->request->get('to'));
        $type = $this->escape($this->request->request->get('type'));

        // Names to database must be in English!
        $toSpecies = Species::load($to, false);
        $fromSpecies = Species::load($from, false);
        // ... but success message in local language
        $toSpeciesMessage = Species::load($to);

        if (!$rec || !$toSpecies || !$fromSpecies || !in_array($type, ['fore', 'back'])) {
            $this->notFound();
        }

        // User confirmed change
        if (!is_null($this->request->request->get('change'))) {
            if ($type == 'fore') {
                // Verify that the data has not been changed already (form resubmitted) by checking "from species"
                if ($rec->speciesNumber() != $fromSpecies->speciesNumber()) {
                    $this->notifyError(_('Foreground species has already been updated.'));
                } else {
                    $sql = "
                        UPDATE birdsounds 
                        SET species_nr = '" . $this->escape($toSpecies->speciesNumber()) . "', 
                        genus = '" . $this->escape($toSpecies->genus()) . "',
                        species = '" . $this->escape($toSpecies->species()) . "',
                        ssp = '',
                        eng_name = '" . $this->escape($toSpecies->commonName()) . "',
                        family = '" . $this->escape($toSpecies->family()) . "',
                        discussed = " . ThreadType::MYSTERY_RESOLVED . "
                        WHERE snd_nr = $xcid";
                    $this->query($sql);

                    // Also update the status of the forum thread...
                    Library::resolveForumMystery($xcid);

                    // Log the successful identification
                    SoundRecognitionUtil::logAcceptedIdentification($rec, $toSpecies, 'fore');
                    $this->notifySuccess(sprintf(_('Species was successfully updated to %s.'),
                        $toSpeciesMessage->htmlDisplayName(false)));
                }
            } else {
                $inBackground = false;
                foreach ($rec->backgroundSpecies() as $back) {
                    if ($back->speciesNumber() == $toSpecies->speciesNumber()) {
                        $inBackground = true;
                        break;
                    }
                }

                if ($inBackground) {
                    $this->notifyError(_('Background species has already been added.'));
                } else {
                    $sql = "
                        INSERT IGNORE INTO birdsounds_background (`snd_nr`, `species_nr`, `scientific`, `english`, `family`)
                        VALUES ($xcid, '" . $toSpecies->speciesNumber() . "', 
                        '" . $this->escape($toSpecies->scientificName()) . "',  '" . $this->escape($toSpecies->commonName()) . "',
                        '" . $this->escape($toSpecies->family()) . "')";
                    $this->query($sql);

                    // Log the successful identification
                    SoundRecognitionUtil::logAcceptedIdentification($rec, $toSpecies, 'back');
                    $this->notifySuccess(sprintf(_('Background species %s was successfully added.'),
                        $toSpeciesMessage->htmlDisplayName(false)));
                }
            }
        }

        return $this->seeOther($this->getUrl('recording', ['xcid' => $xcid]));
    }
}
