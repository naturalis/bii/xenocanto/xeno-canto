<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Library;
use xc\Query;
use xc\QueryParser;
use xc\ResultsTable;
use xc\Species;
use xc\User;
use xc\XCRedis;

class Browse extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public function handlePost()
    {
        $species_nr = $this->request->request->get('species_nr');
        if ($species_nr) {
            $sp = Species::load($species_nr);
            if ($sp) {
                return $this->seeOther($sp->profileURL());
            }
        }

        $query = $this->request->request->get('query');
        $params = [];
        if ($query) {
            $params['query'] = $query;
        }

        return $this->seeOther($this->getUrl('browse', $params));
    }

    public function handleRequest()
    {
        $pagenumber = $this->pageNumber();
        $query = $this->request->query->get('query');
        $num_per_page = $this->defaultNumPerPage();
        $view = $this->request->query->get('view', $this->defaultResultsView());

        $this->redis->setKeyByRequest($this->redisKey("recording-browse-$num_per_page-$view"), $this->request);

        if (!empty($query)) {
            $title = sprintf(_("Results for '%s'"), $this->sanitize($query)) . ' :: ' . sprintf(
                    _('page %d'),
                    $pagenumber
                );
        } else {
            $title = _('Browse Recordings') . ' :: ' . sprintf(_('page %d'), $pagenumber);
        }

        $output = $this->redis->get();

        if (!$output) {
            $output = '<h1>' . _('Recordings') . '</h1>';
            $q = new Query($query);

            $invalid = $q->invalidTaggedQueries();

            if (!empty($invalid)) {
                if (in_array(reset($invalid), QueryParser::$tagNames)) {
                    $output .= HtmlUtil::printErrorMessages(_('Trouble parsing search tags'), [
                        sprintf(
                            _(
                                "Cannot succesfully parse the search term %s. When searching specifically for an operator (%s, %s, %s), please add this as the last tag to your search string, %s."
                            ),
                            '<strong><tt>' . $this->sanitize($query) . '</tt></strong>',
                            "'='",
                            "'&lt;'",
                            "'&gt;'",
                            "e.g. <strong><tt>len:\"&lt;5\" rmk:=</tt></strong>"
                        )
                    ]);
                } else {
                    $output .= HtmlUtil::printErrorMessages(_('Invalid search term'), [
                        sprintf(
                            _(
                                "Your search term %s is invalid. When using operators (%s, %s, %s), the operator plus search term should be escaped between double quotes, e.g. %s or %s."
                            ),
                            '<strong><tt>' . $this->sanitize($query) . '</tt></strong>',
                            "'='",
                            "'&lt;'",
                            "'&gt;'",
                            '<strong><tt>cnt:"=Niger"</tt></strong>',
                            '<strong><tt>q:">B"</tt></strong>'
                        )
                    ]);
                }
            } else {
                //$q->useLocalNameSearch();
                $q->setOrder(
                    $this->escape($this->request->query->get('order')),
                    $this->escape($this->request->query->get('dir'))
                );
                $number_of_hits = $q->numRecordings();
                $num_species = $q->numSpecies();

                if (!$this->showLocations($query)) {
                    $output .= "
                <p class='important'>" . _(
                            'Locations are always hidden when searching for background species, as these species may be restricted'
                        ) . ". <a href='" . $this->getUrl('FAQ') . "#restricted'>" . _('Explain this.') . '</a></p>';
                }

                if ($number_of_hits) {
                    $no_pages = ceil($number_of_hits / $num_per_page);
                    $start_no_shown = ($pagenumber - 1) * $num_per_page + 1;

                    // now actually find the results we want to show
                    $result = $q->execute($pagenumber);
                    if (!$result) {
                        Library::logger()->logError("ERROR: cannot execute query: " . Library::mysqli()->error);
                    }

                    $end_no_shown = $start_no_shown + ($result->num_rows - 1);
                    $output .= ResultsTable::resultsSummary($number_of_hits, $num_species, $query);
                    $output .= "
                <ul class=\"results-actions\">";

                    if (!empty($query) && User::current()) {
                        $output .= "
                            <li>
                            <a href='" . $this->getUrl('export-csv', ['query' => $query]) . "'>" . _(
                                'Export all results as a CSV (spreadsheet) file'
                            ) . '</a> (' . sprintf(_('maximum %d results'), MAX_EXPORT_RECORDS) . ')
                            </li>';
                        if ($this->showLocations($query)) {
                            $output .= "
                            <li>
                            <a href='" . $this->getUrl('locations', ['query' => $query]) . "'>" . _(
                                    'Make a map of all locations of the current selection'
                                ) . '</a>
                            </li>';
                        }
                        $output .= "
                            <li>
                            <a href='" . $this->getUrl('set-add-query', ['query' => $query]) . "'>" . _(
                                'Add all results to a recording set'
                            ) . '</a>
                            </li>';
                    }

                    $links = ResultsTable::makeViewLinks($this->request, $view);
                    $output .= "
                <li>
                <div class=\"results-format\">" . _('Results format') . ": $links </div>
                </li>
                </ul>";

                    $output .= HtmlUtil::pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
                    $output .= ResultsTable::resultsTableForView(
                        $this->request, $result, $view, ['showLocations' => $this->showLocations($query)]
                    );
                    $output .= HtmlUtil::pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
                } else {
                    if ($query) {
                        $message = sprintf(
                            _("No recordings were found that match the search term '%s'."),
                            '<strong>' . $this->sanitize($query) . '</strong>'
                        );
                    } else {
                        $message = _('One or more search terms are required!');
                    }

                    $output .= HtmlUtil::printErrorMessages(_('No recordings found'), [
                        $message . '  ' . sprintf(
                            _("To improve your search please read the <a href='%s'>search tips</a> page."),
                            $this->getUrl('tips')
                        ),
                    ]);
                }
            }
            $this->redis->set($output, 3600);
        }

        return $this->template->render($output, ['title' => $title]);
    }

    private function showLocations($query)
    {
        return true;

        // XC-94 reverted
        //        $pq = new QueryParser($query);
        //        $r = $pq->parse();
        //        foreach ($r->taggedQueries as $tag) {
        //            if (strpos($tag->tagName, 'also') === 0) {
        //                return false;
        //            }
        //        }
        //        return true;
    }

}
