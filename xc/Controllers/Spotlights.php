<?php

namespace xc\Controllers;

use xc\Library;
use xc\Spotlight;

class Spotlights extends Controller
{
    public function listSpotlights()
    {
        $pageTitle = _('Spotlight Recordings');
        $output    = "<div id='primary-content' class='column'>
            <h1>$pageTitle</h1>
            <p>" .
                     _(
                         'Some of the most interesting, rare, or noteworthy recordings in the collection are shown below.'
                     ) . '
            </p>
            ';

        $spotlights = Spotlight::all();
        foreach ($spotlights as $spotlight) {
            $output .= $spotlight->toHtml();
        }

        $output .= "
            </div>
            <div class='column'>
            <section class='tip'>
            <h1>" . _('Nominate a Recording') . '</h1>
            <p>' . sprintf(
            _(
                'Do you know of a recording from the collection that deserves to be spotlighted?  Nominate a recording by %s'
            ),
            Library::obfuscateEmail(CONTACT_EMAIL . '?subject=Spotlight%20Nomination', _('emailing us'))
        ) . '</p>
            </section>
            </div>';

        return $this->template->render($output, ['title' => $pageTitle]);
    }

    public function showSpotlight($id)
    {
        $spotlight = Spotlight::load($id);
        $ogdata    = "
            <meta property='og:title' content='" . $this->sanitize(
            $spotlight->title()
        ) . "' ></meta>
            <meta property='og:type' content='website' ></meta>
            <meta property='og:url' content='" . $this->sanitize(
            $this->getUrl('spotlight', ['id' => $id], true)
        ) . "' ></meta>
            <meta property='og:image' content='" . $this->sanitize(
            $spotlight->image(true)
        ) . "' ></meta>";

        $output = "<div id='primary-content' class='column'>
            <p><a href='" . $this->getUrl('spotlights') . "'>&laquo; " . _(
            'All Spotlights'
        ) . "</a></p>
            <h1>{$spotlight->title()}</h1>";

        $output .= $spotlight->toHtml();

        $output .= '
            </div>';

        $this->template->addHeaderData($ogdata);
        return $this->template->render(
            $output,
            ['title' => $spotlight->title()]
        );
    }
}
