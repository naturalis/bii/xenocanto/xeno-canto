<?php

namespace xc\Controllers;

use Symfony\Component\HttpFoundation\Response;
use xc\License;
use xc\Query;
use xc\Recording;

class ExportCsv extends Controller
{
    private $outstream;

    private $response;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->outstream = fopen('php://temp', 'w+');

        $this->response = new Response();
        $this->response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $this->response->headers->set('Pragma', 'no-cache');
        $this->response->headers->set('Expires', '0');
    }

    public function __destruct()
    {
        fclose($this->outstream);
    }

    public function handleRequest(): Response
    {
        $query = strip_tags($this->request->query->get('query'));
        if (!$query) {
            return $this->badRequest();
        }

        $q = new Query($query);
        $res = $q->execute(Query::NO_PAGING, MAX_EXPORT_RECORDS);

        $header = [
            'Common name',
            'Scientific name',
            'Subspecies',
            'Recordist',
            'Date',
            'Time',
            'Location',
            'Country',
            'Latitude',
            'Longitude',
            'Elevation',
            'Songtype',
            'Remarks',
            'Back_latin',
            'Catalogue number',
            'License',
        ];

        fputcsv($this->outstream, $header);

        while ($row = $res->fetch_object()) {
            $rec = new Recording($row);
            $bgspecies = $rec->backgroundSpecies();
            $bg = [];
            foreach ($bgspecies as $sp) {
                $bg[] = $sp->scientificName();
            }
            $license = License::lookupById($rec->license());

            fputcsv($this->outstream, [
                $rec->commonName(),
                $rec->scientificName(),
                $rec->subspecies(),
                $rec->recordist(),
                $rec->date(),
                $rec->time(),
                (!$rec->species()->restricted() ? $rec->location() : '-'),
                $rec->country(),
                (!$rec->species()->restricted() ? $rec->latitude() : '-'),
                (!$rec->species()->restricted() ? $rec->longitude() : '-'),
                $rec->elevation(),
                $rec->soundType(),
                str_replace("\r\n", "\n", $rec->remarks()),
                implode(',', $bg),
                $rec->xcid(),
                $license->url,
            ]);
        }

        return $this->createResponse('xenocanto.csv');
    }

    private function createResponse($fileName): Response
    {
        rewind($this->outstream);
        $this->response->headers->set('Content-Disposition', "attachment; filename=$fileName");
        $this->response->setContent(stream_get_contents($this->outstream));
        return $this->response;
    }

    public function handleAiSuggestions(): Response
    {
        $sql = "
             SELECT 
                t1.snd_nr, t2.dir as uploader, 
                IFNULL(
                    (   
                        SELECT CONCAT(t4.genus, ' ', t4.species, ' ', t4.ssp) 
                        FROM birdsounds_history AS t4 
                        WHERE t1.snd_nr = t4.snd_nr 
                        ORDER BY t4.historyid LIMIT 1
                    ), 
                    TRIM(CONCAT(t2.genus, ' ', t2. species, ' ', t2.ssp))
                ) AS uploaded_as, 
                GROUP_CONCAT(DISTINCT t1.species_nr SEPARATOR ', ') AS suggested_species_nr, 
                GROUP_CONCAT(DISTINCT CONCAT(t1.species, ' -> ', t1.probability) SEPARATOR ', ') AS suggested_species, 
                GROUP_CONCAT(t4.species_nr SEPARATOR ', ') AS background_species_nrs,
                GROUP_CONCAT(t4.scientific SEPARATOR ', ') AS background_species,
                GROUP_CONCAT(t3.species SEPARATOR ', ') AS accepted_species_nrs,
                GROUP_CONCAT(t3.species SEPARATOR ', ') AS accepted_species,
                GROUP_CONCAT(t3.type SEPARATOR ', ') AS accepted_types
            FROM ai_results_summaries AS t1
            LEFT JOIN birdsounds as t2 ON t1.snd_nr = t2.snd_nr
            LEFT JOIN ai_accepted_log AS t3 ON t1.json_id = t3.json_id AND t1.species_nr = t3.species_nr
            LEFT JOIN birdsounds_background AS t4 ON t2.snd_nr = t4.snd_nr
            WHERE t2.snd_nr IS NOT NULL
            GROUP BY t1.snd_nr
            ORDER BY t1.snd_nr_nr";

        $this->processSql($sql);
        return $this->createResponse('ai-suggestions.csv');
    }

    private function processSql($sql): void
    {
        $res = $this->query($sql);
        $i = 0;

        while ($row = $res->fetch_array(MYSQLI_ASSOC)) {
            if ($i == 0) {
                fputcsv($this->outstream, array_keys($row));
            }
            fputcsv($this->outstream, array_values($row));
            $i++;
        }
    }

    public function handleAiNoSuggestions(): Response
    {
        $sql = "
            SELECT 
                t1.snd_nr, 
                t3.dir as uploader, 
                TRIM(CONCAT(t3.genus, ' ', t3. species, ' ', t3.ssp)) AS uploaded_as, 
                NULL AS suggested_species_nr, 
                NULL AS suggested_species, 
                NULL AS background_species_nrs,
                NULL AS background_species,
                NULL AS accepted_species_nrs,
                NULL AS accepted_species,
                NULL AS accepted_types
            FROM ai_results_json AS t1
            LEFT JOIN ai_results_summaries AS t2 ON t1.snd_nr = t2.snd_nr
            LEFT JOIN birdsounds as t3 on t1.snd_nr = t3.snd_nr
            WHERE t2.snd_nr IS NULL AND t3.snd_nr IS NOT NULL";

        $this->processSql($sql);
        return $this->createResponse('ai-no-suggestions.csv');
    }
}
