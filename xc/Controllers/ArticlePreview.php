<?php

namespace xc\Controllers;

class ArticlePreview extends Controller
{
    public function handleRequest($blogNr)
    {
        $res = $this->query("SELECT * FROM blogs WHERE blognr = $blogNr");
        $row = $res->fetch_object();

        if (!$row) {
            return $this->notFound();
        }

        $title = _('Article Preview');
        $output = "
            <h1>$title</h1>
            <p>
            <a href='" . $this->getUrl('feature-view', ['blognr' => $blogNr]) . "'>" . _('Return to this article') . "</a>
            </p>

            <section class='column forum-thread'>
            <article class='preview'>
            <h2>" . $this->sanitize($row->title) . "</h2>
            ";
        $output .= $this->sanitize($row->blog);
        $output .= '
            </article>
            </section>';

        return $this->template->render($output, ['title' => $title]);
    }
}
