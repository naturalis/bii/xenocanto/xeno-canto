<?php

namespace xc\Controllers;

use xc\ArticleUtil;
use xc\HtmlUtil;
use xc\XCRedis;

class ArticleEdit extends LoggedInController
{
    public function handleDeletePost($blogNr)
    {
        if (empty($blogNr)) {
            return $this->notFound();
        }

        // deletion has already been confirmed
        $blogNr = $this->escape($blogNr);
        $this->query("DELETE from blogs where blognr = $blogNr");
        $this->query("DELETE from feature_discussions where blognr = $blogNr");
        $this->notifySuccess(_('Article was successfully deleted'));

        (new XCRedis())->clearArticleCache();

        return $this->seeOther($this->getUrl('features'));
    }

    public function handlePost($blogNr)
    {
        // the 'edit' form was posted
        $blogNr = intval($blogNr);
        $text = $this->escape($this->request->request->get('blog'));
        $sanitizedText = HtmlUtil::sanitizeHtml($text, HtmlUtil::htmlSanitizerConfigDefault());
        $author = $this->escape($this->request->request->get('author'));
        $title = $this->escape($this->request->request->get('title'));
        $public = intval($this->request->request->get('public'));

        if (empty($title) || empty($author) || empty($sanitizedText)) {
            $this->notifyError(_('All fields are required.'));
            return $this->seeOther($this->getUrl('feature-edit', ['blognr' => $blogNr]));
        }

        $sql = "
            UPDATE blogs 
            SET 
                title = '$title', blog = '$text', author = '$author', date_last_update = NOW(), 
                published = IF(!public and $public, NOW(), published), public = $public
            WHERE blognr = $blogNr";
        $this->query($sql);
        $generatedText = ArticleUtil::generateBlog($this->request->request->get('blog'));
        $escaped = $this->escape($generatedText);
        if ($this->query("UPDATE blogs SET blog_as_html = '$escaped' WHERE blognr = $blogNr")) {
            $this->notifySuccess(_('Article was successfully saved'));
        }

        (new XCRedis())->clearArticleCache();

        return $this->seeOther($this->getUrl('feature-view', ['blognr' => $blogNr]));
    }

    public function handleDeleteRequest($blogNr)
    {
        $blogNr = $this->escape($blogNr);
        $pageTitle = _('Delete Article?');
        $res = $this->query("SELECT * FROM blogs WHERE blognr=$blogNr");
        $output = '';
        if (!$res || $res->num_rows == 0) {
            return $this->notFound();
        }

        $output .= "
            <h1>$pageTitle</h1>

            <section class='edit-feature'>
            <p class='important'>" . sprintf(_('Are you sure you want to delete article %d?  This action cannot be undone.'),
                $blogNr) . '</p>
            ' . ArticleUtil::featureDeleteForm($blogNr, 'post') . "
            <p><a href='" . $this->getUrl('feature-edit',
                ['blognr' => $blogNr]) . "'>&laquo; " . _('No, return to editing') . '</a></p>
            </section>';

        return $this->template->render($output, ['title' => $pageTitle, 'bodyId' => 'feature-edit']);
    }

    public function handleRequest($blogNr)
    {
        $blogNr = $this->escape($blogNr);
        $pageTitle = _('Edit Article');
        $res = $this->query("SELECT * FROM blogs WHERE blognr = $blogNr");
        if (!$res || $res->num_rows == 0) {
            return $this->notFound();
        }

        $output = "
            <h1>$pageTitle</h1>
            <section class='edit-feature'>";

        // submitting a GET form for deletion, will force a confirmation
        $output .= ArticleUtil::featureDeleteForm($blogNr);
        $output .= ArticleUtil::featureEditForm($blogNr);
        $output .= '</section>';

        return $this->template->render($output, ['title' => $pageTitle, 'bodyId' => 'feature-edit']);
    }
}
