<?php

namespace xc\Controllers;

use xc\User;

class ArticlesIndex extends Controller
{
    public function handleRequest()
    {
        $pageTitle = _('Articles');
        // redirect old links to new urls
        $action = $this->request->query->get('action');
        $blogNr = $this->request->query->get('blognr');
        if ($action && $blogNr) {
            $url = '';
            switch ($action) {
                case 'view':
                    $url = $this->getUrl('feature-view', ['blognr' => $blogNr]);
                    break;
                case 'edit':
                    $url = $this->getUrl('feature-edit', ['blognr' => $blogNr]);
                    break;
                case 'preview':
                    $url = $this->getUrl('feature-preview', ['blognr' => $blogNr]);
                    break;
            }

            if ($url) {
                return $this->movedPermanently($url);
            }
        }

        $res = $this->query("SELECT * FROM blogs WHERE blognr != '5' ORDER BY published DESC");

        $openNewBlog = _('Would you like to write an article on animal song?') . ' ';
        if (User::current()) {
            $openNewBlog .= sprintf(_("Then <a href='%s'>create one</a>!"), $this->getUrl('feature-new'));
        } else {
            $openNewBlog .= sprintf(_("Then first <a href='%s'>register</a> as a member,
                or, if you already are, <a href='%s'>log in</a>."), $this->getUrl('register'), $this->getUrl('login'));
        }

        $blogs = "
            <h1>$pageTitle</h1>
            <div class='column'>
            <p>" . _('Read articles on animal song, written by xeno-canto members.
            These articles may be on any topic, such as identification issues, taxonomic
            questions, discussing patterns of biogeographic variation, and so on.') . "</p>

            <p>$openNewBlog</p>

            <h2>" . _('Current Articles') . "</h2>
            <ul class='simple'>
            ";
        while ($row = $res->fetch_object()) {
            $user = User::current();
            if ($user && ($row->dir == $user->userId() || $user->isAdmin())) {
                $editLink = "(<a href='" . $this->getUrl('feature-edit',
                        ['blognr' => $row->blognr]) . "'>" . _('edit') . '</a>)';
            } else {
                $editLink = '';
            }
            $title = $this->sanitize($row->title);
            if (empty($title)) {
                $title = _('[Untitled]');
            }

            $author = $this->sanitize($row->author);

            $draftClass = '';
            if (!$row->public) {
                $draftClass = 'draft';
            }

            if ($row->public || ($user && ($user->isAdmin() || ($row->dir == User::current()->userId())))) {
                $blogs .= "
                    <li class='$draftClass'>" . sprintf(_('%s by %s'),
                        "<a href='" . $this->getUrl('feature-view', ['blognr' => $row->blognr]) . "'><b>$title</b></a>",
                        "$author") . " $editLink</li>";
            }
        }
        $blogs .= '
            </ul>
            </div>';

        $blogs .= "
            <div class='column'>
            <section class='tip'>
            <p>" . sprintf(_("Read the <a href='%s'>tutorial</a> for an overview of all the possibilities."),
                $this->getUrl('feature-view', ['blognr' => 5])) . '</p>
            </section>
            </div>';

        return $this->template->render($blogs, ['title' => $pageTitle, 'bodyId' => 'features']);
    }

}
