<?php

namespace xc\Controllers;

use xc\ForumPost;
use xc\HtmlUtil;
use xc\ThreadType;
use xc\WorldArea;
use xc\XCRedis;

class ForumIndex extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
        $this->redis->setKeyByRequest($this->redisKey('forum-index'), $request);
    }

    public function handleRequest()
    {
        $pagenumber = $this->pageNumber();
        $title = _('Forum') . ' :: ' . sprintf(_('page %d'), $pagenumber);
        $output = $this->redis->get();

        if (!$output) {
            $num_per_page = $this->defaultNumPerPage();
            $selectors = "
                <div class='selector-menu'>" . _('Filter forum topics by world area:') . "  %s</div>
                <div class='selector-menu'>" . _('Filter forum topics by group:') . "  %s</div>
                <div class='selector-menu'>" . _('Filter forum topics by thread type:') . ' %s</div>';

            $area = WorldArea::lookup($this->request->query->get('b'));

            $areaSelector = "<ul class='area-selector'>\n";
            if (!$area) {
                $areaSelector .= "<li class='selected'>" . _('All') . "</li>\n";
            } else {
                $u = $this->getUrl('forum', $this->setQueryParameters(['b' => 'all'], ['pg']));
                $areaSelector .= "<li><a href='$u'>" . _('All') . "</a></li>\n";
            }

            foreach (WorldArea::all() as $cand) {
                if ($area && $cand == $area) {
                    $areaSelector .= "<li class='selected'>" . $cand->desc . "</li>\n";
                } else {
                    $u = $this->getUrl('forum', $this->setQueryParameters(['b' => $cand->branch], ['pg']));
                    $areaSelector .= "<li><a href='$u'>" . $cand->desc . "</a></li>\n";
                }
            }
            $areaSelector .= '</ul>';

            $gid = intval($this->request->query->get('g'));

            $groupSelector = "<ul class='area-selector'>\n";
            if (!$gid) {
                $groupSelector .= "<li class='selected'>" . _('All') . "</li>\n";
            } else {
                $u = $this->getUrl('forum', $this->setQueryParameters(['g' => 'all'], ['pg']));
                $groupSelector .= "<li><a href='$u'>" . _('All') . "</a></li>\n";
            }

            foreach ($this->groups() as $id => $group) {
                if ($gid && $gid == $id) {
                    $groupSelector .= "<li class='selected'>" . ucfirst(_($group)) . "</li>\n";
                } else {
                    $u = $this->getUrl('forum', $this->setQueryParameters(['g' => $id], ['pg']));
                    $groupSelector .= "<li><a href='$u'>" . ucfirst(_($group)) . "</a></li>\n";
                }
            }
            $groupSelector .= '</ul>';

            $threadTypes = ForumPost::getThreadDescriptions(2);
            $type = intval($this->request->query->get('t'));

            $typeSelector = "<ul class='area-selector'>\n";
            if (empty($type)) {
                $typeSelector .= "<li class='selected'>" . _('All') . "</li>\n";
            } else {
                $u = $this->getUrl('forum', $this->setQueryParameters([], ['pg', 't']));
                $typeSelector .= "<li><a href='$u'>" . _('All') . "</a></li>\n";
            }

            // Delete the first (default) label before printing
            unset($threadTypes[key($threadTypes)]);

            foreach ($threadTypes as $key => $threadType) {
                // Break menu in two as it doesn't fit a single line
                if ($key == ThreadType::ADMIN) {
                    $typeSelector .= "</ul>\n<ul class='area-selector'>\n";
                }
                if ($type && $key == $type) {
                    $typeSelector .= "<li class='selected'>" . $threadType . "</li>\n";
                } else {
                    $u = $this->getUrl('forum', $this->setQueryParameters(['t' => $key], ['pg']));
                    $typeSelector .= "<li><a href='$u'>" . $threadType . "</a></li>\n";
                }
            }
            $typeSelector .= '</ul>';

            // build a sql clause that matches the entered search query
            $qSql = 'WHERE 1 ';
            $q = $this->request->query->get('q');
            if ($q) {
                // Only use the inner join on discussion_forum_world for searching.
                // This speeds up regular browsing, as last_activity and replies
                // have been added to forum_world.
                $qSql = "
                    INNER JOIN discussion_forum_world comments USING(topic_nr) " . $qSql;

                $query = $this->escape($q);
                $id = str_ireplace('xc', '', $query);
                $idclause = '';
                if (is_numeric($id)) {
                    $idclause = " OR forum.snd_nr=" . intval($id);
                }
                $qSql .= " AND (forum.subject LIKE '%$query%'
                    OR tax.genus LIKE '%$query%'
                    OR tax.species LIKE '%$query%'
                    OR tax.eng_name LIKE '%$query%'
                    OR birdsounds.recordist LIKE '%$query%'
                    OR comments.text LIKE '%$query%'
                    OR forum.name LIKE '%$query%' $idclause)";
            }

            if ($area) {
                $qSql .= " AND forum.branch = '" . $area->branch . "'";
            }

            if ($gid) {
                $qSql .= " AND forum.group_id = $gid";
            }

            if ($type) {
                $qSql .= " AND forum.type='" . $type . "'";
            }

            // the common SQL shared between both the count and the query
            // Note that inner join on discussion_forum_world has been
            // moved to the search string!
            $sqlBody = "FROM
                forum_world forum 
                LEFT JOIN users ON forum.userid = users.dir
                LEFT JOIN birdsounds ON birdsounds.snd_nr = forum.snd_nr
                LEFT JOIN taxonomy tax ON birdsounds.species_nr = tax.species_nr
                $qSql
            ";

            // the sql used to count the total number of results
            $sqlCount = "SELECT COUNT(DISTINCT topic_nr) as hits $sqlBody";

            //=========================================================
            // output is assembled below here
            //===========================================================

            $res = $this->query($sqlCount);
            $row = $res->fetch_object();
            $number_of_hits = $row->hits;

            $no_pages = ceil($number_of_hits / $num_per_page);
            $start_no_shown = ($pagenumber - 1) * $num_per_page + 1;

            // Build the SQL that will fetch the results for this page
            $sqlforum = "
            SELECT
                forum.subject AS subject,
                forum.name AS originator,
                forum.date AS orig_date,
                forum.topic_nr AS topic_nr,
                forum.type,
                users.dir AS userId,
                forum.replies,
                forum.last_activity AS last_activity
            $sqlBody ";

            // Different grouping and sort if search was used
            $sqlforum .= $q ? 'GROUP BY forum.topic_nr ORDER BY MAX(mes_nr) DESC,topic_nr DESC ' : 'ORDER BY last_activity DESC,topic_nr DESC ';

            $sqlforum .= 'LIMIT ' . ($start_no_shown - 1) . ",$num_per_page";

            $output = "
            <form method='get'>
            <table id='forum-search'>
            <tr>
            <td>
            <input type='text' name='q' value='" . $this->sanitize($q) . "' placeholder='" . _('Search the forum...') . "' />
            </td>
            <td>
            <input type='submit' value='" . _('Search') . "'/>
            </td>
            </tr>
            </table>
            </form>
            <h1>" . _('Discussion Forum') . "</h1>
            <div id='summary'>
            <p>" . _('Questions or comments? Post them here.') . "
            <span class='prominent-button' ><a href='" . $this->getUrl('new_thread') . "'>" . _('Start a new topic') . '</a></span>
            </p>
            </div>' . sprintf($selectors, $areaSelector, $groupSelector, $typeSelector);

            if ($number_of_hits) {
                $output .= "
                <table class='results'>
                <thead>
                <tr>
                <th>" . _('nr') . '</th>
                <th>' . _('Topic') . '</th>
                <th>' . _('Type') . '</th>
                <th>' . _('Posted by') . '</th>
                <th>' . _('Date posted') . '</th>
                <th>' . _('Replies') . '</th>
                <th>' . _('Last Activity') . '</th>
                </tr>
                </thead>
                <tbody>
                ';

                $result = $this->query($sqlforum);

                while ($row = $result->fetch_object()) {
                    $dateposted = HtmlUtil::formatDate('%b %d, %Y', strtotime($row->orig_date));
                    $last_activity = HtmlUtil::formatDate('%b %d, %Y', strtotime($row->last_activity));
                    $user = HtmlUtil::ellipsize($row->originator, 30);
                    $type_key = $row->type && isset(ForumPost::$threadTypeDescriptions[$row->type]) ? $row->type : ThreadType::NONE;
                    if (!empty($row->userId)) {
                        $user = "<a href='" . $this->getUrl('recordist', ['id' => $row->userId]) . "'>$user</a>";
                    }

                    $output .= "
                        <tr>
                        <td>$row->topic_nr</td>
                        <td><a href='" . $this->getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]
                        ) . "'>" . HtmlUtil::ellipsize($row->subject, 60) . "</a></td>
                        <td><span title='" . ForumPost::$threadTypeDescriptions[$type_key] . "'>" . ForumPost::$threadTypeIcons[$type_key] . "</span></td>
                        <td>$user</td>
                        <td>$dateposted</td>";

                    $replies = '';
                    if ($row->replies >= 1) {
                        $replies = $row->replies;
                    }
                    $output .= "
                        <td>$replies</td>";

                    $output .= "
                        <td>$last_activity</td>
                        </tr>";
                }

                $output .= '</tbody></table>';
            } else {
                $message = empty($q) ? _('No forum topics were found') : sprintf(
                    _(
                        "No forum topics were found that match the search term '%s'"
                    ),
                    '<strong>' . $this->sanitize($q) . '</strong>'
                );
                $output .= HtmlUtil::printErrorMessages(_('No results found'), [$message]);
            }
            $output .= HtmlUtil::pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
            $this->redis->set($output, 3600);
        }

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'forum']);
    }

    // Pass unset as normal array
    private function setQueryParameters($replace = [], $unset = [])
    {
        $p = array_merge($this->request->query->all(), (array)$replace);
        if (!empty($unset)) {
            return array_diff_key($p, array_flip((array)$unset));
        }
        return $p;
    }

}
