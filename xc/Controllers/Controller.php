<?php

namespace xc\Controllers;

use Exception;
use xc\Library;

class Controller extends ControllerBase
{
    protected $groupId;

    protected $groups;

    public function authCheck()
    {
        return null;
    }

    public function redisKey($key, $extra = [])
    {
        // Must have initialized FrontController to set language, otherwise fail
        if (!function_exists('app')) {
            throw new Exception('Cannot determine language for Redis key, please set manually');
        }
        $lan = app()->determineBestLanguage();
        if (!is_array($extra)) {
            $extra = [$extra];
        }

        return $key . '-' . ($lan ? $lan->code : 'en') . (!empty($extra) ? '-' . implode('-', $extra) : '');
    }

    public function escape($var)
    {
        return Library::escape($var);
    }

    public function strip($var, $escape = true, $allowTags = false)
    {
        return Library::strip($var, $escape, $allowTags);
    }

    public function localNameLanguage()
    {
        return Library::localNameLanguage();
    }

    public function defaultNumPerPage()
    {
        return Library::defaultNumPerPage();
    }

    public function pageNumber()
    {
        $pagenumber = abs((int)$this->request->query->get('pg'));
        if (empty($pagenumber)) {
            $pagenumber = abs((int)$this->request->query->get('pagenumber'));
        }
        return $pagenumber ?: 1;
    }

    public function sanitize($var)
    {
        return Library::sanitize($var);
    }

    public function defaultResultsView()
    {
        return Library::defaultResultsView();
    }

    protected function getUrl($route, $args = [], $absolute = false)
    {
        return Library::getUrl($route, $args, $absolute);
    }

    protected function notifyError($text)
    {
        Library::notifyError($text);
    }

    protected function notifyWarning($text)
    {
        Library::notifyWarning($text);
    }

    protected function notifySuccess($text)
    {
        Library::notifySuccess($text);
    }

    protected function notifyInfo($text)
    {
        Library::notifyInfo($text);
    }

    protected function groupName($id = null)
    {
        if (!$id) {
            $id = $this->groupId();
        }
        return $this->groups()[$id] ?? null;
    }

    protected function groupId()
    {
        if (!$this->groupId) {
            if (!is_null($this->request->query->get('gid'))) {
                $this->groupId = (int)$this->request->query->get('gid');
                if (!array_key_exists($this->groupId, $this->groups())) {
                    $this->groupId = 0;
                }
                app()->setCookie('gid', $this->groupId);
            } elseif ($this->request->cookies->get('gid')) {
                $this->groupId = (int)$this->request->cookies->get('gid');
            }
        }

        return $this->groupId ?? 0;
    }

    protected function groups()
    {
        if (!$this->groups) {
            $res = $this->query('SELECT id, name FROM groups');
            while ($row = $res->fetch_object()) {
                $this->groups[$row->id] = $row->name;
            }
        }
        return $this->groups;
    }

    public function query($sql)
    {
        return Library::query($sql);
    }
}
