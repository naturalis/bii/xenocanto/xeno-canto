<?php

namespace xc;

use Symfony\Component\HttpFoundation\Request;
use xc\Controllers\FrontController;

class Task extends FrontController
{

    public function __construct($dataDir, $env = 'task')
    {
        parent::__construct(
            Request::create(
                '/', /* uri */
                'GET', /* method */
                [], /* parameters */
                [], /* cookies */
                [], /* files */
                [
                    'HTTP_HOST'      => 'www.xeno-canto.org',
                    'SERVER_NAME'    => 'www.xeno-canto.org',
                    'DOCUMENT_ROOT'  => $dataDir,
                    'REQUEST_METHOD' => 'GET',
                    'HTTPS'          => 'on' // Force https for environment
                ], /* server */ /* content */
            ),
            $dataDir,
            $env
        );
    }
}
