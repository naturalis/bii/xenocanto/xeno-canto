<?php

namespace xc;

use Michelf\Markdown;
use NumberFormatter;
use Symfony\Component\HtmlSanitizer\HtmlSanitizer;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerConfig;

use function PHP81_BC\strftime;

class HtmlUtil
{
    public static function formatUserText($text)
    {
        return self::formatText($text);
    }

    public static function formatText($text, $config = false): string|null
    {
        $markup = Markdown::defaultTransform($text);

        // Default config is pretty flexible, allowing a lot of tags (identical to old strip_tags option)
        if (!$config) {
            $config = self::htmlSanitizerConfigDefault();
        }
        $markup = self::sanitizeHtml($markup, $config);

        $linked = preg_replace_callback('/\bXC\s?(\d+)/i', self::formatRecordingLink(...), $markup);
        $linked = preg_replace_callback('/\bML\s?(\d+)/i', self::formatMLRecordingLink(...), $linked);
        $linked = preg_replace_callback('/\bLNS\s?(\d+)/i', self::formatMLRecordingLink(...), $linked);
        return preg_replace('/<p[^>]*>\s*<\/p>/i', '', $linked);
    }

    public static function htmlSanitizerConfigDefault(): HtmlSanitizerConfig
    {
        return self::htmlSanitizerConfigLimited()
            ->allowElement('iframe', ['src', 'width', 'height', 'scrolling', 'frameborder'])
            ->allowElement('img', ['src', 'alt'])
            ->withMaxInputLength(65500);
    }

    public static function htmlSanitizerConfigLimited(): HtmlSanitizerConfig
    {
        return (new HtmlSanitizerConfig())
            ->allowElement('a', ['href', 'target', 'title'])
            ->allowElement('b')
            ->allowElement('strong')
            ->allowElement('i')
            ->allowElement('em')
            ->allowElement('h1')
            ->allowElement('h2')
            ->allowElement('h3')
            ->allowElement('h4')
            ->allowElement('h5')
            ->allowElement('h6')
            ->allowElement('p')
            ->allowElement('pre')
            ->allowElement('code')
            ->allowElement('ul')
            ->allowElement('ol')
            ->allowElement('li')
            ->allowElement('blockquote')
            ->forceAttribute('a','rel', 'noopener noreferrer');
    }

    public static function sanitizeHtml($text, $config = false): string
    {
        if (!$config) {
            $config = self::htmlSanitizerConfigLimited();
        }
        $htmlSanitizer = new HtmlSanitizer($config);
        return trim($htmlSanitizer->sanitize((string)$text));
    }

    public static function formatUserTextLimited($text)
    {
        return self::formatText((string)$text, self::htmlSanitizerConfigLimited());
    }


    //=====================
    // Create a rating widget for a specific recording
    //=====================

    public static function ratingWidget($xcid, $quality)
    {
        $ratings = [
            1 => ['A', false],
            2 => ['B', false],
            3 => ['C', false],
            4 => ['D', false],
            5 => ['E', false],
        ];

        if (array_key_exists($quality, $ratings)) {
            $ratings[$quality][1] = true;
        }

        $output = "<div class='rating'>
        <ul>
        ";

        for ($i = 1; $i <= count($ratings); $i++) {
            $isSelected = $ratings[$i][1];
            $qName = $ratings[$i][0];
            $class = '';
            if ($isSelected) {
                $class = 'selected';
            }

            // don't need a link to rate a song for its current rating
            if (User::current() && User::current()->canRateRecording($xcid)) {
                $link = "<a href='#' onclick='return RateRecording($xcid,$i)'>$qName</a>";
            } else {
                $link = "<span>$qName</span>";
            }

            $output .= "<li id='rating-$xcid-$i' class='$class'>$link</li>";
        }
        $output .= '
                </ul>
            </div>';
        return $output;
    }

    public static function formatDate($format, $timestamp = null, $locale = null): string
    {
        if (is_null($locale)) {
            $locale = setlocale(LC_TIME, 0);
            if ($locale == 'C') {
                $locale = 'en_US';
            }
        }
        return strftime($format, $timestamp, $locale);
    }

    public static function regionSelect($name, $selected = null, $id = '', $class = '')
    {
        $options = ['world' => _('World')];
        foreach (WorldArea::all() as $area) {
            $options[$area->branch] = $area->desc;
        }
        if (!$selected) {
            $selected = 'world';
        }

        return self::selectInput($options, $name, $selected, $id, $class);
    }

    public static function selectInput($options, $name, $selected = null, $id = '', $class = '', $dataIds = [])
    {
        $html = "<select name='$name' class='$class' id='$id'>";
        foreach ($options as $val => $display) {
            if ($val == html_entity_decode($selected)) {
                $selectedAttr = 'selected';
            } else {
                $selectedAttr = '';
            }
            $dataId = '';
            if (!empty($dataIds) && isset($dataIds[$val])) {
                $dataId = "data-id='$dataIds[$val]'";
            }
            $html .= "<option $dataId value='$val' $selectedAttr>$display</option>";
        }
        $html .= '</select>';
        return $html;
    }

    public static function countrySelect($name, $selected = null, $id = '', $classes = '', $prependEquals = true)
    {
        $list = [];
        $res = Library::query('SELECT country, country_code FROM world_country_list ORDER BY country');
        if ($res) {
            while ($row = $res->fetch_object()) {
                $list[$row->country_code] = $row->country;
            }
        }

        $options = ['' => _('Please choose...')];
        // When used for searching, it's better to prepend an =, so the matches option will be used
        $values = !$prependEquals ? array_values($list) : preg_filter('/^/', '=', array_values($list));
        // yes, we use country name as the key...
        $options = array_merge($options, array_combine($values, array_values($list)));

        return self::selectInput($options, $name, $selected, $id, $classes, array_flip($list));
    }

    public static function notSpecified()
    {
        return "<span class='unspecified'>" . _('not specified') . '</span>';
    }

    public static function threadTypeSelect($type, $name, $selected = null, $id = null, $class = null): string
    {
        $level = $type == 'admin' ? 3 : 1;
        $threadTypes = ForumPost::getThreadDescriptions($level);
        // Rename none option for non-admin select
        if ($level == 1) {
            $threadTypes[ThreadType::NONE] = _('No category');
        }
        return self::selectInput($threadTypes, $name, $selected, $id, $class);
    }

    public static function pageNumberNavigationWidget($request, $numPages, $curPage): string
    {
        // for now, just assume that we need to page between the same url that we're
        // currently viewing, with the same query string, but with the 'pg'
        // parameter changing
        $numEndItems = 1;
        $numMainItems = 9;

        // don't print a page navigation widget if there's nothing to navigate
        if ($numPages < 2 || $numPages < $curPage) {
            return '';
        }

        $params = [];

        $output = "
            <nav class=\"results-pages\">
                <ul>";
        if ($curPage > 1) {
            $params['pg'] = $curPage - 1;
            $output .= "
                <li><a href=\"" . Library::addUrlParamsToCurrentPage($request, $params) . "\">
                    <img width='14' height='14' src=\"/static/img/previous.png\"/> " . _('Previous') . '</a>
                </li>';
        }

        if ($numPages < ($numMainItems + (2 * $numEndItems))) {
            $output .= self::outputPageNumbers($request, 1, $numPages, $params, $curPage);
        } else {
            $firstMainItem = max(1, min($curPage - floor(($numMainItems - 1) / 2), $numPages - $numMainItems));
            $lastMainItem = min($numPages, $firstMainItem + ($numMainItems - 1));

            $missingNumbers = $firstMainItem - 1;
            if ($missingNumbers) {
                // +1 for a potential 'gap' marker
                if ($missingNumbers <= ($numEndItems + 1)) {
                    $output .= self::outputPageNumbers($request, 1, $missingNumbers, $params, $curPage);
                } else {
                    // need gap indicator
                    $output .= self::outputPageNumbers($request, 1, $numEndItems, $params, $curPage);
                    $output .= "<li class=\"gap\"><span>...</span></li>";
                }
            }

            $output .= self::outputPageNumbers($request, $firstMainItem, $lastMainItem, $params, $curPage);

            $missingNumbers = $numPages - $lastMainItem;
            if ($missingNumbers) {
                // +1 for a potential 'gap' marker, again
                if ($missingNumbers <= ($numEndItems + 1)) {
                    $output .= self::outputPageNumbers($request, $lastMainItem + 1, $numPages, $params, $curPage);
                } else {
                    // need gap indicator
                    $output .= "<li class=\"gap\"><span>...</span></li>";
                    $output .= self::outputPageNumbers($request, $numPages - ($numEndItems - 1), $numPages, $params,
                        $curPage);
                }
            }
        }

        if ($curPage < $numPages) {
            $params['pg'] = intval($curPage) + 1;
            $output .= "
            <li><a href=\"" . Library::addUrlParamsToCurrentPage($request, $params) . "\">" . _('Next') . "
                <img width='14' height='14' src=\"/static/img/next.png\"/></a>
            </li>";
        }
        $output .= '
                </ul>
            </nav>';

        return $output;
    }

    private static function outputPageNumbers($request, $start, $end, $params, $curPage): string
    {
        if ($start > $end) {
            return '';
        }

        $output = '';
        for ($i = $start; $i <= $end; $i++) {
            $params['pg'] = intval($i);
            if ($i == $curPage) {
                $output .= "
                    <li class=\"selected\"><span>$i</span></li>";
            } else {
                // need to add one due to zero-based iterator
                $params['pg'] = intval($i);
                $output .= "
                    <li><a href=\"" . Library::addUrlParamsToCurrentPage($request, $params) . "\">$i</a></li>";
            }
        }
        return $output;
    }

    public static function groupSelect($groupId, $groups, $request = false)
    {
        $output = "
            <form method='get'>
            <select name='gid' class='unified-dropdown' onchange='this.form.submit();'>
            <option value='0'>" . _("All groups") . "</option>\n";

        foreach ($groups as $id => $name) {
            $selected = (int)$id == $groupId ? 'selected' : '';
            $output .= "<option value='$id' $selected>" . ucfirst(_($name)) . "</option>";
        }
        $output .= '</select>';
        if ($request) {
            foreach ($request->query->all() as $param => $value) {
                if ($param != 'gid') {
                    $output .= "<input type='hidden' name='" . Library::strip($param) . "' value='" . Library::sanitize($value) . "'>";
                }
            }
        }
        return $output . '</form>';
    }

    public static function printErrorMessages($headline, $messages = []): string
    {
        $output = "<div class=\"error\">
        <h2>$headline</h2>";

        if (!empty($messages)) {
            $output .= '<ul>';
            foreach ($messages as $message) {
                $output .= "<li>$message</li>";
            }
            $output .= '</ul>';
        }
        $output .= '</div>';

        return $output;
    }

    public static function formatMinutesSeconds($duration): string
    {
        $hours = 0;
        $minutes = 0;
        if ($duration > (60 * 60)) {
            $hours = floor($duration / (60 * 60));
        }
        if ($duration >= 60) {
            $remainder = (int)$duration % (60 * 60);
            $minutes = floor($remainder / 60) + ($hours * 60);
        }
        $seconds = (int)$duration % 60;
        return $minutes . ":" . sprintf('%02d', $seconds);
    }

    public static function formatDuration($duration, $iso8601 = false): string
    {
        $str = '';
        if ($iso8601) {
            $str .= 'PT';
        }

        $hours = 0;
        $minutes = 0;
        if ($duration > (60 * 60)) {
            $hours = floor($duration / (60 * 60));
        }
        if ($duration >= 60) {
            $remainder = (int)$duration % (60 * 60);
            $minutes = floor($remainder / 60);
        }

        $seconds = (int)$duration % 60;

        if ($hours) {
            if ($iso8601) {
                $str .= "{$hours}H{$minutes}M";
            } else {
                $str .= "$hours:";
                // if there are hours, make sure minutes is 2 digits
                $str .= sprintf('%02d:', $minutes);
            }
        } elseif ($iso8601) {
            $str .= "{$minutes}M";
        } else {
            $str .= "$minutes:";
        }

        if ($iso8601) {
            $str .= "{$seconds}S";
        } else {
            $str .= sprintf('%02d', $seconds);
        }

        return $str;
    }

    public static function ellipsize($string, $length)
    {
        $string = Library::sanitize($string);
        if (strlen($string) > ($length + 1)) {
            return substr($string, 0, $length) . '…';
        }

        return $string;
    }

    public static function formatNumber($n)
    {
        $locale = setlocale(LC_NUMERIC, 0);
        if ($locale == 'C') {
            $locale = 'en_uk';
        }

        $nf = new NumberFormatter($locale, NumberFormatter::PATTERN_DECIMAL);
        return $nf->format($n);
    }

    // calculate the acceptable number of errors allowed for a set sized $n

    public static function userList($url, $referer = false)
    {
        $html = "<ul>\n";
        $sql = '
            SELECT U.username, U.dir, U.email, COUNT(snd_nr) AS nrecs 
            FROM users U 
            LEFT JOIN birdsounds USING(dir) 
            GROUP BY dir 
            ORDER BY username ASC';
        $res = Library::query($sql);
        while ($row = $res->fetch_object()) {
            $href = $url . $row->dir . ($referer ? '/' . $referer : '');
            $name = empty($row->username) ? '[No user name provided]' : Library::sanitize($row->username);
            $html .= "<li><a href='$href'>$name ($row->email)</a>: $row->nrecs recordings ($row->dir)</li>\n";
        }
        $html .= "\n</ul>";

        return $html;
    }

    public static function userSelectHtml($name = 'u')
    {
        $html = "
            <select name='$name' id='u'>
            <option value='none'>Choose a user</option>";
        $res = Library::query('
            SELECT U.username, U.dir, U.email, COUNT(snd_nr) AS nrecs 
            FROM users U 
            LEFT JOIN birdsounds USING(dir) 
            GROUP BY dir 
            ORDER BY username ASC');
        while ($row = $res->fetch_object()) {
            $name = !empty($row->username) ? Library::sanitize($row->username) : '[No user name provided]';
            $html .= "<option value='$row->dir'>$name - $row->email - $row->nrecs recordings ($row->dir)</option>\n";
        }
        $html .= '</select>';

        return $html;
    }

    public static function extinctSymbol()
    {
        return "<span title='" . _('Extinct') . "'>&dagger;</span>";
    }

    public static function getCountryList()
    {
        static $countryList = null;
        if (!$countryList) {
            $countryList = [];
            $res = Library::query('SELECT country, country_code FROM world_country_list ORDER BY country');
            if ($res) {
                while ($row = $res->fetch_object()) {
                    $countryList[$row->country_code] = $row->country;
                }
            }
        }
        return $countryList;
    }

    private static function formatRecordingLink($matches)
    {
        if (count($matches) < 2) {
            return $matches[0];
        }

        $id = intval($matches[1]);
        return "<a href='" . Library::getUrl('recording', ['xcid' => $id]) . "'>$matches[0]</a>";
    }

    private static function formatMLRecordingLink($matches)
    {
        if (count($matches) < 2) {
            return $matches[0];
        }

        $id = intval($matches[1]);
        return "<a href='//macaulaylibrary.org/audio/$id'>$matches[0]</a>";
    }
}
