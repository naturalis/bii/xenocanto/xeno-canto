<?php

namespace xc;

class RecordingFormData
{

    public $species;

    public $ssp;

    public $soundTypeExtra;

    public $quality;

    public $recordist;

    public $recordingDate;

    public $time;

    public $country;

    public $location;

    public $lng;

    public $lat;

    public $elevation;

    public $remarks;

    public $bgSpecies;

    public $filename;

    public $speciesNr;

    public $license;

    public $id;

    public $device;

    public $microphone;

    public $temperature;

    public $automatic;

    public $specimen;

    public $collectionDate;

    public $soundProperties;

    public $animalSeen;

    public $playbackUsed;

    // extra form data
    public $existingLocation;

    public $recordingIdType;

    public $groupId;

    public $hideMap;

    public function __construct()
    {
        $this->lat = null;
        $this->lng = null;
    }

    public static function fromRecording($rec, $asTemplate = true)
    {
        if (!$rec) {
            return null;
        }

        $d = new RecordingFormData();

        $d->species = $rec->scientificName();
        $d->ssp = $rec->subspecies();
        $d->soundTypeExtra = $rec->soundTypeExtra();
        $d->quality = $rec->rating();
        $d->recordist = $rec->recordist();
        $d->recordingDate = $rec->date();
        $d->time = $rec->time();
        $d->country = $rec->country();
        $d->location = $rec->location();
        $d->lng = $rec->longitude();
        $d->lat = $rec->latitude();
        $d->elevation = $rec->elevation();
        $d->remarks = $rec->remarks();
        $d->bgSpecies = $rec->backgroundAsEntered();
        $d->speciesNr = $rec->species()->speciesNumber();
        $d->license = $rec->license();
        $d->existingLocation = true;

        if (!$asTemplate) {
            $d->filename = $rec->filePath();
            $d->id = $rec->xcid();
        }

        $d->groupId = $rec->groupId();
        $d->device = $rec->device();
        $d->microphone = $rec->microphone();
        $d->playbackUsed = $rec->playbackUsed();
        $d->automatic = $rec->automatic();
        $d->specimen = $rec->specimen();
        $d->temperature = $rec->temperature();
        $d->collectionDate = $rec->collectionDate();
        $d->animalSeen = $rec->animalSeen();
        $d->playbackUsed = $rec->playbackUsed();
        $d->soundProperties = $rec->soundPropertiesAsEntered();

        return $d;
    }
}
