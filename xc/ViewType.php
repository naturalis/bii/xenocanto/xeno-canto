<?php

namespace xc;

class ViewType
{

    public const DETAILED = 0;

    public const CONCISE = 1;

    public const CODES = 2;

    public const SONOGRAMS = 3;
    
}
