<?php
// php -dxdebug.start_with_request=yes tasks/sound-recognition.php
namespace xc;

require_once 'constants.php';

use CURLFile;
use xc\Controllers\ForumTopic;

class SoundRecognitionUtil
{
    private const CURL_TIMEOUT = 120;

    public static function schedule($xcid): \mysqli_result|bool
    {
        $xcid = intval($xcid);
        return Library::query("INSERT INTO ai_queue (snd_nr) VALUES ($xcid) ON DUPLICATE KEY UPDATE snd_nr = $xcid");
    }

    public static function getIdentification($xcid): bool
    {
        $rec = Recording::load($xcid, false);
        if (!$rec) {
            self::logError($xcid, "[Sound recognition] Recording XC$xcid does not exist");
            return false;
        }
        if (!self::getModelResult($rec)) {
            return false;
        }
        if (!self::postProcessResult($rec)) {
            return false;
        }
        return true;
    }

    private static function logError($xcid, $message): \mysqli_result|bool
    {
        Library::logger()->logError($message);
        $sql = "INSERT INTO ai_error_log (`snd_nr`, `message`) VALUES ($xcid, '" . Library::escape($message) . "')";
        return Library::query($sql);
    }

    private static function getModelResult($rec): bool
    {
        $message = "[Sound recognition] ";

        // Don't bother sending to model if sound file is missing
        if (!is_file($rec->filepath())) {
            $message .= "Sound file for XC" . $rec->xcid() . " cannot be located at " . $rec->filepath();
            self::logError($rec->xcid(), $message);
            return false;
        }

        Library::logger()->logInfo($message . "Sending XC" . $rec->xcid() . " to Avesecho model");
        $curlFile = new CURLFile($rec->filepath(), $rec->mimeType(), $rec->filename());
        $success = false;

        $ch = curl_init(AVESECHO_ENDPOINT);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::CURL_TIMEOUT);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['media' => $curlFile]);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $curlError = curl_error($ch);
        }
        curl_close($ch);

        if (empty($result)) {
            $message .= "Model did not return a result for XC" . $rec->xcid();
            if (isset($curlError)) {
                $message .= " (curl error: $curlError)";
            }
            self::logError($rec->xcid(), $message);
        } else {
            Library::logger()->logInfo($message . "Storing identification result XC" . $rec->xcid());
            $sql = "
                INSERT INTO ai_results_json (snd_nr, json) 
                VALUES (" . $rec->xcid() . ", '" . Library::escape($result) . "')";
            if (Library::query($sql)) {
                Library::logger()->logInfo($message . "Success! Removing XC" . $rec->xcid() . " from queue");
                // Update birdsounds to indicate sound has been used for AI (-1 -> 0)
                // If any results have been found, the table will be updated again (0 -> 1)
                Library::query("UPDATE birdsounds SET ai_result = 0 WHERE snd_nr = " . $rec->xcid());
                $success = true;
            } else {
                $message .= "Failed to store identification result of " . $rec->xcid();
                self::logError($rec->xcid(), $message);
            }
        }
        return $success;
    }

    private static function postProcessResult($rec): bool
    {
        $sql = "SELECT id, json FROM ai_results_json WHERE snd_nr = " . $rec->xcid() . " ORDER BY `created` DESC LIMIT 1";
        $res = Library::query($sql);
        $row = $res->fetch_object();
        if (empty($row)) {
            self::logError($rec->xcid(),
                "[Sound recognition] Result for XC" . $rec->xcid() . " not available in database");
            return false;
        }
        $result = json_decode($row->json);
        if (!empty($result->predictions)) {
            Library::logger()->logInfo("[Sound recognition] Post-processsing results for XC" . $rec->xcid());
            self::writeSummary($row->id, $rec, $result);
            self::writeRegions($row->id, $rec, $result);
            // Set flag for searching for AI results
            Library::query("UPDATE birdsounds SET ai_result = 1 WHERE snd_nr = " . $rec->xcid());
        } else {
            Library::logger()->logInfo("[Sound recognition] No identification(s) available for XC" . $rec->xcid());
        }
        return true;
    }

    private static function writeSummary($jsonId, $rec, $result): void
    {
        $summary = self::getSummary($result);
        if (empty($summary)) {
            return;
        }
        // Do not allow for multiple entries, so first delete any previous recognitions
        Library::query("DELETE FROM ai_results_summaries WHERE snd_nr = " . $rec->xcid());

        foreach ($summary as $speciesNr => $predication) {
            $sql = sprintf("
                INSERT INTO ai_results_summaries 
                (`snd_nr`, `json_id`, `model_tag`, `model_version`, `species`, `species_nr`, `probability`) 
                VALUES 
                (%d, %d, '%s', '%s', '%s', '%s', %f)", $rec->xcid(), $jsonId, $result->generated_by->tag,
                $result->generated_by->version, $predication['species'], $speciesNr, $predication['probability']);
            Library::query($sql);
        }
    }

    private static function getSummary($result): array
    {
        $summary = [];
        foreach ($result->predictions as $prediction) {
            if (!empty($prediction->taxa->items)) {
                foreach ($prediction->taxa->items as $taxon) {
                    $speciesNr = self::getSpeciesNumber($taxon->scientific_name);
                    if ($speciesNr && (!isset($summary[$speciesNr]) || $summary[$speciesNr]['probability'] < $taxon->probability)) {
                        $summary[$speciesNr] = [
                            'species' => $taxon->scientific_name,
                            'probability' => $taxon->probability,
                        ];
                    }
                }
            }
        }
        return $summary;
    }

    private static function getSpeciesNumber($scientificName)
    {
        list($genus, $species) = array_pad(explode(' ', $scientificName), 2, null);
        $res = Library::query("SELECT species_nr FROM taxonomy WHERE genus = '$genus' AND species = '$species'");
        $row = $res->fetch_object();
        return $row ? $row->species_nr : false;
    }

    private static function writeRegions($jsonId, $rec, $result): void
    {
        $regions = self::getRegions($result);
        if (empty($regions)) {
            return;
        }
        // Do not allow for multiple entries, so first delete any previous recognitions
        Library::query("DELETE FROM ai_results_regions WHERE snd_nr = " . $rec->xcid());

        foreach ($regions as $predication) {
            $sql = sprintf("
                INSERT INTO ai_results_regions 
                (`snd_nr`, `json_id`, `model_tag`, `model_version`, `species`, `species_nr`, `time_start`, `time_end`, `probability`) 
                VALUES (%d, %d, '%s', '%s', '%s', '%s', %d, %d, %f)", $rec->xcid(), $jsonId, $result->generated_by->tag,
                $result->generated_by->version, $predication['species'], $predication['species_nr'],
                $predication['time_start'], $predication['time_end'], $predication['probability']);
            Library::query($sql);
        }
    }

    private static function getRegions($result): array
    {
        $regions = [];
        foreach ($result->predictions as $prediction) {
            if (!empty($prediction->taxa->items)) {
                foreach ($prediction->taxa->items as $taxon) {
                    $speciesNr = self::getSpeciesNumber($taxon->scientific_name);
                    if ($speciesNr) {
                        $regionBox = self::getRegionBox($result, $prediction->region_group_id);
                        $regions[] = [
                            'species' => $taxon->scientific_name,
                            'species_nr' => $speciesNr,
                            'probability' => $taxon->probability,
                            'time_start' => $regionBox->t1,
                            'time_end' => $regionBox->t2,
                        ];
                    }
                }
            }
        }
        return $regions;
    }

    private static function getRegionBox($result, $regioGroupId)
    {
        foreach ($result->region_groups as $region) {
            if ($region->id == $regioGroupId) {
                return $region->regions[0]->box;
            }
        }
    }

    public static function removeFromQueue($xcid): bool
    {
        return Library::query("DELETE FROM ai_queue WHERE snd_nr = " . intval($xcid));;
    }

    public static function logAcceptedIdentification($rec, $species, $type)
    {
        $sql = "
            SELECT * FROM ai_results_summaries
            WHERE snd_nr = {$rec->xcid()} AND species_nr = '" . $species->speciesNumber() . "'";
        $res = Library::query($sql);
        $row = $res->fetch_object();

        $user = User::current();
        $dir = $user->userId();
        $username = $user->userName();

        $sql = "
            INSERT INTO ai_accepted_log 
            (`snd_nr`, `json_id`, `species_nr`, `species`, `dir`, `username`, `type`, `model_version`, `model_tag`)
            VALUES 
            ({$rec->xcid()}, $row->json_id, '{$species->speciesNumber()}', '{$species->genus()} {$species->species()}',
            '$dir', '$username', '$type', '$row->model_version', '$row->model_tag')";
        return Library::query($sql);
    }

    public static function getIdentifiedSections($xcid): array
    {
        $sql = "
            SELECT model_version, model_tag, species, species_nr, probability, time_start, time_end 
            FROM ai_results_regions 
            WHERE snd_nr = " . intval($xcid) . "
            ORDER BY time_start, species";
        $res = Library::query($sql);
        while ($row = $res->fetch_object()) {
            $ids[] = $row;
        }
        return $ids ?? [];
    }

    public static function postReplyToForum($xcid, $summary = false): void
    {
        // Passing the species list is optional; if not provided, get it first
        if (!is_array($summary)) {
            $summary = self::getIdentifiedSpecies($xcid);
        }
        if (!empty((array)$summary)) {
            $res = Library::query("SELECT topic_nr FROM forum_world WHERE snd_nr = $xcid AND type = " . ThreadType::MYSTERY);
            if ($res->num_rows == 1) {
                $row = $res->fetch_object();
                $text = sprintf(_("The sound recognition model Avesecho suggests checking for the following species in this recording:") . "\n%s\n" . _("The recording page %s shows more details for the recordist.") . " " . _("Please check %s for background information."),
                    self::printSummaryList($summary), "XC$xcid",
                    '<a href="https://xeno-canto.org/article/299">this article</a>');
                ForumTopic::postSystemReply($row->topic_nr, $text, 'Avesecho');
                (new XCRedis())->clearForumCache();
            }
        }
    }

    public static function getIdentifiedSpecies($xcid): array
    {
        $sql = "
            SELECT model_version, model_tag, species, species_nr, probability
            FROM ai_results_summaries 
            WHERE snd_nr = " . intval($xcid) . "
            ORDER BY probability DESC, species";;
        $res = Library::query($sql);
        while ($row = $res->fetch_object()) {
            $ids[] = $row;
        }
        return $ids ?? [];
    }

    private static function printSummaryList($summary): string
    {
        $list = "<ul>\n";
        foreach ($summary as $id) {
            $species = Species::load($id->species_nr);
            $list .= "<li>" . $species->forumName() . "</li>\n";
        }
        $list .= "</ul>\n";
        return $list;
    }

}
