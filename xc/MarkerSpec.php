<?php

namespace xc;

class MarkerSpec
{

    public $url;

    public $height;

    public $width;

    public $anchorX;

    public $anchorY;

    public $legendText;
}
