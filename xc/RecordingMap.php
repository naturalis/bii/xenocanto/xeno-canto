<?php

namespace xc;

abstract class RecordingMap extends Map
{

    protected function createMarker($db_row)
    {
        $rec = new Recording($db_row);
        if (!$rec->hasCoordinates()) {
            return null;
        }
        return $this->createMarkerForRecording($rec);
    }

    abstract protected function createMarkerForRecording($rec);
}
