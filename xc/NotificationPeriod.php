<?php

namespace xc;

class NotificationPeriod
{

    public const DAILY = 0;

    public const WEEKLY = 1;

    public const MONTHLY = 2;

    public static $periods = [
        self::DAILY   => 'daily',
        self::WEEKLY  => 'weekly',
        self::MONTHLY => 'monthly',
    ];

}
