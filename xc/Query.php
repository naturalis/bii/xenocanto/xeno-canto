<?php

/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace xc;

abstract class Condition
{
    abstract public function toSQL();

}

class SimpleCondition extends Condition
{
    private $sql;

    public function __construct($sql)
    {
        $this->sql = $sql;
    }

    public function toSQL()
    {
        return "$this->sql";
    }

}

function getSQL($x)
{
    return $x->toSQL();
}

class AggregateCondition extends Condition
{
    private $conditions;

    private $operator;

    public function __construct($conditions, $operator)
    {
        $this->conditions = $conditions;
        $this->operator = $operator;
    }

    public function addCondition($condition)
    {
        $this->conditions[] = $condition;
    }

    public function toSQL()
    {
        $strings = array_map('\xc\getSQL', $this->conditions);
        $s = implode(" $this->operator ", $strings);

        if (!empty($s)) {
            return "($s)";
        }

        return '';
    }

    public function size()
    {
        return count($this->conditions);
    }
}

class Query
{
    public const NO_PAGING = 0;

    public const OPT_INCLUDE_MYSTERIES = 1;

    private static $localNameLanguage = 'english';

    private static $defaultNumPerPage = 30;

    private $soundProperties = [];

    private $mainSQL;

    private $statsSql;

    private $numRecordings = -1;

    private $numSpecies = -1;

    private $duration = -1;

    private $pageSize;

    private $order = '';

    private $direction;

    private $queryString;

    private $basicTerms;

    private $taggedQueries;

    private $joins = [];

    private $conditions;

    private $groups = [];

    private $usesTagJoins = false;

    private $excludeRestricted;

    private $excludeMystery;

    private $onlyMystery;

    private $useLocalNameSearch;

    public function __construct($queryString)
    {
        $this->pageSize = self::$defaultNumPerPage;
        $this->direction = SortDirection::NORMAL;
        $this->queryString = $queryString;

        $this->setLocalNameSearch();
        $this->buildMainQuery();
    }

    private function setLocalNameSearch()
    {
        if (self::getLocalNameLanguage() != 'english') {
            $this->useLocalNameSearch = true;
        }
        $this->buildMainQuery();
    }

    public static function getLocalNameLanguage()
    {
        return self::$localNameLanguage;
    }

    public static function setLocalNameLanguage($lang)
    {
        if ($lang) {
            self::$localNameLanguage = $lang;
        } else {
            self::$localNameLanguage = 'english';
        }
    }

    private function buildMainQuery()
    {
        // first separate the normal search terms from the tagged words
        $parsedQuery = $this->parse($this->queryString);

        $this->basicTerms = $parsedQuery->basicTerms;
        $this->taggedQueries = $parsedQuery->taggedQueries;
        $this->conditions = new AggregateCondition([], 'AND');

        // if there are indeed tagged words
        foreach ($this->taggedQueries as $tq) {
            $this->buildConditionForTaggedQuery($tq);
            $this->buildJoinForTaggedQuery($tq);
        }

        $this->mainSQL = $this->generateSQL();
    }

    private function parse($query)
    {
        // we can't simply split on spaces, since strings within quotes can contain spaces
        $parser = new QueryParser($query);
        return $parser->parse();
    }

    private function buildConditionForTaggedQuery($tq)
    {
        $condition = null;
        $tag = $tq->tagName;
        $term = Library::escape($tq->searchTerm);

        // Background search is now part of generateSQL()!
        if ($tag == 'also') {
            return null;
        }

        if (strlen($term ?? '') == 0) {
            $condition = new SimpleCondition('0');
        } elseif ($tag == 'rec') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesCondition('recordist', $term);
            } else {
                $condition = $this->containsCondition('recordist', $term);
            }
        } elseif ($tag == 'rmk') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition =  $this->fullTextMatchesCondition('remarks', $term);
            } else {
                $condition = $this->fullTextContainsCondition('remarks', $term);
            }
        } elseif ($tag == 'loc') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesCondition('location', $term);
            } else {
                $condition = $this->fullTextContainsCondition('location', $term);
            }
        } elseif ($tag == 'cnt') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesCondition('country', $term);
            } else {
                $condition = $this->startsWithCondition('country', $term);
            }
        } elseif ($tag == 'lat') {
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $bounds = [];
                $bounds[] = $this->lessThanCondition('latitude', (floatval($term) + 1));
                $bounds[] = $this->greaterThanCondition('latitude', (floatval($term) - 1));
                $condition = new AggregateCondition($bounds, 'AND');
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('latitude', $term);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('latitude', $term);
            }
        } elseif ($tag == 'lon') {
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $bounds = [];
                $bounds[] = $this->lessThanCondition('longitude', (floatval($term) + 1));
                $bounds[] = $this->greaterThanCondition('longitude', (floatval($term) - 1));
                $condition = new AggregateCondition($bounds, 'AND');
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('longitude', $term);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('longitude', $term);
            }
        } elseif ($tag == 'box') {
            $components = explode(',', $term);
            if (count($components) == 4) {
                $bounds = [];
                $bounds[] = new SimpleCondition('latitude > ' . (floatval($components[0])));
                $bounds[] = new SimpleCondition('latitude < ' . (floatval($components[2])));
                if ($components[1] < $components[3]) {
                    // doesn't cross the 180th meridian
                    $bounds[] = new SimpleCondition('longitude > ' . (floatval($components[1])));
                    $bounds[] = new SimpleCondition('longitude < ' . (floatval($components[3])));
                } else {
                    // box does cross 180th meridian.  need to do it in two
                    // sections and 'OR' them together
                    $cx = [];
                    $cx[] = new SimpleCondition('longitude > ' . (floatval($components[1])));
                    $cx[] = new SimpleCondition('longitude <= 180');
                    $cy = [];
                    $cy[] = new SimpleCondition('longitude >= -180');
                    $cy[] = new SimpleCondition('longitude < ' . (floatval($components[3])));
                    $cz = [];
                    $cz[] = new AggregateCondition($cx, 'AND');
                    $cz[] = new AggregateCondition($cy, 'AND');
                    $bounds[] = new AggregateCondition($cz, 'OR');
                }
                $condition = new AggregateCondition($bounds, 'AND');
            } else {
                $condition = new SimpleCondition('1 = 2');
            }
        } elseif ($tag == 'nr') {
            if (strpos($term, '-')) {
                $s = explode('-', $term);
                $begin = intval($s[0]);
                $end = intval($s[1]);

                $bounds = [];
                $bounds[] = new SimpleCondition("birdsounds.snd_nr <= $end");
                $bounds[] = new SimpleCondition("birdsounds.snd_nr >= $begin");
                $condition = new AggregateCondition($bounds, 'AND');
            } elseif (strpos($term, ',')) {
                $nrs = array_unique(explode(',', str_replace(['(', ')'], '', $term)));
                $term = implode(',', $nrs);
                $condition = new SimpleCondition("birdsounds.snd_nr IN ($term)");
            } else {
                $nr = intval($term);
                $condition = $this->matchesCondition('birdsounds.snd_nr', $nr);
            }
        } elseif ($tag == 'ssp') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesCondition('ssp', $term);
            } else {
                $condition = $this->startsWithCondition('ssp', $term);
            }
        } elseif ($tag == 'sp') {
            $condition = $this->matchesCondition('birdsounds.species_nr', $term);
        } elseif ($tag == 'or') {
            $condition = $this->matchesCondition('order_nr', $term);
        } elseif ($tag == 'othertype') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesRegexpCondition('songtype', $term);
            } else {
                $condition = $this->containsCondition('songtype', $term);
            }
        } elseif (in_array($tag, ['len', 'len_gt', 'len_lt'])) {
            $min = floatval($term);
            // Fix result of invalid sound files having 0 length
            if ($min == 0) {
                $min = -1;
            }
            $max = false;
            // Add "exact match" that omits 1% range
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesCondition('length', $min);
            } else {
                if (strpos($term, '-')) {
                    [$min, $max] = array_map('floatval', explode('-', $term));
                }
                $operator = false;
                if (strpos($tag, 'gt') || $tq->operator == $tq::GREATER_THAN_OPERATOR) {
                    $operator = '>';
                } elseif (strpos($tag, 'lt') || $tq->operator == $tq::LESS_THAN_OPERATOR) {
                    $operator = '<';
                }

                $condition = new AggregateCondition([], 'AND');
                // Create a margin for "simple" searches, as these will probably never match exactly
                if (!$max && !$operator) {
                    $condition->addCondition(new SimpleCondition('length >= ' . ($min - ($min * 0.01))));
                    $condition->addCondition(new SimpleCondition('length <= ' . ($min + ($min * 0.01))));
                }
                if ($max) {
                    $condition = new AggregateCondition([], 'AND');
                    $condition->addCondition(new SimpleCondition("length >= $min"));
                    $condition->addCondition(new SimpleCondition("length <= $max"));
                }
                if ($operator) {
                    $condition->addCondition(new SimpleCondition("length $operator= $min"));
                }
            }
        } elseif (in_array($tag, ['q', 'q_gt', 'q_lt'])) {
            $qual_inv['A'] = '1';
            $qual_inv['B'] = '2';
            $qual_inv['C'] = '3';
            $qual_inv['D'] = '4';
            $qual_inv['E'] = '5';
            $qual_inv['a'] = '1';
            $qual_inv['b'] = '2';
            $qual_inv['c'] = '3';
            $qual_inv['d'] = '4';
            $qual_inv['e'] = '5';
            $qual_inv['0'] = '0';

            $qual = $qual_inv[$term] ?? false;
            if ($qual) {
                if (strpos($tag, 'gt') || $tq->operator == $tq::GREATER_THAN_OPERATOR) {
                    $operator = '<';
                } elseif (strpos($tag, 'lt') || $tq->operator == $tq::LESS_THAN_OPERATOR) {
                    $operator = '>';
                } else {
                    $operator = '=';
                }
                $ac = new AggregateCondition([], 'AND');
                $ac->addCondition(new SimpleCondition("quality $operator $qual"));
                $ac->addCondition(new SimpleCondition('quality > 0 '));
                $condition = $ac;
            } else {
                $condition = new SimpleCondition("1 = 2");
            }
        } elseif ($tag == 'gen') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesCondition('birdsounds.genus', $term);
            } else {
                $condition = $this->startsWithCondition('birdsounds.genus', $term);
            }
        } elseif ($tag == 'dir') {
            $condition = $this->matchesCondition('dir', $term);
        } elseif ($tag == 'area') {
            $area = WorldArea::lookup(strtolower($term));
            if ($area) {
                $condition = $this->matchesCondition($area->birdsoundsColumn, 'Y');
            } else { // invalid area, so match nothing
                $condition = new SimpleCondition('1 = 2');
            }
        } elseif ($tag == 'dis') {
            $condition = $this->matchesCondition('discussed', intval($term));
        } elseif ($tag == 'since') {
            if (is_numeric($term)) {
                $condition = new SimpleCondition("datetime >= DATE_SUB(NOW(), INTERVAL $term DAY)");
            } else {
                $tm = strtotime($term);
                if ($tm) {
                    $condition = new SimpleCondition("datetime >= FROM_UNIXTIME($tm)");
                }
            }
        } elseif ($tag == 'until') {
            $tm = strtotime($term);
            if ($tm) {
                $condition = new SimpleCondition("datetime < FROM_UNIXTIME($tm)");
            }
        } elseif ($tag == 'lic') {
            $attrs = explode('-', $term);
            $matchingLicenses = License::lookupByAttributes($attrs);

            if ($matchingLicenses) {
                $licConditions = [];
                foreach ($matchingLicenses as $lic) {
                    $licConditions[] = $this->matchesCondition('license', Library::escape($lic->id));
                }
                $condition = new AggregateCondition($licConditions, 'OR');
            } else {
                // no license like this exists, don't match anything
                $condition = new SimpleCondition('1 = 2');
            }
        } elseif ($tag == 'year') {
            $y = (int)$term;
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $condition = $this->matchesCondition('YEAR(date)', $y);
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('YEAR(date)', $y);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('YEAR(date)', $y);
            }
        } elseif ($tag == 'month') {
            $m = (int)$term;
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $condition = $this->matchesCondition('MONTH(date)', $m);
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('MONTH(date)', $m);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('MONTH(date)', $m);
            }
        } elseif ($tag == 'set') {
            $id = intval($term);
            if ($id) {
                $condition = new SimpleCondition("snd_nr IN (SELECT snd_nr FROM datasets_recordings WHERE datasetid = $id)");
            }
        } elseif ($tag == 'regnr') {
            if ($tq->operator == $tq::MATCHES_OPERATOR) {
                $condition = $this->matchesCondition('collection_specimen', $term);
            } else {
                $condition = $this->containsCondition('collection_specimen', $term);
            }
        } elseif ($tag == 'dev' || $tag == 'dvc') {
            $condition = $this->containsCondition('device', $term);
        } elseif ($tag == 'mic') {
            $condition = $this->containsCondition('microphone', $term);
        } elseif ($tag == 'colyear') {
            $y = (int)$term;
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $condition = $this->matchesCondition('YEAR(collection_date)', $y);
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('YEAR(collection_date)', $y);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('YEAR(collection_date)', $y);
            }
        } elseif ($tag == 'colmonth') {
            $m = (int)$term;
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $condition = $this->matchesCondition('MONTH(collection_date)', $m);
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('MONTH(collection_date)', $m);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('MONTH(collection_date)', $m);
            }
        } elseif (in_array($tag, ['auto', 'playback', 'seen', 'ai'])) {
            $condition = $this->yesNoCondition($tag, $term);
        } elseif ($tag == 'temp') {
            $term = (float)$term;
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $tempConditions[] = $this->greaterThanCondition('temperature', floor($term) - 1);
                $tempConditions[] = $this->lessThanCondition('temperature', floor($term) + 1);
                $condition = new AggregateCondition($tempConditions, 'AND');
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('temperature', $term);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('temperature', $term);
            }
        } elseif ($tag == 'grp' && !is_null($this->getGroupId($term))) {
            $condition = $this->matchesCondition('birdsounds.group_id', $this->getGroupId($term));
        } elseif ($tag == 'smp') {
            $term = (int)$term;
            if (in_array($tq->operator, [$tq::MATCHES_OPERATOR, $tq::DEFAULT_OPERATOR])) {
                $condition = $this->matchesCondition('audio_info.smp', $term);
            } elseif ($tq->operator == $tq::LESS_THAN_OPERATOR) {
                $condition = $this->lessThanCondition('audio_info.smp', $term);
            } elseif ($tq->operator == $tq::GREATER_THAN_OPERATOR) {
                $condition = $this->greaterThanCondition('audio_info.smp', $term);
            }
        }

        // Not a valid tag and not a property either
        if (!$condition && !$this->usesTagJoins) {
            $condition = new SimpleCondition('1 = 2');
        } elseif (!$condition) {
            $condition = new SimpleCondition('1');
        }

        return $this->conditions->addCondition($condition);
    }

    private function matchesCondition($field, $value)
    {
        return new SimpleCondition("$field = '$value'");
    }

    private function containsCondition($field, $value)
    {
        return new SimpleCondition("$field LIKE '%$value%'");
    }

    private function fullTextMatchesCondition($field, $value)
    {
        return new SimpleCondition($this->getMatchAgainst($field, $value, true));
    }

    private function getMatchAgainst($field, $value, $matches = false) {
        $matchAgainst = '';
        foreach (array_unique(array_filter(explode(' ', trim($value)))) as $term) {
            $matchAgainst .= "+$term" . ($matches ? '' : '*') . ' ';
        }
        return $matchAgainst ? "MATCH ($field) AGAINST ('" . trim($matchAgainst) . "' IN BOOLEAN MODE)" : null;
    }

    private function fullTextContainsCondition($field, $value)
    {
        return new SimpleCondition($this->getMatchAgainst($field, $value));
    }

    private function startsWithCondition($field, $value)
    {
        return new SimpleCondition("$field LIKE '$value%'");
    }

    private function lessThanCondition($field, $value)
    {
        return new SimpleCondition("$field < '$value'");
    }

    private function greaterThanCondition($field, $value)
    {
        return new SimpleCondition("$field > '$value'");
    }

    private function matchesRegexpCondition($field, $value)
    {
        $value = str_replace(['\\', '.', '*', '+', '[', ']'], '', $value); // Simple redos prevention
        return new SimpleCondition("$field REGEXP '([[:blank:][:punct:]]|^)$value([[:blank:][:punct:]]|$)'");
    }

    private function yesNoCondition($tag, $term)
    {
        $tagToColumn = [
            'auto' => 'automatic',
            'playback' => 'playback',
            'seen' => 'observed',
            'ai' => 'ai_result'
        ];
        $term = strtolower($term);
        $value = $term == 'yes' ? 1 : ($term == 'no' ? 0 : -1);
        // Return nothing if value does not match yes/no
        return $value == -1 || !isset($tagToColumn[$tag]) ? new SimpleCondition('1 = 2'
        ) : $this->matchesCondition($tagToColumn[$tag], $value);
    }

    private function getGroupId($group)
    {
        // Create a map of groups
        if (empty($this->groups)) {
            $this->groups['soundscape'] = '0';
            $res = Library::query("select id, name from groups");
            while ($row = $res->fetch_object()) {
                $this->groups[$row->name] = $row->id;
            }
        }
        $group = trim(strtolower($group));
        if (in_array($group, $this->groups, true)) {
            return $group;
        }
        return $this->groups[$group] ?? null;
    }

    private function buildJoinForTaggedQuery($tq)
    {
        $id = $this->soundPropertyId($tq->tagName, $tq->searchTerm);
        if ($id) {
            $this->usesTagJoins = true;
            $t = 't' . (count($this->joins) + 1);
            $this->joins[] = "INNER JOIN birdsounds_properties AS $t on birdsounds.snd_nr = $t.snd_nr and $t.property_id = $id";
        }
    }

    private function soundPropertyId($tag, $property)
    {
        $tag = trim(strtolower($tag));
        $property = trim(strtolower($property));

        // Create a map of categories, plus property names and ids
        if (empty($this->soundProperties)) {
            $res = Library::query("select t1.id, t2.tag, t1.property 
                from sound_properties as t1
                left join sound_property_categories as t2 on t1.category_id = t2.id
                where t1.property not in ('uncertain', 'unknown')"
            );
            while ($row = $res->fetch_object()) {
                $this->soundProperties[$row->tag][$row->property] = $row->id;
            }
        }
        return $this->soundProperties[$tag][$property] ?? false;
    }

    private function generateSQL()
    {
        // instead of doing OR statements, we make a union of
        // different separate queries, making sure that
        // for each MySQL uses the indices that exist for each column
        $escapedTerm = Library::escape(implode(' ', $this->basicTerms));
        $tagConditions = '';
        $localNameField = 'birdsounds.eng_name';

        // If no search terms are given, return no results instead of all
        if (empty($this->basicTerms) && empty($this->taggedQueries) && empty($this->joins) && !$this->onlyMystery) {
            $this->conditions->addCondition(new SimpleCondition('1 = 2'));
        }

        // Conditions and joins have been prepared in buildMainQuery()
        if ($this->conditions && $this->conditions->toSQL()) {
            $tagConditions = ' AND ' . $this->conditions->toSQL();
        }

        // Add taxonomy join if necessary
        if ($this->excludeRestricted) {
            $this->addJoin('LEFT JOIN taxonomy ON birdsounds.species_nr = taxonomy.species_nr');
        }
        // Add taxonomy_multilingual join only when using non-English language
        // and when not using also: tag (which only searches for English)
        if ($this->useLocalNameSearch && !$this->tagInQuery('also')) {
            $localNameField = 'cnames.' . self::$localNameLanguage;
            $this->addJoin('LEFT JOIN taxonomy_multilingual AS cnames ON birdsounds.species_nr = cnames.species_nr');
        }

        $sqlStart = "
            SELECT " . self::birdsoundsSelectFields() . ", $localNameField AS localName,
                audio_info.length, audio_info.format, audio_info.smp AS sample_rate
            FROM birdsounds
            LEFT JOIN audio_info USING(snd_nr) ";

        $statsSql = '
            SELECT COUNT(DISTINCT stats.snd_nr) AS numRecordings, COUNT(DISTINCT stats.species_nr) AS numSpecies, 
                SUM(stats.length) AS duration
            FROM (';

        foreach ($this->joins as $join) {
            $sqlStart .= " $join ";
        }

        $sqlStart .= " WHERE birdsounds.species_nr != '' ";

        if ($this->excludeRestricted) {
            $sqlStart .= ' AND taxonomy.restricted = 0 ';
        }

        if ($this->onlyMystery) {
            $sqlStart .= " 
                AND (birdsounds.species_nr = 'mysmys' 
                OR discussed = " . ThreadType::MYSTERY . ' 
                OR discussed = ' . ThreadType::ID_QUESTIONED . ')';
        }

        if ($this->excludeMystery) {
            $sqlStart .= " 
                AND (birdsounds.species_nr != 'mysmys' 
                AND discussed != " . ThreadType::MYSTERY . ' 
                AND discussed != ' . ThreadType::ID_QUESTIONED . ')';
        }

        // Regular query that does not include search for background species
        // 'also' tag has been moved from a tagged query to a dedicated query on a different table
        if (!$this->tagInQuery('also')) {
            $englishSql = "($sqlStart AND birdsounds.eng_name LIKE '%$escapedTerm%' $tagConditions)";

            if (empty($this->basicTerms)) {
                $selectSql = "$sqlStart $tagConditions";
            } elseif (count($this->basicTerms) == 1) {
                $selectSql = "
                    ($sqlStart AND birdsounds.genus = '$escapedTerm' $tagConditions)
                    UNION
                    ($sqlStart AND birdsounds.species = '$escapedTerm' $tagConditions)
                    UNION
                    ($sqlStart AND birdsounds.family = '$escapedTerm' $tagConditions)
                    UNION
                    ($sqlStart AND birdsounds.ssp = '$escapedTerm' $tagConditions)
                    UNION
                    $englishSql";
            } elseif (count($this->basicTerms) <= 3) {
                $genus = Library::escape($this->basicTerms[0]);
                $species = Library::escape($this->basicTerms[1]);
                $ssp = isset($this->basicTerms[2]) ? Library::escape($this->basicTerms[2]) : false;

                $selectSql = "
                    ($sqlStart AND birdsounds.genus = '$genus' AND birdsounds.species = '$species' " .
                    ($ssp ? " AND birdsounds.ssp = '$ssp'" : "") . " $tagConditions)
                    UNION
                    $englishSql";
            } else {
                $selectSql = $englishSql;
            }

            if (!empty($this->basicTerms) && $this->useLocalNameSearch) {
                $selectSql .= " 
                    UNION
                    ($sqlStart AND $localNameField LIKE '%$escapedTerm%' $tagConditions)";
            }
        }

        // Background search
        else {
            $tag = $this->tagInQuery('also');
            $term = Library::escape($tag->searchTerm);
            $basicTermCondition = '';

            $sqlBase = "
                (SELECT " . self::birdsoundsSelectFields() . ", $localNameField AS localName,
                    audio_info.length, audio_info.format, audio_info.smp AS sample_rate
                FROM birdsounds_background as background
                LEFT JOIN audio_info USING(snd_nr)  
                LEFT JOIN birdsounds USING (snd_nr) 
                WHERE %s
                GROUP BY background.snd_nr)";

            // Also genus/species/ssp if provided
            if (!empty($this->basicTerms)) {
                if (count($this->basicTerms) == 1) {
                    $basicTermCondition = " AND (
                        birdsounds.genus = '" . Library::escape($this->basicTerms[0]) . "' OR 
                        birdsounds.species = '" . Library::escape($this->basicTerms[0]) . "' OR
                        birdsounds.family = '" . Library::escape($this->basicTerms[0]) . "' OR
                        birdsounds.ssp = '" . Library::escape($this->basicTerms[0]) . "')";
                } elseif (count($this->basicTerms) <= 3) {
                    $basicTermCondition = " AND 
                        birdsounds.genus = '" . Library::escape($this->basicTerms[0]) . "' AND 
                        birdsounds.species = '" . Library::escape($this->basicTerms[1]) . "'";
                    if (count($this->basicTerms) == 3) {
                        $basicTermCondition .= " AND
                            birdsounds.ssp = '" . Library::escape($this->basicTerms[2]) . "'";
                    }
                }
            }

            if (count(explode(' ', $term)) == 1) {
                if ($tag->operator == $tag::MATCHES_OPERATOR) {
                    // Species cannot exactly match a single word
                    $selectSql =
                        sprintf($sqlBase, "background.english = '$term'" . $basicTermCondition . $tagConditions) . "
                        UNION " .
                        sprintf($sqlBase, "background.family = '$term'" . $basicTermCondition . $tagConditions);
                } else {
                    $selectSql =
                        sprintf($sqlBase, $this->getMatchAgainst('background.english', $term) . $basicTermCondition . $tagConditions) . "
                        UNION " .
                        sprintf($sqlBase, $this->getMatchAgainst('background.scientific', $term) . $basicTermCondition . $tagConditions) . "
                        UNION " .
                        sprintf($sqlBase, "background.family LIKE '$term%'" . $basicTermCondition . $tagConditions);
                }
            } else {
                if ($tag->operator == $tag::MATCHES_OPERATOR) {
                    // Family cannot consists of two words
                    $selectSql =
                        sprintf($sqlBase, $this->getMatchAgainst('background.english', $term, true) . $basicTermCondition . $tagConditions) . "
                        UNION " .
                        sprintf($sqlBase, "background.scientific = '$term'" . $basicTermCondition . $tagConditions);
                } else {
                    $selectSql =
                        sprintf($sqlBase, $this->getMatchAgainst('background.english', $term) . $basicTermCondition . $tagConditions) . "
                        UNION " .
                        sprintf($sqlBase, $this->getMatchAgainst('background.scientific', $term) . $basicTermCondition . $tagConditions);
                }
            }

            // Assuming a free text field will never exactly match a term
            if ($tag->operator != $tag::MATCHES_OPERATOR) {
                $selectSql .= "
                    UNION 
                    ($sqlStart AND {$this->getMatchAgainst('birdsounds.back_extra', $term)} $basicTermCondition $tagConditions)";
            }
        }

        $statsSql .= "
                $selectSql
            ) AS stats";

        $this->statsSql = $statsSql;
        return $selectSql;
    }

    private function addJoin($join)
    {
        if (!in_array($join, $this->joins)) {
            $this->joins[] = $join;
        }
    }

    private function tagInQuery($tag)
    {
        foreach ($this->taggedQueries as $q) {
            if ($q->tagName == $tag) {
                return $q;
            }
        }
        return false;
    }

    public static function birdsoundsSelectFields()
    {
        return '
            birdsounds.genus, birdsounds.species, birdsounds.ssp, birdsounds.eng_name, birdsounds.family, songtype, 
            song_classification, quality, recordist, olddate, birdsounds.date, birdsounds.time, country, location, 
            longitude, latitude, elevation, remarks, back_extra, path, birdsounds.species_nr, birdsounds.order_nr, 
            birdsounds.dir, neotropics, birdsounds.africa, birdsounds.asia, birdsounds.europe, birdsounds.australia, 
            birdsounds.datetime, discussed, license, birdsounds.snd_nr, birdsounds.group_id, temperature, 
            collection_specimen, collection_date, observed, playback, 
            automatic, device, microphone, birdsounds.modified ';
    }

    public static function ignoreMysteryAndPseudoSpeciesSql($tablename = 'birdsounds')
    {
        return ' (' . self::ignoreMysterySql($tablename) . ' AND ' . Species::excludePseudoSpeciesSQL($tablename) . ') ';
    }

    public static function ignoreMysterySql($tablename = 'birdsounds')
    {
        return " ($tablename.discussed != " . ThreadType::MYSTERY . " AND
            $tablename.discussed != " . ThreadType::ID_UNCONFIRMED . " AND
            $tablename.discussed != " . ThreadType::ID_QUESTIONED . ') ';
    }

    public static function setDefaultNumPerPage($num)
    {
        self::$defaultNumPerPage = $num;
    }

    public function setRawSqlClause($sql, $select)
    {
        $count = 'COUNT(*) as numRecordings, COUNT(DISTINCT birdsounds.species_nr) as numSpecies, 
            SUM(audio_info.length) as duration';

        $this->mainSQL = sprintf($sql, $select);
        $this->statsSql = sprintf($sql, $count);
    }

    public function excludeRestricted()
    {
        $this->excludeRestricted = true;
        $this->buildMainQuery();
    }

    public function excludeMystery()
    {
        $this->excludeMystery = true;
        $this->buildMainQuery();
    }

    public function onlyMystery()
    {
        $this->onlyMystery = true;
        $this->buildMainQuery();
    }

    public function setOrder($order, $direction = SortDirection::NORMAL)
    {
        $this->order = $order;
        $this->direction = $direction;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    public function invalidTaggedQueries()
    {
        $invalid = [];
        foreach ($this->taggedQueries as $q) {
            if (strlen($q->searchTerm) > 1 && in_array(substr($q->searchTerm, 0, 1), QueryParser::operators()
                ) || in_array($q->operator, QueryParser::operators(), true)) {
                $invalid[$q->tagName] = $q->searchTerm;
                break;
            }
        }
        return $invalid;
    }

    public function execute($pageNumber = Query::NO_PAGING, $limit = 0)
    {
        $sql = $this->mainSQL . $this->buildOrderClause($this->order, $this->direction);

        if ($pageNumber > 0) {
            $offset = (($pageNumber - 1) * $this->pageSize);
            $sql .= ' LIMIT ' . $offset . ',' . $this->pageSize;
        } elseif (!empty($limit)) {
            // set a max limit to prevent runaway queries from killing the server, etc.
            $sql .= " LIMIT $limit";
        }
        return app()->db()->query($sql);
    }

    private function buildOrderClause($order, $dir)
    {
        $dirText = ($dir == SortDirection::NORMAL) ? 'ASC' : 'DESC';
        $altDirText = ($dir == SortDirection::NORMAL) ? 'DESC' : 'ASC';
        if ($order == '') {
            $order = 'tax';
        }
        if ($order == 'tax' || $order == 'taxonomy') {
            $sqlorder = " ORDER BY order_nr $dirText, quality, snd_nr DESC ";
        }
        if ($order == 'english' || $order == 'en') {
            $sqlorder = " ORDER BY eng_name $dirText, quality, snd_nr DESC";
        }
        if ($order == 'date' || $order == 'dt') {
            $sqlorder = " ORDER BY date $dirText, time $dirText, quality";
        }
        if ($order == 'time' || $order == 'tm') {
            $sqlorder = " ORDER BY time $dirText, quality, snd_nr DESC";
        }
        if ($order == 'elevation' || $order == 'elev') {
            $sqlorder = " ORDER BY elevation $dirText, quality, snd_nr DESC";
        }
        if ($order == 'quality' || $order == 'qual') {
            $sqlorder = " ORDER BY quality $dirText, snd_nr DESC";
        }
        if ($order == 'country' || $order == 'cnt') {
            $sqlorder = " ORDER BY country $dirText, quality, snd_nr DESC";
        }
        if ($order == 'location' || $order == 'loc') {
            $sqlorder = " ORDER BY location $dirText, quality, snd_nr DESC";
        }
        if ($order == 'rec' || $order == 'recordist') {
            $sqlorder = " ORDER BY recordist $dirText, quality, snd_nr DESC";
        }
        if ($order == 'appearance' || $order == 'xc') {
            $sqlorder = " ORDER BY snd_nr $altDirText";
        }
        if ($order == 'songtype' || $order == 'typ') {
            $sqlorder = " ORDER BY songtype $dirText, quality, snd_nr DESC";
        }
        if ($order == 'ssp') {
            $sqlorder = " ORDER BY order_nr $dirText, ssp, snd_nr DESC";
        }
        if ($order == 'length') {
            $sqlorder = " ORDER BY length $dirText, quality, snd_nr DESC";
        }

        return $sqlorder ?? '';
    }

    public function getQueryString()
    {
        return $this->mainSQL . $this->buildOrderClause($this->order, $this->direction);
    }

    public function numSpecies()
    {
        if ($this->numSpecies == -1) {
            $this->queryStats();
        }

        return $this->numSpecies;
    }

    private function queryStats()
    {
        // this calculates the basic stats (number of hits and number of
        // distinct species) in the database, rather than pulling the full set
        // of results up into PHP to calculate them
        $result = app()->db()->query($this->statsSql);
        if ($result) {
            $row = $result->fetch_object();
            if ($row) {
                $this->numRecordings = $row->numRecordings;
                $this->numSpecies = $row->numSpecies;
                $this->duration = $row->duration;
            }
        } else {
            $this->numRecordings = 0;
            $this->numSpecies = 0;
            $this->duration = 0;
        }
    }

    public function duration()
    {
        if ($this->duration == -1) {
            $this->queryStats();
        }

        return $this->duration;
    }

    public function numResultsPages()
    {
        return floor($this->numRecordings() / $this->pageSize) + 1;
    }

    public function numRecordings()
    {
        if ($this->numRecordings == -1) {
            $this->queryStats();
        }

        return $this->numRecordings;
    }

    public function getLocations($limit = 0)
    {
        if (empty($this->queryString)) {
            return null;
        }

        $clause = '';
        if ($limit) {
            $clause = " LIMIT $limit";
        }
        // this calculates the basic stats (number of hits and number of
        // distinct species) in the database, rather than pulling the full set
        // of results up into PHP to calculate them
        $sql = "
            SELECT latitude, longitude, location 
            FROM ($this->mainSQL) as statsTable 
            GROUP BY location $clause";

        return app()->db()->query($sql);
    }

    public function getBasicTerms()
    {
        return $this->basicTerms;
    }

    public function getTaggedQueries()
    {
        return $this->taggedQueries;
    }
}
