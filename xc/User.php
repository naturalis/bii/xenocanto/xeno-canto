<?php

namespace xc;

use xc\Controllers\VerifyEmail;

class User
{
    public const MAX_COOKIE_AGE = 60;

    public const PASSWORD_RESET_CODE_LENGTH = 40;

    private $m_row;

    private $m_checkedAvatar;

    private $m_masquerade;

    private $m_avatar;

    private $m_thumb;

    public function __construct($dbrow)
    {
        $this->m_row = $dbrow;
        $this->m_checkedAvatar = false;
        $this->m_masquerade = false;
    }

    public static function find($username)
    {
        $s = Library::escape($username);
        return self::loadFromCondition("WHERE users.username like '%$s%'", 'ORDER BY nrecordings DESC'
        );
    }

    private static function loadFromCondition($where, $order = '')
    {
        $sql = self::sql($where, $order);
        $res = Library::query($sql);

        $users = [];
        if ($res) {
            while ($row = $res->fetch_object()) {
                $users[] = new User($row);
            }
        }

        return $users;
    }

    private static function sql($whereClause, $orderClause = '')
    {
        if (!$orderClause) {
            $orderClause = 'ORDER BY users.username ASC';
        }
        return "
            SELECT users.*, permissions.*, stats.*
            FROM users 
            LEFT JOIN permissions ON users.dir = permissions.userid 
            LEFT JOIN rec_summary_stats stats ON users.dir = stats.userid AND stats.group_id = 0
            $whereClause 
            $orderClause";
    }

    public static function loadFromEmail($email)
    {
        $s = Library::escape($email);
        $users = User::loadFromCondition(" WHERE users.email = '$s'");
        if ($users) {
            return $users[0];
        }
        return false;
    }

    public static function loginWithCookie($cookieValue)
    {
        $decoded = base64_decode($cookieValue);
        $parts = explode('~#~', $decoded);
        $email = Library::escape($parts[0]);
        $otp = Library::escape($parts[1]);
        $sql = self::sql("
            INNER JOIN user_cookies ON users.dir = user_cookies.userid 
            WHERE users.email = '$email' AND user_cookies.cookie = '$otp' 
            AND DATEDIFF(NOW(), user_cookies.date) < " . User::MAX_COOKIE_AGE
        );
        $res = Library::query($sql);
        if ($res && $res->num_rows == 1) {
            $row = $res->fetch_object();
            Library::query("DELETE FROM user_cookies WHERE userid = '$row->dir' AND cookie = '$otp'");
            $user = new User($row);
            app()->session()->set('user', $user);
            $user->rememberLogin();
            return $user;
        }

        return null;
    }

    public function rememberLogin()
    {
        // it's only possible to remember the login of the currently-logged-in user
        if (!$this->isLoggedIn()) {
            return;
        }

        // clean up old expired cookies
        $sql = 'DELETE FROM user_cookies WHERE DATEDIFF(NOW(), user_cookies.date) > ' . User::MAX_COOKIE_AGE;
        Library::query($sql);

        $userid = $this->m_row->dir;
        $newotp = self::otp();
        Library::query("INSERT INTO user_cookies (userid, cookie, `date`) VALUES ('$userid', '$newotp', NOW())");
        $obfuscated = base64_encode($this->emailAddress() . '~#~' . $newotp);
        app()->setCookie('login', $obfuscated);
    }

    public function isLoggedIn()
    {
        $current = self::current();
        if (!$current) {
            return false;
        }
        return ($current->userId() == $this->userId());
    }

    public static function current()
    {
        $user = app()->session()->get('user');
        // need to do this because the user class moved to the xc namespace, so
        // the value stored in the session may be a different type
        if (!($user instanceof User) || !$user->reloadFromDatabase()) {
            $user = null;
            app()->session()->remove('user');
        }
        return $user;
    }

    public function reloadFromDatabase()
    {
        if (!$this->m_row) {
            return false;
        }
        $sql = self::sql("WHERE dir = '{$this->m_row->dir}'");
        $res = Library::query($sql);

        $this->m_row = $res->fetch_object();
        return $this->m_row;
    }

    public function userId()
    {
        return $this->m_row->dir;
    }

    public static function otp($length = 16)
    {
        // makes a random alpha numeric string of a given length
        $letters = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
        $out = '';
        for ($c = 0; $c < $length; $c++) {
            $out .= $letters[mt_rand(0, count($letters) - 1)];
        }
        return $out;
    }

    public function emailAddress()
    {
        return $this->m_row->email;
    }

    public static function getPasswordResetCode()
    {
        return self::otp(self::PASSWORD_RESET_CODE_LENGTH);
    }

    public static function loginWithResetCode($code)
    {
        if (self::current() || strlen($code) != self::PASSWORD_RESET_CODE_LENGTH) {
            return null;
        }

        $sql = "
            SELECT dir 
            FROM user_password_reset_codes 
            WHERE reset_code = '" . Library::escape($code) . "'
            ORDER BY id DESC
            LIMIT 1";
        $res = Library::query($sql);
        if ($res && $res->num_rows == 1) {
            $row = $res->fetch_object();
            if (isset($row->dir)) {
                $user = self::load($row->dir);
                if ($user) {
                    app()->session()->set('user', $user);
                    return $user;
                }
            }
        }
        return null;
    }

    public static function load($id)
    {
        $s = Library::escape($id);
        $users = User::loadFromCondition(" WHERE users.dir = '$s'");
        if ($users) {
            return $users[0];
        }
        return false;
    }

    public static function loginWithPassword($email, $password)
    {
        $email = Library::escape($email);
        $res = Library::query("SELECT salt FROM users WHERE email = '$email'");
        $row = $res->fetch_object();
        if (!$row) {
            return null;
        }

        $salt = $row->salt;
        $hashed_password = self::hashPassword($password, $salt);
        $sql = self::sql("WHERE email = '$email' and password = '$hashed_password'");
        $res = Library::query($sql);
        if ($res && $res->num_rows == 1) {
            $row = $res->fetch_object();
            $user = new User($row);
            app()->session()->set('user', $user);
            $user->rememberLogin();
            return $user;
        }

        return null;
    }

    public static function hashPassword($password, $salt)
    {
        return hash('sha256', $salt . $password, 0);
    }

    public function getProfileURL($absolute = false)
    {
        return Library::getUrl('recordist', ['id' => $this->m_row->dir], $absolute);
    }

    public function thirdPartyLicenseIsSet()
    {
        return $this->m_row->third_party_license != -1;
    }

    public function updateEmailAddress($newEmail, &$error)
    {
        $validEmail = filter_var($newEmail, FILTER_VALIDATE_EMAIL);
        if ($validEmail === false) {
            $error = sprintf(_('The address %s is not a valid email address.'), "<b>$newEmail</b>");
            return false;
        }

        $e = Library::escape($validEmail);
        // check whether that email is already in use
        $res = Library::query("SELECT dir FROM users WHERE email = '$e'");
        if ($res && $res->num_rows !== 0) {
            $error = sprintf(_('The address %s is already in use by another member'), $validEmail);
            return false;
        }

        $res = Library::query("UPDATE users SET email = '$e' WHERE dir = '{$this->m_row->dir}'");

        if ($res) {
            // Unset verification status when changing email address
            $this->unsetVerification();

            $sql = self::sql("WHERE dir = '{$this->m_row->dir}'");
            $res = Library::query($sql);
            if ($res) {
                $this->m_row = $res->fetch_object();
                $this->sendVerificationEmail();
                $this->rememberLogin();
                return true;
            }
        }

        $error = _('Internal error while updating email address');
        return false;
    }

    private function unsetVerification()
    {
        $sql =  "
            UPDATE users 
            SET verified = 0, verificationCode = '" . self::otp() ."' 
            WHERE dir = '{$this->m_row->dir}'";
        Library::query($sql);
    }

    private function sendVerificationEmail()
    {
        $verificationUrl = Library::getUrl('verify-email', [
            'userid' => $this->userId(),
            'code' => $this->m_row->verificationCode
        ], true);
        $message = VerifyEmail::verificationMessage(Library::sanitize($this->userName()), $verificationUrl);
        (new XCMail($this->emailAddress(), 'Verification code for xeno-canto.org', $message))->send();
    }

    public function userName()
    {
        return Library::sanitize($this->m_row->username);
    }

    public function searchLanguages()
    {
        $languages = [app()->determineBestLanguage()->code => app()->determineBestLanguage()];
        $res = Library::query("SELECT language FROM user_search_languages WHERE userid='{$this->m_row->dir}'");
        if ($res) {
            $locales = array_column($res->fetch_all(MYSQLI_ASSOC), 'language');
            foreach ($locales as $code) {
                $language = Language::lookup($code);
                if ($language) {
                    $languages[$code] = $language;
                }
            }
        }
        return $languages;
    }

    public function blurb()
    {
        return $this->m_row->blurb;
    }

    public function numResultsPerPage()
    {
        if ($this->m_row->num_per_page) {
            return $this->m_row->num_per_page;
        }
        return 30;
    }

    public function viewPreference()
    {
        return $this->m_row->view;
    }

    public function canUploadLargeFiles()
    {
        return $this->isAdmin() || $this->m_row->largefiles;
    }

    public function isAdmin()
    {
        return $this->m_row->admin;
    }

    public function canConfirm()
    {
        return $this->isAdmin() || $this->m_row->canconfirm;
    }

    public function canDownloadRestrictedRecordings()
    {
        return $this->m_row->downloadrestricted;
    }

    public function canModifyRecording($xcid)
    {
        $rec = Recording::load($xcid, false);
        return $this->isAdmin() || $this->isUploader($rec);
    }

    public function isUploader($recording)
    {
        return $recording->recordistID() == $this->userId();
    }

    public function canRateRecording($xcid)
    {
        $canrate = $this->canRateRecordings();

        if ($canrate) {
            return true;
        }

        $xcid = intval($xcid);
        if ($xcid) {
            $res = Library::query("SELECT IF(dir='{$this->m_row->dir}',1,0) as canrate FROM birdsounds WHERE snd_nr=$xcid"
            );
            if ($res) {
                $row = $res->fetch_object();
                if ($row) {
                    return $row->canrate;
                }
            }
        }
        return $canrate;
    }

    public function canRateRecordings()
    {
        return ($this->isVerified() && !$this->m_row->disablerating);
    }

    public function isVerified()
    {
        return $this->m_row->verified;
    }

    public function age()
    {
        // we didn't track this at first, so if it's null, just set it to the
        // approximate point where we started tracking this
        $joindate = $this->m_row->joindate;
        if (!$joindate) {
            $joindate = '2012-11-01 12:00:00';
        }
        return floor((time() - strtotime($joindate)) / (60 * 60));
    }

    public function hasAvatar()
    {
        $this->checkAvatar();
        return !empty($this->m_avatar);
    }

    private function checkAvatar()
    {
        if ($this->m_checkedAvatar) {
            return;
        }

        if ($this->m_row->dir && (@file_exists($_SERVER['DOCUMENT_ROOT'] . "/graphics/memberpics/{$this->m_row->dir}_tb.png"
            ))) {
            $this->m_thumb = "/graphics/memberpics/{$this->m_row->dir}_tb.png";
            $this->m_avatar = "/graphics/memberpics/{$this->m_row->dir}.png";
        }
    }

    public function deleteAvatar()
    {
        $avatars = [
            $_SERVER['DOCUMENT_ROOT'] . "/graphics/memberpics/{$this->m_row->dir}_tb.png",
            $_SERVER['DOCUMENT_ROOT'] . "/graphics/memberpics/{$this->m_row->dir}.png",
        ];
        foreach ($avatars as $file) {
            if (!is_file($file) || !unlink($file)) {
                return false;
            }
        }
        $this->m_checkedAvatar = false;
        $this->m_avatar = false;
        $this->m_thumb = false;
        return true;
    }

    public function getAvatar()
    {
        $this->checkAvatar();
        if ($this->m_avatar) {
            return $this->m_avatar;
        }
        return $this->getDefaultAvatar();
    }

    public static function getDefaultAvatar()
    {
        return '/static/img/avatar-default-200.png';
    }

    public function getThumbnail()
    {
        $this->checkAvatar();
        if ($this->m_thumb) {
            return $this->m_thumb;
        }
        return $this->getDefaultThumbnail();
    }

    public static function getDefaultThumbnail()
    {
        return '/static/img/avatar-default-32.png';
    }

    public function getLanguage()
    {
        return Language::lookup($this->m_row->language);
    }

    public function isMasquerade()
    {
        return $this->m_masquerade;
    }

    public function setMasquerade($masquerade)
    {
        $this->m_masquerade = $masquerade;
    }

    public function canContact()
    {
        return $this->m_row->contactform;
    }

    public function numRecordings()
    {
        return $this->m_row->nrecordings;
    }

    public function logout()
    {
        $cookie = $_COOKIE['login'] ?? false;
        if ($cookie) {
            $decoded = base64_decode($cookie);
            $parts = explode('~#~', $decoded);
            $otp = Library::escape($parts[1]);
            if (!Library::query("delete from user_cookies WHERE userid='{$this->m_row->dir}' AND cookie='$otp'")) {
                exit();
            }
            unset($_COOKIE['login']);
        }
        app()->session()->remove('user');
        app()->setCookie('login', '', time() - 3600);
        app()->session()->invalidate();
    }
}
