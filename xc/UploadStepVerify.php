<?php

namespace xc;

class UploadStepVerify extends UploadStep
{
    private $m_species;
    private $m_map;

    public function __construct(&$data)
    {
        parent::__construct(UploadStep::VERIFY, $data);

        $this->m_species = Species::load($data->speciesNr);

        if ($this->m_species && is_numeric($data->lat) && is_numeric($data->lng)) {
            $this->m_map = new MarkerMap('map-canvas', [
                [
                    'latitude' => $data->lat,
                    'longitude' => $data->lng,
                    'title' => $data->location,
                ],
            ], $this->m_species->rangeMapURL());
        } else {
            $this->m_map = null;
        }

        $this->setFormProperties();
    }

    public function validate($request)
    {
        return true;
    }

    public function pageId()
    {
        return 'upload-verify';
    }

    public function content($request)
    {
        $data = $this->m_data;
        if (!$this->m_species) {
            $this->notifyError(_('Internal Error: no valid species specified'));
            Library::logger(
            )->logError("In verify step: Unable to load species for {$data->speciesNr}\n\nForm data: " . var_export($data
                )
            );
            return;
        }

        if (is_numeric($data->lat) && is_numeric($data->lng)) {
            $lat = $data->lat;
            $lng = $data->lng;
        } else {
            $lng = $lat = "<span class='unspecified'>" . _('Not specified') . '</span>';
        }

        $dateParts = explode('-', $data->recordingDate);
        if (count($dateParts) == 3 && checkdate(intval($dateParts[1]), intval($dateParts[2]), intval($dateParts[0]))) {
            $date = HtmlUtil::formatDate('%B %e, %Y', mktime(0, 0, 0, $dateParts[1], $dateParts[2], $dateParts[0]));
        } else {
            $date = $data->recordingDate;
        }

        $ssp = $data->ssp;
        if (empty($ssp)) {
            $ssp = "<span class='unspecified'>" . _('not specified') . '</span>';
        } else {
            $sspres = Library::query("SELECT ssp from taxonomy_ssp where genus='{$this->m_species->genus()}' AND species='{$this->m_species->species()}'"
            );
            if (!$sspres->num_rows) {
                $ssp = "<span class='flagged'>" . Library::sanitize($ssp) . '</span>';
            } else {
                $validSsps = [];
                while ($row = $sspres->fetch_object()) {
                    $validSsps[] = $row->ssp;
                }

                if (!in_array($ssp, $validSsps, true)) {
                    $options = [];
                    foreach ($validSsps as $option) {
                        if (levenshtein($option, $ssp) < 4) {
                            $options[] = $option;
                        }
                    }

                    $ssp = "<span class='flagged'>" . Library::sanitize($ssp) . '</span>';
                    if ($options) {
                        foreach ($options as $k => $option) {
                            $options[$k] = "'<em>$option</em>'";
                        }
                        $optionstr = implode(' or ', $options);
                        $ssp .= " <span class='hint'>($optionstr?)</span>";
                    }
                }
            }
        }

        if (empty($data->remarks)) {
            $remarks = HtmlUtil::notSpecified();
        } else {
            $remarks = HtmlUtil::formatUserTextLimited($data->remarks);
        }

        $quality = $this->ratingToString($data->quality);

        $filename = basename($data->filename);
        $submitButtonName = htmlspecialchars(_('Submit'), ENT_QUOTES);

        $mapCanvas = '';
        if ($this->m_map) {
            $mapCanvas = "<div id='map-canvas'></div>";
        }

        $licenseLink = "<span class='unspecified'>" . _('none') . '</span>';
        $license = License::lookupById($data->license);
        if ($license) {
            $licenseLink = "<a href='$license->url' target='_blank'>$license->name</a>";
        }

        $speciesName = $this->m_species->htmlDisplayName(false);
        $restrictedSpeciesNote = '';
        if ($this->m_species->restricted()) {
            $restrictedSpeciesNote = "<p class='important'>" . _('This species is restricted'
                ) . '. ' . "<a href='" . Library::getUrl('FAQ'
                ) . "#restricted' target='_blank'>" . _('What does this mean?'
                ) . '</a></p>';
        }

        $method = '';
        if (!$this->isSoundscape()) {
            $method = $this->printGroupCategoryValues('recording method') ?: HtmlUtil::notSpecified();
            // Append collection date only if method is 'in studio'
            if ($data->soundProperties && in_array($this->inStudioInputId(), $data->soundProperties[$data->groupId]
                ) && $data->collectionDate) {
                $method .= ' (' . sprintf(_('collected on %s'), $data->collectionDate) . ')';
            }
        }

        $note = "<p class='important'><strong>" . _('Note'
            ) . '</strong>: ' . sprintf(_('your recording has not been published yet.  You must click the %s button below.'
                ), "<em>$submitButtonName</em>"
            ) . "</p>";
        if (strtolower(pathinfo($data->filename, PATHINFO_EXTENSION)) == 'wav') {
            $note .= "<p class='important'><strong>" . _('Warning'
                ) . '! </strong> ' . _('An mp3 derivative will be created within five minutes. Until then, the original wav file will be used for playback.'
                ) . "</p>";
        }

        $output = "
            <p>" . _('Please review all information below and confirm that it is correct.') . "</p>
            $note
            <div class='column'>
            <h2>" . _('Basic data') . "</h2>
            <table class='key-value'>
            <tr>
                <td>" . _('File') . "</td>
                <td>$filename</td>
            </tr>
            <tr>
                <td>" . _('Date') . "</td>
                <td>$date</td>
            </tr>
            <tr>
                <td>" . _('Time') . '</td>
                <td>' . $this->printValue('time') . '</td>
            </tr>
            <tr>
                <td>' . _('License') . "</td>
                <td>$licenseLink</td>
            </tr>
            <tr>
                <td>" . _('Recordist') . "</td>
                <td>" . $this->printValue('recordist') . "</td>
            </tr>
            <tr>
                <td>" . _('Device') . "</td>
                <td>" . $this->printValue('device') . "</td>
            </tr>
            <tr>
                <td>" . _('Microphone') . "</td>
                <td>" . $this->printValue('microphone') . "</td>
            </tr>
            <tr>
                <td>" . _('Automatic') . "</td>
                <td>$data->automatic</td>
            </tr>
            </table>
            
            <h2>" . _('Sound details') . "</h2>
            <table class='key-value'>";

        if (!$this->isSoundscape()) {
            $output .= "
                <tr>
                    <td>" . _('Species') . "</td>
                    <td>$speciesName $restrictedSpeciesNote</td>
                </tr>
                <tr>
                    <td>" . _('Subspecies') . "</td>
                    <td>$ssp</td>
                </tr>
                <tr>
                    <td>" . _('Sex') . "</td>
                    <td>" . ($this->printGroupCategoryValues('sex') ?: HtmlUtil::notSpecified()) . "</td>
                </tr>
                <tr>
                    <td>" . _('Life stage') . "</td>
                    <td>" . ($this->printGroupCategoryValues('life stage') ?: HtmlUtil::notSpecified()) . "</td>
                </tr>
                <tr>
                    <td>" . _('Animal seen?') . "</td>
                    <td>$data->animalSeen</td>
                </tr>
                <tr><td>" . _('Type') . "</td><td></td></tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;" . _('predefined') . "</td>
                    <td>" . ($this->printGroupCategoryValues('sound type') ?: HtmlUtil::notSpecified()) . "</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;" . _('other') . "</td>
                    <td>" . $this->printValue('soundTypeExtra') . "</td>
                </tr>
                <tr>
                    <td>" . _('Method') . "</td>
                    <td>$method</td>
                </tr>
                <tr>
                    <td>" . _('Playback used?') . "</td>
                    <td>$data->playbackUsed</td>
                </tr>";

            // Hard-coded for the time being...
            if ($data->groupId == 2 || $data->groupId == 4) {
                $temp = $this->printValue('temperature') != HtmlUtil::notSpecified() ? $this->printValue('temperature'
                    ) . ' &deg;C' : HtmlUtil::notSpecified();
                $output .= "
                    <tr>
                    <td>" . _('Temperature') . "</td>
                    <td>$temp</td>
                    </tr>";
            }
        }

        $output .= "
            <tr>
            <td>" . _('Background') . "</td>
            <td><ul>" . $this->printBackgroundSpecies($data->bgSpecies) . "</ul></td>
            </tr>";

        if (!$this->isSoundscape()) {
            $output .= "           
                <tr>
                <td>" . _('Specimen') . "</td>
                <td>" . $this->printValue('specimen') . "</td>
                </tr>";
        }

        $output .= "
            <tr>
            <td>" . _('Quality') . "</td>
            <td>$quality</td>
            </tr> 
            </table>
            
            <h2>" . _('Remarks') . "</h2>
            $remarks
            </div>
 
            <div class='column'>
            <h2>" . _('Location') . "</h2>
            $mapCanvas
            <table class='key-value'>
            <tr>
            <td>" . _('Latitude') . "</td>
            <td>$lat</td>
            </tr>
            <tr>
            <td>" . _('Longitude') . "</td>
            <td>$lng</td>
            </tr>
            <tr>
            <td>" . _('Location Name') . '</td>
            <td>' . $this->printValue('location') . '</td>
            </tr>
            <tr>
            <td>' . _('Country') . "</td>
            <td>$data->country</td>
            </tr>
            <tr>
            <td>" . _('Elevation') . '</td>
            <td>' . $this->printValue('elevation') . " meters</td>
            </tr>
            </table>
            </div>

            <form id='verify-form' method='post'>
            <input type='hidden' name='i' value='{$request->query->get('i')}'/>
            <input type='hidden' name='upload-step' value='" . UploadStep::VERIFY . "'/>

            <div class='button-box'>
            <input type='submit' id='upload-step-edit' name='edit' value='&laquo; " . htmlspecialchars(_('Edit'),
                ENT_QUOTES
            ) . "'/>
            <input type='submit' id='upload-step-submit' name='finish' value='$submitButtonName'/>
            </div>
            </form>" . $this->script();

        return $output;
    }

    private function ratingToString($quality)
    {
        $strings = ['', 'A', 'B', 'C', 'D', 'E'];

        if ($quality > 0 && $quality < count($strings)) {
            return $strings[$quality];
        }

        return '';
    }

    private function printGroupCategoryValues($category)
    {
        $groupId = $this->m_data->groupId;
        $selectedProperties = $this->m_data->soundProperties ?? [];
        $values = [];

        if ($selectedProperties) {
            foreach ($this->properties[$groupId][$category] as $propertyId => $property) {
                if (isset($selectedProperties[$groupId]) && in_array($propertyId, $selectedProperties[$groupId])) {
                    $values[] = $property;
                }
            }
        }

        return $values ? implode(', ', $values) : null;
    }

    private function printValue($category)
    {
        if (!isset($this->m_data->$category) || $this->m_data->$category == '') {
            return HtmlUtil::notSpecified();
        }
        return Library::sanitize($this->m_data->$category);
    }

    private function printBackgroundSpecies($bgText)
    {
        if (!empty($bgText)) {
            $names = explode(',', $bgText);
            for ($i = 0; $i < count($names); $i++) {
                $name = trim($names[$i]);
                if (!empty($name)) {
                    $spnr = Species::getSpeciesNumberForString($name);
                    if (empty($spnr)) {
                        $bgExtra[] = $name;
                    } else {
                        $bgSpecies[] = Species::load($spnr, true);
                    }
                }
            }
        }

        $bghtml = '';
        foreach ($bgSpecies ?? [] as $species) {
            $bghtml .= "<li>{$species->htmlDisplayName(false)}</li>";
        }
        foreach ($bgExtra ?? [] as $extra) {
            $bghtml .= '<li><span class="flagged">' . Library::sanitize($extra) . '</span></li>';
        }
        if (empty($bghtml)) {
            $bghtml = "<li><span class='unspecified'>" . _('none') . '</span></li>';
        }

        return $bghtml;
    }

    public function script()
    {
        if ($this->m_map) {
            return $this->m_map->getJS();
        }
        return '';
    }
}
