<?php

namespace xc;

class ResultsTable
{
    public static function resultsSummary($numHits, $numSpecies, $query)
    {
        $summary = '';
        if (empty($query)) {
            $summary .= sprintf(ngettext('%d result from %d species', '%d results from %d species', intval($numHits)
            ), $numHits, $numSpecies
            );
        } else {
            $summary .= sprintf(ngettext("%d result from %d species for query '%s'",
                "%d results from %d species for query '%s'", intval($numHits)
            ), $numHits, $numSpecies, "<span class='search-term'>" . Library::sanitize($query) . '</span>'
            );
        }

        $summary .= ' (' . _('foreground species only') . ") </p>";

        return $summary;
    }

    public static function regionSelect($name, $selected = null, $id = '', $class = '')
    {
        $options = ['world' => _('World')];
        foreach (WorldArea::all() as $area) {
            $options[$area->branch] = $area->desc;
        }
        if (!$selected) {
            $selected = 'world';
        }

        return self::selectInput($options, $name, $selected, $id, $class);
    }

    public static function selectInput($options, $name, $selected = null, $id = '', $class = '')
    {
        $html = "<select name='$name' class='$class' id='$id'>";
        foreach ($options as $val => $display) {
            if ($val == html_entity_decode($selected)) {
                $selectedAttr = 'selected';
            } else {
                $selectedAttr = '';
            }
            $html .= "<option value='$val' $selectedAttr>$display</option>";
        }
        $html .= '</select>';
        return $html;
    }

    public static function countrySelect($name, $selected = null, $id = '', $classes = '', $prependEquals = true)
    {
        $list = HtmlUtil::getCountryList();
        $options = ['' => _('Please choose...')];

        // When used for searching, it's better to prepend an =, so the matches option will be used
        $values = !$prependEquals ? array_values($list) : $values = preg_filter('/^/', '=', array_values($list));

        // yes, we use country name as the key...
        $options = array_merge($options, array_combine($values, array_values($list))
        );

        return self::selectInput($options, $name, $selected, $id, $classes);
    }

    public static function threadTypeSelect($type, $name, $selected = null, $id = null, $class = null): string
    {
        $level = $type == 'admin' ? 3 : 1;
        $threadTypes = ForumPost::getThreadDescriptions($level);
        // Rename none option for non-admin select
        if ($level == 1) {
            $threadTypes[ThreadType::NONE] = _('No category');
        }
        return self::selectInput($threadTypes, $name, $selected, $id, $class);
    }

    public static function resultsTableForView($request, $results, $view, $options = null)
    {
        $actionsCallback = null;
        $showLocations = true;
        $showSortSelect = true;

        if ($options) {
            extract($options);
        }

        $view = intval($view);
        $tableClass = $view == ViewType::SONOGRAMS ? 'sonos' : 'results';

        $output = '';
        if ($view == ViewType::SONOGRAMS && $showSortSelect) {
            $output .= self::sortSortSelect($request, $showLocations);
        }
        $output .= "<table class='$tableClass'>";

        // the detailed and concise views
        if ($view != ViewType::SONOGRAMS) {
            $output .= self::topTableRow($request, $view);
        }

        $prevRecording = null;
        $row = $results->fetch_object();

        $perRow = 3;
        for ($j = 1; $row; $j++) {
            $rec = new Recording($row);

            // If location should be shown depends on passed variable or recorded species
            // (the first is to disable locations for backgroud species).
            $showLocation = $showLocations;
            if ($showLocation) {
                $showLocation = !$rec->species()->restricted;
            }

            // get next row to see if we're at the end or not
            $nextRow = $results->fetch_object();

            if ($view != ViewType::SONOGRAMS) {
                $output .= self::rowDetailedConcise($rec, $prevRecording, $view, $actionsCallback, $showLocation);
            } else {
                // Sono flow is not yet supported, so just display sonograms
                if ($j % $perRow == 1) {
                    $output .= '<tr>';
                }

                $tdclass = 'sonobox';
                if (!$prevRecording || ($rec->speciesNumber() != $prevRecording->speciesNumber())) {
                    $tdclass .= ' new-species';
                }

                $output .= "<td class='$tdclass'>";
                $output .= $rec->player(['showDate' => true, 'showLocation' => $showLocation, 'linkFields' => true,]);

                $output .= self::linksToForum($rec);
                $remarks = $rec->printRemarks();
                if ($remarks) {
                    $output .= "<div class='remarks readmore'>$remarks</div>";
                }
                $output .= HtmlUtil::ratingWidget($rec->xcid(), $rec->rating());
                $output .= '<p>' . self::backgroundDropdownBox($rec);
                if ($actionsCallback) {
                    $output .= call_user_func($actionsCallback, $rec);
                } else {
                    $output .= self::defaultRecordingActions($rec);
                }

                $output .= '</p>
                   </td>';

                // if the results are finished, but we haven't finished our current
                // table row, add a couple of empty boxes;
                if (!$nextRow && (($j % $perRow) != 0)) {
                    for (; ($j % $perRow) != 0; $j++) {
                        $output .= '<td></td>';
                    }
                }

                if ($j % $perRow == 0) {
                    $output .= '</tr>';
                }
            }

            $row = $nextRow;
            $prevRecording = $rec;
        }// for i =1 to many

        $output .= '</table>';

        return $output;
    }

    private static function sortSortSelect($request, $showLocations)
    {
        $taxonomySelected = '';
        $englishSelected = '';
        $recordistSelected = '';
        $dateSelected = '';
        $timeSelected = '';
        $countrySelected = '';
        $locationSelected = '';
        $elevationSelected = '';
        $appearanceSelected = '';
        $lengthSelected = '';
        $qualitySelected = '';

        switch ($request->query->get('order')) {
            case 'en':
                $englishSelected = 'selected';
                break;
            case 'rec':
            case 'recordist':
                $recordistSelected = 'selected';
                break;
            case 'dt':
            case 'date':
                $dateSelected = 'selected';
                break;
            case 'tm':
            case 'time':
                $timeSelected = 'selected';
                break;
            case 'cnt':
            case 'country':
                $countrySelected = 'selected';
                break;
            case 'loc':
            case 'location':
                $locationSelected = 'selected';
                break;
            case 'elev':
            case 'elevation':
                $elevationSelected = 'selected';
                break;
            case 'xc':
            case 'appearance':
                $appearanceSelected = 'selected';
                break;
            case 'length':
                $lengthSelected = 'selected';
                break;
            case 'qual':
                $qualitySelected = 'selected';
                break;
            default:
                $taxonomySelected = 'selected';
                break;
        }

        $sortSelect = "
                <form class='sort-order-form' method='get'>" . _('Sort by') . ': ';

        $sortSelect .= "
            <select name='order'>
                <option value='tax' $taxonomySelected>" . _('Taxonomy') . "</option>
                <option value='en' $englishSelected>" . _('Common name') . "</option>
                <option value='length' $lengthSelected>" . _('Length') . "</option>
                <option value='rec' $recordistSelected>" . _('Recordist') . "</option>
                <option value='dt' $dateSelected>" . _('Date') . "</option>
                <option value='tm' $timeSelected>" . _('Time') . "</option>
                <option value='cnt' $countrySelected>" . _('Country') . '</option>';
        if ($showLocations) {
            $sortSelect .= "
                <option value='loc' $locationSelected>" . _('Location Name') . '</option>';
        }
        $sortSelect .= "
                <option value='elev' $elevationSelected>" . _('Elevation') . "</option>
                <option value='qual' $qualitySelected>" . _('Quality') . "</option>
                <option value='xc' $appearanceSelected>" . _('Catalog Number') . '</option>
            </select>';

        $normalSelected = '';
        $reverseSelected = '';
        if ($request->query->get('dir') === SortDirection::REVERSE) {
            $reverseSelected = 'selected';
        } else {
            $normalSelected = 'selected';
        }

        $sortSelect .= "
            <select name='dir'>
                <option value='" . SortDirection::NORMAL . "' $normalSelected>" . _('Normal') . "</option>
                <option value='" . SortDirection::REVERSE . "' $reverseSelected>" . _('Reverse') . '</option>
            </select>';
        foreach ($_GET as $key => $val) {
            if (!in_array($key, ['order', 'dir', 'pg'])) {
                $sortSelect .= "<input type='hidden' name='" . htmlentities($key) . "' value='" . htmlentities($val
                    ) . "'>";
            }
        }
        $sortSelect .= "
                <input type='hidden' name='pg' value='1'>
                <input type='submit' value='" . _('Submit') . "'>
            </form>";
        return $sortSelect;
    }

    private static function topTableRow($request, $view)
    {
        $toprow = '
            <thead>
              <tr>
              <th></th>
              ';

        // english (scientific) name
        $toprow .= "
            <th><a href=\"" . self::makeOrderUrl($request, 'en') . "\">" . _('Common name') . "</a> /
            <a href=\"" . self::makeOrderUrl($request, 'tax') . "\">" . _('Scientific') . "</a></th>
            <th><a href=\"" . self::makeOrderUrl($request, 'length') . "\" >" . _('Length') . "</a></th>
            <th><a href=\"" . self::makeOrderUrl($request, 'rec') . "\">" . _('Recordist') . '</a></th>';

        // date AND time
        if ($view == ViewType::DETAILED) {
            $toprow .= "
                <th><a href=\"" . self::makeOrderUrl($request, 'dt') . "\">" . _('Date') . '</a></th>';
            $toprow .= "
                <th><a href=\"" . self::makeOrderUrl($request, 'tm') . "\">" . _('Time') . '</a></th>';
        }

        // country, loc, elev
        $toprow .= "
            <th><a href=\"" . self::makeOrderUrl($request, 'cnt') . "\">" . _('Country') . '</a></th>';
        $toprow .= "
            <th><a href=\"" . self::makeOrderUrl($request, 'loc') . "\">" . _('Location') . '</a></th>';
        $toprow .= "
            <th><a href=\"" . self::makeOrderUrl($request, 'elev') . "\">" . _('Elev. (m)') . '</a></th>';

        // type, remarks, cat.nr
        if ($view == ViewType::DETAILED) {
            $toprow .= "
                <th>" . _('Type (predef. / other)') . '</th>
                <th>' . _('Remarks') . '</th>
                <th>' . _('Actions') . ' / <a href="' . self::makeOrderUrl($request, 'qual') . '">' . _('Quality'
                ) . '</a></th>';
        }

        $toprow .= "
            <th><a href=\"" . self::makeOrderUrl($request, 'xc') . "\">" . _('Cat.nr.') . '</a></th>
            </tr>
            </thead>';

        return $toprow;
    }

    private static function makeOrderUrl($request, $field)
    {
        $params = $request->query->all();
        $vars = [];
        if (isset($params['order']) and $params['order'] == $field) {
            if (isset($params['dir'])) {
                $vars['dir'] = !$params['dir'];
            } else {
                $vars['dir'] = SortDirection::REVERSE;
            }
        } else {
            $vars['dir'] = SortDirection::NORMAL;
        }
        $vars['order'] = $field;

        return Library::addUrlParamsToCurrentPage($request, $vars);
    }

    private static function rowDetailedConcise(
        $recording, $prevRecording, $view, $actionsCallback, $showLocations = true)
    {
        if (empty($prevRecording) || ($recording->commonName() == $prevRecording->commonName())) {
            $class = '';
        } else {
            $class = "class='new-species'";
        }

        $name = $recording->htmlDisplayName();
        $lic = License::lookupById($recording->license());

        $output = "
            <tr $class>
            <td>{$recording->miniPlayer()}</td>
            <td>$name </td>
            <td> {$recording->lengthString()}</td>
            <td> <a href='" . Library::getUrl('recordist', ['id' => $recording->recordistId()]) . "'>{$recording->recordist()}</a></td>
        ";

        if ($view == ViewType::DETAILED) {
            $output .= "
                <td>{$recording->date()}</td>
                <td> {$recording->time()}</td>
      ";
        }
        $output .= "
        <td>{$recording->country()}</td>";

        if (!$showLocations) {
            $output .= '<td>-</td>';
        } elseif ($recording->hasCoordinates()) {
            $output .= "
                <td><a href=\"{$recording->locationURL()}\">{$recording->location()}</a></td>";
        } else {
            $output .= "
                <td>{$recording->location()}</td>";
        }

        $output .= "<td>{$recording->elevation()}</td>";

        if ($view == ViewType::DETAILED) {
            $output .= "<td>{$recording->soundType()}</td>";
            $remarks = self::remarksDetailedView($recording);
            if (!empty($remarks)) {
                $output .= "<td>$remarks</td>";
            }

            $output .= '<td>';
            if (!$recording->species()->restricted()) {
                $output .= "<a href='{$recording->downloadURL()}'><img class='icon' src='/static/img/download.png' width='14' height='14' ";
                $output .= " title='" . sprintf(_('Download file %s'),
                        htmlspecialchars("'{$recording->suggestedFilename()}'", ENT_QUOTES)
                    ) . "'></a>";
            }

            $url = Library::getUrl('new_thread', ['xcid' => $recording->xcid()]);

            $output .= "<a class='tooltip' title='" . _('Open a discussion, mail the recordist'
                ) . "' href='$url'><img class='icon' width='14' height='14' src='/static/img/discuss.png' alt='discuss icon'/></a>";

            if ($actionsCallback) {
                $output .= call_user_func($actionsCallback, $recording);
            } else {
                $output .= self::defaultRecordingActions($recording);
            }

            // score song links if user is logged
            $output .= HtmlUtil::ratingWidget($recording->xcid(), $recording->rating());
            $output .= '</td>';
        }

        // the catalogue number
        $output .= "<td style='white-space: nowrap;'><a href=\"{$recording->URL()}\">XC{$recording->xcid()} <span title=\"$lic->name\"><a href=\"$lic->url\"><img class='icon' width='14' height='14' src='/static/img/cc.png'></a></span></td>
          ";

        $output .= '  
            </tr>
        ';

        return $output;
    }

    // Format text to show a summary of the search results

    private static function remarksDetailedView($recording)
    {
        // we add drop down boxes under various links in the
        // remarks. they are slowly collected below.
        $output = '';
        $remarks = $recording->printRemarks();

        if (!empty($remarks)) {
            $output .= "<div class='remarks readmore'>$remarks</div>";
        }

        $output .= self::linksToForum($recording);
        $output .= self::backgroundDropdownBox($recording);
        $output .= self::sonoLinkRemarks($recording);
        $output .= '</div>';

        return $output;
    }

    private static function linksToForum($recording)
    {
        $nr = $recording->xcid();
        $status = $recording->status();
        $output = '';

        // links to the forum
        if ($status !== ThreadType::NONE) {
            $res = Library::query("SELECT type, topic_nr FROM forum_world WHERE snd_nr = $nr");
            $row = $res->fetch_object();

            // this shouldn't happen, but...
            if (!$row) {
                return '';
            }

            $forumLink = "<a href='" . Library::getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]
                ) . "'>" . _('See the forum'
                ) . '</a>.';

            if ($status == ThreadType::ID_QUESTIONED) {
                $output .= '<strong>' . _('ID under discussion') . "</strong>. $forumLink";
            } elseif ($status == ThreadType::ID_RESOLVED) {
                $output .= '<strong>' . _('ID has been discussed and resolved') . "</strong>. $forumLink";
            } elseif ($status == ThreadType::MYSTERY) {
                $output .= '<strong>' . _('Mystery recording') . "</strong>. $forumLink";
            } elseif ($status == ThreadType::MYSTERY_RESOLVED) {
                $output .= '<strong>' . _('Originally a mystery recording') . "</strong>. $forumLink";
            } elseif ($status == ThreadType::ID_UNCONFIRMED) {
                $output .= '<strong>' . _('ID requires confirmation') . "</strong>. $forumLink";
            } else {
                $output .= '' . _('Recording (not its ID) has been discussed') . ". $forumLink";
            }
            return "<p>$output</p>";
        }
        return '';
    }

    private static function backgroundDropdownBox($recording)
    {
        $html = '';
        // the [also] background box
        $list = htmlspecialchars(self::getBackgroundList($recording), ENT_QUOTES);

        if ($list != '') {
            $html .= " <span class='tooltip' data-qtip-header='" . _('Other Species'
                ) . "' data-qtip-content='$list'>[" . _('also') . ']</span>
            ';
        }
        return $html;
    }

    private static function getBackgroundList($recording)
    {
        $html = '';
        $bgspecies = $recording->backgroundSpecies();
        $extra = $recording->backgroundExtra();

        if ($bgspecies || $extra) {
            $html .= '<ul>';
            for ($i = 0; $i < count($bgspecies); $i++) {
                $html .= "<li>" . $bgspecies[$i]->htmlDisplayName() . "</li>";
            }
            for ($i = 0; $i < count($extra); $i++) {
                $html .= '<li>' . $extra[$i] . '</li>';
            }
            $html .= '</ul>';
        }

        return $html;
    }

    private static function sonoLinkRemarks($recording)
    {
        $url = $recording->sonoURL(SonoSize::LARGE);
        $name = $recording->commonName();
        if (!$recording->isPseudoSpecies()) {
            $name .= " ({$recording->fullScientificName()})";
        }
        $sonoTitle = htmlspecialchars("XC{$recording->xcid()}: $name by {$recording->recordist()}", ENT_QUOTES
        );
        return "<a class='fancybox' rel='sono' href='$url' title='$sonoTitle'>[" . _('sono') . ']</a>';
    }

    private static function defaultRecordingActions($rec)
    {
        $user = User::current();
        $output = '';

        if ($user) {
            if ($user->isAdmin() || $user->isUploader($rec)) {
                $output .= "<a href='" . Library::getUrl('revise', ['snd_nr' => $rec->xcid()]
                    ) . "' title='" . _('Revise recording'
                    ) . "'><img class='icon' width='14' height='14' src='/static/img/edit.png' /></a>
                <a href='" . Library::getUrl('delete', ['snd_nr' => $rec->xcid()]) . "' title='" . _('Delete recording'
                    ) . "'><img class='icon' width='14' height='14' src='/static/img/delete.png' /></a>";
            }
            $output .= "<a href='" . Library::getUrl('recording-add-to-set', ['xcid' => $rec->xcid()]
                ) . "' title='" . _('Add to set'
                ) . "'><img class='icon' width='14' height='14' src='/static/img/bookmark-add.png' /></a>";
        }
        return $output;
    }

    public static function userTable($request, $alphabetRoute, $userRoute, $userRouteExtraParams = [])
    {
        $selected = Library::escape($request->query->get('q')) ?: '';
        if ($selected == '_') {
            $selected = '\_';
        }

        $letterSql = "
            SELECT DISTINCT UPPER(LEFT(username, 1)) AS letter 
            FROM users
            ORDER BY IF(letter RLIKE '^[a-z]', 1, 2), letter";
        $res = Library::query($letterSql);

        $alphabet = "<nav class='results-pages'>
            <p><ul>";
        if ($res && $res->num_rows > 0) {
            while ($row = $res->fetch_object()) {
                $letter = $row->letter;
                if (trim($letter) == '' || urlencode($letter) == '%E2%80%AA') {
                    if (trim($letter) == '') {
                        if (strtoupper($selected) == 'ALL') {
                            $alphabet .= "<li class='selected'><span>All</span></li>";
                        } else {
                            $alphabet .= "<li><a href='" . Library::getUrl($alphabetRoute, ['q' => 'all']
                                ) . "'>All</a></li>";
                        }
                    }
                    $letter = '&nbsp;&nbsp;';
                    $alphabet .= '</ul></p><p><ul>';
                }
                if (strtoupper($row->letter) == strtoupper($selected)) {
                    $alphabet .= "<li class='selected'><span>$letter</span></li>";
                } else {
                    $alphabet .= "<li><a href='" . Library::getUrl($alphabetRoute, ['q' => $row->letter]
                        ) . "'>$letter</a></li>";
                }
            }
        }
        $alphabet .= "</ul></p>
            </nav>";

        $table = "
            <table class='results'>
                <th>User name</th>
                <th>Email</th>
                <th>ID (links to user profile)</th>
                <th>Recordings</th>";

        $where = "WHERE U.username LIKE '$selected%'";
        if ($selected == 'all') {
            $where = '';
        } elseif (empty($selected)) {
            $where = "WHERE U.username = ''";
        }
        $userSql = "
            SELECT U.username, U.dir, U.email, COUNT(snd_nr) AS nrecs 
            FROM users U 
            LEFT JOIN birdsounds USING(dir) 
            $where
            GROUP BY dir 
            ORDER BY username ASC";
        $res = Library::query($userSql);
        while ($row = $res->fetch_object()) {
            $dir = '<a href="' . Library::getUrl('recordist', ['id' => $row->dir]) . '">' . $row->dir . '</a>';
            $name = !empty($row->username) ? Library::sanitize($row->username) : '[No user name provided]';
            $action = '<a href="' . Library::getUrl($userRoute, ['id' => $row->dir] + $userRouteExtraParams
                ) . '">' . $name . '</a>';
            $table .= "
                <tr>
                    <td>$action</td>
                    <td><a href='mailto:$row->email'>$row->email</a></td>
                    <td>$dir</td>
                    <td>$row->nrecs</td>
                </tr>";
        }
        $table .= '</table>';

        return $alphabet . $table;
    }

    public static function makeViewLinks($request, $view)
    {
        $detailedLink = Library::addUrlParamsToCurrentPage($request, ['view' => ViewType::DETAILED]);
        $conciseLink = Library::addUrlParamsToCurrentPage($request, ['view' => ViewType::CONCISE]);
        $sonogramsLink = Library::addUrlParamsToCurrentPage($request, ['view' => ViewType::SONOGRAMS]);
        $detailed = _('detailed');
        $concise = _('concise');
        $sonograms = _('sonograms');

        if ($view == ViewType::DETAILED || is_null($view)) {
            return "
                <ul>
                <li>$detailed</li>
                <li><a href=\"$conciseLink\">$concise</a></li>
                <li><a href=\"$sonogramsLink\">$sonograms</a></li>
                </ul>
            ";
        }
        if ($view == ViewType::CONCISE) {
            return "
                <ul>
                <li><a href=\"$detailedLink\">$detailed</a></li>
                <li>$concise</li>
                <li><a href=\"$sonogramsLink\">$sonograms</a></li>
                </ul>
            ";
        }
        if ($view == ViewType::SONOGRAMS) {
            return "
                <ul>
                <li><a href=\"$detailedLink\">$detailed</a></li>
                <li><a href=\"$conciseLink\">$concise</a></li>
                <li>$sonograms</li>
                </ul>
            ";
        }
    }
}
