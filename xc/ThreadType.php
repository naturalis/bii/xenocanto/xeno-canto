<?php

namespace xc;

class ThreadType
{

    public const NONE = 0;

    public const ID_QUESTIONED = 1;

    public const ID_RESOLVED = 10;

    public const MYSTERY = 2;

    public const MYSTERY_RESOLVED = 20;

    public const REPORT_RESOLVED = 30;

    public const ID_UNCONFIRMED = 3;

    public const RECORDING_DISCUSS = 21;

    public const ADMIN = 31;

    public const TAXONOMY = 32;

    public const REQUEST = 33;

    public const REQUEST_HANDLED = 40;

    public const BUG = 34;

    public const BUG_RESOLVED = 41;

    public const MAP = 42;

    public const MAP_RESOLVED = 43;

    public const HARDWARE = 35;

    public const SOFTWARE = 36;

    public const ID_GENERAL = 37;


    //const OTHER = 38;
}
