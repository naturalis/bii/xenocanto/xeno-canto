<?php

namespace xc;

class StaticMap
{
    private string $icon;
    private string $center;
    private string $size = "514x250";
    private int $zoom = 6;
    private string $anchor = "7,7";

    public function __construct($latitude = 0, $longitude = 0)
    {
        $this->icon = 'https://xeno-canto.org' . Marker::$icons[0];
        $this->center = "$latitude,$longitude";
    }

    public function getMapImage(): string
    {
        if (empty(GOOGLE_MAPS_SECRET_KEY)) {
            return 'Google secret key is not configured in GOOGLE_MAPS_SECRET_KEY env';
        }
        $mapParameters = [
            'center' => $this->center,
            'zoom' => $this->zoom,
            'size' => $this->size,
            'key' => GOOGLE_MAPS_API_KEY,
            'markers' => "anchor:$this->anchor|icon:$this->icon|$this->center"
        ];
        $unsignedUrl = 'https://maps.googleapis.com/maps/api/staticmap?' . http_build_query($mapParameters);

        return "<img src='" . $this->signUrl($unsignedUrl) . "'>";
    }

    public function setIcon($icon): void
    {
        $this->icon = $icon;
    }

    public function setSize($width, $height): void
    {
        $this->size = $width . 'x' . $height;
    }

    public function setAnchor($x, $y): void
    {
        $this->anchor = $x . ',' . $y;
    }

    public function setCenter($lat, $lon): void
    {
        $this->center = $lat . ',' . $lon;
    }

    public function setZoom($zoom): void
    {
        $this->zoom = intval($zoom);
    }

    private function encodeBase64UrlSafe($value): string
    {
        return str_replace(['+', '/'], ['-', '_'], base64_encode($value));
    }

    private function decodeBase64UrlSafe($value): string
    {
        return base64_decode(str_replace(['-', '_'], ['+', '/'], $value));
    }

    private function signUrl($urlToSign): string
    {
        $url = parse_url($urlToSign);
        $urlPartsToSign = $url['path'] . "?" . $url['query'];

        // Decode the private key into its binary format
        $decodedKey = $this->decodeBase64UrlSafe(GOOGLE_MAPS_SECRET_KEY);

        // Create a signature using the private key and the URL-encoded
        // string using HMAC SHA1. This signature will be binary.
        $signature = hash_hmac("sha1", $urlPartsToSign, $decodedKey, true);
        $encodedSignature = $this->encodeBase64UrlSafe($signature);

        return $urlToSign . "&signature=" . $encodedSignature;
    }
}
