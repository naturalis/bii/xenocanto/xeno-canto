<?php

namespace xc;

class LocationMap extends Map
{

    private $m_spec;

    public function __construct($mapCanvasId, $result, $cluster = true)
    {
        // just a single marker spec for all results
        $this->m_spec = new MarkerSpec();
        $this->m_spec->url = Marker::$icons[4];
        $this->m_spec->height = 14;
        $this->m_spec->width = 14;
        $this->m_spec->anchorX = 7;
        $this->m_spec->anchorY = 7;

        parent::__construct($mapCanvasId, $result, $cluster);
    }

    protected function createMarker($row)
    {
        if (!$row->latitude && !$row->longitude) {
            return null;
        }

        $marker = new Marker();
        $marker->spec = $this->m_spec;
        $marker->title = $row->location;
        $marker->animate = true;
        $marker->latitude = $row->latitude;
        $marker->longitude = $row->longitude;
        $marker->contentType = Marker::Location;
        $marker->zIndex = 50;

        return $marker;
    }
}
