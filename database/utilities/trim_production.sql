# Use this dump to trim and anonymise an existing XC production database
# All users except for the top five recordists plus two admins are deleted
# All recordings except for those from the persons above are deleted
# The remaining users are assigned a fake email address and a new password (user_name@test-xc.org; password: xc-user)
# A new admin with full permissions is added (admin@test-xc.org, password: xc-admin)



# Redundant tables
DROP TABLE IF EXISTS `annotations`;
DROP TABLE IF EXISTS `background`;
DROP TABLE IF EXISTS `bgcache`;
DROP TABLE IF EXISTS `bg_suggest`;
DROP TABLE IF EXISTS `BirdCLEF2014`;
DROP TABLE IF EXISTS `BirdCLEF2015`;
DROP TABLE IF EXISTS `BirdCLEF2015top1000`;
DROP TABLE IF EXISTS `BirdCLEF2017`;
DROP TABLE IF EXISTS `birdsounds_backup_20190814`;
DROP TABLE IF EXISTS `birdsounds_test`;
DROP TABLE IF EXISTS `Boesman`;
DROP TABLE IF EXISTS `cupaitem_t`;
DROP TABLE IF EXISTS `cutparts_t`;
DROP TABLE IF EXISTS `cuts_t`;
DROP TABLE IF EXISTS `country_t`;
DROP TABLE IF EXISTS `cuts_p`;
DROP TABLE IF EXISTS `departam_p`;
DROP TABLE IF EXISTS `datetry`;
DROP TABLE IF EXISTS `discussion_world`;
DROP TABLE IF EXISTS `distinct_taxa_birdsounds_all`;
DROP TABLE IF EXISTS `equipm_p`;
DROP TABLE IF EXISTS `families_t`;
DROP TABLE IF EXISTS `familybirdsoundsstatsWP`;
DROP TABLE IF EXISTS `familydbAstatsWP`;
DROP TABLE IF EXISTS `ffts_old`;
DROP TABLE IF EXISTS `ffts`;
DROP TABLE IF EXISTS `flickr_cache`;
DROP TABLE IF EXISTS `grid_uniq`;
DROP TABLE IF EXISTS `grid_uniq2`;
DROP TABLE IF EXISTS `grid_uniq3`;
DROP TABLE IF EXISTS `gridmap`;
DROP TABLE IF EXISTS `gridrange_species_lists`;
DROP TABLE IF EXISTS `gridranges`;
DROP TABLE IF EXISTS `growth`;
DROP TABLE IF EXISTS `infonatura`;
DROP TABLE IF EXISTS `joe_tmp`;
DROP TABLE IF EXISTS `lists`;
DROP TABLE IF EXISTS `locality_t`;
DROP TABLE IF EXISTS `mysteries2birdsounds`;
DROP TABLE IF EXISTS `paraguay_list`;
DROP TABLE IF EXISTS `people_t`;
DROP TABLE IF EXISTS `profilescoordinates`;
DROP TABLE IF EXISTS `quality_scores_per_recordist`;
DROP TABLE IF EXISTS `quality_scores`;
DROP TABLE IF EXISTS `samesongs`;
DROP TABLE IF EXISTS `similar_sounds`;
DROP TABLE IF EXISTS `sjoerd`;
DROP TABLE IF EXISTS `song_study_correct_taxa`;
DROP TABLE IF EXISTS `song_study_species`;
DROP TABLE IF EXISTS `song_study`;
DROP TABLE IF EXISTS `species_t`;
DROP TABLE IF EXISTS `spotlight`;
DROP TABLE IF EXISTS `ssporder_t`;
DROP TABLE IF EXISTS `subspec_t`;
DROP TABLE IF EXISTS `tapeitem_oud`;
DROP TABLE IF EXISTS `tapeitem_t`;
DROP TABLE IF EXISTS `tapes_t`;
DROP TABLE IF EXISTS `taxonomy_multilingual_next`;
DROP TABLE IF EXISTS `taxonomy_next`;
DROP TABLE IF EXISTS `taxonomy_ssp_next`;
DROP TABLE IF EXISTS `tmp`;
DROP TABLE IF EXISTS `training_data_IDkeys`;
DROP TABLE IF EXISTS `translations`;
DROP TABLE IF EXISTS `uicn`;
DROP TABLE IF EXISTS `users_march2014`;
DROP TABLE IF EXISTS `users2`;
DROP TABLE IF EXISTS `whole_list_scores`;
DROP TABLE IF EXISTS `worldgrowth`;


TRUNCATE TABLE `birdsounds_play_stats`;
TRUNCATE TABLE `birdsounds_trash`;
TRUNCATE TABLE `birdsounds_user_ratings`;
TRUNCATE TABLE `datasets_recordings`;
TRUNCATE TABLE `donations`;
TRUNCATE TABLE `mails_sent`;
TRUNCATE TABLE `notifications`;
TRUNCATE TABLE `permissions`;
TRUNCATE TABLE `raster_distributions`;
TRUNCATE TABLE `spammers`;
TRUNCATE TABLE `user_cookies`;
TRUNCATE TABLE `users_pruned`;


# Delete all but the top 5 recordists, plus WP
DELETE FROM `users` WHERE `dir` NOT IN ('OOECIWCSWV', 'BTOFEKXFGW', 'YTUXOCTUEM', 'LELYWQKUZX', 'PWDLINYMKL', 'AEFZWKWLMO');

# Anonymise users (password is set to "xc-user")
UPDATE `users` SET `email`=CONCAT(LOWER(REPLACE(`username`, ' ', '_')),'@test-xc.org'), `password`='c9066be199b02a1063e2f98007d656ba4cf01c27454221c676c5d2a392cdeb88', `blurb`='I am just a test user now', `verificationCode`="";

# Create XC admin (password is set to "xc-admin")
INSERT INTO `users` (`username`, `email`, `password`, `salt`, `dir`, `language`, `num_per_page`, `view`, `notification`, `downsample`, `blurb`, `region`, `license`, `joindate`, `verified`, `verificationCode`, `contactform`, `third_party_license`)
VALUES
	('XC admin', 'admin@test-xc.org', 'f8fd91b5c8b96e5caa402e66c141dd2750f9e3e3fc9822c9ded7a59c9d2730f7', 'rwupn', 'PTWHKIXGOF', 'en', 15, 0, 1, 0, 'I am an XC test admin', '', 'CC-by-nc-sa-4.0', NULL, 1, NULL, 1, 1);
INSERT INTO `permissions` (`userid`, `admin`, `canconfirm`, `largefiles`, `disablerating`, `downloadrestricted`)
VALUES
	('PTWHKIXGOF', 1, 1, 1, 1, 1);


# Delete orphaned recordings; this will take some time!
# First remove data from birdsounds; rest will depend on records in this table
DELETE FROM `birdsounds` WHERE `dir` NOT IN (SELECT `dir` FROM `users`);

# Sounds
DELETE t1 FROM audio_info t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;
DELETE t1 FROM birdsounds_background t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;
DELETE t1 FROM birdsounds_history t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;
DELETE t1 FROM birdsounds_properties t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;
DELETE t1 FROM latest_recordists t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;
DELETE t1 FROM mysteries_world t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;
DELETE t1 FROM sonograms t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;
DELETE t1 FROM total_play_stats t1 LEFT JOIN birdsounds t2 ON t1.snd_nr = t2.snd_nr WHERE t2.snd_nr IS NULL;

DELETE FROM `blogs` WHERE `dir` NOT IN (SELECT `dir` FROM `users`);
DELETE FROM `datasets` WHERE `userid` NOT IN (SELECT `dir` FROM `users`);
DELETE FROM `feature_discussions` WHERE `dir` NOT IN (SELECT `dir` FROM `users`);
DELETE FROM `rec_common_species` WHERE `userid` NOT IN (SELECT `dir` FROM `users`);
DELETE FROM `rec_countries` WHERE `userid` NOT IN (SELECT `dir` FROM `users`);
DELETE FROM `rec_summary_stats` WHERE `userid` NOT IN (SELECT `dir` FROM `users`);
DELETE FROM `rec_unique_species` WHERE `userid` NOT IN (SELECT `dir` FROM `users`);
DELETE FROM `user_search_languages` WHERE `userid` NOT IN (SELECT `dir` FROM `users`);
