CREATE TABLE `user_password_reset_codes` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `dir` varchar(10) DEFAULT NULL,
    `reset_code` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `dir` (`dir`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;