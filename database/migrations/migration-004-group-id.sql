-- Add group table and group_id column where relevant

CREATE TABLE `groups` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `dwca` tinyint(1) NOT NULL DEFAULT 0,
  `slow_factor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `groups` (`id`, `name`, `dwca`, `slow_factor`)
VALUES
	(1,'birds',0,null),
	(2,'grasshoppers',1,10);


ALTER TABLE `birdsounds` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1'  AFTER `snd_nr`;
ALTER TABLE `birdsounds_trash` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1'  AFTER `snd_nr`;
ALTER TABLE `birdsounds` ADD INDEX (`group_id`);
ALTER TABLE `birdsounds_trash` ADD INDEX (`group_id`);

ALTER TABLE `taxonomy` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1'  AFTER `species_nr`;
ALTER TABLE `taxonomy_multilingual` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1'  AFTER `species_nr`;
ALTER TABLE `taxonomy_ssp` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1'  AFTER `species_nr`;
ALTER TABLE `taxonomy` ADD INDEX (`group_id`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`group_id`);
ALTER TABLE `taxonomy_ssp` ADD INDEX (`group_id`);
ALTER TABLE `taxonomy_ssp` CHANGE `family_english` `family_english` VARCHAR(255)   NULL;

ALTER TABLE `latest_recordists` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1'  AFTER `snd_nr`;
ALTER TABLE `latest_species` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1'  AFTER `snd_nr`;
ALTER TABLE `latest_statistics` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `latest_recordists` DROP INDEX `userid_UNIQUE`;
ALTER TABLE `latest_recordists` ADD UNIQUE INDEX (`userid`, `group_id`);
ALTER TABLE `latest_species` ADD INDEX (`group_id`);

ALTER TABLE `stats_date` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `stats_date` ADD INDEX (`group_id`);

ALTER TABLE `wa_collection_stats` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `wa_collection_stats` DROP PRIMARY KEY;
ALTER TABLE `wa_collection_stats` ADD UNIQUE INDEX (`branch`, `group_id`);
ALTER TABLE `wa_new_species` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `wa_new_species` ADD INDEX (`group_id`);
ALTER TABLE `wa_countries` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `wa_countries` ADD INDEX (`group_id`);
ALTER TABLE `wa_recordists` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `wa_recordists` ADD INDEX (`group_id`);
ALTER TABLE `wa_common_species` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `wa_common_species` ADD INDEX (`group_id`);

ALTER TABLE `rec_summary_stats` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `rec_summary_stats` DROP PRIMARY KEY;
ALTER TABLE `rec_summary_stats` ADD UNIQUE INDEX (`userid`, `group_id`);
ALTER TABLE `rec_common_species` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `rec_common_species` ADD INDEX (`group_id`);
ALTER TABLE `rec_countries` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `rec_countries` ADD INDEX (`group_id`);
ALTER TABLE `rec_unique_species` ADD `group_id` TINYINT  NOT NULL  DEFAULT '1';
ALTER TABLE `rec_unique_species` ADD INDEX (`group_id`);

-- Correct group_id and order name
UPDATE taxonomy SET `order` = 'ORTHOPTERA', group_id = 2 WHERE  `order` = 'ORTHOPTERIFORMES';
UPDATE taxonomy SET group_id = 0 WHERE species_nr IN ('envrec', 'mysmys');
UPDATE birdsounds SET group_id = 2 WHERE species_nr IN (SELECT species_nr FROM taxonomy WHERE group_id = 2);
UPDATE taxonomy set group_id = 0 WHERE species_nr IN ('mysmys', 'envrec');

-- Unrelated but why not
UPDATE users SET `view` = 0 WHERE `view` = 2;

-- Fix stats tables!
DROP TABLE IF EXISTS `latest_statistics`;
CREATE TABLE `latest_statistics` (
    `nr_recordings` int(11) NOT NULL,
    `total_duration` int(11) NOT NULL,
    `nr_recordists` int(11) NOT NULL,
    `nr_species` int(11) NOT NULL,
    `nr_ssp` int(11) NOT NULL DEFAULT 0,
    `nr_gbif` int(11) NOT NULL,
    `group_id` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `latest_species`;
CREATE TABLE `latest_species` (
    `species_nr` varchar(10) NOT NULL,
    `snd_nr` int(11) NOT NULL,
    `group_id` tinyint(4) NOT NULL DEFAULT 1,
    UNIQUE KEY `species_nr_UNIQUE` (`species_nr`),
    KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `latest_recordists`;
CREATE TABLE `latest_recordists` (
    `userid` varchar(20) NOT NULL,
    `snd_nr` int(11) NOT NULL,
    `group_id` tinyint(4) NOT NULL DEFAULT 1,
    UNIQUE KEY `userid` (`userid`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;