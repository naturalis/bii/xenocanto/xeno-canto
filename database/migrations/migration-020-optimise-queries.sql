ALTER TABLE `birdsounds_background` ADD INDEX (`scientific`);
ALTER TABLE `birdsounds_background` ADD INDEX (`english`);
ALTER TABLE `birdsounds_background` ADD INDEX (`family`);
ALTER TABLE `birdsounds_background` ADD FULLTEXT INDEX `scientific_ft` (`scientific`);
ALTER TABLE `birdsounds_background` ADD FULLTEXT INDEX `english_ft` (`english`);

ALTER TABLE `birdsounds` ADD INDEX (`family`);
ALTER TABLE `birdsounds` ADD INDEX (`ssp`);
ALTER TABLE `birdsounds` ADD FULLTEXT INDEX `eng_name_ft` (`eng_name`);
ALTER TABLE `birdsounds` ADD FULLTEXT INDEX `back_extra_ft` (`back_extra`);
ALTER TABLE `birdsounds` ADD FULLTEXT INDEX `remarks_ft` (`remarks`);
ALTER TABLE `birdsounds` ADD INDEX `genus_species_ssp` (`genus`, `species`, `ssp`);
