DROP TABLE IF EXISTS `country_t`;
DROP TABLE IF EXISTS `cuts_p`;
DROP TABLE IF EXISTS `departam_p`;
DROP TABLE IF EXISTS `enjoynature`;
DROP TABLE IF EXISTS `mysteries_world`;
DROP TABLE IF EXISTS `spam_forum`;
DROP TABLE IF EXISTS `spotlights`;
DROP TABLE IF EXISTS `tips`;


ALTER TABLE `birdsounds` MODIFY `snd_nr` int(11) NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `birdsounds` DROP IF EXISTS `background`;
ALTER TABLE `birdsounds` DROP IF EXISTS `back_nrs`;
ALTER TABLE `birdsounds` DROP IF EXISTS `back_english`;
ALTER TABLE `birdsounds` DROP IF EXISTS `back_latin`;
ALTER TABLE `birdsounds` DROP IF EXISTS `back_families`;
ALTER TABLE `birdsounds` DROP KEY IF EXISTS `snd_nr_2`;
ALTER TABLE `birdsounds` DROP KEY IF EXISTS `datetime_2`;
ALTER TABLE `birdsounds` DROP KEY IF EXISTS `observed_2`;
ALTER TABLE `birdsounds` DROP KEY IF EXISTS `playback_2`;
ALTER TABLE `birdsounds` DROP KEY IF EXISTS `automatic_2`;
ALTER TABLE `birdsounds` DROP KEY IF EXISTS `temperature_2`;

ALTER TABLE `birdsounds_play_stats` ADD INDEX (`date`);

ALTER TABLE `blogs` MODIFY COLUMN `blognr` INT(11)  NOT NULL  AUTO_INCREMENT FIRST;
ALTER TABLE `blogs` ADD INDEX (`dir`);

ALTER TABLE `discuss_news_world` MODIFY `nr` int(11) NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `discuss_news_world` MODIFY `news_nr` int(11) NOT NULL DEFAULT 0 AFTER `nr`;

ALTER TABLE `discussion_forum_world` MODIFY COLUMN `mes_nr` INT(11)  NOT NULL  AUTO_INCREMENT FIRST;
ALTER TABLE `discussion_forum_world` MODIFY COLUMN `topic_nr` INT(6)  NOT NULL  DEFAULT 0 AFTER `mes_nr`;
ALTER TABLE `discussion_forum_world` CHANGE `branch` `branch` VARCHAR(10) NOT NULL DEFAULT '';

ALTER TABLE `feature_discussions` MODIFY COLUMN `mes_nr` INT(11)  NOT NULL  AUTO_INCREMENT FIRST;
ALTER TABLE `feature_discussions` MODIFY COLUMN `blognr` INT(6)  NOT NULL  DEFAULT 0 AFTER `mes_nr`;
ALTER TABLE `feature_discussions` ADD INDEX (`blognr`);

ALTER TABLE `forum_world` MODIFY COLUMN `topic_nr` INT(11)  NOT NULL  AUTO_INCREMENT FIRST;

ALTER TABLE `news_world` MODIFY COLUMN `news_nr` INT(11)  NOT NULL  AUTO_INCREMENT FIRST;

ALTER TABLE `newstuff` MODIFY COLUMN `nr` INT(9)  NOT NULL  AUTO_INCREMENT FIRST;

ALTER TABLE `notifications` MODIFY COLUMN `id_nr` INT(11)  NOT NULL  AUTO_INCREMENT FIRST;

ALTER TABLE `sonogram_queue` DROP INDEX IF EXISTS `snd_nr_UNIQUE`;

ALTER TABLE `sonograms` DROP INDEX IF EXISTS `snd_nr_UNIQUE`;

ALTER TABLE `spotlight_v2` ADD INDEX (`featured_recording`);
ALTER TABLE `spotlight_v2` ADD INDEX (`enabled`);

ALTER TABLE `total_play_stats` DROP INDEX IF EXISTS `snd_nr_UNIQUE`;

ALTER TABLE `users` DROP INDEX IF EXISTS `dir`;
ALTER TABLE `users` MODIFY COLUMN `dir` VARCHAR(20)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '' FIRST;

ALTER TABLE `users_pruned` DROP INDEX IF EXISTS `dir`;
ALTER TABLE `users_pruned` DROP INDEX IF EXISTS `username`;
ALTER TABLE `users_pruned` DROP INDEX IF EXISTS `third_party_license`;
ALTER TABLE `users_pruned` MODIFY COLUMN `dir` VARCHAR(20)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '' FIRST;
