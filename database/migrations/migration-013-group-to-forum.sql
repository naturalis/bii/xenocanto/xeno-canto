-- Ouch, wrong date format used in some tables, sometime intermixed!
UPDATE feature_discussions SET `date` = STR_TO_DATE(`date`,'%d-%m-%Y') WHERE SUBSTRING(`date`, 3, 1) = '-';
UPDATE discussion_forum_world SET `date` = STR_TO_DATE(`date`,'%d-%m-%Y') WHERE SUBSTRING(`date`, 3, 1) = '-';

-- Add group_id and missing indices
ALTER TABLE `forum_world` ADD `group_id` TINYINT(4)  NULL  DEFAULT 0  AFTER `last_activity`;
ALTER TABLE `forum_world` DROP INDEX `topic_nr`;
ALTER TABLE `forum_world` ADD INDEX (`snd_nr`);
ALTER TABLE `forum_world` ADD INDEX (`group_id`);
ALTER TABLE `forum_world` ADD INDEX (`userid`);
ALTER TABLE `forum_world` ADD INDEX (`branch`);

UPDATE `forum_world` SET group_id = 0;
UPDATE `forum_world` AS t1
    LEFT JOIN `birdsounds` AS t2 ON t1.snd_nr = t2.snd_nr
    SET t1.group_id = IFNULL(t2.group_id, 0)
    WHERE t1.snd_nr != 0;

ALTER TABLE `discussion_forum_world` DROP INDEX `mes_nr`;
ALTER TABLE `discussion_forum_world` ADD INDEX (`topic_nr`);
ALTER TABLE `discussion_forum_world` ADD INDEX (`dir`);

ALTER TABLE `feature_discussions` DROP INDEX `mes_nr`;
ALTER TABLE `feature_discussions` ADD INDEX (`dir`);


