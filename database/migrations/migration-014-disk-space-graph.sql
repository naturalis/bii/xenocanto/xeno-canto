ALTER TABLE `audio_info` DROP INDEX `snd_nr`;
ALTER TABLE `audio_info` DROP INDEX `snd_nr_UNIQUE`;
ALTER TABLE `audio_info` ADD INDEX (`format`);

ALTER TABLE `stats_date` DROP IF EXISTS  `disk_space_mb`;

ALTER TABLE `stats_date` ADD `disk_space_wav` INT  NULL  DEFAULT NULL  AFTER `group_id`;
ALTER TABLE `stats_date` ADD `disk_space_mp3` INT  NULL  DEFAULT NULL  AFTER `group_id`;