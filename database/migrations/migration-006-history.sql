ALTER TABLE `birdsounds` ADD `form_data` TEXT  NULL;
ALTER TABLE `birdsounds_history` ADD `form_data` TEXT  NULL;
ALTER TABLE `birdsounds_trash` ADD `form_data` TEXT  NULL;

DROP TRIGGER IF EXISTS `sound_updated`;

DELIMITER |

CREATE
    TRIGGER `sound_updated` BEFORE UPDATE ON `birdsounds`
    FOR EACH ROW
BEGIN
    IF (
        NEW.genus <> OLD.genus OR
        NEW.species <> OLD.species OR
        NEW.ssp <> OLD.ssp OR
        NEW.eng_name <> OLD.eng_name OR
        NEW.songtype <> OLD.songtype OR
        NEW.recordist <> OLD.recordist OR
        COALESCE(NEW.longitude,9999) <> COALESCE(OLD.longitude,9999) OR
        COALESCE(NEW.latitude,9999) <> COALESCE(OLD.latitude,9999) OR
        NEW.remarks <> OLD.remarks OR
        NEW.license <> OLD.license OR
        NEW.form_data <> OLD.form_data
    )
    THEN
        INSERT INTO birdsounds_history VALUES (
            DEFAULT,
            NOW(),
            OLD.snd_nr,
            OLD.genus,
            OLD.species,
            OLD.ssp,
            OLD.eng_name,
            OLD.songtype,
            OLD.recordist,
            OLD.longitude,
            OLD.latitude,
            OLD.remarks,
            OLD.license,
            OLD.form_data
        );
    END IF;
END;

|

DELIMITER ;