-- Table to store missing sounds as detected by a script

CREATE TABLE  IF NOT EXISTS `birdsounds_missing` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `snd_nr` int(11) NOT NULL,
  `checked` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reason` varchar(300) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;