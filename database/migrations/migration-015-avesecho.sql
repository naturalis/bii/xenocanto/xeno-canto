# Dump of table ai_accepted_log
# ------------------------------------------------------------

CREATE TABLE `ai_accepted_log` (
   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
   `snd_nr` int(11) NOT NULL,
   `json_id` int(11) NOT NULL,
   `species_nr` varchar(10) NOT NULL,
   `species` varchar(100) NOT NULL,
   `dir` varchar(11) DEFAULT NULL,
   `username` varchar(80) DEFAULT NULL,
   `type` varchar(10) NOT NULL,
   `model_version` varchar(75) NOT NULL,
   `model_tag` varchar(75) NOT NULL,
   `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
   PRIMARY KEY (`id`),
   KEY `snd_nr` (`snd_nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `ai_error_log` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `snd_nr` int(11) NOT NULL,
    `message` text NOT NULL,
    `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `snd_nr` (`snd_nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

# Dump of table ai_queue
# ------------------------------------------------------------

CREATE TABLE `ai_queue` (
    `snd_nr` int(11) unsigned NOT NULL,
    `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    PRIMARY KEY (`snd_nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table ai_results_json
# ------------------------------------------------------------

CREATE TABLE `ai_results_json` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `snd_nr` int(11) NOT NULL,
   `json` longtext NOT NULL,
   `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
   PRIMARY KEY (`id`),
   KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table ai_results_regions
# ------------------------------------------------------------

CREATE TABLE `ai_results_regions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `snd_nr` int(11) NOT NULL,
  `json_id` int(11) NOT NULL,
  `model_tag` varchar(75) NOT NULL,
  `model_version` varchar(75) NOT NULL,
  `species` varchar(100) NOT NULL,
  `species_nr` varchar(10) NOT NULL,
  `time_start` int(6) NOT NULL,
  `time_end` int(6) NOT NULL,
  `probability` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `snd_nr` (`snd_nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table ai_results_summaries
# ------------------------------------------------------------

CREATE TABLE `ai_results_summaries` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `snd_nr` int(11) NOT NULL,
    `json_id` int(11) NOT NULL,
    `model_tag` varchar(75) NOT NULL,
    `model_version` varchar(75) NOT NULL,
    `species` varchar(100) NOT NULL,
    `species_nr` varchar(10) NOT NULL,
    `probability` float NOT NULL,
    `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `snd_nr` (`snd_nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


# Add to birdsounds

ALTER TABLE `birdsounds` ADD `ai_result` TINYINT(1) NULL DEFAULT -1  AFTER `automatic`;
ALTER TABLE `birdsounds` ADD INDEX (`ai_result`);
UPDATE `birdsounds` SET `ai_result` = 0 WHERE snd_nr IN (SELECT DISTINCT snd_nr FROM ai_results_json);
UPDATE `birdsounds` SET `ai_result` = 1 WHERE snd_nr IN (SELECT DISTINCT snd_nr FROM ai_results_summaries);
