ALTER TABLE `datasets_recordings` ADD INDEX (`datasetid`);


ALTER TABLE `birdsounds_trash` DROP INDEX `snd_nr_2`;
ALTER TABLE `birdsounds_trash` DROP INDEX `quality`;
ALTER TABLE `birdsounds_trash` DROP INDEX `neotropics`;
ALTER TABLE `birdsounds_trash` DROP INDEX `country`;
ALTER TABLE `birdsounds_trash` DROP INDEX `africa`;
ALTER TABLE `birdsounds_trash` DROP INDEX `asia`;
ALTER TABLE `birdsounds_trash` DROP INDEX `license`;
ALTER TABLE `birdsounds_trash` DROP INDEX `europe`;

ALTER TABLE `birdsounds_trash` ADD INDEX (`delete_date`);
ALTER TABLE `birdsounds_trash` ADD INDEX (`delete_user`);
