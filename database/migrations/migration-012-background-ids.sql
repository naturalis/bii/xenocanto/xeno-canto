CREATE TABLE `rec_summary_stats_next` (
    `userid` varchar(15) NOT NULL,
    `nrecordings` int(11) NOT NULL DEFAULT 0,
    `nspecies` int(11) NOT NULL DEFAULT 0,
    `nunique` int(11) NOT NULL DEFAULT 0,
    `length` double NOT NULL DEFAULT 0,
    `ncountries` int(11) NOT NULL DEFAULT 0,
    `nlocations` int(11) NOT NULL DEFAULT 0,
    `ncomments` int(11) NOT NULL DEFAULT 0,
    `group_id` tinyint(4) NOT NULL DEFAULT 1,
    `nbackids` int(11) NOT NULL DEFAULT 0,
    UNIQUE KEY `userid` (`userid`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO `rec_summary_stats_next` (SELECT *, 0 FROM rec_summary_stats);
DROP TABLE `rec_summary_stats`;
RENAME TABLE `rec_summary_stats_next` TO `rec_summary_stats`;


CREATE TABLE `latest_statistics_next` (
    `nr_recordings` int(11) NOT NULL DEFAULT 0,
    `total_duration` int(11) NOT NULL DEFAULT 0,
    `nr_recordists` int(11) NOT NULL DEFAULT 0,
    `nr_species` int(11) NOT NULL DEFAULT 0,
    `nr_ssp` int(11) NOT NULL DEFAULT 0,
    `nr_gbif` int(11) NOT NULL DEFAULT 0,
    `group_id` tinyint(4) NOT NULL DEFAULT 1,
    `nr_ids` int(11) NOT NULL DEFAULT 0,
    `nr_backids` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO `latest_statistics_next` (SELECT *, 0, 0 FROM latest_statistics);
DROP TABLE `latest_statistics`;
RENAME TABLE `latest_statistics_next` TO `latest_statistics`;