# ************************************************************
# Sequel Ace SQL dump
# Version 20062
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.5.24-MariaDB-1:10.5.24+maria~ubu2004)
# Database: xenocanto
# Generation Time: 2024-04-08 08:54:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table group_sound_properties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_sound_properties`;

CREATE TABLE `group_sound_properties` (
  `group_id` int(11) unsigned NOT NULL,
  `property_id` int(11) unsigned NOT NULL,
  `sort_order` tinyint(3) NOT NULL DEFAULT 99,
  UNIQUE KEY `group_id` (`group_id`,`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `group_sound_properties` WRITE;
/*!40000 ALTER TABLE `group_sound_properties` DISABLE KEYS */;

INSERT INTO `group_sound_properties` (`group_id`, `property_id`, `sort_order`)
VALUES
	(1,1,1),
	(1,2,2),
	(1,3,1),
	(1,4,2),
	(1,5,3),
	(1,7,1),
	(1,8,2),
	(1,9,3),
	(1,10,4),
	(1,11,5),
	(1,12,6),
	(1,13,7),
	(1,14,8),
	(1,15,9),
	(1,16,10),
	(1,17,99),
	(1,18,98),
	(1,24,1),
	(1,25,2),
	(1,26,3),
	(1,28,0),
	(1,29,0),
	(1,30,0),
	(1,31,6),
	(2,1,1),
	(2,2,2),
	(2,3,1),
	(2,6,2),
	(2,17,99),
	(2,19,1),
	(2,20,2),
	(2,21,3),
	(2,22,4),
	(2,23,5),
	(2,24,1),
	(2,27,2),
	(2,28,0),
	(2,29,0),
	(2,30,0),
	(2,31,6),
	(2,32,6),
	(2,33,4),
	(3,1,1),
	(3,2,2),
	(3,3,1),
	(3,4,3),
	(3,7,2),
	(3,24,1),
	(3,25,10),
	(3,28,0),
	(3,29,0),
	(3,30,0),
	(3,31,99),
	(3,34,3),
	(3,35,4),
	(3,36,3),
	(3,37,5),
	(3,38,2),
	(3,39,3),
	(3,40,4),
	(3,41,2),
	(3,42,3),
	(3,43,11),
	(3,47,12),
	(4,1,1),
	(4,2,2),
	(4,3,1),
	(4,4,2),
	(4,17,6),
	(4,24,1),
	(4,25,2),
	(4,27,3),
	(4,28,3),
	(4,29,3),
	(4,30,0),
	(4,31,4),
	(4,40,4),
	(4,44,1),
	(4,45,5),
	(4,46,3);

/*!40000 ALTER TABLE `group_sound_properties` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `dwca` tinyint(1) NOT NULL DEFAULT 0,
  `slow_factor` int(11) DEFAULT NULL,
  `gbif` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `dwca`, `slow_factor`, `gbif`)
VALUES
	(1,'birds',0,NULL,'https://www.gbif.org/dataset/b1047888-ae52-4179-9dd5-5448ea342a24'),
	(2,'grasshoppers',1,10,'https://www.gbif.org/dataset/ecce3417-d4fd-4fab-9009-09ad3f3f62b3'),
	(3,'bats',2,10,'https://www.gbif.org/dataset/6bd8ad26-218d-4657-8549-c30d69356664'),
	(4,'frogs',0,NULL,NULL);

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sound_properties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sound_properties`;

CREATE TABLE `sound_properties` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `property` varchar(50) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `sound_properties` WRITE;
/*!40000 ALTER TABLE `sound_properties` DISABLE KEYS */;

INSERT INTO `sound_properties` (`id`, `property`, `category_id`)
VALUES
	(1,'male',1),
	(2,'female',1),
	(3,'adult',2),
	(4,'juvenile',2),
	(5,'nestling',2),
	(6,'nymph',2),
	(7,'song',3),
	(8,'subsong',3),
	(9,'call',3),
	(10,'alarm call',3),
	(11,'flight call',3),
	(12,'nocturnal flight call',3),
	(13,'begging call',3),
	(14,'drumming',3),
	(15,'duet',3),
	(16,'dawn song',3),
	(17,'aberrant',3),
	(18,'imitation',3),
	(19,'calling song',3),
	(20,'courtship song',3),
	(21,'rivalry song',3),
	(22,'disturbance song',3),
	(23,'flight song',3),
	(24,'field recording',4),
	(25,'in the hand',4),
	(26,'in net',4),
	(27,'studio recording',4),
	(28,'uncertain',1),
	(29,'uncertain',2),
	(30,'uncertain',3),
	(31,'unknown',4),
	(32,'searching song',3),
	(33,'female song',3),
	(34,'roosting',4),
	(35,'in enclosure',4),
	(36,'emerging from roost',4),
	(37,'roped',4),
	(38,'echolocation',3),
	(39,'social call',3),
	(40,'distress call',3),
	(41,'subadult',2),
	(42,'fluorescent light tag',4),
	(43,'hand-release',4),
	(44,'advertisement call',3),
	(45,'release call',3),
	(46,'territorial call',3),
	(47,'feeding buzz',3);

/*!40000 ALTER TABLE `sound_properties` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sound_property_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sound_property_categories`;

CREATE TABLE `sound_property_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL DEFAULT '',
  `tag` varchar(15) DEFAULT NULL,
  `multiple` tinyint(1) DEFAULT 1,
  `sort_order` tinyint(3) NOT NULL DEFAULT 99,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `sound_property_categories` WRITE;
/*!40000 ALTER TABLE `sound_property_categories` DISABLE KEYS */;

INSERT INTO `sound_property_categories` (`id`, `category`, `tag`, `multiple`, `sort_order`)
VALUES
	(1,'sex','sex',1,1),
	(2,'life stage','stage',1,2),
	(3,'sound type','type',1,3),
	(4,'recording method','method',0,5);

/*!40000 ALTER TABLE `sound_property_categories` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
