<?php

require_once 'checklists-common.php';

$db = new JDB();
$db->query('DROP TABLE IF EXISTS checklists_branch');
$db->query(
    'CREATE  TABLE `checklists_branch` (
    `species_nr` VARCHAR(10) NOT NULL ,
    `branch` VARCHAR(50) NOT NULL ,
    UNIQUE INDEX `spperbranch` (`species_nr` ASC, `branch` ASC) ,
    INDEX `branch` (`branch` ASC) )'
);

$files = [
    'america'   => 'tools/checklists/area-america-3.3.csv',
    'asia'      => 'tools/checklists/area-asia-3.3.csv',
    'africa'    => 'tools/checklists/area-africa-3.3.csv',
    'australia' => 'tools/checklists/area-australia-3.3.csv',
    'europe'    => 'tools/checklists/area-europe-3.3.csv',
];

populateChecklists($files, 'checklists_branch');
