<?php

$renames =
    [
    ];
$splits  =
    [
        [
            ['Muscicapa', 'striata', 'balearica'],
            [
                'Muscicapa',
                'tyrrhenica',
                'balearica',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'flammeus'],
            [
                'Pyrocephalus',
                'obscurus',
                'flammeus',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'mexicanus'],
            [
                'Pyrocephalus',
                'obscurus',
                'mexicanus',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'blatteus'],
            [
                'Pyrocephalus',
                'obscurus',
                'blatteus',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'pinicola'],
            [
                'Pyrocephalus',
                'obscurus',
                'pinicola',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'saturatus'],
            [
                'Pyrocephalus',
                'obscurus',
                'saturatus',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'piurae'],
            [
                'Pyrocephalus',
                'obscurus',
                'piurae',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'ardens'],
            [
                'Pyrocephalus',
                'obscurus',
                'ardens',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'cocachacrae'],
            [
                'Pyrocephalus',
                'obscurus',
                'cocachacrae',
            ],
        ],
        [
            ['Heliangelus', 'amethysticollis', 'violiceps'],
            [
                'Heliangelus',
                'clarisse',
                'violiceps',
            ],
        ],
        [
            ['Heliangelus', 'amethysticollis', 'verdiscutus'],
            [
                'Heliangelus',
                'clarisse',
                'verdiscutus',
            ],
        ],
        [
            ['Arremon', 'flavirostris', 'devillii'],
            [
                'Arremon',
                'flavirostris',
                'polionotus',
            ],
        ],
        [
            ['Cercotrichas', 'coryphoeus', 'abbotti'],
            [
                'Cercotrichas',
                'coryphoeus',
                'abboti',
            ],
        ],
        [
            ['Cyphorhinus', 'arada', 'urbanoi'],
            [
                'Cyphorhinus',
                'arada',
                'arada',
            ],
        ],
        [
            ['Cyphorhinus', 'arada', 'faroensis'],
            [
                'Cyphorhinus',
                'arada',
                'arada',
            ],
        ],
        [['Tyto', 'alba', 'javanica'], ['Tyto', 'javanica', 'javanica']],
        [['Tyto', 'alba', 'stertens'], ['Tyto', 'javanica', 'stertens']],
        [
            ['Tyto', 'delicatula', 'sumbaensis'],
            [
                'Tyto',
                'javanica',
                'sumbaensis',
            ],
        ],
        [['Tyto', 'delicatula', 'meeki'], ['Tyto', 'javanica', 'meeki']],
        [
            ['Tyto', 'delicatula', 'delicatula'],
            [
                'Tyto',
                'javanica',
                'delicatula',
            ],
        ],
        [
            ['Tyto', 'delicatula', 'crassirostris'],
            [
                'Tyto',
                'javanica',
                'crassirostris',
            ],
        ],
        [
            ['Tyto', 'delicatula', 'interposita'],
            [
                'Tyto',
                'javanica',
                'interposita',
            ],
        ],
        [
            ['Psophia', 'leucoptera', 'ochroptera'],
            [
                'Psophia',
                'crepitans',
                'ochroptera',
            ],
        ],
        [
            ['Geospiza', 'conirostris', 'propinqua'],
            [
                'Geospiza',
                'propinqua',
                '',
            ],
        ],
        [
            ['Geospiza', 'conirostris', 'conirostris'],
            [
                'Geospiza',
                'conirostris',
                '',
            ],
        ],
        [
            ['Geospiza', 'difficilis', 'septentrionalis'],
            [
                'Geospiza',
                'septentrionalis',
                '',
            ],
        ],
        [
            ['Geospiza', 'difficilis', 'acutirostris'],
            [
                'Geospiza',
                'acutirostris',
                '',
            ],
        ],
        [
            ['Muscicapa', 'striata', 'tyrrhenica'],
            [
                'Muscicapa',
                'tyrrhenica',
                'tyrrhenica',
            ],
        ],
        [
            ['Calliope', 'pectoralis', 'tschebaiewi'],
            [
                'Calliope',
                'tschebaiewi',
                '',
            ],
        ],
        [
            ['Calliope', 'pectoralis', 'pectoralis'],
            [
                'Calliope',
                'pectoralis',
                'pectoralis',
            ],
        ],
        [
            ['Melanopareia', 'torquata', 'bitorquata'],
            [
                'Melanopareia',
                'bitorquata',
                '',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'obscurus'],
            [
                'Pyrocephalus',
                'obscurus',
                'obscurus',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'nanus'],
            [
                'Pyrocephalus',
                'nanus',
                '',
            ],
        ],
        [
            ['Pyrocephalus', 'rubinus', 'dubius'],
            [
                'Pyrocephalus',
                'dubius',
                '',
            ],
        ],
        [
            ['Schistes', 'geoffroyi', 'albogularis'],
            [
                'Schistes',
                'albogularis',
                '',
            ],
        ],
        [
            ['Schistes', 'geoffroyi', 'geoffroyi'],
            [
                'Schistes',
                'geoffroyi',
                'geoffroyi',
            ],
        ],
        [
            ['Heliangelus', 'amethysticollis', 'clarisse'],
            [
                'Heliangelus',
                'clarisse',
                'clarisse',
            ],
        ],
        [
            ['Heliangelus', 'amethysticollis', 'spencei'],
            [
                'Heliangelus',
                'spencei',
                '',
            ],
        ],
        [
            ['Leucocarbo', 'chalconotus', 'chalconotus'],
            [
                'Leucocarbo',
                'chalconotus',
                '',
            ],
        ],
        [
            ['Leucocarbo', 'chalconotus', 'stewarti'],
            [
                'Leucocarbo',
                'stewarti',
                '',
            ],
        ],
        [
            ['Psophia', 'leucoptera', 'leucoptera'],
            [
                'Psophia',
                'leucoptera',
                '',
            ],
        ],
    ];
$lumps   =
    [
    ];
