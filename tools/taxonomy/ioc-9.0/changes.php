<?php

$renames =
    [
        [['Automolus', 'rubiginosus'], ['Clibanornis', 'rubiginosus']],
        [['Automolus', 'rufipectus'], ['Clibanornis', 'rufipectus']],
        [['Babax', 'woodi'], ['Pterorhinus', 'woodi']],
        [['Calidris', 'pygmea'], ['Calidris', 'pygmaea']],
        [['Crateroscelis', 'nigrorufa'], ['Aethomyias', 'nigrorufus']],
        [['Crocias', 'albonotatus'], ['Laniellus', 'albonotatus']],
        [['Crocias', 'langbianis'], ['Laniellus', 'langbianis']],
        [['Garrulax', 'berthemyi'], ['Pterorhinus', 'berthemyi']],
        [['Garrulax', 'bieti'], ['Ianthocincla', 'bieti']],
        [['Garrulax', 'cinereifrons'], ['Argya', 'cinereifrons']],
        [['Garrulax', 'delesserti'], ['Pterorhinus', 'delesserti']],
        [['Garrulax', 'galbanus'], ['Pterorhinus', 'galbanus']],
        [['Garrulax', 'gularis'], ['Pterorhinus', 'gularis']],
        [
            ['Garrulax', 'konkakinhensis'],
            [
                'Ianthocincla',
                'konkakinhensis',
            ],
        ],
        [['Garrulax', 'maximus'], ['Ianthocincla', 'maxima']],
        [['Garrulax', 'nuchalis'], ['Pterorhinus', 'nuchalis']],
        [['Garrulax', 'perspicillatus'], ['Pterorhinus', 'perspicillatus']],
        [
            ['Garrulax', 'poecilorhynchus'],
            [
                'Pterorhinus',
                'poecilorhynchus',
            ],
        ],
        [['Garrulax', 'ruficeps'], ['Pterorhinus', 'ruficeps']],
        [['Garrulax', 'ruficollis'], ['Pterorhinus', 'ruficollis']],
        [['Garrulax', 'sukatschewi'], ['Ianthocincla', 'sukatschewi']],
        [['Garrulax', 'vassali'], ['Pterorhinus', 'vassali']],
        [['Garrulax', 'ocellatus'], ['Ianthocincla', 'ocellata']],
        [['Garrulax', 'lunulatus'], ['Ianthocincla', 'lunulata']],
        [['Garrulax', 'mitratus'], ['Pterorhinus', 'mitratus']],
        [['Garrulax', 'davidi'], ['Pterorhinus', 'davidi']],
        [['Garrulax', 'treacheri'], ['Pterorhinus', 'treacheri']],
        [['Gyalophylax', 'hellmayri'], ['Synallaxis', 'hellmayri']],
        [
            ['Hylocryptus', 'erythrocephalus'],
            [
                'Clibanornis',
                'erythrocephalus',
            ],
        ],
        [['Hylocryptus', 'rectirostris'], ['Clibanornis', 'rectirostris']],
        [['Hyloctistes', 'subulatus'], ['Automolus', 'subulatus']],
        [['Hyloctistes', 'virgatus'], ['Automolus', 'virgatus']],
        [['Kupeornis', 'gilberti'], ['Turdoides', 'gilberti']],
        [['Kupeornis', 'rufocinctus'], ['Turdoides', 'rufocinctus']],
        [['Microcarbo', 'pygmeus'], ['Microcarbo', 'pygmaeus']],
        [['Philydor', 'lichtensteini'], ['Anabacerthia', 'lichtensteini']],
        [['Sericornis', 'arfakianus'], ['Aethomyias', 'arfakianus']],
        [
            ['Sericornis', 'perspicillatus'],
            [
                'Aethomyias',
                'perspicillatus',
            ],
        ],
        [['Sericornis', 'rufescens'], ['Aethomyias', 'rufescens']],
        [['Simoxenops', 'striatus'], ['Syndactyla', 'striata']],
        [['Simoxenops', 'ucayalae'], ['Syndactyla', 'ucayalae']],
        [
            ['Siptornopsis', 'hypochondriaca'],
            [
                'Synallaxis',
                'hypochondriaca',
            ],
        ],
        [['Synallaxis', 'propinqua'], ['Mazaria', 'propinqua']],
        [['Turdoides', 'altirostris'], ['Argya', 'altirostris']],
        [['Turdoides', 'gularis'], ['Argya', 'gularis']],
        [['Turdoides', 'longirostris'], ['Argya', 'longirostris']],
        [['Turdoides', 'malcolmi'], ['Argya', 'malcolmi']],
        [['Turdoides', 'rufescens'], ['Argya', 'rufescens']],
        [['Crateroscelis', 'murina'], ['Origma', 'murina']],
        [
            ['Hypogramma', 'hypogrammicum'],
            [
                'Kurochkinegramma',
                'hypogrammicum',
            ],
        ],
        [['Turdoides', 'striata'], ['Argya', 'striata']],
        [['Philydor', 'ruficaudatum'], ['Anabacerthia', 'ruficaudata']],
        [
            ['Sericornis', 'citreogularis'],
            [
                'Neosericornis',
                'citreogularis',
            ],
        ],
        [['Minla', 'strigula'], ['Actinodura', 'strigula']],
        [['Garrulax', 'pectoralis'], ['Pterorhinus', 'pectoralis']],
        [['Turdoides', 'fulva'], ['Argya', 'fulva']],
        [['Minla', 'cyanouroptera'], ['Actinodura', 'cyanouroptera']],
        [['Phyllanthus', 'atripennis'], ['Turdoides', 'atripennis']],
        [['Turdoides', 'subrufa'], ['Argya', 'subrufa']],
        [['Turdoides', 'rubiginosa'], ['Argya', 'rubiginosa']],
        [['Turdoides', 'caudata'], ['Argya', 'caudata']],
        [['Deconychura', 'stictolaema'], ['Certhiasomus', 'stictolaemus']],
        [['Garrulax', 'sannio'], ['Pterorhinus', 'sannio']],
        [['Garrulax', 'striatus'], ['Grammatoptila', 'striata']],
        [['Garrulax', 'albogularis'], ['Pterorhinus', 'albogularis']],
        [['Turdoides', 'affinis'], ['Argya', 'affinis']],
        [['Gryllotalpa', 'dorsalis'], ['Conocephalus', 'dorsalis']],
        [['Gryllotalpa', 'fuscus'], ['Conocephalus', 'fuscus']],
        [['Crateroscelis', 'robusta'], ['Origma', 'robusta']],
    ];
$splits  =
    [
        [
            ['Sericornis', 'frontalis', 'ashbyi'],
            [
                'Sericornis',
                'maculatus',
                'ashbyi',
            ],
        ],
        [
            ['Sericornis', 'frontalis', 'balstoni'],
            [
                'Sericornis',
                'maculatus',
                'balstoni',
            ],
        ],
        [
            ['Zosterops', 'palpebrosus', 'buxtoni'],
            [
                'Zosterops',
                'melanurus',
                'buxtoni',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'difficilis'],
            [
                'Zosterops',
                'japonicus',
                'difficilis',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'diuatae'],
            [
                'Zosterops',
                'japonicus',
                'diuatae',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'halconensis'],
            [
                'Zosterops',
                'japonicus',
                'halconensis',
            ],
        ],
        [
            ['Sericornis', 'frontalis', 'maculatus'],
            [
                'Sericornis',
                'maculatus',
                'maculatus',
            ],
        ],
        [
            ['Zosterops', 'palpebrosus', 'melanurus'],
            [
                'Zosterops',
                'melanurus',
                'melanurus',
            ],
        ],
        [
            ['Sericornis', 'frontalis', 'mellori'],
            [
                'Sericornis',
                'maculatus',
                'mellori',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'montanus'],
            [
                'Zosterops',
                'japonicus',
                'montanus',
            ],
        ],
        [
            ['Ramphocaenus', 'melanurus', 'obscurus'],
            [
                'Ramphocaenus',
                'sticturus',
                'obscurus',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'parkesi'],
            [
                'Zosterops',
                'japonicus',
                'parkesi',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'pectoralis'],
            [
                'Zosterops',
                'japonicus',
                'pectoralis',
            ],
        ],
        [
            ['Ramphocaenus', 'melanurus', 'sticturus'],
            [
                'Ramphocaenus',
                'sticturus',
                'sticturus',
            ],
        ],
        [
            ['Zosterops', 'palpebrosus', 'unicus'],
            [
                'Zosterops',
                'citrinella',
                'unicus',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'vulcani'],
            [
                'Zosterops',
                'japonicus',
                'vulcani',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'whiteheadi'],
            [
                'Zosterops',
                'japonicus',
                'whiteheadi',
            ],
        ],
        [
            ['Zosterops', 'palpebrosus', 'williamsoni'],
            [
                'Zosterops',
                'simplex',
                'williamsoni',
            ],
        ],
        [['Turdoides', 'fulva', 'acaciae'], ['Argya', 'fulva', 'acaciae']],
        [
            ['Turdoides', 'affinis', 'affinis'],
            [
                'Argya',
                'affinis',
                'affinis',
            ],
        ],
        [
            ['Minla', 'cyanouroptera', 'aglae'],
            [
                'Actinodura',
                'cyanouroptera',
                'aglae',
            ],
        ],
        [
            ['Garrulax', 'albogularis', 'albogularis'],
            [
                'Pterorhinus',
                'albogularis',
                'albogularis',
            ],
        ],
        [
            ['Garrulax', 'sannio', 'albosuperciliaris'],
            [
                'Pterorhinus',
                'sannio',
                'albosuperciliaris',
            ],
        ],
        [
            ['Heterophasia', 'annectens', 'annectens'],
            [
                'Leioptila',
                'annectens',
                'annectens',
            ],
        ],
        [
            ['Garrulax', 'ocellatus', 'artemisiae'],
            [
                'Ianthocincla',
                'ocellata',
                'artemisiae',
            ],
        ],
        [
            ['Sericornis', 'spilodera', 'aruensis'],
            [
                'Aethomyias',
                'spilodera',
                'aruensis',
            ],
        ],
        [
            ['Garrulax', 'rufogularis', 'assamensis'],
            [
                'Ianthocincla',
                'rufogularis',
                'assamensis',
            ],
        ],
        [
            ['Hyloctistes', 'virgatus', 'assimilis'],
            [
                'Automolus',
                'virgatus',
                'assimilis',
            ],
        ],
        [
            ['Phyllanthus', 'atripennis', 'atripennis'],
            [
                'Turdoides',
                'atripennis',
                'atripennis',
            ],
        ],
        [
            ['Turdoides', 'aylmeri', 'aylmeri'],
            [
                'Argya',
                'aylmeri',
                'aylmeri',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'bastille'],
            [
                'Origma',
                'robusta',
                'bastille',
            ],
        ],
        [
            ['Phyllanthus', 'atripennis', 'bohndorffi'],
            [
                'Turdoides',
                'atripennis',
                'bohndorffi',
            ],
        ],
        [
            ['Babax', 'lanceolatus', 'bonvaloti'],
            [
                'Pterorhinus',
                'lanceolatus',
                'bonvaloti',
            ],
        ],
        [
            ['Turdoides', 'aylmeri', 'boranensis'],
            [
                'Argya',
                'aylmeri',
                'boranensis',
            ],
        ],
        [
            ['Garrulax', 'striatus', 'brahmaputra'],
            [
                'Grammatoptila',
                'striata',
                'brahmaputra',
            ],
        ],
        [
            ['Turdoides', 'fulva', 'buchanani'],
            [
                'Argya',
                'fulva',
                'buchanani',
            ],
        ],
        [
            ['Sericornis', 'papuensis', 'buergersi'],
            [
                'Aethomyias',
                'papuensis',
                'buergersi',
            ],
        ],
        [
            ['Garrulax', 'caerulatus', 'caerulatus'],
            [
                'Pterorhinus',
                'caerulatus',
                'caerulatus',
            ],
        ],
        [
            ['Sericornis', 'citreogularis', 'cairnsi'],
            [
                'Neosericornis',
                'citreogularis',
                'cairnsi',
            ],
        ],
        [
            ['Crateroscelis', 'murina', 'capitalis'],
            [
                'Origma',
                'murina',
                'capitalis',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'caquetae'],
            [
                'Clibanornis',
                'rubiginosus',
                'caquetae',
            ],
        ],
        [
            ['Minla', 'strigula', 'castanicauda'],
            [
                'Actinodura',
                'strigula',
                'castanicauda',
            ],
        ],
        [
            ['Turdoides', 'caudata', 'caudata'],
            [
                'Argya',
                'caudata',
                'caudata',
            ],
        ],
        [['Calamanthus', 'cautus', 'cautus'], ['Hylacola', 'cauta', 'cauta']],
        [
            ['Kupeornis', 'chapini', 'chapini'],
            [
                'Turdoides',
                'chapini',
                'chapini',
            ],
        ],
        [
            ['Garrulax', 'chinensis', 'chinensis'],
            [
                'Pterorhinus',
                'chinensis',
                'chinensis',
            ],
        ],
        [
            ['Garrulax', 'cineraceus', 'cineraceus'],
            [
                'Ianthocincla',
                'cineracea',
                'cineracea',
            ],
        ],
        [
            ['Garrulax', 'cineraceus', 'cinereiceps'],
            [
                'Ianthocincla',
                'cineracea',
                'cinereiceps',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'cinnamomeigula'],
            [
                'Clibanornis',
                'rubiginosus',
                'cinnamomeigula',
            ],
        ],
        [
            ['Sericornis', 'citreogularis', 'citreogularis'],
            [
                'Neosericornis',
                'citreogularis',
                'citreogularis',
            ],
        ],
        [
            ['Deconychura', 'stictolaema', 'clarior'],
            [
                'Certhiasomus',
                'stictolaemus',
                'clarior',
            ],
        ],
        [['Garrulax', 'sannio', 'comis'], ['Pterorhinus', 'sannio', 'comis']],
        [
            ['Garrulax', 'davidi', 'concolor'],
            [
                'Pterorhinus',
                'davidi',
                'concolor',
            ],
        ],
        [
            ['Hyloctistes', 'virgatus', 'cordobae'],
            [
                'Automolus',
                'virgatus',
                'cordobae',
            ],
        ],
        [
            ['Garrulax', 'courtoisi', 'courtoisi'],
            [
                'Pterorhinus',
                'courtoisi',
                'courtoisi',
            ],
        ],
        [
            ['Garrulax', 'striatus', 'cranbrooki'],
            [
                'Grammatoptila',
                'striata',
                'cranbrooki',
            ],
        ],
        [
            ['Minla', 'cyanouroptera', 'cyanouroptera'],
            [
                'Actinodura',
                'cyanouroptera',
                'cyanouroptera',
            ],
        ],
        [
            ['Garrulax', 'treacheri', 'damnatus'],
            [
                'Pterorhinus',
                'treacheri',
                'damnatus',
            ],
        ],
        [
            ['Garrulax', 'davidi', 'davidi'],
            [
                'Pterorhinus',
                'davidi',
                'davidi',
            ],
        ],
        [
            ['Heterophasia', 'annectens', 'davisoni'],
            [
                'Leioptila',
                'annectens',
                'davisoni',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'deficiens'],
            [
                'Origma',
                'robusta',
                'deficiens',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'diamondi'],
            [
                'Origma',
                'robusta',
                'diamondi',
            ],
        ],
        [['Turdoides', 'earlei', 'earlei'], ['Argya', 'earlei', 'earlei']],
        [
            ['Turdoides', 'caudata', 'eclipes'],
            [
                'Argya',
                'caudata',
                'eclipes',
            ],
        ],
        [
            ['Chlorocharis', 'emiliae', 'emiliae'],
            [
                'Zosterops',
                'emiliae',
                'emiliae',
            ],
        ],
        [
            ['Turdoides', 'rubiginosa', 'schnitzeri'],
            [
                'Argya',
                'rubiginosa',
                'emini',
            ],
        ],
        [
            ['Garrulax', 'albogularis', 'eous'],
            [
                'Pterorhinus',
                'albogularis',
                'eous',
            ],
        ],
        [
            ['Heterophasia', 'annectens', 'eximia'],
            [
                'Leioptila',
                'annectens',
                'eximia',
            ],
        ],
        [
            ['Sericornis', 'spilodera', 'ferrugineus'],
            [
                'Aethomyias',
                'spilodera',
                'ferrugineus',
            ],
        ],
        [
            ['Philydor', 'ruficaudatum', 'flavipectus'],
            [
                'Anabacerthia',
                'ruficaudata',
                'flavipectus',
            ],
        ],
        [['Turdoides', 'fulva', 'fulva'], ['Argya', 'fulva', 'fulva']],
        [
            ['Automolus', 'rubiginosus', 'fumosus'],
            [
                'Clibanornis',
                'rubiginosus',
                'fumosus',
            ],
        ],
        [
            ['Chlorocharis', 'emiliae', 'fusciceps'],
            [
                'Zosterops',
                'emiliae',
                'fusciceps',
            ],
        ],
        [
            ['Garrulax', 'chinensis', 'germaini'],
            [
                'Pterorhinus',
                'chinensis',
                'germaini',
            ],
        ],
        [
            ['Sericornis', 'spilodera', 'granti'],
            [
                'Aethomyias',
                'spilodera',
                'granti',
            ],
        ],
        [
            ['Garrulax', 'ocellatus', 'griseicauda'],
            [
                'Ianthocincla',
                'ocellata',
                'griseicauda',
            ],
        ],
        [
            ['Garrulax', 'treacheri', 'griswoldi'],
            [
                'Pterorhinus',
                'treacheri',
                'griswoldi',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'guerrerensis'],
            [
                'Clibanornis',
                'rubiginosus',
                'guerrerensis',
            ],
        ],
        [
            ['Sericornis', 'spilodera', 'guttatus'],
            [
                'Aethomyias',
                'spilodera',
                'guttatus',
            ],
        ],
        [
            ['Calamanthus', 'cautus', 'halmaturinus'],
            [
                'Hylacola',
                'cauta',
                'halmaturina',
            ],
        ],
        [
            ['Turdoides', 'rubiginosa', 'heuglini'],
            [
                'Argya',
                'rubiginosa',
                'heuglini',
            ],
        ],
        [
            ['Turdoides', 'huttoni', 'huttoni'],
            [
                'Argya',
                'huttoni',
                'huttoni',
            ],
        ],
        [
            ['Turdoides', 'subrufa', 'hyperythra'],
            [
                'Argya',
                'subrufa',
                'hyperythra',
            ],
        ],
        [
            ['Hypogramma', 'hypogrammicum', 'hypogrammicum'],
            [
                'Kurochkinegramma',
                'hypogrammicum',
                'hypogrammicum',
            ],
        ],
        [
            ['Garrulax', 'rufogularis', 'intensior'],
            [
                'Ianthocincla',
                'rufogularis',
                'intensior',
            ],
        ],
        [
            ['Sericornis', 'citreogularis', 'intermedius'],
            [
                'Neosericornis',
                'citreogularis',
                'intermedius',
            ],
        ],
        [
            ['Zosterops', 'japonicus', 'yesoensis'],
            [
                'Zosterops',
                'japonicus',
                'japonicus',
            ],
        ],
        [['Babax', 'waddelli', 'jomo'], ['Pterorhinus', 'waddelli', 'jomo']],
        [
            ['Kupeornis', 'chapini', 'kalindei'],
            [
                'Turdoides',
                'chapini',
                'kalindei',
            ],
        ],
        [
            ['Garrulax', 'caerulatus', 'kaurensis'],
            [
                'Pterorhinus',
                'caerulatus',
                'kaurensis',
            ],
        ],
        [
            ['Turdoides', 'aylmeri', 'keniana'],
            [
                'Argya',
                'aylmeri',
                'keniana',
            ],
        ],
        [
            ['Babax', 'koslowi', 'koslowi'],
            [
                'Pterorhinus',
                'koslowi',
                'koslowi',
            ],
        ],
        [
            ['Babax', 'lanceolatus', 'lanceolatus'],
            [
                'Pterorhinus',
                'lanceolatus',
                'lanceolatus',
            ],
        ],
        [
            ['Garrulax', 'caerulatus', 'latifrons'],
            [
                'Pterorhinus',
                'caerulatus',
                'latifrons',
            ],
        ],
        [
            ['Babax', 'lanceolatus', 'latouchei'],
            [
                'Pterorhinus',
                'lanceolatus',
                'latouchei',
            ],
        ],
        [
            ['Hyloctistes', 'subulatus', 'lemae'],
            [
                'Automolus',
                'subulatus',
                'lemae',
            ],
        ],
        [
            ['Garrulax', 'lunulatus', 'liangshanensis'],
            [
                'Ianthocincla',
                'lunulata',
                'liangshanensis',
            ],
        ],
        [
            ['Hypogramma', 'hypogrammicum', 'lisettae'],
            [
                'Kurochkinegramma',
                'hypogrammicum',
                'lisettae',
            ],
        ],
        [
            ['Garrulax', 'caerulatus', 'livingstoni'],
            [
                'Pterorhinus',
                'caerulatus',
                'livingstoni',
            ],
        ],
        [
            ['Garrulax', 'chinensis', 'lochmius'],
            [
                'Pterorhinus',
                'chinensis',
                'lochmius',
            ],
        ],
        [
            ['Garrulax', 'lunulatus', 'lunulatus'],
            [
                'Ianthocincla',
                'lunulata',
                'lunulata',
            ],
        ],
        [
            ['Calamanthus', 'cautus', 'macrorhynchus'],
            [
                'Hylacola',
                'cauta',
                'macrorhyncha',
            ],
        ],
        [
            ['Garrulax', 'ocellatus', 'maculipectus'],
            [
                'Ianthocincla',
                'ocellata',
                'maculipectus',
            ],
        ],
        [
            ['Garrulax', 'mitratus', 'major'],
            [
                'Pterorhinus',
                'mitratus',
                'major',
            ],
        ],
        [
            ['Turdoides', 'striata', 'malabarica'],
            [
                'Argya',
                'striata',
                'malabarica',
            ],
        ],
        [
            ['Minla', 'strigula', 'malayana'],
            [
                'Actinodura',
                'strigula',
                'malayana',
            ],
        ],
        [
            ['Hypogramma', 'hypogrammicum', 'mariae'],
            [
                'Kurochkinegramma',
                'hypogrammicum',
                'mariae',
            ],
        ],
        [
            ['Turdoides', 'fulva', 'maroccana'],
            [
                'Argya',
                'fulva',
                'maroccana',
            ],
        ],
        [
            ['Sericornis', 'papuensis', 'meeki'],
            [
                'Aethomyias',
                'papuensis',
                'meeki',
            ],
        ],
        [
            ['Turdoides', 'aylmeri', 'mentalis'],
            [
                'Argya',
                'aylmeri',
                'mentalis',
            ],
        ],
        [
            ['Garrulax', 'mitratus', 'mitratus'],
            [
                'Pterorhinus',
                'mitratus',
                'mitratus',
            ],
        ],
        [
            ['Heterophasia', 'annectens', 'mixta'],
            [
                'Leioptila',
                'annectens',
                'mixta',
            ],
        ],
        [
            ['Crateroscelis', 'murina', 'monacha'],
            [
                'Origma',
                'murina',
                'monacha',
            ],
        ],
        [
            ['Garrulax', 'chinensis', 'monachus'],
            [
                'Pterorhinus',
                'chinensis',
                'monachus',
            ],
        ],
        [
            ['Chlorocharis', 'emiliae', 'moultoni'],
            [
                'Zosterops',
                'emiliae',
                'moultoni',
            ],
        ],
        [
            ['Crateroscelis', 'murina', 'murina'],
            [
                'Origma',
                'murina',
                'murina',
            ],
        ],
        [
            ['Turdoides', 'squamiceps', 'muscatensis'],
            [
                'Argya',
                'squamiceps',
                'muscatensis',
            ],
        ],
        [
            ['Hypogramma', 'hypogrammicum', 'natunense'],
            [
                'Kurochkinegramma',
                'hypogrammicum',
                'natunense',
            ],
        ],
        [
            ['Hyloctistes', 'virgatus', 'nicaraguae'],
            [
                'Automolus',
                'virgatus',
                'nicaraguae',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'nigricauda'],
            [
                'Clibanornis',
                'rubiginosus',
                'nigricauda',
            ],
        ],
        [
            ['Hypogramma', 'hypogrammicum', 'nuchale'],
            [
                'Kurochkinegramma',
                'hypogrammicum',
                'nuchale',
            ],
        ],
        [
            ['Kupeornis', 'chapini', 'nyombensis'],
            [
                'Turdoides',
                'chapini',
                'nyombensis',
            ],
        ],
        [
            ['Garrulax', 'sannio', 'oblectans'],
            [
                'Pterorhinus',
                'sannio',
                'oblectans',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'obscurus'],
            [
                'Clibanornis',
                'rubiginosus',
                'obscurus',
            ],
        ],
        [
            ['Salpinctes', 'obsoletus', 'proximus'],
            [
                'Salpinctes',
                'obsoletus',
                'obsoletus',
            ],
        ],
        [
            ['Salpinctes', 'obsoletus', 'pulverius'],
            [
                'Salpinctes',
                'obsoletus',
                'obsoletus',
            ],
        ],
        [
            ['Garrulax', 'rufogularis', 'occidentalis'],
            [
                'Ianthocincla',
                'rufogularis',
                'occidentalis',
            ],
        ],
        [
            ['Garrulax', 'ocellatus', 'ocellatus'],
            [
                'Ianthocincla',
                'ocellata',
                'ocellata',
            ],
        ],
        [
            ['Minla', 'cyanouroptera', 'orientalis'],
            [
                'Actinodura',
                'cyanouroptera',
                'orientalis',
            ],
        ],
        [
            ['Turdoides', 'striata', 'orientalis'],
            [
                'Argya',
                'striata',
                'orientalis',
            ],
        ],
        [
            ['Hylocryptus', 'erythrocephalus', 'palamblae'],
            [
                'Clibanornis',
                'erythrocephalus',
                'palamblae',
            ],
        ],
        [
            ['Crateroscelis', 'murina', 'pallida'],
            [
                'Origma',
                'murina',
                'pallida',
            ],
        ],
        [
            ['Sericornis', 'papuensis', 'papuensis'],
            [
                'Aethomyias',
                'papuensis',
                'papuensis',
            ],
        ],
        [
            ['Calamanthus', 'pyrrhopygius', 'parkeri'],
            [
                'Hylacola',
                'pyrrhopygia',
                'parkeri',
            ],
        ],
        [
            ['Ramphocelus', 'passerinii', ''],
            [
                'Ramphocelus',
                'passerinii',
                'passerinii',
            ],
        ],
        [
            ['Garrulax', 'pectoralis', 'pectoralis'],
            [
                'Pterorhinus',
                'pectoralis',
                'pectoralis',
            ],
        ],
        [
            ['Calamanthus', 'pyrrhopygius', 'pedleri'],
            [
                'Hylacola',
                'pyrrhopygia',
                'pedleri',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'peninsularis'],
            [
                'Origma',
                'robusta',
                'peninsularis',
            ],
        ],
        [
            ['Garrulax', 'pectoralis', 'picticollis'],
            [
                'Pterorhinus',
                'pectoralis',
                'picticollis',
            ],
        ],
        [
            ['Garrulax', 'chinensis', 'propinquus'],
            [
                'Pterorhinus',
                'chinensis',
                'propinquus',
            ],
        ],
        [
            ['Calamanthus', 'pyrrhopygius', 'pyrropygius'],
            [
                'Hylacola',
                'pyrrhopygia',
                'pyrropygia',
            ],
        ],
        [
            ['Garrulax', 'pectoralis', 'robini'],
            [
                'Pterorhinus',
                'pectoralis',
                'robini',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'robusta'],
            [
                'Origma',
                'robusta',
                'robusta',
            ],
        ],
        [
            ['Heterophasia', 'annectens', 'roundi'],
            [
                'Leioptila',
                'annectens',
                'roundi',
            ],
        ],
        [
            ['Turdoides', 'rubiginosa', 'rubiginosa'],
            [
                'Argya',
                'rubiginosa',
                'rubiginosa',
            ],
        ],
        [
            ['Phyllanthus', 'atripennis', 'rubiginosus'],
            [
                'Turdoides',
                'atripennis',
                'rubiginosus',
            ],
        ],
        [
            ['Garrulax', 'rufogularis', 'rufiberbis'],
            [
                'Ianthocincla',
                'rufogularis',
                'rufiberbis',
            ],
        ],
        [
            ['Philydor', 'ruficaudatum', 'ruficaudatum'],
            [
                'Anabacerthia',
                'ruficaudata',
                'ruficaudata',
            ],
        ],
        [
            ['Garrulax', 'rufogularis', 'rufitinctus'],
            [
                'Ianthocincla',
                'rufogularis',
                'rufitincta',
            ],
        ],
        [
            ['Minla', 'cyanouroptera', 'rufodorsalis'],
            [
                'Actinodura',
                'cyanouroptera',
                'rufodorsalis',
            ],
        ],
        [
            ['Garrulax', 'rufogularis', 'rufogularis'],
            [
                'Ianthocincla',
                'rufogularis',
                'rufogularis',
            ],
        ],
        [
            ['Turdoides', 'huttoni', 'salvadorii'],
            [
                'Argya',
                'huttoni',
                'salvadorii',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'sanfordi'],
            [
                'Origma',
                'robusta',
                'sanfordi',
            ],
        ],
        [
            ['Garrulax', 'sannio', 'sannio'],
            [
                'Pterorhinus',
                'sannio',
                'sannio',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'sasaimae'],
            [
                'Clibanornis',
                'rubiginosus',
                'sasaimae',
            ],
        ],
        [
            ['Heterophasia', 'annectens', 'saturata'],
            [
                'Leioptila',
                'annectens',
                'saturata',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'saturatus'],
            [
                'Clibanornis',
                'rubiginosus',
                'saturatus',
            ],
        ],
        [
            ['Deconychura', 'stictolaema', 'secunda'],
            [
                'Certhiasomus',
                'stictolaemus',
                'secundus',
            ],
        ],
        [
            ['Garrulax', 'pectoralis', 'semitorquatus'],
            [
                'Pterorhinus',
                'pectoralis',
                'semitorquatus',
            ],
        ],
        [
            ['Turdoides', 'rubiginosa', 'bowdleri'],
            [
                'Argya',
                'rubiginosa',
                'sharpii',
            ],
        ],
        [
            ['Garrulax', 'striatus', 'sikkimensis'],
            [
                'Grammatoptila',
                'striata',
                'sikkimensis',
            ],
        ],
        [
            ['Garrulax', 'courtoisi', 'simaoensis'],
            [
                'Pterorhinus',
                'courtoisi',
                'simaoensis',
            ],
        ],
        [
            ['Minla', 'strigula', 'simlaensis'],
            [
                'Actinodura',
                'strigula',
                'simlaensis',
            ],
        ],
        [
            ['Turdoides', 'striata', 'sindiana'],
            [
                'Argya',
                'striata',
                'sindiana',
            ],
        ],
        [
            ['Turdoides', 'striata', 'somervillei'],
            [
                'Argya',
                'striata',
                'somervillei',
            ],
        ],
        [['Turdoides', 'earlei', 'sonivia'], ['Argya', 'earlei', 'sonivia']],
        [
            ['Minla', 'cyanouroptera', 'sordida'],
            [
                'Actinodura',
                'cyanouroptera',
                'sordida',
            ],
        ],
        [
            ['Minla', 'cyanouroptera', 'sordidior'],
            [
                'Actinodura',
                'cyanouroptera',
                'sordidior',
            ],
        ],
        [
            ['Sericornis', 'spilodera', 'spilodera'],
            [
                'Aethomyias',
                'spilodera',
                'spilodera',
            ],
        ],
        [
            ['Turdoides', 'squamiceps', 'squamiceps'],
            [
                'Argya',
                'squamiceps',
                'squamiceps',
            ],
        ],
        [
            ['Deconychura', 'stictolaema', 'stictolaema'],
            [
                'Certhiasomus',
                'stictolaemus',
                'stictolaemus',
            ],
        ],
        [
            ['Garrulax', 'cineraceus', 'strenuus'],
            [
                'Ianthocincla',
                'cineracea',
                'strenua',
            ],
        ],
        [
            ['Garrulax', 'striatus', 'striatus'],
            [
                'Grammatoptila',
                'striata',
                'striata',
            ],
        ],
        [
            ['Turdoides', 'striata', 'striata'],
            [
                'Argya',
                'striata',
                'striata',
            ],
        ],
        [
            ['Minla', 'strigula', 'strigula'],
            [
                'Actinodura',
                'strigula',
                'strigula',
            ],
        ],
        [
            ['Garrulax', 'caerulatus', 'subcaerulatus'],
            [
                'Pterorhinus',
                'caerulatus',
                'subcaerulatus',
            ],
        ],
        [
            ['Philydor', 'ruficaudatum', 'subflavescens'],
            [
                'Anabacerthia',
                'ruficaudata',
                'subflavescens',
            ],
        ],
        [
            ['Garrulax', 'pectoralis', 'subfusus'],
            [
                'Pterorhinus',
                'pectoralis',
                'subfusus',
            ],
        ],
        [
            ['Turdoides', 'subrufa', 'subrufa'],
            [
                'Argya',
                'subrufa',
                'subrufa',
            ],
        ],
        [
            ['Turdoides', 'affinis', 'taprobanus'],
            [
                'Argya',
                'affinis',
                'taprobanus',
            ],
        ],
        [['Minla', 'strigula', 'traii'], ['Actinodura', 'strigula', 'traii']],
        [
            ['Garrulax', 'treacheri', 'treacheri'],
            [
                'Pterorhinus',
                'treacheri',
                'treacheri',
            ],
        ],
        [
            ['Chlorocharis', 'emiliae', 'trinitae'],
            [
                'Zosterops',
                'emiliae',
                'trinitae',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'venezuelanus'],
            [
                'Clibanornis',
                'rubiginosus',
                'venezuelanus',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'veraepacis'],
            [
                'Clibanornis',
                'rubiginosus',
                'veraepacis',
            ],
        ],
        [
            ['Garrulax', 'striatus', 'vibex'],
            [
                'Grammatoptila',
                'striata',
                'vibex',
            ],
        ],
        [
            ['Hyloctistes', 'virgatus', 'virgatus'],
            [
                'Automolus',
                'virgatus',
                'virgatus',
            ],
        ],
        [
            ['Babax', 'waddelli', 'waddelli'],
            [
                'Pterorhinus',
                'waddelli',
                'waddelli',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'watkinsi'],
            [
                'Clibanornis',
                'rubiginosus',
                'watkinsi',
            ],
        ],
        [
            ['Garrulax', 'albogularis', 'whistleri'],
            [
                'Pterorhinus',
                'albogularis',
                'whistleri',
            ],
        ],
        [
            ['Calamanthus', 'cautus', 'whitlocki'],
            [
                'Hylacola',
                'cauta',
                'whitlocki',
            ],
        ],
        [
            ['Minla', 'cyanouroptera', 'wingatei'],
            [
                'Actinodura',
                'cyanouroptera',
                'wingatei',
            ],
        ],
        [
            ['Minla', 'cyanouroptera', 'wirthi'],
            [
                'Actinodura',
                'cyanouroptera',
                'wirthi',
            ],
        ],
        [
            ['Turdoides', 'squamiceps', 'yemensis'],
            [
                'Argya',
                'squamiceps',
                'yemensis',
            ],
        ],
        [
            ['Minla', 'strigula', 'yunnanensis'],
            [
                'Actinodura',
                'strigula',
                'yunnanensis',
            ],
        ],
        [
            ['Babax', 'koslowi', 'yuquensis'],
            [
                'Pterorhinus',
                'koslowi',
                'yuquensis',
            ],
        ],
        [
            ['Oryzoborus', 'maximiliani', 'magnirostris'],
            [
                'Oryzoborus',
                'maximiliani',
                '',
            ],
        ],
        [
            ['Oryzoborus', 'maximiliani', 'maximiliani'],
            [
                'Oryzoborus',
                'maximiliani',
                '',
            ],
        ],
        [['Urochroa', 'bougueri', 'bougueri'], ['Urochroa', 'bougueri', '']],
        [['Vireo', 'olivaceus', 'olivaceus'], ['Vireo', 'olivaceus', '']],
        [['Vireo', 'olivaceus', 'agilis'], ['Vireo', 'chivi', 'agilis']],
        [
            ['Zosterops', 'senegalensis', 'anderssoni'],
            [
                'Zosterops',
                'anderssoni',
                'anderssoni',
            ],
        ],
        [
            ['Zosterops', 'palpebrosus', 'auriventer'],
            [
                'Zosterops',
                'auriventer',
                'auriventer',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'brunnescens'],
            [
                'Clibanornis',
                'rubiginosus',
                'brunnescens',
            ],
        ],
        [['Vireo', 'olivaceus', 'caucae'], ['Vireo', 'chivi', 'caucae']],
        [['Vireo', 'olivaceus', 'chivi'], ['Vireo', 'chivi', 'chivi']],
        [
            ['Calandrella', 'blanfordi', 'daarodensis'],
            [
                'Calandrella',
                'eremica',
                'daarodensis',
            ],
        ],
        [
            ['Dicrurus', 'adsimilis', 'divaricatus'],
            [
                'Dicrurus',
                'divaricatus',
                'divaricatus',
            ],
        ],
        [['Vireo', 'olivaceus', 'diversus'], ['Vireo', 'chivi', 'diversus']],
        [
            ['Calandrella', 'blanfordi', 'eremica'],
            [
                'Calandrella',
                'eremica',
                'eremica',
            ],
        ],
        [
            ['Zosterops', 'abyssinicus', 'flavilateralis'],
            [
                'Zosterops',
                'flavilateralis',
                'flavilateralis',
            ],
        ],
        [
            ['Vireo', 'olivaceus', 'griseobarbatus'],
            [
                'Vireo',
                'chivi',
                'griseobarbatus',
            ],
        ],
        [
            ['Zosterops', 'japonicus', 'hainanus'],
            [
                'Zosterops',
                'simplex',
                'hainanus',
            ],
        ],
        [
            ['Zosterops', 'abyssinicus', 'jubaensis'],
            [
                'Zosterops',
                'flavilateralis',
                'jubaensis',
            ],
        ],
        [
            ['Dicrurus', 'adsimilis', 'lugubris'],
            [
                'Dicrurus',
                'divaricatus',
                'lugubris',
            ],
        ],
        [
            ['Vireo', 'olivaceus', 'pectoralis'],
            [
                'Vireo',
                'chivi',
                'pectoralis',
            ],
        ],
        [
            ['Zosterops', 'salvadorii', ''],
            [
                'Zosterops',
                'simplex',
                'salvadorii',
            ],
        ],
        [
            ['Zosterops', 'japonicus', 'simplex'],
            [
                'Zosterops',
                'simplex',
                'simplex',
            ],
        ],
        [
            ['Vireo', 'olivaceus', 'solimoensis'],
            [
                'Vireo',
                'chivi',
                'solimoensis',
            ],
        ],
        [
            ['Zosterops', 'senegalensis', 'stierlingi'],
            [
                'Zosterops',
                'anderssoni',
                'stierlingi',
            ],
        ],
        [
            ['Zosterops', 'everetti', 'tahanensis'],
            [
                'Zosterops',
                'auriventer',
                'tahanensis',
            ],
        ],
        [
            ['Vireo', 'olivaceus', 'tobagensis'],
            [
                'Vireo',
                'chivi',
                'tobagensis',
            ],
        ],
        [
            ['Zosterops', 'senegalensis', 'tongensis'],
            [
                'Zosterops',
                'anderssoni',
                'tongensis',
            ],
        ],
        [['Vireo', 'olivaceus', 'vividior'], ['Vireo', 'chivi', 'vividior']],
        [
            ['Zosterops', 'everetti', 'wetmorei'],
            [
                'Zosterops',
                'auriventer',
                'wetmorei',
            ],
        ],
        [
            ['Automolus', 'ochrolaemus', 'exsertus'],
            [
                'Automolus',
                'exsertus',
                '',
            ],
        ],
        [
            ['Brachypteryx', 'montana', 'cruralis'],
            [
                'Brachypteryx',
                'cruralis',
                '',
            ],
        ],
        [
            ['Brachypteryx', 'montana', 'goodfellowi'],
            [
                'Brachypteryx',
                'goodfellowi',
                '',
            ],
        ],
        [
            ['Brachypteryx', 'montana', 'sinensis'],
            [
                'Brachypteryx',
                'sinensis',
                '',
            ],
        ],
        [['Dicrurus', 'ludwigii', 'elgonensis'], ['Dicrurus', 'sharpei', '']],
        [['Dicrurus', 'ludwigii', 'sharpei'], ['Dicrurus', 'sharpei', '']],
        [['Dicrurus', 'modestus', 'atactus'], ['Dicrurus', 'atactus', '']],
        [
            ['Grallaricula', 'ferrugineipectus', 'leymebambae'],
            [
                'Grallaricula',
                'leymebambae',
                '',
            ],
        ],
        [
            ['Laniarius', 'poensis', 'holomelas'],
            [
                'Laniarius',
                'holomelas',
                '',
            ],
        ],
        [
            ['Myrmothera', 'campanisona', 'subcanescens'],
            [
                'Myrmothera',
                'subcanescens',
                '',
            ],
        ],
        [['Ninox', 'boobook', 'fusca'], ['Ninox', 'fusca', '']],
        [['Ninox', 'boobook', 'plesseni'], ['Ninox', 'plesseni', '']],
        [['Ninox', 'boobook', 'rotiensis'], ['Ninox', 'rotiensis', '']],
        [['Ninox', 'squamipila', 'hantu'], ['Ninox', 'hantu', '']],
        [['Ninox', 'squamipila', 'squamipila'], ['Ninox', 'squamipila', '']],
        [['Urochroa', 'bougueri', 'leucura'], ['Urochroa', 'leucura', '']],
        [
            ['Zosterops', 'abyssinicus', 'socotranus'],
            [
                'Zosterops',
                'socotranus',
                '',
            ],
        ],
        [
            ['Zosterops', 'maderaspatanus', 'aldabrensis'],
            [
                'Zosterops',
                'aldabrensis',
                '',
            ],
        ],
        [
            ['Zosterops', 'poliogastrus', 'mbuluensis'],
            [
                'Zosterops',
                'mbuluensis',
                '',
            ],
        ],
        [
            ['Zosterops', 'poliogastrus', 'winifredae'],
            [
                'Zosterops',
                'winifredae',
                '',
            ],
        ],
    ];

$lumps =
    [
        [
            ['Nothura', 'chacoensis', ''],
            [
                'Nothura',
                'maculosa',
                'chacoensis',
            ],
        ],
        [
            ['Ramphocelus', 'costaricensis', ''],
            [
                'Ramphocelus',
                'passerinii',
                'costaricensis',
            ],
        ],
        [
            ['Calandrella', 'erlangeri', ''],
            [
                'Calandrella',
                'blanfordi',
                'erlangeri',
            ],
        ],
        [
            ['Scytalopus', 'sp.nov.Alto_Pisones', ''],
            [
                'Scytalopus',
                'alvarezlopezi',
                '',
            ],
        ],
        [
            ['Lanius', 'pallidirostris', ''],
            [
                'Lanius',
                'excubitor',
                'pallidirostris',
            ],
        ],
    ];
