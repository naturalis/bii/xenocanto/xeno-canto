<?php

require_once 'KLogger.php';

class Database
{

    public $aborting;

    public $runid;

    public $mysqli;

    private string $trigger = '
        CREATE 
        TRIGGER `%s` BEFORE UPDATE ON `%s`
            FOR EACH ROW
            BEGIN
                IF (
                    NEW.genus <> OLD.genus OR
                    NEW.species <> OLD.species OR
                    NEW.ssp <> OLD.ssp OR
                    NEW.eng_name <> OLD.eng_name OR
                    NEW.songtype <> OLD.songtype OR
                    NEW.recordist <> OLD.recordist OR
                    COALESCE(NEW.longitude,9999) <> COALESCE(OLD.longitude,9999) OR
                    COALESCE(NEW.latitude,9999) <> COALESCE(OLD.latitude,9999) OR
                    NEW.remarks <> OLD.remarks OR
                    NEW.license <> OLD.license OR
                    NEW.form_data <> OLD.form_data
                )
                THEN
                    INSERT INTO `%s` VALUES (
                        DEFAULT,
                        NOW(),
                        OLD.snd_nr,
                        OLD.genus,
                        OLD.species,
                        OLD.ssp,
                        OLD.eng_name,
                        OLD.songtype,
                        OLD.recordist,
                        OLD.longitude,
                        OLD.latitude,
                        OLD.remarks,
                        OLD.license,
                        OLD.form_data
                    );
                END IF;
            END;
    ';

    private $logPath = '/tmp/';

    private $log;

    private $debug;

    private $hasErrors = false;

    private $mode;


    public function __construct()
    {
        $this->mysqli = new mysqli(
            getenv('MYSQL_HOST'), getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD'), getenv('MYSQL_DATABASE')
        );

        if ($this->mysqli->connect_error) {
            $this->abort($this->mysqli->connect_error);
        }
        $this->mysqli->set_charset('utf8');
        $this->mysqli->query("set sql_mode = ''");
        $this->aborting = false;
    }

    public function abort($msg)
    {
        if ($this->debug) {
            $this->log->LogError($msg, false);
            $this->hasErrors = true;
            return "Error! $msg\n";
        }
        if (!$this->aborting) {
            echo "$msg\n\n";
            $this->aborting = true;
            echo 'Restoring backed-up tables...';
            $this->restoreTable('birdsounds', "birdsounds_backup_$this->runid");
            $this->restoreTable('birdsounds_background', "birdsounds_background_backup_$this->runid");
            $this->restoreTable('birdsounds_history', "birdsounds_history_backup_$this->runid");
            $this->restoreTable('taxonomy', "taxonomy_backup_{$this->runid}");
            $this->restoreTable('taxonomy_ssp', "taxonomy_ssp_backup_$this->runid");
            $this->restoreTable('taxonomy_multilingual', "taxonomy_multilingual_backup_$this->runid");
        }
        die("Update aborted...\n");
    }

    public function restoreTable($tableName, $backupTableName)
    {
        $this->query("TRUNCATE $tableName");
        $this->query("INSERT INTO $tableName SELECT * FROM $backupTableName");
    }

    public function query($sql)
    {
        $res = $this->mysqli->query($sql);
        if ($res === false) {
            $this->abort($this->mysqli->error);
        }
        return $res;
    }

    public function affected()
    {
        return $this->mysqli->affected_rows;
    }

    public function __destruct()
    {
        $this->restoreOriginalMode();
        $this->mysqli->close();
    }

    private function restoreOriginalMode()
    {
        if ($this->mode) {
            $this->mysqli->query("set global sql_mode = '$this->mode'");
        }
    }

    public function setDebugging($bool = true)
    {
        $this->debug = (bool)$bool;
    }

    public function createUpdateTables()
    {
        $this->query('DROP TABLE IF EXISTS taxonomy_next');
        $this->query('CREATE TABLE taxonomy_next LIKE taxonomy');
        $this->query('DROP TABLE IF EXISTS taxonomy_ssp_next');
        $this->query('CREATE TABLE taxonomy_ssp_next LIKE taxonomy_ssp');
        $this->query('DROP TABLE IF EXISTS taxonomy_multilingual_next');
        $this->query('CREATE TABLE taxonomy_multilingual_next LIKE taxonomy_multilingual');
        $this->backupTable('birdsounds', 'birdsounds_next');
        $this->backupTable('birdsounds_background', 'birdsounds_background_next');
        $this->backupTable('birdsounds_history', 'birdsounds_history_next');
        $this->createTrigger('sound_updated_next', 'birdsounds_next', 'birdsounds_history_next');
    }

    public function backupTable($tableName, $backupTableName)
    {
        $this->query("DROP TABLE IF EXISTS $backupTableName");
        $this->query("CREATE TABLE $backupTableName LIKE $tableName");
        $this->query("INSERT INTO $backupTableName SELECT * FROM $tableName");
    }

    public function createTrigger($name, $triggerTable, $updateTable)
    {
        $this->dropTrigger($name);
        $this->mysqli->multi_query(sprintf($this->trigger, $name, $triggerTable, $updateTable));
    }

    public function dropTrigger($name)
    {
        $this->query("DROP TRIGGER IF EXISTS `$name`");
    }

    public function createBackupTables()
    {
        $this->runid = date('Ymd_his');
        $this->backupTable('birdsounds', "birdsounds_backup_$this->runid");
        $this->backupTable('birdsounds_background', "birdsounds_background_backup_$this->runid");
        $this->backupTable('taxonomy', "taxonomy_backup_$this->runid");
        $this->backupTable('taxonomy_ssp', "taxonomy_ssp_backup_$this->runid");
        $this->backupTable('taxonomy_multilingual', "taxonomy_multilingual_backup_$this->runid");
        $this->backupTable('birdsounds_history', "birdsounds_history_backup_$this->runid");
    }

    public function setLogging()
    {
        $this->logPath .= 'xc-classification-test-' . date('Y-m-d') . '.log';
        // Always start with a clean slate
        if (file_exists($this->logPath)) {
            unlink($this->logPath);
        }
        $this->log = new KLogger($this->logPath, KLogger::DEBUG);
    }

    public function getLogPath()
    {
        return $this->logPath;
    }

    public function hasErrors()
    {
        return $this->hasErrors;
    }

    public function fixBackgroundByNr($fromNr, $toNr)
    {
        $stmt = $this->prepare('UPDATE birdsounds_background SET species_nr = ? WHERE species_nr = ?');
        $stmt->bind_param('ss', $toNr, $fromNr);
        if (!$stmt->execute()) {
            $this->abort($stmt->error);
        }
        $stmt->close();
    }

    public function prepare($sql)
    {
        $stmt = $this->mysqli->prepare($sql);
        if (!$stmt) {
            $this->abort($this->mysqli->error);
        }
        return $stmt;
    }

    public function fixBackgroundByScientific($scientific, $toNr)
    {
        $stmt = $this->prepare('UPDATE birdsounds_background SET species_nr = ? WHERE scientific = ?');
        $stmt->bind_param('ss', $toNr, $scientific);
        if (!$stmt->execute()) {
            $this->abort($stmt->error);
        }
        $stmt->close();
    }

    public function findSpeciesNumberNext($genus, $species)
    {
        return $this->findSpeciesNumber($genus, $species, 'taxonomy_next');
    }

    public function findSpeciesNumber($genus, $species, $table = 'taxonomy')
    {
        $species_nr = null;
        $stmt = $this->prepare("SELECT species_nr FROM $table WHERE genus = ? and species = ?");
        $stmt->bind_param('ss', $genus, $species);
        if (!$stmt->execute()) {
            $this->abort($stmt->error);
        }
        if (!$stmt->bind_result($species_nr)) {
            $this->abort($stmt->error);
        }
        $type = $table == 'taxonomy' ? 'current' : 'next';
        if (!$stmt->fetch()) {
            // $this->abort($stmt->error);
            $this->abort("Species number for $genus $species ($type) does not exist!");
            return null;
        }
        $stmt->close();
        return $species_nr;
    }

    public function close()
    {
        $this->mysqli->close();
    }

    public function speciesNextExists($genus, $species)
    {
        return $this->speciesExists($genus, $species, 'taxonomy_next');
    }

    public function speciesExists($genus, $species, $table = 'taxonomy')
    {
        $species_nr = null;
        $stmt = $this->prepare("SELECT species_nr FROM $table WHERE genus = ? and species = ?");
        $stmt->bind_param('ss', $genus, $species);
        if (!$stmt->execute()) {
            $this->abort($stmt->error);
        }
        if (!$stmt->bind_result($species_nr)) {
            $this->abort($stmt->error);
        }
        if (!$stmt->fetch()) {
            return false;
        }
        $stmt->close();
        return true;
    }

    public function tableExists($table)
    {
        $res = $this->query("SHOW TABLES LIKE '$table'");
        return (bool)$res->fetch_object();
    }

    public function getSpeciesNext($genus, $species)
    {
        return $this->getSpecies($genus, $species, 'taxonomy_next');
    }

    public function getSpecies($genus, $species, $table = 'taxonomy')
    {
        $res = $this->query(
            "SELECT family, genus, species, eng_name, IOC_order_nr AS order_nr, species_nr
            FROM $table 
            WHERE genus = '$genus' and species = '$species'"
        );
        return $res->fetch_object() ?? null;
    }

    public function getRecordingNext($xcid)
    {
        return $this->getRecording($xcid, 'birdsounds_next');
    }

    public function getRecording($xcid, $table = 'birdsounds')
    {
        $res = $this->query("SELECT * FROM $table WHERE snd_nr = '$xcid'");
        return $res->fetch_object() ?? null;
    }

    public function findBackgroundData($text, $sndNr)
    {
        $stmt = $this->prepare(
            "SELECT id, species_nr FROM birdsounds_background WHERE (scientific = ? OR english = ?) AND snd_nr = ?"
        );
        $stmt->bind_param('sss', $text, $text, $sndNr);
        if (!$stmt->execute()) {
            $this->abort($stmt->error);
        }
        if (!$stmt->bind_result($id, $speciesNr)) {
            $this->abort($stmt->error);
        }
        if (!$stmt->fetch()) {
            return null;
        }
        $stmt->close();
        return (object)['id' => $id, 'speciesNr' => $speciesNr];
    }

    public function findSpeciesNameNext($species_nr)
    {
        return $this->findSpeciesName($species_nr, 'taxonomy_next');
    }

    public function findSpeciesName($species_nr, $table = 'taxonomy')
    {
        $genus = $species = null;
        $stmt = $this->prepare("SELECT genus, species FROM $table WHERE species_nr = ?");
        if (!$stmt->bind_param('s', $species_nr)) {
            $this->abort($stmt->error);
        }
        if (!$stmt->execute()) {
            $this->abort($stmt->error);
        }
        if (!$stmt->bind_result($genus, $species)) {
            $this->abort($stmt->error);
        }
        $stmt->fetch();
        $stmt->close();
        return trim($genus . ' ' . $species);
    }

    public function verifyEnglishNames()
    {
        $sql = '
            select t1.species_nr as id, t1.genus, t1.species, 
                t1.eng_name as xml, t2.english as csv 
            from taxonomy_next as t1
            left join taxonomy_multilingual_next as t2 on t1.species_nr = t2.species_nr
            where t2.species_nr is not null and t1.eng_name != t2.english';
        $res = $this->query($sql);
        if ($res->num_rows > 0) {
            $msg = "Non-matching English names between xml and csv files!\nPlease correct the csv file for the following entries:\n";
            while ($row = $res->fetch_object()) {
                $msg .= "$row->id: $row->genus $row->species (xml: $row->xml; csv: $row->csv)\n";
            }
            $this->abort($msg);
        }
    }

    public function generateSpeciesNumber()
    {
        do {
            // makes a random alpha numeric string of a given length
            $letters = range('a', 'z');
            $out = '';
            for ($c = 0; $c < 6; $c++) {
                $out .= $letters[mt_rand(0, count($letters) - 1)];
            }
        } while ($this->speciesNumberExists($out));
        return $out;
    }

    protected function speciesNumberExists($species_nr)
    {
        $readback = null;
        // make sure it's not already used
        $stmt = $this->prepare("SELECT species_nr FROM taxonomy WHERE species_nr = ?");
        $stmt->bind_param('s', $species_nr);
        if (!$stmt->execute()) {
            $this->abort($stmt->error);
        }
        if (!$stmt->bind_result($readback)) {
            $this->abort($stmt->error);
        }
        $stmt->fetch();
        $stmt->close();
        return !is_null($readback);
    }

    private function setOriginalMode()
    {
        $res = $this->mysqli->query('select @@sql_mode as mode');
        $row = $res->fetch_object();
        $this->mode = $row->mode;
    }

}