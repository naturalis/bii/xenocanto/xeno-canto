<?php

class Taxon
{

    public static $order = 0.0;

    public $commonName;

    public $latinName;

    public $authority;

    public $note;

    public $extinct;

    public $id;

    public $orderNr;

    public function __construct($id)
    {
        Taxon::$order += 1.0;
        $this->commonName = '';
        $this->latinName = '';
        $this->authority = '';
        $this->note = '';
        $this->extinct = false;
        $this->id = $id;
        $this->orderNr = Taxon::$order;
    }
}
