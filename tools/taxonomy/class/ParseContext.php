<?php

class ParseContext
{

    private $elementStack;

    private $taxa;

    private $db;

    public function __construct($db)
    {
        $this->elementStack = [];
        $this->taxa = [];
        $this->db = $db;
    }

    public function handle_element_start($parser, $name, $attributes)
    {
        $this->elementStack[] = $name;
        if (($name === 'ORDER')
            || ($name === 'FAMILY')
            || ($name === 'GENUS')
            || ($name === 'SPECIES')
            || ($name === 'SUBSPECIES')
        ) {
            $taxon = new Taxon($this->db->generateSpeciesNumber());
            if (isset($attributes['EXTINCT']) && ($attributes['EXTINCT'] === 'yes')) {
                $taxon->extinct = true;
            }
            $this->taxa[] = $taxon;
        }
        return true;
    }

    public function handle_element_end($parser, $name)
    {
        if ($name === 'SPECIES') {
            //echo("species: {$this->taxa[3]->commonName} ({$this->taxa[2]->latinName} {$this->taxa[3]->latinName})\n");
            $stmt = $this->db->prepare(
                '
                INSERT INTO taxonomy_next
                (taxonomy_next.order, family, family_english, genus, species, eng_name, species_nr, IOC_order_nr, authority, extinct)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
            );

            $success = false;
            for ($j = 0; $j < 3; $j++) {
                $id = $this->db->generateSpeciesNumber();
                if (!$stmt->bind_param(
                    'sssssssdsi',
                    $this->taxa[0]->latinName,
                    $this->taxa[1]->latinName,
                    $this->taxa[1]->commonName,
                    $this->taxa[2]->latinName,
                    $this->taxa[3]->latinName,
                    $this->taxa[3]->commonName,
                    $id,
                    $this->taxa[3]->orderNr,
                    $this->taxa[3]->authority,
                    $this->taxa[3]->extinct
                )
                ) {
                    $this->db->abort($stmt->error);
                }

                if ($stmt->execute() !== false) {
                    $this->taxa[3]->id = $id;
                    $success = true;
                    break;
                }
            }

            if (!$success) {
                $this->db->abort($stmt->error);
            }
        } elseif ($name === 'SUBSPECIES') {
            //echo("    - subspecies: {$this->taxa[2]->latinName} {$this->taxa[3]->latinName} {$this->taxa[4]->latinName}\n");
            $stmt = $this->db->prepare(
                '
                INSERT INTO taxonomy_ssp_next
                (taxonomy_ssp_next.order, family, family_english, genus, species, eng_name, species_nr, ssp, author, extinct)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
            );

            if (!$stmt->bind_param(
                'sssssssssi',
                $this->taxa[0]->latinName,
                $this->taxa[1]->latinName,
                $this->taxa[1]->commonName,
                $this->taxa[2]->latinName,
                $this->taxa[3]->latinName,
                $this->taxa[3]->commonName,
                $this->taxa[3]->id,
                $this->taxa[4]->latinName,
                $this->taxa[4]->authority,
                $this->taxa[4]->extinct
            )
            ) {
                $this->db->abort($stmt->error);
            }
            $stmt->execute();
        }

        array_pop($this->elementStack);
        if (($name === 'ORDER')
            || ($name === 'FAMILY')
            || ($name === 'GENUS')
            || ($name === 'SPECIES')
            || ($name === 'SUBSPECIES')
        ) {
            array_pop($this->taxa);
        }
        return true;
    }

    public function handle_character_data($parser, $data)
    {
        if (end($this->elementStack) === 'LATIN_NAME') {
            $this->appendTaxonProperty('latinName', $data);
        } elseif (end($this->elementStack) === 'ENGLISH_NAME') {
            $this->appendTaxonProperty('commonName', $data);
        } elseif (end($this->elementStack) === 'AUTHORITY') {
            $this->appendTaxonProperty('authority', $data);
        } elseif (end($this->elementStack) === 'NOTE') {
            $this->appendTaxonProperty('note', $data);
        } elseif (end($this->elementStack) === 'EXTINCT') {
            $this->appendTaxonProperty('extinct', $data);
        }
    }

    private function appendTaxonProperty($name, $value)
    {
        $taxon = array_pop($this->taxa);
        if (!$taxon->$name) {
            $taxon->name = '';
        }
        $taxon->$name .= $value == '&' ? ' & ' : trim($value);
        $this->taxa[] = $taxon;
    }
}
