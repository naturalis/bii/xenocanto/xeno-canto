<?php

// format of this file:  There should be 3 variables: $renames, $splits, $lumps.
// Each of these variables should be an array of changes.  Each element of the
// array should also be a two-element array.  The first element is the source
// taxon, the second one the destination.  Each of these taxa is a three-element
// array, specifying genus, species, ssp...
// Entirely new species do not need to be listed here, unless we have added them
// to our taxonomy under a provisional name (e.g. sp.nov.fooBar)
// Family changes and changes in linear order are not adressed here. Should they be? WP

$renames = [
    //array(array("genus", "species"), array("genus", "species")),
    // 4.4 species changes
    [['Nesoenas', 'picturata'], ['Nesoenas', 'picturatus']],
    [['Nesoenas', 'rodericana'], ['Nesoenas', 'rodericanus']],
    [['Pachycephala', 'graeffii'], ['Pachycephala', 'vitiensis']],
    [['Basileuterus', 'basilicus'], ['Myiothlypis', 'basilica']],
    [['Tockus', 'bradfieldi'], ['Lophoceros', 'bradfieldi']],
    [['Tockus', 'alboterminatus'], ['Lophoceros', 'alboterminatus']],
    [['Tockus', 'fasciatus'], ['Lophoceros', 'fasciatus']],
    [['Tockus', 'hemprichii'], ['Lophoceros', 'hemprichii']],
    [['Tockus', 'nasutus'], ['Lophoceros', 'nasutus']],
    [['Tockus', 'camurus'], ['Lophoceros', 'camurus']],
    [['Tockus', 'pallidirostris'], ['Lophoceros', 'pallidirostris']],
    [['Tockus', 'hartlaubi'], ['Horizocerus', 'hartlaubi']],
    [
        ['Tropicranus', 'albocristatus'],
        [
            'Horizocerus',
            'albocristatus',
        ],
    ],
    [['Aceros', 'cassidix'], ['Rhyticeros', 'cassidix']],
    [['Aceros', 'waldeni'], ['Rhabdotorrhinus', 'waldeni']],
    [
        ['Aceros', 'leucocephalus'],
        [
            'Rhabdotorrhinus',
            'leucocephalus',
        ],
    ],
    [['Penelopides', 'exarhatus'], ['Rhabdotorrhinus', 'exarhatus']],
    [['Aceros', 'corrugatus'], ['Rhabdotorrhinus', 'corrugatus']],

    // 4.3 species changes
    [
        ['Necropsittacus', 'rodericanus'],
        [
            'Necropsittacus',
            'rodricanus',
        ],
    ],
    [['Melampitta', 'gigantea'], ['Megalampitta', 'gigantea']],
    [['Zosterops', 'capensis'], ['Zosterops', 'virens']],
    [['Cacicus', 'melanicterus'], ['Cassiculus', 'melanicterus']],
    [['Ocyalus', 'latirostris'], ['Cacicus', 'latirostris']],
    [['Clypicterus', 'oseryi'], ['Cacicus', 'oseryi']],
    [['Dives', 'atroviolaceus'], ['Ptiloxena', 'atroviolaceus']],
    [['Curaeus', 'forbesi'], ['Anumara', 'forbesi']],
    [['Agelaioides', 'oreopsar'], ['Oreopsar', 'bolivianus']],
    [['Zosterops', 'capensis'], ['Zosterops', 'virens']],

    // 4.2 species changes
    [['Vanellus', 'malarbaricus'], ['Vanellus', 'malabaricus']],
    [['Mirafra', 'hova'], ['Eremopterix', 'hova']],
    [['Melanocorypha', 'leucoptera'], ['Alauda', 'leucoptera']],
    [['Calandrella', 'raytal'], ['Alaudala', 'raytal']],
    [['Calandrella', 'rufescens'], ['Alaudala', 'rufescens']],
    [['Calandrella', 'somalica'], ['Alaudala', 'somalica']],
    [['Calandrella', 'cheleensis'], ['Alaudala', 'cheleensis']],
    [['Calandrella', 'athensis'], ['Alaudala', 'athensis']],
    [['Pseudalaemon', 'fremantlii'], ['Spizocorys', 'fremantlii']],
    [['Spelaeornis', 'formosus'], ['Elachura', 'formosa']],
    [['Nesocichla', 'eremita'], ['Turdus', 'eremita']],
    [['Psophocichla', 'litsitsirupa'], ['Turdus', 'litsitsirupa']],
    // array(array("Myrmelocichla", "monticola"), array("Myrmecocichla", "monticola")),
    // array(array("Myrmelocichla", "arnotti"), array("Myrmecocichla", "arnotti")),
    // array(array("Myrmelocichla", "collaris"), array("Myrmecocichla", "collaris"))
];

$splits = [

    //array(array("genus", "species", "ssp"), array("genus", "species", "ssp or NULL")),
    // 4.4 splits
    [
        ['Rallus', 'elegans', 'tenuirostris'],
        [
            'Rallus',
            'elegans',
            null,
        ],
    ],
    [
        ['Rallus', 'longirostris', 'crepitans'],
        [
            'Rallus',
            'crepitans',
            'crepitans',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'caribaeus'],
        [
            'Rallus',
            'crepitans',
            'caribaeus',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'grossi'],
        [
            'Rallus',
            'crepitans',
            'grossi',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'insularum'],
        [
            'Rallus',
            'crepitans',
            'insularum',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'leucophaeus'],
        [
            'Rallus',
            'crepitans',
            'leucophaeus',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'pallidus'],
        [
            'Rallus',
            'crepitans',
            'pallidus',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'saturatus'],
        [
            'Rallus',
            'crepitans',
            'saturatus',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'scottii'],
        [
            'Rallus',
            'crepitans',
            'scottii',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'waynei'],
        [
            'Rallus',
            'crepitans',
            'waynei',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'belizensis'],
        [
            'Rallus',
            'crepitans',
            'belizensis',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'coryi'],
        [
            'Rallus',
            'crepitans',
            'coryi',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'obsoletus'],
        [
            'Rallus',
            'obsoletus',
            'obsoletus',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'levipes'],
        [
            'Rallus',
            'obsoletus',
            'levipes',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'beldingi'],
        [
            'Rallus',
            'obsoletus',
            'beldingi',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'yumanensis'],
        [
            'Rallus',
            'obsoletus',
            'yumanensis',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'rhizophorae'],
        [
            'Rallus',
            'obsoletus',
            'rhizoporae',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'nayaritensis'],
        [
            'Rallus',
            'obsoletus',
            'nayaritensis',
        ],
    ],
    [
        ['Caloramphus', 'fuliginosus', 'hayii'],
        [
            'Caloramphus',
            'hayii',
            null,
        ],
    ],
    [
        ['Megalaima', 'australis', 'duvaucelii'],
        [
            'Megalaima',
            'duvaucelii',
            null,
        ],
    ],
    [
        ['Megalaima', 'australis', 'cyanotis'],
        [
            'Megalaima',
            'duvaucelii',
            'cyanotis',
        ],
    ],
    [
        ['Megalaima', 'australis', 'orientalis'],
        [
            'Megalaima',
            'duvaucelii',
            'orientalis',
        ],
    ],
    [
        ['Megalaima', 'australis', 'stuarti'],
        [
            'Megalaima',
            'duvaucelii',
            'stuarti',
        ],
    ],
    [
        ['Megalaima', 'australis', 'tanamassae'],
        [
            'Megalaima',
            'duvaucelii',
            'tanamassae',
        ],
    ],
    [
        ['Megalaima', 'australis', 'gigantorhina'],
        [
            'Megalaima',
            'duvaucelii',
            'gigantorhina',
        ],
    ],
    [
        ['Megalaima', 'australis', 'australis'],
        [
            'Megalaima',
            'australis',
            null,
        ],
    ],
    [
        ['Phibalura', 'flavirostris', 'boliviana'],
        [
            'Phibalura',
            'boliviana',
            null,
        ],
    ],
    [
        ['Cercomacra', 'nigrescens', 'fuscicauda'],
        [
            'Cercomacra',
            'fuscicauda',
            null,
        ],
    ],
    [
        ['Pachycephala', 'implicata', 'richardsi'],
        [
            'Pachycephala',
            'richardsi',
            null,
        ],
    ],
    [
        ['Basileuterus', 'tristriatus', 'melanotis'],
        [
            'Basileuterus',
            'melanotis',
            null,
        ],
    ],
    [
        ['Basileuterus', 'tristriatus', 'chitrensis'],
        [
            'Basileuterus',
            'melanotis',
            'chitrensis',
        ],
    ],
    [
        ['Basileuterus', 'tristriatus', 'tacarcunae'],
        [
            'Basileuterus',
            'tacarcunae',
            null,
        ],
    ],
    [
        ['Basileuterus', 'tristriatus', 'punctipectus'],
        [
            'Basileuterus',
            'punctipectus',
            'punctipectus',
        ],
    ],
    [
        ['Basileuterus', 'tristriatus', 'inconspicuus'],
        [
            'Basileuterus',
            'punctipectus',
            'inconspicuus',
        ],
    ],
    [
        ['Basileuterus', 'tristriatus', 'canens'],
        [
            'Basileuterus',
            'punctipectus',
            'canens',
        ],
    ],
    [
        ['Myiothlypis', 'bivittata', 'roraimae'],
        [
            'Myiothlypis',
            'roraimae',
            null,
        ],
    ],
    [
        ['Agelaioides', 'badius', 'fringillarius'],
        [
            'Agelaioides',
            'fringillarius',
            null,
        ],
    ],

    [
        ['Gallirallus', 'philippensis', 'wahgiensis'],
        [
            'Gallirallus',
            'philippensis',
            'reducta',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'leucophaeus'],
        [
            'Rallus',
            'crepitans',
            'caribaeus',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'grossi'],
        [
            'Rallus',
            'crepitans',
            'pallidus',
        ],
    ],
    [
        ['Rallus', 'longirostris', 'belizensis'],
        [
            'Rallus',
            'crepitans',
            'pallidus',
        ],
    ],
    [
        ['Porzana', 'pusilla', 'obscura'],
        [
            'Porzana',
            'pusilla',
            'intermedia',
        ],
    ],
    [
        ['Fulica', 'americana', 'peruviana'],
        [
            'Fulica',
            'americana',
            'americana',
        ],
    ],
    [
        ['Grus', 'canadensis', 'rowani'],
        [
            'Grus',
            'canadensis',
            'tabida',
        ],
    ],
    [['Grus', 'grus', 'grus'], ['Grus', 'grus', null]],
    [['Grus', 'grus', 'archibaldi'], ['Grus', 'grus', null]],
    [['Grus', 'grus', 'lilfordi'], ['Grus', 'grus', null]],
    [
        ['Turnix', 'suscitator', 'interrumpens'],
        [
            'Turnix',
            'suscitator',
            'atrogularis',
        ],
    ],
    [
        ['Vanellus', 'senegallus', 'major'],
        [
            'Vanellus',
            'senegallus',
            'senegallus',
        ],
    ],
    [['Jacana', 'spinosa', 'spinosa'], ['Jacana', 'spinosa', null]],
    [
        ['Jacana', 'spinosa', 'gymnostoma'],
        [
            'Jacana',
            'spinosa',
            null,
        ],
    ],
    [['Jacana', 'spinosa', 'violacea'], ['Jacana', 'spinosa', null]],
    [
        ['Thinocorus', 'rumicivorus', 'pallidus'],
        [
            'Thinocorus',
            'rumicivorus',
            'cuneicauda',
        ],
    ],
    [
        ['Numenius', 'americanus', 'americanus'],
        [
            'Numenius',
            'americanus',
            null,
        ],
    ],
    [
        ['Numenius', 'americanus', 'parvus'],
        [
            'Numenius',
            'americanus',
            null,
        ],
    ],
    [
        ['Calidris', 'maritima', 'belcheri'],
        [
            'Calidris',
            'maritima',
            null,
        ],
    ],
    [
        ['Calidris', 'maritima', 'littoralis'],
        [
            'Calidris',
            'maritima',
            null,
        ],
    ],
    [
        ['Calidris', 'maritima', 'maritima'],
        [
            'Calidris',
            'maritima',
            null,
        ],
    ],
    [
        ['Glareola', 'pratincola', 'erlangeri'],
        [
            'Glareola',
            'pratincola',
            'fuelleborni',
        ],
    ],
    [
        ['Glareola', 'pratincola', 'riparia'],
        [
            'Glareola',
            'pratincola',
            'fuelleborni',
        ],
    ],
    [
        ['Glareola', 'nuchalis', 'torrens'],
        [
            'Glareola',
            'nuchalis',
            'nuchalis',
        ],
    ],
    [
        ['Chroicocephalus', 'novaehollandiae', 'gunni'],
        [
            'Chroicocephalus',
            'novaehollandiae',
            'novaehollandiae',
        ],
    ],
    [
        ['Thalasseus', 'bergii', 'enigma'],
        [
            'Thalasseus',
            'bergii',
            'bergii',
        ],
    ],
    [
        ['Phaetusa', 'simplex', 'chloropoda'],
        [
            'Phaetusa',
            'simplex',
            null,
        ],
    ],
    [
        ['Phaetusa', 'simplex', 'simplex'],
        [
            'Phaetusa',
            'simplex',
            null,
        ],
    ],
    [
        ['Fratercula', 'arctica', 'grabae'],
        [
            'Fratercula',
            'arctica',
            null,
        ],
    ],
    [
        ['Fratercula', 'arctica', 'naumanni'],
        [
            'Fratercula',
            'arctica',
            null,
        ],
    ],
    [
        ['Fratercula', 'arctica', 'arctica'],
        [
            'Fratercula',
            'arctica',
            null,
        ],
    ],
    [
        ['Columba', 'palumbus', 'excelsa'],
        [
            'Columba',
            'palumbus',
            'palumbus',
        ],
    ],
    [
        ['Patagioenas', 'fasciata', 'letonai'],
        [
            'Patagioenas',
            'fasciata',
            'fasciata',
        ],
    ],
    [
        ['Patagioenas', 'fasciata', 'parva'],
        [
            'Patagioenas',
            'fasciata',
            'fasciata',
        ],
    ],
    [
        ['Patagioenas', 'inornata', 'exigua'],
        [
            'Patagioenas',
            'inornata',
            null,
        ],
    ],
    [
        ['Patagioenas', 'inornata', 'wetmorei'],
        [
            'Patagioenas',
            'inornata',
            null,
        ],
    ],
    [
        ['Patagioenas', 'inornata', 'inornata'],
        [
            'Patagioenas',
            'inornata',
            null,
        ],
    ],
    [
        ['Nesoenas', 'picturata', 'picturata'],
        [
            'Nesoenas',
            'picturatus',
            'picturatus',
        ],
    ],
    [
        ['Nesoenas', 'picturata', 'rostrata'],
        [
            'Nesoenas',
            'picturatus',
            'rostratus',
        ],
    ],
    [
        ['Nesoenas', 'picturata', 'aldabrana'],
        [
            'Nesoenas',
            'picturatus',
            'aldabranus',
        ],
    ],
    [
        ['Streptopelia', 'orientalis', 'orii'],
        [
            'Streptopelia',
            'orientalis',
            'orientalis',
        ],
    ],
    [
        ['Spilopelia', 'senegalensis', 'sokotrae'],
        [
            'Spilopelia',
            'senegalensis',
            'senegalensis',
        ],
    ],
    [
        ['Macropygia', 'amboinensis', 'hueskeri'],
        [
            'Macropygia',
            'amboinensis',
            'carteretia',
        ],
    ],
    [
        ['Macropygia', 'mackinlayi', 'goodsoni'],
        [
            'Macropygia',
            'mackinlayi',
            'arossi',
        ],
    ],
    [
        ['Macropygia', 'mackinlayi', 'krakari'],
        [
            'Macropygia',
            'mackinlayi',
            'arossi',
        ],
    ],
    [
        ['Claravis', 'mondetoura', 'mondetoura'],
        [
            'Claravis',
            'mondetoura',
            null,
        ],
    ],
    [
        ['Claravis', 'mondetoura', 'ochoterena'],
        [
            'Claravis',
            'mondetoura',
            null,
        ],
    ],
    [
        ['Claravis', 'mondetoura', 'salvini'],
        [
            'Claravis',
            'mondetoura',
            null,
        ],
    ],
    [
        ['Claravis', 'mondetoura', 'umbrina'],
        [
            'Claravis',
            'mondetoura',
            null,
        ],
    ],
    [
        ['Claravis', 'mondetoura', 'pulchra'],
        [
            'Claravis',
            'mondetoura',
            null,
        ],
    ],
    [
        ['Claravis', 'mondetoura', 'inca'],
        [
            'Claravis',
            'mondetoura',
            null,
        ],
    ],
    [
        ['Uropelia', 'campestris', 'campestris'],
        [
            'Uropelia',
            'campestris',
            null,
        ],
    ],
    [
        ['Uropelia', 'campestris', 'figginsi'],
        [
            'Uropelia',
            'campestris',
            null,
        ],
    ],
    [
        ['Leptotila', 'verreauxi', 'zapluta'],
        [
            'Leptotila',
            'verreauxi',
            'verreauxi',
        ],
    ],
    [
        ['Ptilinopus', 'magnificus', 'interpositus'],
        [
            'Ptilinopus',
            'magnificus',
            'puella',
        ],
    ],
    [
        ['Ptilinopus', 'magnificus', 'septentrionalis'],
        [
            'Ptilinopus',
            'magnificus',
            'poliurus',
        ],
    ],
    [
        ['Ptilinopus', 'nainus', 'minimus'],
        [
            'Ptilinopus',
            'nainus',
            null,
        ],
    ],
    [
        ['Ptilinopus', 'nainus', 'nainus'],
        [
            'Ptilinopus',
            'nainus',
            null,
        ],
    ],
    [
        ['Tauraco', 'livingstonii', 'cabanisi'],
        [
            'Tauraco',
            'livingstonii',
            'reichenowi',
        ],
    ],
    [
        ['Crotophaga', 'sulcirostris', 'pallidula'],
        [
            'Crotophaga',
            'sulcirostris',
            null,
        ],
    ],
    [
        ['Crotophaga', 'sulcirostris', 'sulcirostris'],
        [
            'Crotophaga',
            'sulcirostris',
            null,
        ],
    ],
    [['Tapera', 'naevia', 'excellens'], ['Tapera', 'naevia', null]],
    [['Tapera', 'naevia', 'naevia'], ['Tapera', 'naevia', null]],
    [
        ['Neomorphus', 'geoffroyi', 'maximiliani'],
        [
            'Neomorphus',
            'geoffroyi',
            'geoffroyi',
        ],
    ],
    [
        ['Neomorphus', 'geoffroyi', 'geoffroyi'],
        [
            'Neomorphus',
            'geoffroyi',
            'amazonicus',
        ],
    ],
    [
        ['Centropus', 'superciliosus', 'sokotrae'],
        [
            'Centropus',
            'superciliosus',
            'superciliosus',
        ],
    ],
    [['Tyto', 'alba', 'poensis'], ['Tyto', 'alba', 'affinis']],
    [['Tyto', 'alba', 'lucayana'], ['Tyto', 'alba', 'pratincola']],
    [['Tyto', 'alba', 'subandeana'], ['Tyto', 'alba', 'guatemalae']],
    [
        ['Otus', 'lempiji', 'hypnodes'],
        [
            'Otus',
            'lempiji',
            'lempiji',
        ],
    ],
    [
        ['Otus', 'lempiji', 'cnephaeus'],
        [
            'Otus',
            'lempiji',
            'lempiji',
        ],
    ],
    [
        ['Margarobyas', 'lawrencii', 'exsul'],
        [
            'Margarobyas',
            'lawrencii',
            null,
        ],
    ],
    [
        ['Margarobyas', 'lawrencii', 'lawrencii'],
        [
            'Margarobyas',
            'lawrencii',
            null,
        ],
    ],
    [['Strix', 'varia', 'georgica'], ['Strix', 'varia', null]],
    [['Strix', 'varia', 'helveolum'], ['Strix', 'varia', null]],
    [['Strix', 'varia', 'varia'], ['Strix', 'varia', null]],
    [
        ['Glaucidium', 'palmarum', 'oberholseri'],
        [
            'Glaucidium',
            'palmarum',
            null,
        ],
    ],
    [
        ['Glaucidium', 'palmarum', 'griscomi'],
        [
            'Glaucidium',
            'palmarum',
            null,
        ],
    ],
    [
        ['Glaucidium', 'palmarum', 'palmarum'],
        [
            'Glaucidium',
            'palmarum',
            null,
        ],
    ],
    [
        ['Glaucidium', 'griseiceps', 'occultum'],
        [
            'Glaucidium',
            'griseiceps',
            null,
        ],
    ],
    [
        ['Glaucidium', 'griseiceps', 'rarum'],
        [
            'Glaucidium',
            'griseiceps',
            null,
        ],
    ],
    [
        ['Glaucidium', 'griseiceps', 'griseiceps'],
        [
            'Glaucidium',
            'griseiceps',
            null,
        ],
    ],
    [
        ['Glaucidium', 'tephronotum', 'elgonense'],
        [
            'Glaucidium',
            'tephronotum',
            'medje',
        ],
    ],
    [
        ['Athene', 'cunicularia', 'apurensis'],
        [
            'Athene',
            'cunicularia',
            'brachyptera',
        ],
    ],
    [
        ['Athene', 'cunicularia', 'arubensis'],
        [
            'Athene',
            'cunicularia',
            'brachyptera',
        ],
    ],
    [
        ['Athene', 'cunicularia', 'intermedia'],
        [
            'Athene',
            'cunicularia',
            'nanodes',
        ],
    ],
    [
        ['Athene', 'cunicularia', 'punensis'],
        [
            'Athene',
            'cunicularia',
            'nanodes',
        ],
    ],
    [
        ['Aegolius', 'ridgwayi', 'rostrata'],
        [
            'Aegolius',
            'ridgwayi',
            null,
        ],
    ],
    [
        ['Aegolius', 'ridgwayi', 'tacanensis'],
        [
            'Aegolius',
            'ridgwayi',
            null,
        ],
    ],
    [
        ['Aegolius', 'ridgwayi', 'ridgwayi'],
        [
            'Aegolius',
            'ridgwayi',
            null,
        ],
    ],
    [['Ninox', 'rufa', 'aruensis'], ['Ninox', 'rufa', 'humeralis']],
    [
        ['Chordeiles', 'gundlachii', 'vicinus'],
        [
            'Chordeiles',
            'gundlachii',
            null,
        ],
    ],
    [
        ['Chordeiles', 'gundlachii', 'gundlachii'],
        [
            'Chordeiles',
            'gundlachii',
            null,
        ],
    ],
    [
        ['Lurocalis', 'semitorquatus', 'noctivagus'],
        [
            'Lurocalis',
            'semitorquatus',
            'stonei',
        ],
    ],
    [
        ['Nyctidromus', 'albicollis', 'intercedens'],
        [
            'Nyctidromus',
            'albicollis',
            'albicollis',
        ],
    ],
    [
        ['Antrostomus', 'rufus', 'saltarius'],
        [
            'Antrostomus',
            'rufus',
            'rutilus',
        ],
    ],
    [
        ['Caprimulgus', 'affinis', 'undulatus'],
        [
            'Caprimulgus',
            'affinis',
            'affinis',
        ],
    ],
    [
        ['Caprimulgus', 'affinis', 'kasuidori'],
        [
            'Caprimulgus',
            'affinis',
            'affinis',
        ],
    ],
    [
        ['Collocalia', 'esculenta', 'hypogrammica'],
        [
            'Collocalia',
            'esculenta',
            'spilogaster',
        ],
    ],
    [
        ['Panyptila', 'cayennensis', 'veraecrucis'],
        [
            'Panyptila',
            'cayennensis',
            null,
        ],
    ],
    [
        ['Panyptila', 'cayennensis', 'cayennensis'],
        [
            'Panyptila',
            'cayennensis',
            null,
        ],
    ],
    [
        ['Colibri', 'coruscans', 'rostratus'],
        [
            'Colibri',
            'coruscans',
            'germanus',
        ],
    ],
    [
        ['Chlorostilbon', 'melanorhynchus', 'pumilus'],
        [
            'Chlorostilbon',
            'melanorhynchus',
            null,
        ],
    ],
    [
        ['Chlorostilbon', 'melanorhynchus', 'melanorhynchus'],
        [
            'Chlorostilbon',
            'melanorhynchus',
            null,
        ],
    ],
    [
        ['Cynanthus', 'latirostris', 'toroi'],
        [
            'Cynanthus',
            'latirostris',
            null,
        ],
    ],
    [
        ['Chrysuronia', 'oenone', 'alleni'],
        [
            'Chrysuronia',
            'oenone',
            'josephinae',
        ],
    ],
    [
        ['Trogon', 'melanurus', 'occidentalis'],
        [
            'Trogon',
            'melanurus',
            'eumorphus',
        ],
    ],
    [
        ['Trogon', 'viridis', 'melanopterus'],
        [
            'Trogon',
            'viridis',
            'viridis',
        ],
    ],
    [
        ['Trogon', 'mexicanus', 'lutescens'],
        [
            'Trogon',
            'mexicanus',
            'mexicanus',
        ],
    ],
    [
        ['Trogon', 'collaris', 'flavidior'],
        [
            'Trogon',
            'collaris',
            'aurantiiventris',
        ],
    ],
    [
        ['Coracias', 'spatulatus', 'weigalli'],
        [
            'Coracias',
            'spatulatus',
            null,
        ],
    ],
    [
        ['Coracias', 'spatulatus', 'spatulatus'],
        [
            'Coracias',
            'spatulatus',
            null,
        ],
    ],
    [
        ['Caridonax', 'fulgidus', 'gracilirostris'],
        [
            'Caridonax',
            'fulgidus',
            null,
        ],
    ],
    [
        ['Caridonax', 'fulgidus', 'fulgidus'],
        [
            'Caridonax',
            'fulgidus',
            null,
        ],
    ],
    [
        ['Momotus', 'subrufescens', 'conexus'],
        [
            'Momotus',
            'subrufescens',
            'subrufescens',
        ],
    ],
    [
        ['Momotus', 'subrufescens', 'olivaresi'],
        [
            'Momotus',
            'subrufescens',
            'subrufescens',
        ],
    ],
    [
        ['Rhyticeros', 'undulatus', 'aequabilis'],
        [
            'Rhyticeros',
            'undulatus',
            null,
        ],
    ],
    [
        ['Rhyticeros', 'undulatus', 'undulatus'],
        [
            'Rhyticeros',
            'undulatus',
            null,
        ],
    ],
    [
        ['Galbula', 'leucogastra', 'viridissima'],
        [
            'Galbula',
            'leucogastra',
            null,
        ],
    ],
    [
        ['Galbula', 'leucogastra', 'leucogastra'],
        [
            'Galbula',
            'leucogastra',
            null,
        ],
    ],
    [
        ['Notharchus', 'hyperrhynchus', 'cryptoleucus'],
        [
            'Notharchus',
            'hyperrhynchus',
            'hyperrhynchus',
        ],
    ],
    [['Bucco', 'capensis', 'dugandi'], ['Bucco', 'capensis', null]],
    [['Bucco', 'capensis', 'capensis'], ['Bucco', 'capensis', null]],
    [
        ['Aulacorhynchus', 'derbianus', 'nigrirostris'],
        [
            'Aulacorhynchus',
            'derbianus',
            null,
        ],
    ],
    [
        ['Aulacorhynchus', 'derbianus', 'derbianus'],
        [
            'Aulacorhynchus',
            'derbianus',
            null,
        ],
    ],
    [
        ['Indicator', 'xanthonotus', 'fulvus'],
        [
            'Indicator',
            'xanthonotus',
            null,
        ],
    ],
    [
        ['Indicator', 'xanthonotus', 'xanthonotus'],
        [
            'Indicator',
            'xanthonotus',
            null,
        ],
    ],
    [
        ['Picumnus', 'olivaceus', 'malleolus'],
        [
            'Picumnus',
            'olivaceus',
            'olivaceus',
        ],
    ],
    [
        ['Melanerpes', 'uropygialis', 'fuscescens'],
        [
            'Melanerpes',
            'uropygialis',
            'uropygialis',
        ],
    ],
    [
        ['Dendropicos', 'elliotii', 'kupeensis'],
        [
            'Dendropicos',
            'elliotii',
            null,
        ],
    ],
    [
        ['Dendropicos', 'elliotii', 'gabela'],
        [
            'Dendropicos',
            'elliotii',
            'elliotii',
        ],
    ],
    [
        ['Dendrocopos', 'maculatus', 'leytensis'],
        [
            'Dendrocopos',
            'maculatus',
            'fulvifasciatus',
        ],
    ],
    [
        ['Dendrocopos', 'maculatus', 'maculatus'],
        [
            'Dendrocopos',
            'maculatus',
            'menagei',
        ],
    ],
    [
        ['Dendrocopos', 'ramsayi', 'siasiensis'],
        [
            'Dendrocopos',
            'ramsayi',
            null,
        ],
    ],
    [
        ['Dendrocopos', 'ramsayi', 'ramsayi'],
        [
            'Dendrocopos',
            'ramsayi',
            null,
        ],
    ],
    [
        ['Dendrocopos', 'canicapillus', 'nagamichii'],
        [
            'Dendrocopos',
            'canicapillus',
            'kaleensis',
        ],
    ],
    [
        ['Dendrocopos', 'canicapillus', 'szetschuanensis'],
        [
            'Dendrocopos',
            'canicapillus',
            'kaleensis',
        ],
    ],
    [
        ['Dendrocopos', 'canicapillus', 'omissus'],
        [
            'Dendrocopos',
            'canicapillus',
            'kaleensis',
        ],
    ],
    [
        ['Dendrocopos', 'canicapillus', 'obscurus'],
        [
            'Dendrocopos',
            'canicapillus',
            'kaleensis',
        ],
    ],
    [
        ['Celeus', 'flavus', 'peruvianus'],
        [
            'Celeus',
            'flavus',
            'flavus',
        ],
    ],
    [
        ['Celeus', 'flavus', 'tectricialis'],
        [
            'Celeus',
            'flavus',
            'flavus',
        ],
    ],
    [
        ['Chrysophlegma', 'flavinucha', 'lylei'],
        [
            'Chrysophlegma',
            'flavinucha',
            'flavinucha',
        ],
    ],
    [
        ['Picus', 'chlorolophus', 'laotianus'],
        [
            'Picus',
            'chlorolophus',
            'chlorolophus',
        ],
    ],
    [['Picus', 'viridanus', 'weberi'], ['Picus', 'viridanus', null]],
    [
        ['Picus', 'viridanus', 'viridanus'],
        [
            'Picus',
            'viridanus',
            null,
        ],
    ],
    [
        ['Picus', 'vittatus', 'eisenhoferi'],
        [
            'Picus',
            'vittatus',
            null,
        ],
    ],
    [
        ['Picus', 'vittatus', 'connectens'],
        [
            'Picus',
            'vittatus',
            null,
        ],
    ],
    [['Picus', 'vittatus', 'eurous'], ['Picus', 'vittatus', null]],
    [['Picus', 'vittatus', 'vittatus'], ['Picus', 'vittatus', null]],
    [
        ['Gecinulus', 'grantia', 'poilanei'],
        [
            'Gecinulus',
            'grantia',
            'indochinensis',
        ],
    ],
    [
        ['Falco', 'berigora', 'occidentalis'],
        [
            'Falco',
            'berigora',
            'berigora',
        ],
    ],
    [
        ['Falco', 'berigora', 'tasmanica'],
        [
            'Falco',
            'berigora',
            'berigora',
        ],
    ],
    [
        ['Trichoglossus', 'haematodus', 'intermedius'],
        [
            'Trichoglossus',
            'haematodus',
            'haematodus',
        ],
    ],
    [
        ['Trichoglossus', 'moluccanus', 'eyrei'],
        [
            'Trichoglossus',
            'moluccanus',
            'moluccanus',
        ],
    ],
    [
        ['Poicephalus', 'senegalus', 'mesotypus'],
        [
            'Poicephalus',
            'senegalus',
            'senegalus',
        ],
    ],
    [['Ara', 'severus', 'castaneifrons'], ['Ara', 'severus', null]],
    [['Ara', 'severus', 'severus'], ['Ara', 'severus', null]],
    [
        ['Pyrrhura', 'picta', 'pantchenkoi'],
        [
            'Pyrrhura',
            'picta',
            'caeruleiceps',
        ],
    ],
    [
        ['Pyrrhura', 'amazonum', 'microtera'],
        [
            'Pyrrhura',
            'amazonum',
            'amazonum',
        ],
    ],
    [['Touit', 'surdus', 'chryseurus'], ['Touit', 'surdus', null]],
    [['Touit', 'surdus', 'surdus'], ['Touit', 'surdus', null]],

    //4.3 splits and associated ssp  rearrangements
    [
        ['Coracopsis', 'nigra', 'barklyi'],
        [
            'Coracopsis',
            'barklyi',
            null,
        ],
    ],
    [
        ['Lepidocolaptes', 'albolineatus', 'duidae'],
        [
            'Lepidocolaptes',
            'duidae',
            null,
        ],
    ],
    [
        ['Lepidocolaptes', 'albolineatus', 'fuscicapillus'],
        [
            'Lepidocolaptes',
            'fuscicapillus',
            null,
        ],
    ],
    [
        ['Lepidocolaptes', 'albolineatus', 'madeirae'],
        [
            'Lepidocolaptes',
            'fuscicapillus',
            'madeirae',
        ],
    ],
    [
        ['Lepidocolaptes', 'albolineatus', 'layardi'],
        [
            'Lepidocolaptes',
            'layardi',
            null,
        ],
    ],
    [
        ['Cracticus', 'torquatus', 'argenteus'],
        [
            'Cracticus',
            'argenteus',
            null,
        ],
    ],


    [
        ['Tinamus', 'solitarius', 'pernambucensis'],
        [
            'Tinamus',
            'solitarius',
            'solitarius',
        ],
    ],
    [
        ['Dromaius', 'novaehollandiae', 'rothschildi'],
        [
            'Dromaius',
            'novaehollandiae',
            'Null',
        ],
    ],
    [
        ['Anas', 'superciliosa', 'rogersi'],
        [
            'Anas',
            'superciliosa',
            'superciliosa',
        ],
    ],
    [
        ['Ortalis', 'vetula', 'intermedia'],
        [
            'Ortalis',
            'vetula',
            'vetula',
        ],
    ],
    [
        ['Bonasa', 'umbellus', 'affinis'],
        [
            'Bonasa',
            'umbellus',
            'umbelloides',
        ],
    ],
    [
        ['Tetrao', 'urogallus', 'rudolfi'],
        [
            'Tetrao',
            'urogallus',
            'crassirostris',
        ],
    ],
    [
        ['Lagopus', 'muta', 'gabrielsoni'],
        [
            'Lagopus',
            'muta',
            'townsendi',
        ],
    ],
    [
        ['Lagopus', 'muta', 'sanfordi'],
        [
            'Lagopus',
            'muta',
            'atkhensis',
        ],
    ],
    [
        ['Lagopus', 'muta', 'chamberlaini'],
        [
            'Lagopus',
            'muta',
            'atkhensis',
        ],
    ],
    [
        ['Lagopus', 'lagopus', 'muriei'],
        [
            'Lagopus',
            'lagopus',
            'alexandrae',
        ],
    ],
    [['Lerwa', 'lerwa', 'major'], ['Lerwa', 'lerwa', null]],
    [['Lerwa', 'lerwa', 'callipygia'], ['Lerwa', 'lerwa', null]],
    [
        ['Scleroptila', 'shelleyi', 'canidorsalis'],
        [
            'Scleroptila',
            'shelleyi',
            'shelleyi',
        ],
    ],
    [
        ['Pternistis', 'bicalcaratus', 'thornei'],
        [
            'Pternistis',
            'bicalcaratus',
            'bicalcaratus',
        ],
    ],
    [
        ['Pternistis', 'hartlaubi', 'bradfieldi'],
        [
            'Pternistis',
            'hartlaubi',
            null,
        ],
    ],
    [
        ['Pternistis', 'hartlaubi', 'crypticus'],
        [
            'Pternistis',
            'hartlaubi',
            null,
        ],
    ],
    [
        ['Excalfactoria', 'chinensis', 'palmeri'],
        [
            'Excalfactoria',
            'chinensis',
            'chinensis',
        ],
    ],
    [
        ['Polyplectron', 'bicalcaratum', 'bakeri'],
        [
            'Polyplectron',
            'bicalcaratum',
            null,
        ],
    ],
    [
        ['Polyplectron', 'bicalcaratum', 'ghigii'],
        [
            'Polyplectron',
            'bicalcaratum',
            null,
        ],
    ],
    [
        ['Butorides', 'virescens', 'maculata'],
        [
            'Butorides',
            'virescens',
            'virescens',
        ],
    ],
    [
        ['Butorides', 'striata', 'chloriceps'],
        [
            'Butorides',
            'striata',
            'javanica',
        ],
    ],
    [
        ['Egretta', 'tricolor', 'occidentalis'],
        [
            'Egretta',
            'tricolor',
            'ruficollis',
        ],
    ],
    [
        ['Sula', 'dactylatra', 'bedouti'],
        [
            'Sula',
            'dactylatra',
            'personata',
        ],
    ],
    [
        ['Sula', 'dactylatra', 'californica'],
        [
            'Sula',
            'dactylatra',
            'personata',
        ],
    ],
    [
        ['Aviceda', 'leuphotes', 'wolfei'],
        [
            'Aviceda',
            'leuphotes',
            null,
        ],
    ],
    [
        ['Torgos', 'tracheliotos', 'nubicus'],
        [
            'Torgos',
            'tracheliotos',
            null,
        ],
    ],
    [
        ['Melierax', 'metabates', 'neumanni'],
        [
            'Melierax',
            'metabates',
            null,
        ],
    ],
    [
        ['Accipiter', 'tachiro', 'croizati'],
        [
            'Accipiter',
            'toussenelii',
            'unduliventer',
        ],
    ],
    [
        ['Accipiter', 'tachiro', 'canescens'],
        [
            'Accipiter',
            'toussenelii',
            'canescens',
        ],
    ],
    [
        ['Collocalia', 'esculenta', 'kalili'],
        [
            'Collocalia',
            'esculenta',
            'heinrothi',
        ],
    ],
    [
        ['Eurystomus', 'glaucurus', 'pulcherrimus'],
        [
            'Eurystomus',
            'glaucurus',
            'suahelicus',
        ],
    ],
    [
        ['Dacelo', 'leachii', 'superflua'],
        [
            'Dacelo',
            'leachii',
            'intermedia',
        ],
    ],
    [
        ['Dendropicos', 'fuscescens', 'intermedius'],
        [
            'Dendropicos',
            'fuscescens',
            'natalensis',
        ],
    ],
    [
        ['Herpetotheres', 'cachinnans', 'chapmani'],
        [
            'Herpetotheres',
            'cachinnans',
            'cachinnans',
        ],
    ],
    [
        ['Herpetotheres', 'cachinnans', 'queribundus'],
        [
            'Herpetotheres',
            'cachinnans',
            'cachinnans',
        ],
    ],
    [
        ['Zosterops', 'capensis', 'caniviridis'],
        [
            'Zosterops',
            'virens',
            null,
        ],
    ],
    [
        ['Zosterops', 'capensis', 'atmorii'],
        [
            'Zosterops',
            'virens',
            null,
        ],
    ],
    [
        ['Zosterops', 'pallidus', 'pallidus'],
        [
            'Zosterops',
            'pallidus',
            null,
        ],
    ],
    [
        ['Zosterops', 'pallidus', 'sundevalli'],
        [
            'Zosterops',
            'pallidus',
            null,
        ],
    ],
    [
        ['Crithagra', 'gularis', 'elgonensis'],
        [
            'Crithagra',
            'canicapilla',
            'elgonensis',
        ],
    ],
    [
        ['Crithagra', 'gularis', 'montanorum'],
        [
            'Crithagra',
            'canicapilla',
            'montanorum',
        ],
    ],
    [
        ['Basileuterus', 'tristriatus', 'chitrensis'],
        [
            'Basileuterus',
            'tristriatus',
            'melanotis',
        ],
    ],

    // 4.2 splits  and associated ssp  rearrangements
    [
        ['Psittacus', 'erithacus', 'timneh'],
        [
            'Psittacus',
            'timneh',
            null,
        ],
    ],
    [
        ['Oxypogon', 'guerinii', 'cyanolaemus'],
        [
            'Oxypogon',
            'cyanolaemus',
            null,
        ],
    ],
    [
        ['Oxypogon', 'guerinii', 'lindenii'],
        [
            'Oxypogon',
            'lindenii',
            null,
        ],
    ],
    [
        ['Oxypogon', 'guerinii', 'stuebelii'],
        [
            'Oxypogon',
            'stuebelii',
            null,
        ],
    ],
    [
        ['Hemitriccus', 'minor', 'cohnhafti'],
        [
            'Hemitriccus',
            'cohnhafti',
            null,
        ],
    ],
    [
        ['Sirystes', 'sibilator', 'albocinereus'],
        [
            'Sirystes',
            'albocinereus',
            null,
        ],
    ],
    [
        ['Sirystes', 'sibilator', 'subcanescens'],
        [
            'Sirystes',
            'subcanescens',
            null,
        ],
    ],
    [
        ['Epinecrophylla', 'haematonota', 'pyrrhonota'],
        [
            'Epinecrophylla',
            'pyrrhonota',
            null,
        ],
    ],
    [
        ['Epinecrophylla', 'haematonota', 'amazonica'],
        [
            'Epinecrophylla',
            'amazonica',
            null,
        ],
    ],
    [
        ['Hylopezus', 'macularius', 'paraensis'],
        [
            'Hylopezus',
            'paraensis',
            null,
        ],
    ],
    [
        ['Hylopezus', 'macularius', 'whittakeri'],
        [
            'Hylopezus',
            'whittakeri',
            null,
        ],
    ],
    [
        ['Sclerurus', 'mexicanus', 'obscurior'],
        [
            'Sclerurus',
            'obscurior',
            null,
        ],
    ],
    [
        ['Rhopophilus', 'pekinensis', 'leptorhynchus'],
        [
            'Rhopophilus',
            'pekinensis',
            'pekinensis',
            null,
        ],
    ],
    [
        ['Turdus', 'lherminieri', 'montserrati'],
        [
            'Turdus',
            'lherminieri',
            'dorotheae',
            null,
        ],
    ],
    [
        ['Muscicapa', 'latirostris', 'pooensis'],
        [
            'Muscicapa',
            'latirostris',
            'poonensis',
            null,
        ],
    ],
];

$lumps = [
    //aray(array("genus", "species", NULL), array("genus", "species", "ssp")),
    // 4.2. changes/lumps
    [
        ['Heteromirafra', 'sidamoensis', null],
        [
            'Heteromirafra',
            'archeri',
            'sidamoensis',
        ],
    ],

    // 4.3 changes/lumps
    [
        ['Cisticola', 'lepe', null],
        [
            'Cisticola',
            'erythrops',
            'lepe',
        ],
    ],
    [
        ['Anthus', 'pseudosimilis', null],
        [
            'Anthus',
            'cinnamomeus',
            'pseudosimilis',
        ],
    ],
    [
        ['Anthus', 'longicaudatus', null],
        [
            'Anthus',
            'vaalensis',
            'longicaudatus',
        ],
    ],

    // 4.4 changes/lumps
    [
        ['Pachycephala', 'graeffii', null],
        [
            'Pachycephala',
            'vitiensis',
        ],
    ],
    [
        ['Pachycephala', 'graeffii', 'torquata'],
        [
            'Pachycephala',
            'vitiensis',
            'torquata',
        ],
    ],
    [
        ['Pachycephala', 'graeffii', 'graeffi'],
        [
            'Pachycephala',
            'vitiensis',
            'graeffi',
        ],
    ],
    [
        ['Pachycephala', 'graeffii', 'ambigua'],
        [
            'Pachycephala',
            'vitiensis',
            'ambigua',
        ],
    ],
    [
        ['Pachycephala', 'graeffii', 'aurantiiventris'],
        [
            'Pachycephala',
            'vitiensis',
            'aurantiiventris',
        ],
    ],
    [
        ['Pachycephala', 'graeffii', 'bella'],
        [
            'Pachycephala',
            'vitiensis',
            'bella',
        ],
    ],
    [
        ['Pachycephala', 'graeffii', 'koroana'],
        [
            'Pachycephala',
            'vitiensis',
            'koroana',
        ],
    ],
    [
        ['Pachycephala', 'graeffii', 'optata'],
        [
            'Pachycephala',
            'vitiensis',
            'optata',
        ],
    ],
];
