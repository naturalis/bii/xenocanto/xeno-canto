ALTER TABLE taxonomy_multilingual ADD COLUMN `chinese_tw` VARCHAR(255) DEFAULT NULL AFTER chinese;
ALTER TABLE taxonomy_multilingual ADD COLUMN `lithuanian` VARCHAR(255) DEFAULT NULL AFTER japanese;
