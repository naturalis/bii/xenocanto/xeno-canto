<?php

$renames =
    [
        [['Ramphocelus', 'bresilius'], ['Ramphocelus', 'bresilia']],
        [['Dixiphia', 'pipra'], ['Pseudopipra', 'pipra']],
        [
            ['Dixiphia', 'erythrocephala'],
            [
                'Ceratopipra',
                'erythrocephala',
            ],
        ],
        [['Dixiphia', 'mentalis'], ['Ceratopipra', 'mentalis']],
        [['Chen', 'caerulescens'], ['Anser', 'caerulescens']],
        [['Threskiornis', 'moluccus'], ['Threskiornis', 'molucca']],
        [['Leptocoma', 'sericea'], ['Leptocoma', 'aspasia']],

        [['Chen', 'rossii'], ['Anser', 'rossii']],
        [['Chen', 'canagica'], ['Anser', 'canagicus']],
        [['Porphyrio', 'martinicus'], ['Porphyrio', 'martinica']],
        [['Xenicus', 'lyalli'], ['Traversia', 'lyalli']],
        [['Dixiphia', 'cornuta'], ['Ceratopipra', 'cornuta']],
        [['Dixiphia', 'chloromeros'], ['Ceratopipra', 'chloromeros']],
        [['Dixiphia', 'rubrocapilla'], ['Ceratopipra', 'rubrocapilla']],
        [['Xenopipo', 'unicolor'], ['Chloropipo', 'unicolor']],
        [['Xenopipo', 'flavicapilla'], ['Chloropipo', 'flavicapilla']],
    ];

$splits =
    [
        [
            ['Alaudala', 'cheleensis', 'niethammeri'],
            [
                'Alaudala',
                'rufescens ',
                'aharonii',
            ],
        ],
        [
            ['Certhia', 'americana', 'guerrerensis'],
            [
                'Certhia',
                'americana',
                'alticola',
            ],
        ],
        [
            ['Certhia', 'americana', 'jaliscensis'],
            [
                'Certhia',
                'americana',
                'alticola',
            ],
        ],
        [
            ['Chalcomitra', 'amethystina', 'adjuncta'],
            [
                'Chalcomitra',
                'amethystina',
                'amethystina',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'anthracina'],
            [
                'Pseudopipra',
                'pipra',
                'anthracina',
            ],
        ],
        [
            ['Muscisaxicola', 'cinereus', 'argentinus'],
            [
                'Muscisaxicola',
                'cinereus',
                'argentina',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'aspasioides'],
            [
                'Leptocoma',
                'aspasia',
                'aspasioides',
            ],
        ],
        [
            ['Chen', 'caerulescens', 'atlantica'],
            [
                'Anser',
                'caerulescens',
                'atlanticus',
            ],
        ],
        [
            ['Chen', 'caerulescens', 'atlantica'],
            [
                'Anser',
                'caerulescens',
                'atlanticus',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'auricapilla'],
            [
                'Leptocoma',
                'aspasia',
                'auricapilla',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'auriceps'],
            [
                'Leptocoma',
                'aspasia',
                'auriceps',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'bernsteini'],
            [
                'Erythropitta',
                'rufiventris',
                'bernsteini',
            ],
        ],
        [
            ['Thryomanes', 'bewickii', 'altus'],
            [
                'Thryomanes',
                'bewickii',
                'bewickii',
            ],
        ],
        [
            ['Euplectes', 'axillaris', 'quanzae'],
            [
                'Euplectes',
                'axillaris',
                'bocagei',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'bolivari'],
            [
                'Pseudopipra',
                'pipra',
                'bolivari',
            ],
        ],
        [
            ['Certhia', 'brachydactyla', 'stresemanni'],
            [
                'Certhia',
                'brachydactyla',
                'brachydactyla',
            ],
        ],
        [
            ['Uropsila', 'leucogastra', 'australis'],
            [
                'Uropsila',
                'leucogastra',
                'brachyura',
            ],
        ],
        [
            ['Troglodytes', 'aedon', 'nitidus'],
            [
                'Troglodytes',
                'aedon',
                'brunneicollis',
            ],
        ],
        [
            ['Uraeginthus', 'bengalus', 'littoralis'],
            [
                'Uraeginthus',
                'bengalus',
                'brunneigularis',
            ],
        ],
        [
            ['Colibri', 'thalassinus', 'cabanidis'],
            [
                'Colibri',
                'cyanotus',
                'cabanidis',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'caeruleogula'],
            [
                'Leptocoma',
                'aspasia',
                'caeruleogula',
            ],
        ],
        [
            ['Euplectes', 'capensis', 'macrorhynchus'],
            [
                'Euplectes',
                'capensis',
                'capensis',
            ],
        ],
        [
            ['Estrilda', 'astrild', 'schoutedeni'],
            [
                'Estrilda',
                'astrild',
                'cavendishi',
            ],
        ],
        [
            ['Anthreptes', 'malacensis ', 'nesophilus'],
            [
                'Anthreptes',
                'malacensis ',
                'celebensis',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'cephaleucos'],
            [
                'Pseudopipra',
                'pipra',
                'cephaleucos',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'chlorolaema'],
            [
                'Leptocoma',
                'aspasia',
                'chlorolaema',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'christianae'],
            [
                'Leptocoma',
                'aspasia',
                'christianae',
            ],
        ],
        [
            ['Geokichla', 'citrina', 'gibsonhilli'],
            [
                'Geokichla',
                'citrina',
                'citrina',
            ],
        ],
        [
            ['Catharus', 'aurantiirostris', 'aenopennis'],
            [
                'Catharus',
                'aurantiirostris',
                'clarus',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'cochrani'],
            [
                'Leptocoma',
                'aspasia',
                'cochrani',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'comata'],
            [
                'Pseudopipra',
                'pipra',
                'comata',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'coracina'],
            [
                'Pseudopipra',
                'pipra',
                'coracina',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'corinna'],
            [
                'Leptocoma',
                'aspasia',
                'corinna',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'cornelia'],
            [
                'Leptocoma',
                'aspasia',
                'cornelia',
            ],
        ],
        [
            ['Campylorhynchus', 'zonatus', 'panamensis'],
            [
                'Campylorhynchus',
                'zonatus',
                'costaricensis',
            ],
        ],
        [
            ['Colibri', 'thalassinus', 'crissalis'],
            [
                'Colibri',
                'cyanotus',
                'crissalis',
            ],
        ],
        [
            ['Ploceus', 'ocularis', 'tenuirostris'],
            [
                'Ploceus',
                'ocularis',
                'crocatus',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'cyanonota'],
            [
                'Erythropitta',
                'rufiventris',
                'cyanonota',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'digglesi'],
            [
                'Erythropitta',
                'macklotii',
                'digglesi',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'discolor'],
            [
                'Pseudopipra',
                'pipra',
                'discolor',
            ],
        ],
        [
            ['Ramphocelus', 'bresilius', 'dorsalis'],
            [
                'Ramphocelus',
                'bresilia',
                'dorsalis',
            ],
        ],
        [
            ['Thryomanes', 'bewickii', 'atrestus'],
            [
                'Thryomanes',
                'bewickii',
                'drymoecus',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'eichhorni'],
            [
                'Leptocoma',
                'aspasia',
                'eichhorni',
            ],
        ],
        [
            ['Estrilda', 'erythronotos', 'soligena'],
            [
                'Estrilda',
                'erythronotos',
                'erythronotos',
            ],
        ],
        [
            ['Dicaeum', 'everetti', 'sordidum'],
            [
                'Dicaeum',
                'everetti',
                'everetti',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'extima'],
            [
                'Erythropitta',
                'novaehibernicae',
                'extima',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'finschii'],
            [
                'Erythropitta',
                'macklotii',
                'finschii',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'gazellae'],
            [
                'Erythropitta',
                'novaehibernicae',
                'gazellae',
            ],
        ],
        [
            ['Amblyornis', 'macgregoriae', 'germana'],
            [
                'Amblyornis',
                'macgregoriae',
                'germana',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'grayi'],
            [
                'Leptocoma',
                'aspasia',
                'grayi',
            ],
        ],
        [
            ['Catharus', 'aurantiirostris', 'russatus'],
            [
                'Catharus',
                'aurantiirostris',
                'griseiceps',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'habenichti'],
            [
                'Erythropitta',
                'macklotii',
                'habenichti',
            ],
        ],
        [
            ['Xenopipo', 'holochlora', 'holochlora'],
            [
                'Cryptopipo',
                'holochlora',
                'holochlora',
            ],
        ],
        [
            ['Troglodytes', 'troglodytes', 'zagrossiensis'],
            [
                'Troglodytes',
                'troglodytes',
                'hyrcanus',
            ],
        ],
        [
            ['Estrilda', 'astrild', 'sousae'],
            [
                'Estrilda',
                'astrild',
                'jagoensis',
            ],
        ],
        [
            ['Colibri', 'thalassinus', 'kerdeli'],
            [
                'Colibri',
                'cyanotus',
                'kerdeli',
            ],
        ],
        [
            ['Chalcomitra', 'amethystina', 'doggetti'],
            [
                'Chalcomitra',
                'amethystina',
                'kirkii',
            ],
        ],
        [
            ['Cercotrichas', 'leucophrys', 'simulator'],
            [
                'Cercotrichas',
                'leucophrys',
                'leucophrys',
            ],
        ],
        [
            ['Oenanthe ', 'leucopyga', 'aegra'],
            [
                'Oenanthe ',
                'leucopyga',
                'leucopyga',
            ],
        ],
        [
            ['Xenopipo', 'holochlora', 'litae'],
            [
                'Cryptopipo',
                'holochlora',
                'litae',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'loriae'],
            [
                'Erythropitta',
                'macklotii',
                'loriae',
            ],
        ],
        [
            ['Ficedula', 'luzoniensis', 'mindorensis'],
            [
                'Ficedula',
                'luzoniensis',
                'luzoniensis',
            ],
        ],
        [
            ['Copsychus', 'malabaricus', 'interpositus'],
            [
                'Copsychus',
                'malabaricus',
                'macrourus',
            ],
        ],
        [
            ['Catharus', 'dryas', 'ecuadoreanus'],
            [
                'Catharus',
                'dryas',
                'maculatus',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'maforensis'],
            [
                'Leptocoma',
                'aspasia',
                'maforensis',
            ],
        ],
        [
            ['Anthreptes', 'malacensis ', 'anambae'],
            [
                'Anthreptes',
                'malacensis ',
                'malacensis ',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'mariae'],
            [
                'Leptocoma',
                'aspasia',
                'mariae',
            ],
        ],
        [
            ['Lonchura', 'spectabilis', 'sepikensis'],
            [
                'Lonchura',
                'spectabilis',
                'mayri',
            ],
        ],
        [
            ['Catharus', 'aurantiirostris', 'bangsi'],
            [
                'Catharus',
                'aurantiirostris',
                'melpomene',
            ],
        ],
        [
            ['Thryomanes', 'bewickii', 'murinus'],
            [
                'Thryomanes',
                'bewickii',
                'mexicanus',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'microlopha'],
            [
                'Pseudopipra',
                'pipra',
                'microlopha',
            ],
        ],
        [
            ['Cinnyris', 'bifasciatus', 'strophium'],
            [
                'Cinnyris',
                'bifasciatus',
                'microrhynchus',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'minima'],
            [
                'Pseudopipra',
                'pipra',
                'minima',
            ],
        ],
        [
            ['Passer', 'moabiticus', 'mesopotamicus'],
            [
                'Passer',
                'moabiticus',
                'moabiticus',
            ],
        ],
        [['Dicaeum', 'agile', 'remotum'], ['Dicaeum', 'agile', 'modestum']],
        [
            ['Melaenornis', 'pallidus', 'aquaemontis'],
            [
                'Melaenornis',
                'pallidus',
                'murinus',
            ],
        ],
        [
            ['Copsychus', 'saularis', 'zacnecus'],
            [
                'Copsychus',
                'saularis',
                'musicus',
            ],
        ],
        [
            ['Copsychus', 'saularis', 'nesiarchus'],
            [
                'Copsychus',
                'saularis',
                'musicus',
            ],
        ],
        [
            ['Copsychus', 'saularis', 'masculus'],
            [
                'Copsychus',
                'saularis',
                'musicus',
            ],
        ],
        [
            ['Copsychus', 'saularis', 'pagiensis'],
            [
                'Copsychus',
                'saularis',
                'musicus',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'mysorensis'],
            [
                'Leptocoma',
                'aspasia',
                'mysorensis',
            ],
        ],
        [
            ['Sitta', 'neumayer', 'rupicola'],
            [
                'Sitta',
                'neumayer',
                'neumayer',
            ],
        ],
        [
            ['Sitta', 'neumayer', 'syriaca'],
            [
                'Sitta',
                'neumayer',
                'neumayer',
            ],
        ],
        [
            ['Bubalornis', 'niger', 'militaris'],
            [
                'Bubalornis',
                'niger',
                'niger',
            ],
        ],
        [
            ['Ploceus', 'cucullatus', 'paroptus'],
            [
                'Ploceus',
                'cucullatus',
                'nigriceps',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'nigriscapularis'],
            [
                'Leptocoma',
                'aspasia',
                'nigriscapularis',
            ],
        ],
        [
            ['Lonchura', 'punctulata', 'holmesi'],
            [
                'Lonchura',
                'punctulata',
                'nisoria',
            ],
        ],
        [
            ['Anthus', 'novaeseelandiae', 'reischeki'],
            [
                'Anthus',
                'novaeseelandiae',
                'novaeseelandiae',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'occulta'],
            [
                'Pseudopipra',
                'pipra',
                'occulta',
            ],
        ],
        [
            ['Ploceus', 'ocularis', 'brevior'],
            [
                'Ploceus',
                'ocularis',
                'ocularis',
            ],
        ],
        [
            ['Cercotrichas', 'paena', 'damarensis'],
            [
                'Cercotrichas',
                'paena',
                'paena',
            ],
        ],
        [
            ['Alaudala', 'cheleensis', 'persica'],
            [
                'Alaudala',
                'rufescens ',
                'persica',
            ],
        ],
        [['Dixiphia', 'pipra', 'pipra'], ['Pseudopipra', 'pipra', 'pipra']],
        [
            ['Erythropitta', 'erythrogaster', 'piroensis'],
            [
                'Erythropitta',
                'rubrinucha',
                'piroensis',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'porphyrolaema'],
            [
                'Leptocoma',
                'aspasia',
                'porphyrolaema',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'proserpina'],
            [
                'Leptocoma',
                'aspasia',
                'proserpina',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'pygmaea'],
            [
                'Pseudopipra',
                'pipra',
                'pygmaea',
            ],
        ],
        [
            ['Threskiornis', 'moluccus', 'pygmaeus'],
            [
                'Threskiornis',
                'molucca',
                'pygmaeus',
            ],
        ],
        [
            ['Cinnyris', 'regius', 'kivuensis'],
            [
                'Cinnyris',
                'regius',
                'regius',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'salvadorii'],
            [
                'Leptocoma',
                'aspasia',
                'salvadorii',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'sangirensis'],
            [
                'Leptocoma',
                'aspasia',
                'sangirensis',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'saturatior'],
            [
                'Terpsiphone',
                'affinis',
                'saturatior',
            ],
        ],
        [
            ['Copsychus', 'saularis', 'erimelas'],
            [
                'Copsychus',
                'saularis',
                'saularis',
            ],
        ],
        [
            ['Copsychus', 'saularis', 'prosthopellus'],
            [
                'Copsychus',
                'saularis',
                'saularis',
            ],
        ],
        [
            ['Dixiphia', 'pipra', 'separabilis'],
            [
                'Pseudopipra',
                'pipra',
                'separabilis',
            ],
        ],
        [
            ['Monticola', 'sharpei', 'salomonseni'],
            [
                'Monticola',
                'sharpei',
                'sharpei',
            ],
        ],
        [
            ['Ploceus', 'cucullatus', 'dilutescens'],
            [
                'Ploceus',
                'cucullatus',
                'spilonotus',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'splendida'],
            [
                'Erythropitta',
                'novaehibernicae',
                'splendida',
            ],
        ],
        [
            ['Ploceus', 'bicolor', 'sylvanus'],
            [
                'Ploceus',
                'bicolor',
                'stictifrons',
            ],
        ],
        [
            ['Ploceus', 'subaureus', 'tongensis'],
            [
                'Ploceus',
                'subaureus',
                'subaureus',
            ],
        ],
        [
            ['Amandava', 'subflava', 'niethammeri'],
            [
                'Amandava',
                'subflava',
                'subflava',
            ],
        ],
        [
            ['Xenopipo', 'holochlora', 'suffusa'],
            [
                'Cryptopipo',
                'holochlora',
                'suffusa',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'talautensis'],
            [
                'Leptocoma',
                'aspasia',
                'talautensis',
            ],
        ],
        [
            ['Pogonocichla', 'stellata', 'hygrica'],
            [
                'Pogonocichla',
                'stellata',
                'transvaalensis',
            ],
        ],
        [
            ['Pogonocichla', 'stellata', 'chirindensis'],
            [
                'Pogonocichla',
                'stellata',
                'transvaalensis',
            ],
        ],
        [['Dixiphia', 'pipra', 'unica'], ['Pseudopipra', 'pipra', 'unica']],
        [
            ['Anthus', 'vaalensis', 'clanceyi'],
            [
                'Anthus',
                'vaalensis',
                'vaalensis',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'veronica'],
            [
                'Leptocoma',
                'aspasia',
                'veronica',
            ],
        ],
        [
            ['Xenopipo', 'holochlora', 'viridior'],
            [
                'Cryptopipo',
                'holochlora',
                'viridior',
            ],
        ],
        [
            ['Lonchura', 'spectabilis', 'gajduseki'],
            [
                'Lonchura',
                'spectabilis',
                'wahgiensis',
            ],
        ],
        [
            ['Threskiornis', 'moluccus', 'moluccus'],
            [
                'Threskiornis',
                'molucca',
                'molucca',
            ],
        ],
        [['Dixiphia', 'pipra', 'pipra'], ['Pseudopipra', 'pipra', 'pipra']],
        [
            ['Dixiphia', 'mentalis', 'mentalis'],
            [
                'Ceratopipra',
                'mentalis',
                'mentalis',
            ],
        ],
        [
            ['Dixiphia', 'erythrocephala', 'erythrocephala'],
            [
                'Ceratopipra',
                'erythrocephala',
                'erythrocephala',
            ],
        ],
        [
            ['Xenopipo', 'holochlora', 'holochlora'],
            [
                'Cryptopipo',
                'holochlora',
                'holochlora',
            ],
        ],
        [
            ['Leptocoma', 'sericea', 'sericea'],
            [
                'Leptocoma',
                'aspasia',
                'aspacia',
            ],
        ],
        [
            ['Ramphocelus', 'bresilius', 'bresilius'],
            [
                'Ramphocelus',
                'bresilia',
                'bresilia',
            ],
        ],
        [
            ['Pterodroma', 'macroptera', 'gouldi'],
            [
                'Pterodroma',
                'gouldi',
                null,
            ],
        ],
        [
            ['Pterodroma', 'macroptera', 'macroptera'],
            [
                'Pterodroma',
                'macroptera',
                null,
            ],
        ],
        [
            ['Oceanodroma', 'leucorhoa', 'socorroensis'],
            [
                'Oceanodroma',
                'socorroensis',
                null,
            ],
        ],
        [
            ['Oceanodroma', 'leucorhoa', 'cheimomnestes'],
            [
                'Oceanodroma',
                'cheimomnestes',
                null,
            ],
        ],
        [['Otus', 'scops', 'cyprius'], ['Otus', 'cyprius', null]],
        [
            ['Colibri', 'thalassinus', 'thalassinus'],
            [
                'Colibri',
                'thalassinus',
                null,
            ],
        ],
        [
            ['Colibri', 'thalassinus', 'cyanotus'],
            [
                'Colibri',
                'cyanotus',
                'cyanotus',
            ],
        ],
        [
            ['Dinopium', 'benghalense', 'psarodes'],
            [
                'Dinopium',
                'psarodes',
                null,
            ],
        ],
        [
            ['Psittacara', 'chloropterus', 'maugei'],
            [
                'Psittacara',
                'maugei',
                null,
            ],
        ],
        [
            ['Psittacara', 'chloropterus', 'chloropterus'],
            [
                'Psittacara',
                'chloropterus',
                null,
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'dohertyi'],
            [
                'Erythropitta',
                'dohertyi',
                null,
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'celebensis'],
            [
                'Erythropitta',
                'celebensis',
                null,
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'palliceps'],
            [
                'Erythropitta',
                'palliceps',
                null,
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'caeruleitorques'],
            [
                'Erythropitta',
                'caeruleitorques',
                null,
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'rubrinucha'],
            [
                'Erythropitta',
                'rubrinucha',
                'rubrinucha',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'rufiventris'],
            [
                'Erythropitta',
                'rufiventris',
                'rufiventris',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'meeki'],
            [
                'Erythropitta',
                'meeki',
                null,
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'novaehibernicae'],
            [
                'Erythropitta',
                'novaehibernicae',
                'novaehibernicae',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'macklotii'],
            [
                'Erythropitta',
                'macklotii',
                'macklotii',
            ],
        ],
        [
            ['Asthenes', 'vilcabambae', 'vilcabambae'],
            [
                'Asthenes',
                'vilcabambae',
                null,
            ],
        ],
        [
            ['Asthenes', 'vilcabambae', 'ayacuchensis'],
            [
                'Asthenes',
                'ayacuchensis',
                null,
            ],
        ],
        [
            ['Pycnonotus', 'bimaculatus', 'snouckaerti'],
            [
                'Pycnonotus',
                'snouckaerti',
                null,
            ],
        ],
        [
            ['Cantorchilus', 'modestus', 'modestus'],
            [
                'Cantorchilus',
                'modestus',
                null,
            ],
        ],
        [
            ['Cantorchilus', 'modestus', 'zeledoni'],
            [
                'Cantorchilus',
                'zeledoni',
                null,
            ],
        ],
        [
            ['Cantorchilus', 'modestus', 'elutus'],
            [
                'Cantorchilus',
                'elutus',
                null,
            ],
        ],
        [
            ['Henicorhina', 'leucophrys', 'anachoreta'],
            [
                'Henicorhina',
                'anachoreta',
                null,
            ],
        ],
        [
            ['Zoothera', 'mollissima', 'griseiceps'],
            [
                'Zoothera',
                'griseiceps',
                null,
            ],
        ],
        [['Fringilla', 'teydea', 'teydea'], ['Fringilla', 'teydea', null]],
        [
            ['Fringilla', 'teydea', 'polatzeki'],
            [
                'Fringilla',
                'polatzeki',
                null,
            ],
        ],
        [
            ['Stymphalornis', 'acutirostris', null],
            [
                'Stymphalornis',
                'acutirostris',
                'acutirostris',
            ],
        ],
        [
            ['Myadestes', 'unicolor', 'unicolor'],
            [
                'Myadestes',
                'unicolor',
                null,
            ],
        ],
        [['Cyornis', 'turcosus', 'turcosus'], ['Cyornis', 'turcosus', null]],
        [
            ['Vauriella', 'gularis', 'gularis'],
            [
                'Vauriella',
                'gularis',
                null,
            ],
        ],
        [['Oenanthe', 'moesta', 'moesta'], ['Oenanthe', 'moesta', null]],
        [
            ['Cinnyris', 'minullus', 'minullus'],
            [
                'Cinnyris',
                'minullus',
                null,
            ],
        ],
        [['Cinnyris', 'afer', 'afer'], ['Cinnyris', 'afer', null]],
        [
            ['Philetairus', 'socius', 'socius'],
            [
                'Philetairus',
                'socius',
                null,
            ],
        ],
        [['Ploceus', 'velatus', 'velatus'], ['Ploceus', 'velatus', null]],
        [
            ['Ploceus', 'olivaceiceps', 'olivaceiceps'],
            [
                'Ploceus',
                'olivaceiceps',
                null,
            ],
        ],
        [['Euplectes', 'orix', 'orix'], ['Euplectes', 'orix', null]],
        [
            ['Pyrenestes', 'sanguineus', 'sanguineus'],
            [
                'Pyrenestes',
                'sanguineus',
                null,
            ],
        ],
        [
            ['Uraeginthus', 'granatinus', 'granatinus'],
            [
                'Uraeginthus',
                'granatinus',
                null,
            ],
        ],
        [['Estrilda', 'melpoda', 'melpoda'], ['Estrilda', 'melpoda', null]],
    ];

$lumps =
    [
        [['Fulica', 'caribaea', null], ['Fulica', 'americana', null]],

        [
            ['Myadestes', 'unicolor', 'pallens'],
            [
                'Myadestes',
                'unicolor',
                null,
            ],
        ]
        ,
        [
            ['Cyornis', 'turcosus', 'rupatensis'],
            [
                'Cyornis',
                'turcosus',
                null,
            ],
        ]
        ,
        [['Vauriella', 'gularis', 'kamlae'], ['Vauriella', 'gularis', null]]
        ,
        [
            ['Oenanthe', 'moesta', 'brooksbanki'],
            [
                'Oenanthe',
                'moesta',
                null,
            ],
        ]
        ,
        [
            ['Cinnyris', 'minullus', 'amadoni'],
            [
                'Cinnyris',
                'minullus',
                null,
            ],
        ]
        ,
        [['Cinnyris', 'afer', 'saliens'], ['Cinnyris', 'afer', null]]
        ,
        [
            ['Philetairus', 'socius', 'geminus'],
            [
                'Philetairus',
                'socius',
                null,
            ],
        ]
        ,
        [
            ['Philetairus', 'socius', 'xericus'],
            [
                'Philetairus',
                'socius',
                null,
            ],
        ]
        ,
        [['Ploceus', 'velatus', 'nigrifrons'], ['Ploceus', 'velatus', null]]
        ,
        [['Ploceus', 'velatus', 'peixotoi'], ['Ploceus', 'velatus', null]]
        ,
        [
            ['Ploceus', 'olivaceiceps', 'vicarius'],
            [
                'Ploceus',
                'olivaceiceps',
                null,
            ],
        ]
        ,
        [['Euplectes', 'orix', 'sundevalli'], ['Euplectes', 'orix', null]]
        ,
        [['Euplectes', 'orix', 'turgidus'], ['Euplectes', 'orix', null]]
        ,
        [['Euplectes', 'orix', 'nigrifrons'], ['Euplectes', 'orix', null]]
        ,
        [
            ['Pyrenestes', 'sanguineus', 'coccineus'],
            [
                'Pyrenestes',
                'sanguineus',
                null,
            ],
        ]
        ,
        [
            ['Uraeginthus', 'granatinus', 'siccatus'],
            [
                'Uraeginthus',
                'granatinus',
                null,
            ],
        ]
        ,
        [
            ['Uraeginthus', 'granatinus', 'retusus'],
            [
                'Uraeginthus',
                'granatinus',
                null,
            ],
        ]
        ,
        [
            ['Estrilda', 'melpoda', 'tschadensis'],
            [
                'Estrilda',
                'melpoda',
                null,
            ],
        ],
    ];
