<?php

$renames =
    [
        [['Puffinus', 'pacificus'], ['Ardenna', 'pacifica']],
        [['Puffinus', 'bulleri'], ['Ardenna', 'bulleri']],
        [['Puffinus', 'griseus'], ['Ardenna', 'grisea']],
        [['Puffinus', 'tenuirostris'], ['Ardenna', 'tenuirostris']],
        [['Puffinus', 'creatopus'], ['Ardenna', 'creatopus']],
        [['Puffinus', 'carneipes'], ['Ardenna', 'carneipes']],
        [['Puffinus', 'gravis'], ['Ardenna', 'gravis']],
        [['Dryocopus', 'galeatus'], ['Celeus', 'galeatus']],
        [['Leucosticte', 'sillemi'], ['Carpodacus', 'sillemi']],
    ];

$splits =
    [
        [
            ['Puffinus', 'assimilis', 'elegans'],
            [
                'Puffinus',
                'elegans',
                null,
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'alberti'],
            [
                'Todiramphus',
                'tristrami',
                'alberti',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'ambrynensis'],
            [
                'Petroica',
                'pusilla',
                'ambrynensis',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'amoenus'],
            [
                'Todiramphus',
                'sacer',
                'amoenus',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'becki'],
            [
                'Petroica',
                'pusilla',
                'becki',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'bennetti'],
            [
                'Todiramphus',
                'tristrami',
                'bennetti',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'borneensis'],
            [
                'Terpsiphone',
                'affinis',
                'borneensis',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'brachyurus'],
            [
                'Todiramphus',
                'sacer',
                'brachyurus',
            ],
        ],
        [
            ['Drymodes', 'superciliaris', 'brevirostris'],
            [
                'Drymodes',
                'beccarii',
                'brevirostris',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'burmae'],
            [
                'Terpsiphone',
                'affinis',
                'burmae',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'cognata'],
            [
                'Petroica',
                'pusilla',
                'cognata',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'colcloughi'],
            [
                'Todiramphus',
                'sordidus',
                'colcloughi',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'dennisi'],
            [
                'Petroica',
                'pusilla',
                'dennisi',
            ],
        ],
        [
            ['Bradypterus', 'baboecala', 'elgonensis'],
            [
                'Bradypterus',
                'centralis',
                'elgonensis',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'erromangae'],
            [
                'Todiramphus',
                'sacer',
                'erromangae',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'eximius'],
            [
                'Todiramphus',
                'sacer',
                'eximius',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'feminina'],
            [
                'Petroica',
                'pusilla',
                'feminina',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'floris'],
            [
                'Terpsiphone',
                'affinis',
                'floris',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'indochinensis'],
            [
                'Terpsiphone',
                'affinis',
                'indochinensis',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'insularis'],
            [
                'Terpsiphone',
                'affinis',
                'insularis',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'juliae'],
            [
                'Todiramphus',
                'sacer',
                'juliae',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'kleinschmidti'],
            [
                'Petroica',
                'pusilla',
                'kleinschmidti',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'kulambangrae'],
            [
                'Petroica',
                'pusilla',
                'kulambangrae',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'mala'],
            [
                'Todiramphus',
                'sacer',
                'mala',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'manuae'],
            [
                'Todiramphus',
                'sacer',
                'manuae',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'marinus'],
            [
                'Todiramphus',
                'sacer',
                'marinus',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'matthiae'],
            [
                'Todiramphus',
                'tristrami',
                'matthiae',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'melanodera'],
            [
                'Todiramphus',
                'sacer',
                'melanodera',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'nicobarica'],
            [
                'Terpsiphone',
                'affinis',
                'nicobarica',
            ],
        ],
        [
            ['Drymodes', 'superciliaris', 'nigriceps'],
            [
                'Drymodes',
                'beccarii',
                'nigriceps',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'novaehiberniae'],
            [
                'Todiramphus',
                'tristrami',
                'novaehiberniae',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'nusae'],
            [
                'Todiramphus',
                'tristrami',
                'nusae',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'orii'],
            [
                'Todiramphus',
                'albicilla',
                'orii',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'ornatus'],
            [
                'Todiramphus',
                'sacer',
                'ornatus',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'owstoni'],
            [
                'Todiramphus',
                'albicilla',
                'owstoni',
            ],
        ],
        [['Ceyx', 'lepidus', 'pallidus'], ['Ceyx', 'meeki', 'pallidus']],
        [
            ['Todiramphus', 'chloris', 'pavuvu'],
            [
                'Todiramphus',
                'sacer',
                'pavuvu',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'pealei'],
            [
                'Todiramphus',
                'sacer',
                'pealei',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'pilbara'],
            [
                'Todiramphus',
                'sordidus',
                'pilbara',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'polymorpha'],
            [
                'Petroica',
                'pusilla',
                'polymorpha',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'procera'],
            [
                'Terpsiphone',
                'affinis',
                'procera',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'pusilla'],
            [
                'Petroica',
                'pusilla',
                'pusilla',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'regina'],
            [
                'Todiramphus',
                'sacer',
                'regina',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'santoensis'],
            [
                'Todiramphus',
                'sacer',
                'santoensis',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'caspius'],
            [
                'Porphyrio',
                'poliocephalus',
                'seistanicus',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'septentrionalis'],
            [
                'Petroica',
                'pusilla',
                'septentrionalis',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'similis'],
            [
                'Petroica',
                'pusilla',
                'similis',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'solomonis'],
            [
                'Todiramphus',
                'sacer',
                'solomonis',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'soror'],
            [
                'Petroica',
                'pusilla',
                'soror',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'sororum'],
            [
                'Todiramphus',
                'sacer',
                'sororum',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'stresemanni'],
            [
                'Todiramphus',
                'tristrami',
                'stresemanni',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'sumbaensis'],
            [
                'Terpsiphone',
                'affinis',
                'sumbaensis',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'tannensis'],
            [
                'Todiramphus',
                'sacer',
                'tannensis',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'taveunensis'],
            [
                'Petroica',
                'pusilla',
                'taveunensis',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'torresianus'],
            [
                'Todiramphus',
                'sacer',
                'torresianus',
            ],
        ],
        //array(array("Puffinus","elegans", "tunneyi"),array("Puffinus","assimilis", "tunneyi")),
        [
            ['Todiramphus', 'chloris', 'utupuae'],
            [
                'Todiramphus',
                'sacer',
                'utupuae',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'vicina'],
            [
                'Todiramphus',
                'sacer',
                'vicina',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'vitiensis'],
            [
                'Todiramphus',
                'sacer',
                'vitiensis',
            ],
        ],
        [
            ['Bradypterus', 'baboecala', 'centralis'],
            [
                'Bradypterus',
                'centralis',
                'centralis',
            ],
        ],
        [
            ['Petroica', 'multicolor', 'multicolor'],
            [
                'Petroica',
                'multicolor',
                null,
            ],
        ],
        [
            ['Petroica', 'australis', 'longipes'],
            [
                'Petroica',
                'longipes',
                null,
            ],
        ],
        [
            ['Petroica', 'australis', 'australis'],
            [
                'Petroica',
                'australis',
                'australis',
            ],
        ],
        [
            ['Drymodes', 'superciliaris', 'beccarii'],
            [
                'Drymodes',
                'beccarii',
                'beccarii',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'incei'],
            [
                'Terpsiphone',
                'incei',
                null,
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'affinis'],
            [
                'Terpsiphone',
                'affinis',
                'affinis',
            ],
        ],
        [
            ['Terpsiphone', 'paradisi', 'paradisi'],
            [
                'Terpsiphone',
                'paradisi',
                'paradisi',
            ],
        ],
        [
            ['Ceyx', 'argentatus', 'flumenicola'],
            [
                'Ceyx',
                'flumenicola',
                null,
            ],
        ],
        [['Ceyx', 'argentatus', 'argentatus'], ['Ceyx', 'argentatus', null]],
        [['Ceyx', 'lepidus', 'gentianus'], ['Ceyx', 'gentianus', null]],
        [
            ['Ceyx', 'lepidus', 'nigromaxilla'],
            [
                'Ceyx',
                'nigromaxilla',
                null,
            ],
        ],
        [['Ceyx', 'lepidus', 'malaitae'], ['Ceyx', 'malaitae', null]],
        [['Ceyx', 'lepidus', 'collectoris'], ['Ceyx', 'collectoris', null]],
        [['Ceyx', 'lepidus', 'meeki'], ['Ceyx', 'meeki', 'meeki']],
        [['Ceyx', 'lepidus', 'sacerdotis'], ['Ceyx', 'sacerdotis', null]],
        [['Ceyx', 'lepidus', 'mulcatus'], ['Ceyx', 'mulcatus', null]],
        [['Ceyx', 'lepidus', 'dispar'], ['Ceyx', 'dispar', null]],
        [['Ceyx', 'lepidus', 'solitarius'], ['Ceyx', 'solitarius', null]],
        [['Ceyx', 'lepidus', 'wallacii'], ['Ceyx', 'wallacii', null]],
        [['Ceyx', 'lepidus', 'cajeli'], ['Ceyx', 'cajeli', null]],
        [['Ceyx', 'lepidus', 'margarethae'], ['Ceyx', 'margarethae', null]],
        [
            ['Todiramphus', 'chloris', 'sacer'],
            [
                'Todiramphus',
                'sacer',
                'sacer',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'colonus'],
            [
                'Todiramphus',
                'colonus',
                null,
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'sordidus'],
            [
                'Todiramphus',
                'sordidus',
                'sordidus',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'albicilla'],
            [
                'Todiramphus',
                'albicilla',
                'albicilla',
            ],
        ],
        [
            ['Todiramphus', 'chloris', 'tristrami'],
            [
                'Todiramphus',
                'tristrami',
                'tristrami',
            ],
        ],
        [
            ['Todiramphus', 'cinnamominus', 'reichenbachii'],
            [
                'Todiramphus',
                'reichenbachii',
                null,
            ],
        ],
        [
            ['Todiramphus', 'cinnamominus', 'pelewensis'],
            [
                'Todiramphus',
                'pelewensis',
                null,
            ],
        ],
        [
            ['Todiramphus', 'cinnamominus', 'cinnamominus'],
            [
                'Todiramphus',
                'cinnamominus',
                'cinnamominus',
            ],
        ],
        [
            ['Todiramphus', 'gambieri', 'gertrudae'],
            [
                'Todiramphus',
                'gertrudae',
                null,
            ],
        ],
        [
            ['Todiramphus', 'gambieri', 'gambieri'],
            [
                'Todiramphus',
                'gambieri',
                null,
            ],
        ],
        [
            ['Puffinus', 'assimilis', 'elegans'],
            [
                'Puffinus',
                'elegans',
                null,
            ],
        ],
        [
            ['Bambusicola', 'thoracicus', 'sonorivox'],
            [
                'Bambusicola',
                'sonorivox',
                null,
            ],
        ],
        [
            ['Bambusicola', 'thoracicus', 'thoracicus'],
            [
                'Bambusicola',
                'thoracicus',
                null,
            ],
        ],
    ];

$lumps =
    [

        [
            ['Drymodes', 'superciliaris', 'superciliaris'],
            [
                'Drymodes',
                'superciliaris',
                null,
            ],
        ],
    ];
