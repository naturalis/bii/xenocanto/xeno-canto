<?php

$renames =
    [
        [['Anous', 'albivittus'], ['Anous', 'albivitta']],
        [['Cercomacra', 'fuscicauda'], ['Cercomacroides', 'fuscicauda']],
        [['Cercomacra', 'laeta'], ['Cercomacroides', 'laeta']],
        [['Cercomacra', 'nigrescens'], ['Cercomacroides', 'nigrescens']],
        [['Cercomacra', 'parkeri'], ['Cercomacroides', 'parkeri']],
        [['Cercomacra', 'serva'], ['Cercomacroides', 'serva']],
        [['Cercomacra', 'tyrannina'], ['Cercomacroides', 'tyrannina']],
        [['Chlorospingus', 'flavovirens'], ['Bangsia', 'flavovirens']],
        [['Cyornis', 'pallipes'], ['Cyornis', 'pallidipes']],
        [['Dicrurus', 'annectans'], ['Dicrurus', 'annectens']],
        [['Gymnopithys', 'lunulatus'], ['Oneillornis', 'lunulatus']],
        [['Gymnopithys', 'salvini'], ['Oneillornis', 'salvini']],
        [['Heterophasia', 'annectans'], ['Heterophasia', 'annectens']],
        [['Iduna', 'aedon'], ['Arundinax', 'aedon']],
        [['Myrmeciza', 'atrothorax'], ['Myrmophylax', 'atrothorax']],
        [['Myrmeciza', 'berlepschi'], ['Sipia', 'berlepschi']],
        [['Myrmeciza', 'castanea'], ['Sciaphylax', 'castanea']],
        [['Myrmeciza', 'disjuncta'], ['Aprositornis', 'disjuncta']],
        [['Myrmeciza', 'exsul'], ['Poliocrania', 'exsul']],
        [['Myrmeciza', 'ferruginea'], ['Myrmoderus', 'ferrugineus']],
        [['Myrmeciza', 'fortis'], ['Hafferia', 'fortis']],
        [['Myrmeciza', 'goeldii'], ['Akletos', 'goeldii']],
        [['Myrmeciza', 'griseiceps'], ['Ampelornis', 'griseiceps']],
        [['Myrmeciza', 'hemimelaena'], ['Sciaphylax', 'hemimelaena']],
        [['Myrmeciza', 'hyperythra'], ['Myrmelastes', 'hyperythrus']],
        [['Myrmeciza', 'immaculata'], ['Hafferia', 'immaculata']],
        [['Myrmeciza', 'laemosticta'], ['Sipia', 'laemosticta']],
        [['Myrmeciza', 'loricata'], ['Myrmoderus', 'loricatus']],
        [['Myrmeciza', 'melanoceps'], ['Akletos', 'melanoceps']],
        [['Myrmeciza', 'nigricauda'], ['Sipia', 'nigricauda']],
        [['Myrmeciza', 'palliata'], ['Sipia', 'palliata']],
        [['Myrmeciza', 'pelzelni'], ['Ammonastes', 'pelzelni']],
        [['Myrmeciza', 'ruficauda'], ['Myrmoderus', 'ruficauda']],
        [['Myrmeciza', 'squamosa'], ['Myrmoderus', 'squamosus']],
        [['Myrmeciza', 'zeledoni'], ['Hafferia', 'zeledoni']],
        [['Notopholia', 'corrusca'], ['Notopholia', 'corusca']],
        [['Passer', 'rutilans'], ['Passer', 'cinnamomeus']],
        [['Percnostola', 'lophotes'], ['Myrmoborus', 'lophotes']],
        [['Phyllergates', 'cuculatus'], ['Phyllergates', 'cucullatus']],
        [['Pseudalethe', 'poliocephala'], ['Chamaetylas', 'poliocephala']],
        [['Pseudalethe', 'choloensis'], ['Chamaetylas', 'choloensis']],
        [['Pseudalethe', 'fuelleborni'], ['Chamaetylas', 'fuelleborni']],
        [['Pseudalethe', 'poliophrys'], ['Chamaetylas', 'poliophrys']],
        [['Schistocichla', 'brunneiceps'], ['Myrmelastes', 'brunneiceps']],
        [['Schistocichla', 'caurensis'], ['Myrmelastes', 'caurensis']],
        [['Schistocichla', 'humaythae'], ['Myrmelastes', 'humaythae']],
        [['Schistocichla', 'leucostigma'], ['Myrmelastes', 'leucostigma']],
        [['Schistocichla', 'rufifacies'], ['Myrmelastes', 'rufifacies']],
        [['Schistocichla', 'saturata'], ['Myrmelastes', 'saturatus']],
        [['Schistocichla', 'schistacea'], ['Myrmelastes', 'schistaceus']],
        [['Sturnella', 'bellicosa'], ['Leistes', 'bellicosus']],
        [['Sturnella', 'defilippii'], ['Leistes', 'defilippii']],
        [['Sturnella', 'loyca'], ['Leistes', 'loyca']],
        [['Sturnella', 'militaris'], ['Leistes', 'militaris']],
        [['Sturnella', 'superciliaris'], ['Leistes', 'superciliaris']],
        [['Sugomel', 'nigrum'], ['Sugomel', 'niger']],
        [['Tadorna', 'radjah'], ['Radjah', 'radjah']],
    ];
$splits  =
    [
        [['Anas', 'crecca', 'nimia'], ['Anas', 'crecca', '']],
        [['Anas', 'crecca', 'crecca'], ['Anas', 'crecca', '']],
        [
            ['Anous', 'albivittus', 'skottsbergii'],
            [
                'Anous',
                'albivitta',
                'skottsbergii',
            ],
        ],
        [
            ['Anous', 'albivittus', 'imitatrix'],
            [
                'Anous',
                'albivitta',
                'imitatrix',
            ],
        ],
        [
            ['Anous', 'albivittus', 'albivittus'],
            [
                'Anous',
                'albivitta',
                'albivitta',
            ],
        ],
        [['Anthus', 'furcatus', 'furcatus'], ['Anthus', 'furcatus', '']],
        [
            ['Anthus', 'furcatus', 'brevirostris'],
            [
                'Anthus',
                'brevirostris',
                '',
            ],
        ],
        [
            ['Anthus', 'lutescens', 'peruvianus'],
            [
                'Anthus',
                'peruvianus',
                '',
            ],
        ],
        [['Aythya', 'australis', 'extima'], ['Aythya', 'australis', '']],
        [['Aythya', 'australis', 'australis'], ['Aythya', 'australis', '']],
        [
            ['Branta', 'hutchinsii', 'asiatica'],
            [
                'Branta',
                'hutchinsii',
                'leucopareia',
            ],
        ],
        [
            ['Calamanthus', 'campestris', 'rubiginosis'],
            [
                'Calamanthus',
                'campestris',
                'rubiginosus',
            ],
        ],
        [
            ['Caprimulgus', 'ruwenzorii', 'guttifer'],
            [
                'Caprimulgus',
                'poliocephalus',
                'guttifer',
            ],
        ],
        [
            ['Caprimulgus', 'ruwenzorii', 'koesteri'],
            [
                'Caprimulgus',
                'poliocephalus',
                'koesteri',
            ],
        ],
        [
            ['Caprimulgus', 'ruwenzorii', 'ruwenzorii'],
            [
                'Caprimulgus',
                'poliocephalus',
                'ruwenzorii',
            ],
        ],
        [['Catharus', 'dryas', 'dryas'], ['Catharus', 'dryas', '']],
        [
            ['Catharus', 'dryas', 'maculatus'],
            [
                'Catharus',
                'maculatus',
                'maculatus',
            ],
        ],
        [
            ['Catharus', 'dryas', 'blakei'],
            [
                'Catharus',
                'maculatus',
                'blakei',
            ],
        ],
        [
            ['Cercomacra', 'laeta', 'waimiri'],
            [
                'Cercomacroides',
                'laeta',
                'waimiri',
            ],
        ],
        [
            ['Cercomacra', 'laeta', 'sabinoi'],
            [
                'Cercomacroides',
                'laeta',
                'sabinoi',
            ],
        ],
        [
            ['Cercomacra', 'laeta', 'laeta'],
            [
                'Cercomacroides',
                'laeta',
                'laeta',
            ],
        ],
        [
            ['Cercomacra', 'nigrescens', 'aequatorialis'],
            [
                'Cercomacroides',
                'nigrescens',
                'aequatorialis',
            ],
        ],
        [
            ['Cercomacra', 'nigrescens', 'notata'],
            [
                'Cercomacroides',
                'nigrescens',
                'notata',
            ],
        ],
        [
            ['Cercomacra', 'nigrescens', 'approximans'],
            [
                'Cercomacroides',
                'nigrescens',
                'approximans',
            ],
        ],
        [
            ['Cercomacra', 'nigrescens', 'ochrogyna'],
            [
                'Cercomacroides',
                'nigrescens',
                'ochrogyna',
            ],
        ],
        [
            ['Cercomacra', 'nigrescens', 'nigrescens'],
            [
                'Cercomacroides',
                'nigrescens',
                'nigrescens',
            ],
        ],
        [
            ['Cercomacra', 'tyrannina', 'crepera'],
            [
                'Cercomacroides',
                'tyrannina',
                'crepera',
            ],
        ],
        [
            ['Cercomacra', 'tyrannina', 'vicina'],
            [
                'Cercomacroides',
                'tyrannina',
                'vicina',
            ],
        ],
        [
            ['Cercomacra', 'tyrannina', 'saturatior'],
            [
                'Cercomacroides',
                'tyrannina',
                'saturatior',
            ],
        ],
        [
            ['Cercomacra', 'tyrannina', 'tyrannina'],
            [
                'Cercomacroides',
                'tyrannina',
                'tyrannina',
            ],
        ],
        [
            ['Certhia', 'familiaris', 'brittanica'],
            [
                'Certhia',
                'familiaris',
                'britanica',
            ],
        ],
        [
            ['Chalybura', 'urochrysia', 'intermedia'],
            [
                'Chalybura',
                'buffonii',
                'intermedia',
            ],
        ],
        [['Cinnyris', 'manoensis', 'pintoi'], ['Cinnyris', 'gertrudis', '']],
        [
            ['Cinnyris', 'manoensis', 'manoensis'],
            [
                'Cinnyris',
                'manoensis',
                '',
            ],
        ],
        [['Cyornis', 'pallipes', ''], ['Cyornis', 'pallidipes', '']],
        [
            ['Cyornis', 'ruficauda', 'occularis'],
            [
                'Cyornis',
                'ruficauda',
                'ocularis',
            ],
        ],
        [
            ['Foudia', 'eminentissima', 'cosobrina'],
            [
                'Foudia',
                'eminentissima',
                'consobrina',
            ],
        ],
        [
            ['Goura', 'scheepmakeri', 'scheepmakeri'],
            [
                'Goura',
                'scheepmakeri',
                '',
            ],
        ],
        [['Goura', 'scheepmakeri', 'sclaterii'], ['Goura', 'sclaterii', '']],
        [
            ['Heterophasia', 'annectans', 'mixta'],
            [
                'Heterophasia',
                'annectens',
                'mixta',
            ],
        ],
        [
            ['Heterophasia', 'annectans', 'saturata'],
            [
                'Heterophasia',
                'annectens',
                'saturata',
            ],
        ],
        [
            ['Heterophasia', 'annectans', 'davisoni'],
            [
                'Heterophasia',
                'annectens',
                'davisoni',
            ],
        ],
        [
            ['Heterophasia', 'annectans', 'roundi'],
            [
                'Heterophasia',
                'annectens',
                'roundi',
            ],
        ],
        [
            ['Heterophasia', 'annectans', 'eximia'],
            [
                'Heterophasia',
                'annectens',
                'eximia',
            ],
        ],
        [
            ['Heterophasia', 'annectans', 'annectans'],
            [
                'Heterophasia',
                'annectens',
                'annectens',
            ],
        ],
        [
            ['Iduna', 'aedon', 'rufescens'],
            [
                'Arundinax',
                'aedon',
                'rufescens',
            ],
        ],
        [['Iduna', 'aedon', 'aedon'], ['Arundinax', 'aedon', 'aedon']],
        [
            ['Laticilla', 'burnesii', 'nepalicola'],
            [
                'Laticilla',
                'burnesii',
                'nipalensis',
            ],
        ],
        [
            ['Malurus', 'lamberti', 'dulcis'],
            [
                'Malurus',
                'assimilis',
                'dulcis',
            ],
        ],
        [
            ['Malurus', 'lamberti', 'rogersi'],
            [
                'Malurus',
                'assimilis',
                'rogersi',
            ],
        ],
        [
            ['Malurus', 'lamberti', 'bernieri'],
            [
                'Malurus',
                'assimilis',
                'bernieri',
            ],
        ],
        [
            ['Malurus', 'lamberti', 'assimilis'],
            [
                'Malurus',
                'assimilis',
                'assimilis',
            ],
        ],
        [['Malurus', 'lamberti', 'lamberti'], ['Malurus', 'lamberti', '']],
        [
            ['Megascops', 'roraimae', ''],
            [
                'Megascops',
                'roraimae',
                'roraimae',
            ],
        ],
        [
            ['Melozone', 'biarcuata', 'hartwegi'],
            [
                'Melozone',
                'biarcuata',
                '',
            ],
        ],
        [
            ['Melozone', 'biarcuata', 'biarcuata'],
            [
                'Melozone',
                'biarcuata',
                '',
            ],
        ],
        [
            ['Myrmeciza', 'atrothorax', 'metae'],
            [
                'Myrmophylax',
                'atrothorax',
                'metae',
            ],
        ],
        [
            ['Myrmeciza', 'atrothorax', 'tenebrosa'],
            [
                'Myrmophylax',
                'atrothorax',
                'tenebrosa',
            ],
        ],
        [
            ['Myrmeciza', 'atrothorax', 'maynana'],
            [
                'Myrmophylax',
                'atrothorax',
                'maynana',
            ],
        ],
        [
            ['Myrmeciza', 'atrothorax', 'melanura'],
            [
                'Myrmophylax',
                'atrothorax',
                'melanura',
            ],
        ],
        [
            ['Myrmeciza', 'atrothorax', 'atrothorax'],
            [
                'Myrmophylax',
                'atrothorax',
                'atrothorax',
            ],
        ],
        [
            ['Myrmeciza', 'castanea', 'centunculorum'],
            [
                'Sciaphylax',
                'castanea',
                'centunculorum',
            ],
        ],
        [
            ['Myrmeciza', 'castanea', 'castanea'],
            [
                'Sciaphylax',
                'castanea',
                'castanea',
            ],
        ],
        [
            ['Myrmeciza', 'exsul', 'occidentalis'],
            [
                'Poliocrania',
                'exsul',
                'occidentalis',
            ],
        ],
        [
            ['Myrmeciza', 'exsul', 'cassini'],
            [
                'Poliocrania',
                'exsul',
                'cassini',
            ],
        ],
        [
            ['Myrmeciza', 'exsul', 'niglarus'],
            [
                'Poliocrania',
                'exsul',
                'niglarus',
            ],
        ],
        [
            ['Myrmeciza', 'exsul', 'maculifer'],
            [
                'Poliocrania',
                'exsul',
                'maculifer',
            ],
        ],
        [['Myrmeciza', 'exsul', 'exsul'], ['Poliocrania', 'exsul', 'exsul']],
        [
            ['Myrmeciza', 'ferruginea', 'eluta'],
            [
                'Myrmoderus',
                'ferrugineus',
                'elutus',
            ],
        ],
        [
            ['Myrmeciza', 'ferruginea', 'ferruginea'],
            [
                'Myrmoderus',
                'ferrugineus',
                'ferrugineus',
            ],
        ],
        [
            ['Myrmeciza', 'fortis', 'incanescens'],
            [
                'Hafferia',
                'fortis',
                'incanescens',
            ],
        ],
        [
            ['Myrmeciza', 'fortis', 'fortis'],
            [
                'Hafferia',
                'fortis',
                'fortis',
            ],
        ],
        [
            ['Myrmeciza', 'hemimelaena', 'pallens'],
            [
                'Sciaphylax',
                'hemimelaena',
                'pallens',
            ],
        ],
        [
            ['Myrmeciza', 'hemimelaena', 'hemimelaena'],
            [
                'Sciaphylax',
                'hemimelaena',
                'hemimelaena',
            ],
        ],
        [
            ['Myrmeciza', 'immaculata', 'concepcion'],
            [
                'Hafferia',
                'immaculata',
                'concepcion',
            ],
        ],
        [
            ['Myrmeciza', 'immaculata', 'immaculata'],
            [
                'Hafferia',
                'immaculata',
                'immaculata',
            ],
        ],
        [
            ['Myrmeciza', 'ruficauda', 'soror'],
            [
                'Myrmoderus',
                'ruficauda',
                'soror',
            ],
        ],
        [
            ['Myrmeciza', 'ruficauda', 'ruficauda'],
            [
                'Myrmoderus',
                'ruficauda',
                'ruficauda',
            ],
        ],
        [
            ['Myrmeciza', 'zeledoni', 'macrorhyncha'],
            [
                'Hafferia',
                'zeledoni',
                'macrorhyncha',
            ],
        ],
        [
            ['Myrmeciza', 'zeledoni', 'zeledoni'],
            [
                'Hafferia',
                'zeledoni',
                'zeledoni',
            ],
        ],
        [
            ['Notopholia', 'corrusca', 'vaughani'],
            [
                'Notopholia',
                'corusca',
                'vaughani',
            ],
        ],
        [
            ['Notopholia', 'corrusca', 'corrusca'],
            [
                'Notopholia',
                'corusca',
                'corusca',
            ],
        ],
        [
            ['Nyctibius', 'grandis', 'guatemalensis'],
            [
                'Nyctibius',
                'grandis',
                '',
            ],
        ],
        [['Nyctibius', 'grandis', 'grandis'], ['Nyctibius', 'grandis', '']],
        [
            ['Onychoprion', 'fuscatus', 'kermadeci'],
            [
                'Onychoprion',
                'fuscatus',
                'serratus',
            ],
        ],
        [['Oxyura', 'jamaicensis', 'rubida'], ['Oxyura', 'jamaicensis', '']],
        [
            ['Oxyura', 'jamaicensis', 'jamaicensis'],
            [
                'Oxyura',
                'jamaicensis',
                '',
            ],
        ],
        [
            ['Passer', 'rutilans', 'intensior'],
            [
                'Passer',
                'cinnamomeus',
                'intensior',
            ],
        ],
        [
            ['Passer', 'rutilans', 'rutilans'],
            [
                'Passer',
                'cinnamomeus',
                'rutilans',
            ],
        ],
        [
            ['Passer', 'rutilans', 'cinnamomeus'],
            [
                'Passer',
                'cinnamomeus',
                'cinnamomeus',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'coronatus'],
            [
                'Phyllergates',
                'cucullatus',
                'coronatus',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'thais'],
            [
                'Phyllergates',
                'cucullatus',
                'thais',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'malayanus'],
            [
                'Phyllergates',
                'cucullatus',
                'malayanus',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'cinereicollis'],
            [
                'Phyllergates',
                'cucullatus',
                'cinereicollis',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'viridicollis'],
            [
                'Phyllergates',
                'cucullatus',
                'viridicollis',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'philippinus'],
            [
                'Phyllergates',
                'cucullatus',
                'philippinus',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'everetti'],
            [
                'Phyllergates',
                'cucullatus',
                'everetti',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'riedeli'],
            [
                'Phyllergates',
                'cucullatus',
                'riedeli',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'stentor'],
            [
                'Phyllergates',
                'cucullatus',
                'stentor',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'meisei'],
            [
                'Phyllergates',
                'cucullatus',
                'meisei',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'hedymeles'],
            [
                'Phyllergates',
                'cucullatus',
                'hedymeles',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'dumasi'],
            [
                'Phyllergates',
                'cucullatus',
                'dumasi',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'batjanensis'],
            [
                'Phyllergates',
                'cucullatus',
                'batjanensis',
            ],
        ],
        [
            ['Phyllergates', 'cuculatus', 'cuculatus'],
            [
                'Phyllergates',
                'cucullatus',
                'cucullatus',
            ],
        ],
        [['Poospiza', 'whitii', 'wagneri'], ['Poospiza', 'whitii', '']],
        [['Poospiza', 'whitii', 'whitii'], ['Poospiza', 'whitii', '']],
        [
            ['Porphyrio', 'indicus', 'viridis'],
            [
                'Porphyrio',
                'poliocephalus',
                'viridis',
            ],
        ],
        [['Porphyrio', 'indicus', 'indicus'], ['Porphyrio', 'indicus', '']],
        [
            ['Pseudalethe', 'choloensis', 'namuli'],
            [
                'Chamaetylas',
                'choloensis',
                'namuli',
            ],
        ],
        [
            ['Pseudalethe', 'choloensis', 'choloensis'],
            [
                'Chamaetylas',
                'choloensis',
                'choloensis',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'compsonota'],
            [
                'Chamaetylas',
                'poliocephala',
                'compsonota',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'hallae'],
            [
                'Chamaetylas',
                'poliocephala',
                'hallae',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'giloensis'],
            [
                'Chamaetylas',
                'poliocephala',
                'giloensis',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'carruthersi'],
            [
                'Chamaetylas',
                'poliocephala',
                'carruthersi',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'akeleyae'],
            [
                'Chamaetylas',
                'poliocephala',
                'akeleyae',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'vandeweghei'],
            [
                'Chamaetylas',
                'poliocephala',
                'vandeweghei',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'kungwensis'],
            [
                'Chamaetylas',
                'poliocephala',
                'kungwensis',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'ufipae'],
            [
                'Chamaetylas',
                'poliocephala',
                'ufipae',
            ],
        ],
        [
            ['Pseudalethe', 'poliocephala', 'poliocephala'],
            [
                'Chamaetylas',
                'poliocephala',
                'poliocephala',
            ],
        ],
        [
            ['Pseudalethe', 'poliophrys', 'kaboboensis'],
            [
                'Chamaetylas',
                'poliophrys',
                'kaboboensis',
            ],
        ],
        [
            ['Pseudalethe', 'poliophrys', 'poliophrys'],
            [
                'Chamaetylas',
                'poliophrys',
                'poliophrys',
            ],
        ],
        [
            ['Schistocichla', 'caurensis', 'australis'],
            [
                'Myrmelastes',
                'caurensis',
                'australis',
            ],
        ],
        [
            ['Schistocichla', 'caurensis', 'caurensis'],
            [
                'Myrmelastes',
                'caurensis',
                'caurensis',
            ],
        ],
        [
            ['Schistocichla', 'leucostigma', 'subplumbea'],
            [
                'Myrmelastes',
                'leucostigma',
                'subplumbeus',
            ],
        ],
        [
            ['Schistocichla', 'leucostigma', 'intensa'],
            [
                'Myrmelastes',
                'leucostigma',
                'intensus',
            ],
        ],
        [
            ['Schistocichla', 'leucostigma', 'infuscata'],
            [
                'Myrmelastes',
                'leucostigma',
                'infuscatus',
            ],
        ],
        [
            ['Schistocichla', 'leucostigma', 'leucostigma'],
            [
                'Myrmelastes',
                'leucostigma',
                'leucostigma',
            ],
        ],
        [
            ['Schistocichla', 'saturata', 'obscura'],
            [
                'Myrmelastes',
                'saturatus',
                'obscurus',
            ],
        ],
        [
            ['Schistocichla', 'saturata', 'saturata'],
            [
                'Myrmelastes',
                'saturatus',
                'saturatus',
            ],
        ],
        [
            ['Spatula', 'rhynchotis', 'variegata'],
            [
                'Spatula',
                'rhynchotis',
                '',
            ],
        ],
        [
            ['Spatula', 'rhynchotis', 'rhynchotis'],
            [
                'Spatula',
                'rhynchotis',
                '',
            ],
        ],
        [
            ['Tadorna', 'radjah', 'rufitergum'],
            [
                'Radjah',
                'radjah',
                'rufitergum',
            ],
        ],
        [['Tadorna', 'radjah', 'radjah'], ['Radjah', 'radjah', 'radjah']],
    ];
$lumps   =
    [

        [
            ['Caprimulgus', 'ruwenzorii', ''],
            [
                'Caprimulgus',
                'poliocephalus',
                '',
            ],
        ],
    ];
