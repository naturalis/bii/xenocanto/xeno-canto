<?php

$renames =
    [
        [['Gallinula', 'angulata'], ['Paragallinula', 'angulata']],
        [['Mascarinus', 'mascarin'], ['Mascarinus', 'mascarinus']],
        [['Suiriri', 'islerorum'], ['Suiriri', 'affinis']],

    ];

$splits =
    [
        [
            ['Epinecrophylla', 'dentei', null],
            [
                'Epinecrophylla',
                'amazonica',
                'dentei',
            ],
        ],
        [
            ['Synallaxis', 'albescens', 'pullata'],
            [
                'Synallaxis',
                'albigularis',
                'albigularis',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'obscurus'],
            [
                'Mionectes',
                'oleagineus',
                'assimilis',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'dyscolus'],
            [
                'Mionectes',
                'oleagineus',
                'assimilis',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'lutescens'],
            [
                'Mionectes',
                'oleagineus',
                'assimilis',
            ],
        ],
        [
            ['Mitrephanes', 'phaeocercus', 'vividus'],
            [
                'Mitrephanes',
                'phaeocercus',
                'aurantiiventris',
            ],
        ],
        [
            ['Tolmomyias', 'flaviventris', 'collingwoodi'],
            [
                'Tolmomyias',
                'flaviventris',
                'aurulentus',
            ],
        ],
        [
            ['Asthenes', 'modesta', 'navasi'],
            [
                'Asthenes',
                'modesta',
                'australis',
            ],
        ],
        [
            ['Mitrephanes', 'phaeocercus', 'eminulus'],
            [
                'Mitrephanes',
                'phaeocercus',
                'berlepschi',
            ],
        ],
        [
            ['Dixiphia', 'erythrocephala', 'flammiceps'],
            [
                'Dixiphia',
                'erythrocephala',
                'berlepschi',
            ],
        ],
        [
            ['Thamnophilus', 'bernardi', 'piurae'],
            [
                'Thamnophilus',
                'bernardi',
                'bernardi',
            ],
        ],
        [
            ['Thamnophilus', 'bernardi', 'cajamarcae'],
            [
                'Thamnophilus',
                'bernardi',
                'bernardi',
            ],
        ],
        [
            ['Gallinula', 'melanops', 'bogotensis'],
            [
                'Porphyriops',
                'melanops',
                'bogotensis',
            ],
        ],
        [
            ['Corydon', 'sumatranus', 'orientalis'],
            [
                'Corydon',
                'sumatranus',
                'brunnescens',
            ],
        ],
        [
            ['Sclerurus', 'caudacutus', 'olivascens'],
            [
                'Sclerurus',
                'caudacutus',
                'brunneus',
            ],
        ],
        [
            ['Suiriri', 'suiriri', 'affinis'],
            [
                'Suiriri',
                'suiriri',
                'burmeisteri',
            ],
        ],
        [
            ['Syndactyla', 'rufosuperciliata', 'similis'],
            [
                'Syndactyla',
                'rufosuperciliata',
                'cabanisi',
            ],
        ],
        [
            ['Thamnophilus', 'caerulescens', 'albonotatus'],
            [
                'Thamnophilus',
                'caerulescens',
                'caerulescens',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'calayensis'],
            [
                'Ficedula',
                'luzoniensis',
                'calayensis',
            ],
        ],
        [
            ['Manacus', 'vitellinus', 'cerritus'],
            [
                'Manacus',
                'vitellinus',
                'candei',
            ],
        ],
        [
            ['Camptostoma', 'obsoletum', 'bogotense'],
            [
                'Camptostoma',
                'obsoletum',
                'caucae',
            ],
        ],
        [
            ['Tyrannus', 'caudifasciatus', 'flavescens'],
            [
                'Tyrannus',
                'caudifasciatus',
                'caudifasciatus',
            ],
        ],
        [
            ['Thamnophilus', 'caerulescens', 'pernambucensis'],
            [
                'Thamnophilus',
                'caerulescens',
                'cearensis',
            ],
        ],
        [
            ['Capsiempis', 'flaveola', 'amazona'],
            [
                'Capsiempis',
                'flaveola',
                'cerula',
            ],
        ],
        [
            ['Mionectes', 'striaticollis', 'selvae'],
            [
                'Mionectes',
                'striaticollis',
                'columbianus',
            ],
        ],
        [
            ['Furnarius', 'rufus', 'schuhmacheri'],
            [
                'Furnarius',
                'rufus',
                'commersoni',
            ],
        ],
        [
            ['Lepidocolaptes', 'souleyetii', 'insignis'],
            [
                'Lepidocolaptes',
                'souleyetii',
                'compressus',
            ],
        ],
        [
            ['Gallinula', 'melanops', 'crassirostris'],
            [
                'Porphyriops',
                'melanops',
                'crassirostris',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'daggayana'],
            [
                'Ficedula',
                'luzoniensis',
                'daggayana',
            ],
        ],
        [
            ['Silvicultrix', 'diadema', 'meridana'],
            [
                'Silvicultrix',
                'diadema',
                'diadema',
            ],
        ],
        [
            ['Thamnophilus', 'doliatus', 'fraterculus'],
            [
                'Thamnophilus',
                'doliatus',
                'doliatus',
            ],
        ],
        [
            ['Myrmoborus', 'myotherinus', 'napensis'],
            [
                'Myrmoborus',
                'myotherinus',
                'elegans',
            ],
        ],
        [
            ['Synallaxis', 'erythrothorax', 'furtiva'],
            [
                'Synallaxis',
                'erythrothorax',
                'erythrothorax',
            ],
        ],
        [
            ['Myiopagis', 'gaimardii', 'subcinerea'],
            [
                'Myiopagis',
                'gaimardii',
                'gaimardii',
            ],
        ],
        [
            ['Silvicultrix', 'diadema', 'cajamarcae'],
            [
                'Silvicultrix',
                'diadema',
                'gratiosa',
            ],
        ],
        [
            ['Formicivora', 'grisea', 'deluzae'],
            [
                'Formicivora',
                'grisea',
                'grisea',
            ],
        ],
        [
            ['Geositta', 'rufipennis', 'fragai'],
            [
                'Geositta',
                'rufipennis',
                'hoyi',
            ],
        ],
        [
            ['Oxyruncus', 'cristatus', 'tocantinsi'],
            [
                'Oxyruncus',
                'cristatus',
                'hypoglaucus',
            ],
        ],
        [
            ['Myrmeciza', 'immaculata', 'brunnea'],
            [
                'Myrmeciza',
                'immaculata',
                'immaculata',
            ],
        ],
        [
            ['Synallaxis', 'albescens', 'griseonota'],
            [
                'Synallaxis',
                'albescens',
                'inaequalis',
            ],
        ],
        [
            ['Thamnophilus', 'doliatus', 'yucatanensis'],
            [
                'Thamnophilus',
                'doliatus',
                'intermedius',
            ],
        ],
        [
            ['Thamnophilus', 'doliatus', 'pacificus'],
            [
                'Thamnophilus',
                'doliatus',
                'intermedius',
            ],
        ],
        [
            ['Sittasomus', 'griseicapillus', 'harrisoni'],
            [
                'Sittasomus',
                'griseicapillus',
                'jaliscensis',
            ],
        ],
        [
            ['Upucerthia', 'validirostris', 'pallida'],
            [
                'Upucerthia',
                'validirostris',
                'jelskii',
            ],
        ],
        [
            ['Myrmoborus', 'leucophrys', 'griseigula'],
            [
                'Myrmoborus',
                'leucophrys',
                'leucophrys',
            ],
        ],
        [
            ['Mionectes', 'macconnelli', 'amazonus'],
            [
                'Mionectes',
                'macconnelli',
                'macconnelli',
            ],
        ],
        [
            ['Geospiza', 'conirostris', 'darwini'],
            [
                'Geospiza',
                'conirostris',
                'magnirostris',
            ],
        ],
        [
            ['Campylorhamphus', 'trochilirostris', 'omissus'],
            [
                'Campylorhamphus',
                'trochilirostris',
                'major',
            ],
        ],
        [
            ['Campylorhamphus', 'trochilirostris', 'guttistriatus'],
            [
                'Campylorhamphus',
                'trochilirostris',
                'major',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'malindangensis'],
            [
                'Ficedula',
                'luzoniensis',
                'malindangensis',
            ],
        ],
        [
            ['Agriornis', 'montanus', 'leucurus'],
            [
                'Agriornis',
                'montanus',
                'maritimus',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'matutumensis'],
            [
                'Ficedula',
                'luzoniensis',
                'matutumensis',
            ],
        ],
        [
            ['Phaenostictus', 'mcleannani', 'chocoanus'],
            [
                'Phaenostictus',
                'mcleannani',
                'mcleannani',
            ],
        ],
        [
            ['Pseudocolaptes', 'boissonneautii', 'pallidus'],
            [
                'Pseudocolaptes',
                'boissonneautii',
                'medianus',
            ],
        ],
        [
            ['Thamnophilus', 'caerulescens', 'subandinus'],
            [
                'Thamnophilus',
                'caerulescens',
                'melanchrous',
            ],
        ],
        [
            ['Gallinula', 'melanops', 'melanops'],
            [
                'Porphyriops',
                'melanops',
                'melanops',
            ],
        ],
        [
            ['Myrmeciza', 'atrothorax', 'obscurata'],
            [
                'Myrmeciza',
                'atrothorax',
                'melanura',
            ],
        ],
        [
            ['Myrmeciza', 'atrothorax', 'griseiventris'],
            [
                'Myrmeciza',
                'atrothorax',
                'melanura',
            ],
        ],
        [
            ['Syndactyla', 'subalaris', 'colligata'],
            [
                'Syndactyla',
                'subalaris',
                'mentalis',
            ],
        ],
        [
            ['Syndactyla', 'subalaris', 'ruficrissa'],
            [
                'Syndactyla',
                'subalaris',
                'mentalis',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'mindorensis'],
            [
                'Ficedula',
                'luzoniensis',
                'mindorensis',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'montigena'],
            [
                'Ficedula',
                'luzoniensis',
                'montigena',
            ],
        ],
        [
            ['Phaeomyias', 'murina', 'ignobilis'],
            [
                'Phaeomyias',
                'murina',
                'murina',
            ],
        ],
        [
            ['Hylophylax', 'naevioides', 'subsimilis'],
            [
                'Hylophylax',
                'naevioides',
                'naevioides',
            ],
        ],
        [
            ['Mecocerculus', 'leucophrys', 'palliditergum'],
            [
                'Mecocerculus',
                'leucophrys',
                'nigriceps',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'nigrorum'],
            [
                'Ficedula',
                'luzoniensis',
                'nigrorum',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'chloronotus'],
            [
                'Mionectes',
                'oleagineus',
                'oleagineus',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'intensus'],
            [
                'Mionectes',
                'oleagineus',
                'oleagineus',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'hauxwelli'],
            [
                'Mionectes',
                'oleagineus',
                'oleagineus',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'wallacei'],
            [
                'Mionectes',
                'oleagineus',
                'oleagineus',
            ],
        ],
        [
            ['Mionectes', 'oleagineus', 'maynanus'],
            [
                'Mionectes',
                'oleagineus',
                'oleagineus',
            ],
        ],
        [
            ['Leptopogon', 'amaurocephalus', 'obscuritergum'],
            [
                'Leptopogon',
                'amaurocephalus',
                'orinocensis',
            ],
        ],
        [
            ['Corythopis', 'torquatus', 'subtorquatus'],
            [
                'Corythopis',
                'torquatus',
                'orquatus',
            ],
        ],
        [
            ['Glyphorynchus', 'spirurus', 'sublestus'],
            [
                'Glyphorynchus',
                'spirurus',
                'pectoralis',
            ],
        ],
        [
            ['Myiarchus', 'swainsoni', 'albimarginatus'],
            [
                'Myiarchus',
                'swainsoni',
                'pelzelni',
            ],
        ],
        [
            ['Contopus', 'pertinax', 'pallidiventris'],
            [
                'Contopus',
                'pertinax',
                'pertinax',
            ],
        ],
        [
            ['Dendroplex', 'picus', 'borreroi'],
            [
                'Dendroplex',
                'picus',
                'peruvianus',
            ],
        ],
        [
            ['Pithys', 'albifrons', 'brevibarba'],
            [
                'Pithys',
                'albifrons',
                'peruvianus',
            ],
        ],
        [
            ['Mitrephanes', 'phaeocercus', 'burleighi'],
            [
                'Mitrephanes',
                'phaeocercus',
                'phaeocercus',
            ],
        ],
        [
            ['Mitrephanes', 'phaeocercus', 'nicaraguae'],
            [
                'Mitrephanes',
                'phaeocercus',
                'phaeocercus',
            ],
        ],
        [
            ['Dendroplex', 'picus', 'rufescens'],
            [
                'Dendroplex',
                'picus',
                'picus',
            ],
        ],
        [
            ['Dendroplex', 'picus', 'bahiae'],
            [
                'Dendroplex',
                'picus',
                'picus',
            ],
        ],
        [
            ['Leptopogon', 'amaurocephalus', 'faustus'],
            [
                'Leptopogon',
                'amaurocephalus',
                'pileatus',
            ],
        ],
        [
            ['Lepidocolaptes', 'angustirostris', 'dabbenei'],
            [
                'Lepidocolaptes',
                'angustirostris',
                'praedatus',
            ],
        ],
        [
            ['Xiphocolaptes', 'promeropirhynchus', 'ignotus'],
            [
                'Xiphocolaptes',
                'promeropirhynchus',
                'promeropirhynchus',
            ],
        ],
        [
            ['Camptostoma', 'obsoletum', 'venezuelae'],
            [
                'Camptostoma',
                'obsoletum',
                'pusillum',
            ],
        ],
        [
            ['Thamnophilus', 'doliatus', 'subradiatus'],
            [
                'Thamnophilus',
                'doliatus',
                'radiatus',
            ],
        ],
        [
            ['Thamnophilus', 'doliatus', 'signatus'],
            [
                'Thamnophilus',
                'doliatus',
                'radiatus',
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'rara'],
            [
                'Ficedula',
                'luzoniensis',
                'rara',
            ],
        ],
        [['Sayornis', 'saya', 'pallidus'], ['Sayornis', 'saya', 'saya']],
        [
            ['Thamnophilus', 'schistaceus', 'dubius'],
            [
                'Thamnophilus',
                'schistaceus',
                'schistaceus',
            ],
        ],
        [
            ['Thamnophilus', 'schistaceus', 'inornatus'],
            [
                'Thamnophilus',
                'schistaceus',
                'schistaceus',
            ],
        ],
        [
            ['Phleocryptes', 'melanops', 'juninensis'],
            [
                'Phleocryptes',
                'melanops',
                'schoenobaenus',
            ],
        ],
        [
            ['Synallaxis', 'scutata', 'teretiala'],
            [
                'Synallaxis',
                'scutata',
                'scutata',
            ],
        ],
        [
            ['Mecocerculus', 'leucophrys', 'gularis'],
            [
                'Mecocerculus',
                'leucophrys',
                'setophagoides',
            ],
        ],
        [
            ['Contopus', 'sordidulus', 'griscomi'],
            [
                'Contopus',
                'sordidulus',
                'sordidulus',
            ],
        ],
        [
            ['Mionectes', 'striaticollis', 'poliocephalus'],
            [
                'Mionectes',
                'striaticollis',
                'striaticollis',
            ],
        ],
        [
            ['Cnipodectes', 'subbrunneus', 'panamensis'],
            [
                'Cnipodectes',
                'subbrunneus',
                'subbrunneus',
            ],
        ],
        [
            ['Leptopogon', 'superciliaris', 'hellmayeri'],
            [
                'Leptopogon',
                'superciliaris',
                'superciliaris',
            ],
        ],
        [
            ['Leptopogon', 'superciliaris', 'transandinus'],
            [
                'Leptopogon',
                'superciliaris',
                'superciliaris',
            ],
        ],
        [
            ['Leptopogon', 'superciliaris', 'poliocephalus'],
            [
                'Leptopogon',
                'superciliaris',
                'superciliaris',
            ],
        ],
        [
            ['Leptopogon', 'superciliaris', 'venezuelensis'],
            [
                'Leptopogon',
                'superciliaris',
                'superciliaris',
            ],
        ],
        [
            ['Leptopogon', 'superciliaris', 'pariae'],
            [
                'Leptopogon',
                'superciliaris',
                'superciliaris',
            ],
        ],
        [
            ['Sittasomus', 'griseicapillus', 'levis'],
            [
                'Sittasomus',
                'griseicapillus',
                'sylvioides',
            ],
        ],
        [
            ['Sittasomus', 'griseicapillus', 'veraguensis'],
            [
                'Sittasomus',
                'griseicapillus',
                'sylvioides',
            ],
        ],
        [
            ['Sittasomus', 'griseicapillus', 'enochrus'],
            [
                'Sittasomus',
                'griseicapillus',
                'sylvioides',
            ],
        ],
        [
            ['Cercomacra', 'tyrannina', 'rufiventris'],
            [
                'Cercomacra',
                'tyrannina',
                'tyrannina',
            ],
        ],
        [
            ['Upucerthia', 'validirostris', 'rufescens'],
            [
                'Upucerthia',
                'validirostris',
                'validirostris',
            ],
        ],
        [
            ['Mionectes', 'olivaceus', 'pallidus'],
            [
                'Mionectes',
                'olivaceus',
                'venezuelensis',
            ],
        ],
        [
            ['Mionectes', 'olivaceus', 'meridae'],
            [
                'Mionectes',
                'olivaceus',
                'venezuelensis',
            ],
        ],
        [
            ['Sittasomus', 'griseicapillus', 'viridior'],
            [
                'Sittasomus',
                'griseicapillus',
                'viridis',
            ],
        ],
        [
            ['Cranioleuca', 'vulpina', 'alopecias'],
            [
                'Cranioleuca',
                'vulpina',
                'vulpina',
            ],
        ],
        [
            ['Automolus', 'rubiginosus', 'moderatus'],
            [
                'Automolus',
                'rubiginosus',
                'watkinsi',
            ],
        ],
        [
            ['Formicarius', 'analis', 'olivaceus'],
            [
                'Formicarius',
                'analis',
                'zamorae',
            ],
        ],
        [
            ['Systellura', 'longirostris', 'decussata'],
            [
                'Systellura',
                'decussata',
                null,
            ],
        ],
        [
            ['Cistothorus', 'platensis', 'stellaris'],
            [
                'Cistothorus',
                'stellaris',
                null,
            ],
        ],
        [
            ['Ficedula', 'hyperythra', 'luzoniensis'],
            [
                'Ficedula',
                'luzoniensis',
                'luzoniensis',
            ],
        ],
        [
            ['Atlapetes', 'albofrenatus', 'meridae'],
            [
                'Atlapetes',
                'meridae',
                null,
            ],
        ],
    ];
$lumps  =
    [

        [
            ['Atlapetes', 'albofrenatus', 'albofrenatus'],
            [
                'Atlapetes',
                'albofrenatus',
                null,
            ],
        ]
        ,

        //array(array("Puffinus","elegans", "elegans"),array("Puffinus","elegans", NULL)) ,
        //array(array("Puffinus","elegans", "tunneyi"),array("Puffinus","elegans", NULL)) ,

        [['Hydrornis', 'irena', 'ripleyi'], ['Hydrornis', 'irena', null]]
        ,
        [['Hydrornis', 'irena', 'irena'], ['Hydrornis', 'irena', null]]
        ,
        [
            ['Pseudasthenes', 'cactorum', 'monticola'],
            [
                'Pseudasthenes',
                'cactorum',
                null,
            ],
        ]
        ,
        [
            ['Pseudasthenes', 'cactorum', 'lachayensis'],
            [
                'Pseudasthenes',
                'cactorum',
                null,
            ],
        ]
        ,
        [
            ['Pseudasthenes', 'cactorum', 'cactorum'],
            [
                'Pseudasthenes',
                'cactorum',
                null,
            ],
        ]
        ,
        [
            ['Synallaxis', 'frontalis', 'fuscipennis'],
            [
                'Synallaxis',
                'frontalis',
                null,
            ],
        ]
        ,
        [
            ['Synallaxis', 'frontalis', 'frontalis'],
            [
                'Synallaxis',
                'frontalis',
                null,
            ],
        ]
        ,
        [
            ['Cranioleuca', 'semicinerea', 'goyana'],
            [
                'Cranioleuca',
                'semicinerea',
                null,
            ],
        ]
        ,
        [
            ['Cranioleuca', 'semicinerea', 'semicinerea'],
            [
                'Cranioleuca',
                'semicinerea',
                null,
            ],
        ]
        ,
        [
            ['Syndactyla', 'dimidiata', 'baeri'],
            [
                'Syndactyla',
                'dimidiata',
                null,
            ],
        ]
        ,
        [
            ['Syndactyla', 'dimidiata', 'dimidiata'],
            [
                'Syndactyla',
                'dimidiata',
                null,
            ],
        ]
        ,
        [
            ['Hypoedaleus', 'guttatus', 'leucogaster'],
            [
                'Hypoedaleus',
                'guttatus',
                null,
            ],
        ]
        ,
        [
            ['Hypoedaleus', 'guttatus', 'guttatus'],
            [
                'Hypoedaleus',
                'guttatus',
                null,
            ],
        ]
        ,
        [
            ['Sakesphorus', 'canadensis', 'paraguanae'],
            [
                'Sakesphorus',
                'luctuosus',
                null,
            ],
        ]
        ,
        [
            ['Sakesphorus', 'luctuosus', 'araguayae'],
            [
                'Sakesphorus',
                'luctuosus',
                null,
            ],
        ]
        ,
        [
            ['Sakesphorus', 'luctuosus', 'luctuosus'],
            [
                'Sakesphorus',
                'luctuosus',
                null,
            ],
        ]
        ,
        [
            ['Thamnophilus', 'nigriceps', 'magdalenae'],
            [
                'Thamnophilus',
                'nigriceps',
                null,
            ],
        ]
        ,
        [
            ['Thamnophilus', 'nigriceps', 'nigriceps'],
            [
                'Thamnophilus',
                'nigriceps',
                null,
            ],
        ]
        ,
        [
            ['Dysithamnus', 'puncticeps', 'intensus'],
            [
                'Dysithamnus',
                'puncticeps',
                null,
            ],
        ]
        ,
        [
            ['Dysithamnus', 'puncticeps', 'flemmingi'],
            [
                'Dysithamnus',
                'puncticeps',
                null,
            ],
        ]
        ,
        [
            ['Dysithamnus', 'puncticeps', 'puncticeps'],
            [
                'Dysithamnus',
                'puncticeps',
                null,
            ],
        ]
        ,
        [
            ['Epinecrophylla', 'fulviventris', 'costaricensis'],
            [
                'Epinecrophylla',
                'fulviventris',
                null,
            ],
        ]
        ,
        [
            ['Epinecrophylla', 'fulviventris', 'salmoni'],
            [
                'Epinecrophylla',
                'fulviventris',
                null,
            ],
        ]
        ,
        [
            ['Epinecrophylla', 'fulviventris', 'fulviventris'],
            [
                'Epinecrophylla',
                'fulviventris',
                null,
            ],
        ]
        ,
        [
            ['Terenura', 'humeralis', 'transfluvialis'],
            [
                'Terenura',
                'humeralis',
                null,
            ],
        ]
        ,
        [
            ['Terenura', 'humeralis', 'humeralis'],
            [
                'Terenura',
                'humeralis',
                null,
            ],
        ]
        ,
        [
            ['Cercomacra', 'serva', 'hypomelaena'],
            [
                'Cercomacra',
                'serva',
                null,
            ],
        ]
        ,
        [['Cercomacra', 'serva', 'serva'], ['Cercomacra', 'serva', null]]
        ,
        [
            ['Cercomacra', 'nigricans', 'atrata'],
            [
                'Cercomacra',
                'nigricans',
                null,
            ],
        ]
        ,
        [
            ['Cercomacra', 'nigricans', 'nigricans'],
            [
                'Cercomacra',
                'nigricans',
                null,
            ],
        ]
        ,
        [
            ['Hypocnemoides', 'maculicauda', 'orientalis'],
            [
                'Hypocnemoides',
                'maculicauda',
                null,
            ],
        ]
        ,
        [
            ['Hypocnemoides', 'maculicauda', 'maculicauda'],
            [
                'Hypocnemoides',
                'maculicauda',
                null,
            ],
        ]
        ,
        [
            ['Gymnopithys', 'salvini', 'maculatus'],
            [
                'Gymnopithys',
                'salvini',
                null,
            ],
        ]
        ,
        [
            ['Gymnopithys', 'salvini', 'salvini'],
            [
                'Gymnopithys',
                'salvini',
                null,
            ],
        ]
        ,
        [
            ['Hylophylax', 'punctulatus', 'subochraceous'],
            [
                'Hylophylax',
                'punctulatus',
                null,
            ],
        ]
        ,
        [
            ['Hylophylax', 'punctulatus', 'punctulatus'],
            [
                'Hylophylax',
                'punctulatus',
                null,
            ],
        ]
        ,
        [
            ['Phyllomyias', 'griseiceps', 'cristatus'],
            [
                'Phyllomyias',
                'griseiceps',
                null,
            ],
        ]
        ,
        [
            ['Phyllomyias', 'griseiceps', 'caucae'],
            [
                'Phyllomyias',
                'griseiceps',
                null,
            ],
        ]
        ,
        [
            ['Phyllomyias', 'griseiceps', 'pallidiceps'],
            [
                'Phyllomyias',
                'griseiceps',
                null,
            ],
        ]
        ,
        [
            ['Phyllomyias', 'griseiceps', 'griseiceps'],
            [
                'Phyllomyias',
                'griseiceps',
                null,
            ],
        ]
        ,
        [
            ['Ornithion', 'brunneicapillus', 'dilutum'],
            [
                'Ornithion',
                'brunneicapillus',
                null,
            ],
        ]
        ,
        [
            ['Ornithion', 'brunneicapillus', 'brunneicapillus'],
            [
                'Ornithion',
                'brunneicapillus',
                null,
            ],
        ]
        ,
        [
            ['Camptostoma', 'imberbe', 'ridgwayi'],
            [
                'Camptostoma',
                'imberbe',
                null,
            ],
        ]
        ,
        [
            ['Camptostoma', 'imberbe', 'thyellophilum'],
            [
                'Camptostoma',
                'imberbe',
                null,
            ],
        ]
        ,
        [
            ['Camptostoma', 'imberbe', 'imberbe'],
            [
                'Camptostoma',
                'imberbe',
                null,
            ],
        ]
        ,
        [
            ['Euscarthmus', 'rufomarginatus', 'savannophilus'],
            [
                'Euscarthmus',
                'rufomarginatus',
                null,
            ],
        ]
        ,
        [
            ['Euscarthmus', 'rufomarginatus', 'rufomarginatus'],
            [
                'Euscarthmus',
                'rufomarginatus',
                null,
            ],
        ]
        ,
        [
            ['Pseudelaenia', 'leucospodia', 'cinereifrons'],
            [
                'Pseudelaenia',
                'leucospodia',
                null,
            ],
        ]
        ,
        [
            ['Pseudelaenia', 'leucospodia', 'leucospodia'],
            [
                'Pseudelaenia',
                'leucospodia',
                null,
            ],
        ]
        ,
        [
            ['Zimmerius', 'bolivianus', 'viridissimus'],
            [
                'Zimmerius',
                'bolivianus',
                null,
            ],
        ]
        ,
        [
            ['Zimmerius', 'bolivianus', 'bolivianus'],
            [
                'Zimmerius',
                'bolivianus',
                null,
            ],
        ]
        ,
        [
            ['Pogonotriccus', 'poecilotis', 'pifanoi'],
            [
                'Pogonotriccus',
                'poecilotis',
                null,
            ],
        ]
        ,
        [
            ['Pogonotriccus', 'poecilotis', 'poecilotis'],
            [
                'Pogonotriccus',
                'poecilotis',
                null,
            ],
        ]
        ,
        [
            ['Mionectes', 'roraimae', 'mercedesfosterae'],
            [
                'Mionectes',
                'roraimae',
                null,
            ],
        ]
        ,
        [
            ['Mionectes', 'roraimae', 'roraimae'],
            [
                'Mionectes',
                'roraimae',
                null,
            ],
        ]
        ,
        [
            ['Leptopogon', 'rufipectus', 'venezuelanus'],
            [
                'Leptopogon',
                'rufipectus',
                null,
            ],
        ]
        ,
        [
            ['Leptopogon', 'rufipectus', 'rufipectus'],
            [
                'Leptopogon',
                'rufipectus',
                null,
            ],
        ]
        ,
        [
            ['Platyrinchus', 'cancrominus', 'timothei'],
            [
                'Platyrinchus',
                'cancrominus',
                null,
            ],
        ]
        ,
        [
            ['Platyrinchus', 'cancrominus', 'dilutus'],
            [
                'Platyrinchus',
                'cancrominus',
                null,
            ],
        ]
        ,
        [
            ['Platyrinchus', 'cancrominus', 'cancrominus'],
            [
                'Platyrinchus',
                'cancrominus',
                null,
            ],
        ]
        ,
        [
            ['Pyrocephalus', 'rubinus', 'major'],
            [
                'Pyrocephalus',
                'rubinus',
                null,
            ],
        ]
        ,
        [
            ['Iodopleura', 'isabellae', 'paraensis'],
            [
                'Iodopleura',
                'isabellae',
                null,
            ],
        ]
        ,
        [
            ['Iodopleura', 'isabellae', 'isabellae'],
            [
                'Iodopleura',
                'isabellae',
                null,
            ],
        ]
        ,
        [
            ['Phylloscopus', 'ibericus', 'biscayensis'],
            [
                'Phylloscopus',
                'ibericus',
                null,
            ],
        ]
        ,
        [
            ['Phylloscopus', 'ibericus', 'ibericus'],
            [
                'Phylloscopus',
                'ibericus',
                null,
            ],
        ],
    ];
