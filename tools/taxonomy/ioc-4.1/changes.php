<?php

// format of this file:  There should be 3 variables: $renames, $splits, $lumps.
// Each of these variables should be an array of changes.  Each element of the
// array should also be a two-element array.  The first element is the source
// taxon, the second one the destination.  Each of these taxa is a three-element
// array, specifying genus, species, ssp...
//
// Entirely new species do not need to be listed here, unless we have added them
// to our taxonomy under a provisional name (e.g. sp.nov.fooBar)

$renames = [
    //array(array("genus", "species"), array("genus", "species")),

    // 4.1 species changes
    [
        ['herpsilochmus', 'sp.nov.Aripuana_Ji_Parana'],
        [
            'herpsilochmus',
            'stotzi',
        ],
    ],
    [
        ['herpsilochmus', 'sp.nov.Madeira_Purus'],
        [
            'herpsilochmus',
            'praedictus',
        ],
    ],

    // 3.4. taxonomy changes
    [
        ['Scleroptila', 'levaillantoides'],
        [
            'Scleroptila',
            'gutturalis',
        ],
    ],
    [['Megascops', 'flammeolus'], ['Psiloscops', 'flammeolus']],
    [
        ['Lichenostomus', 'flavicollis'],
        [
            'Nesoptilotis',
            'flavicollis',
        ],
    ],
    [['Lichenostomus', 'leucotis'], ['Nesoptilotis', 'leucotis']],
    [['Lichenostomus', 'hindwoodi'], ['Bolemoreus', 'hindwoodi']],
    [['Lichenostomus', 'frenatus'], ['Bolemoreus', 'frenatus']],
    [['Lichenostomus', 'chrysops'], ['Caligavis', 'chrysops']],
    [
        ['Lichenostomus', 'subfrenatus'],
        [
            'Caligavis',
            'subfrenata',
        ],
    ],
    [['Lichenostomus', 'obscurus'], ['Caligavis', 'obscura']],
    [['Lichenostomus', 'unicolor'], ['Stomiopera', 'unicolor']],
    [['Lichenostomus', 'flavus'], ['Stomiopera', 'flava']],
    [['Lichenostomus', 'versicolor'], ['Gavicalis', 'versicolor']],
    [
        ['Lichenostomus', 'fasciogularis'],
        [
            'Gavicalis',
            'fasciogularis',
        ],
    ],
    [['Lichenostomus', 'virescens'], ['Gavicalis', 'virescens']],
    [['Lichenostomus', 'keartlandi'], ['Ptilotula', 'keartlandi']],
    [['Lichenostomus', 'ornatus'], ['Ptilotula', 'ornata']],
    [['Lichenostomus', 'plumulus'], ['Ptilotula', 'plumula']],
    [['Lichenostomus', 'fuscus'], ['Ptilotula', 'fusca']],
    [['Lichenostomus', 'flavescens'], ['Ptilotula', 'flavescens']],
    [
        ['Lichenostomus', 'penicillatus'],
        [
            'Ptilotula',
            'penicillata',
        ],
    ],
    [['Pitohui', 'cristatus'], ['Ornorectes', 'cristatus']],
    [['Pitohui', 'incertus'], ['Pseudorectes', 'incertus']],
    [['Pitohui', 'ferrugineus'], ['Pseudorectes', 'ferrugineus']],
    [['Pitohui', 'nigrescens'], ['Melanorectes', 'nigrescens']],
    [
        ['Colluricincla', 'sanghirensis'],
        [
            'Coracornis',
            'sanghirensis',
        ],
    ],
    [['Colluricincla', 'umbrina'], ['Colluricincla', 'tenebrosa']],
    [
        ['Colluricincla', 'tenebrosa'],
        [
            'Pachycephala',
            'tenebrosa',
        ],
    ],
    [['Prinia', 'burnesii'], ['Laticilla', 'burnesii']],
    [
        ['Phylloscopus', 'poliocephalus'],
        [
            'Phylloscopus',
            'maforensis',
        ],
    ],
    [['Spinus', 'thibetanus'], ['Spinus', 'thibetana']],
    // 3.4. meliphagidae revisions
    [['lichenostomus', 'frenatus'], ['bolemoreus', 'frenatus']],
    [['lichenostomus', 'hindwoodi'], ['bolemoreus', 'hindwoodi']],
    [['lichenostomus', 'chrysops'], ['caligavis', 'chrysops']],
    [
        ['lichenostomus', 'subfrenatus'],
        [
            'caligavis',
            'subfrenata',
        ],
    ],
    [['lichenostomus', 'obscurus'], ['caligavis', 'obscura']],
    [['lichenostomus', 'unicolor'], ['stomiopera', 'unicolor']],
    [['lichenostomus', 'flavus'], ['stomiopera', 'flava']],
    [['lichenostomus', 'versicolor'], ['gavicalis', 'versicolor']],
    [
        ['lichenostomus', 'fasciogularis'],
        [
            'gavicalis',
            'fasciogularis',
        ],
    ],
    [['lichenostomus', 'virescens'], ['gavicalis', 'virescens']],
    [['lichenostomus', 'flavescens'], ['ptilotula', 'flavescens']],
    [['lichenostomus', 'fuscus'], ['ptilotula', 'fusca']],
    [['lichenostomus', 'keartlandi'], ['ptilotula', 'keartlandi']],
    [['lichenostomus', 'plumulus'], ['ptilotula', 'plumula']],
    [['lichenostomus', 'ornatus'], ['ptilotula', 'ornata']],
    [
        ['lichenostomus', 'penicillatus'],
        [
            'ptilotula',
            'penicillata',
        ],
    ],

    // 3.5 taxonomy
    [['Gyps', 'rueppellii'], ['Gyps', 'rueppelli']],
    [['Ictinaetus', 'malayensis'], ['Ictinaetus', 'malaiensis']],
    [['Eupodotis', 'coerulescens'], ['Eupodotis', 'caerulescens']],
    [['Laterallus', 'spilonotus'], ['Laterallus', 'spilonota']],
    [['Streptopelia', 'risoria'], ['Streptopelia', 'roseogrisea']],
    [
        ['Reinwardtoena', 'reinwardtii'],
        [
            'Reinwardtoena',
            'reinwardti',
        ],
    ],
    [['Ptilinopus', 'nanus'], ['Ptilinopus', 'nainus']],
    [
        ['Chalcopsitta', 'sintillata'],
        [
            'Chalcopsitta',
            'scintillata',
        ],
    ],
    [['Psittacula', 'calthropae'], ['Psittacula', 'calthrapae']],
    [['Charmosyna', 'aureicincta'], ['Charmosyna', 'amabilis']],
    [
        ['Orthopsittaca', 'manilata'],
        [
            'Orthopsittaca',
            'manilatus',
        ],
    ],
    [['Nandayus', 'nenday'], ['Aratinga', 'nenday']],
    [['Gymnoglaux', 'lawrencii'], ['Margarobyas', 'lawrencii']],
    [['Ninox', 'spilonota'], ['Ninox', 'spilonotus']],
    [['Setopagis', 'heterurus'], ['Setopagis', 'heterura']],
    [['Setopagis', 'parvulus'], ['Setopagis', 'parvula']],
    [['Setopagis', 'maculosus'], ['Setopagis', 'maculosa']],
    [
        ['Sephanoides', 'sephanoides'],
        [
            'Sephanoides',
            'sephaniodes',
        ],
    ],
    [['Sappho', 'sparganura'], ['Sappho', 'sparganurus']],
    [
        ['Finschia', 'novaeseelandiae'],
        [
            'Mohoua',
            'novaeseelandiae',
        ],
    ],
    [
        ['Corcorax', 'melanoramphos'],
        [
            'Corcorax',
            'melanorhamphos',
        ],
    ],
    [['Prinia', 'cinerascens'], ['Laticilla', 'cinerascens']],
    [['Geomalia', 'heinrichi'], ['Zoothera', 'heinrichi']],
    // aratinga revisions
    [['Aratinga', 'nana'], ['Eupsittula', 'nana']],
    [['Aratinga', 'canicularis'], ['Eupsittula', 'canicularis']],
    [['Aratinga', 'aurea'], ['Eupsittula', 'aurea']],
    [['Aratinga', 'pertinax'], ['Eupsittula', 'pertinax']],
    [['Aratinga', 'cactorum'], ['Eupsittula', 'cactorum']],
    [
        ['Aratinga', 'acuticaudata'],
        [
            'Thectocercus',
            'acuticaudatus',
        ],
    ],
    [['Aratinga', 'holochlora'], ['Psittacara', 'holochlorus']],
    [['Aratinga', 'brevipes'], ['Psittacara', 'brevipes']],
    [['Aratinga', 'rubritorquis'], ['Psittacara', 'rubritorquis']],
    [['Aratinga', 'strenua'], ['Psittacara', 'strenuus']],
    [['Aratinga', 'wagleri'], ['Psittacara', 'wagleri']],
    [['Aratinga', 'mitrata'], ['Psittacara', 'mitratus']],
    [['Aratinga', 'erythrogenys'], ['Psittacara', 'erythrogenys']],
    [['Aratinga', 'finschi'], ['Psittacara', 'finschi']],
    [
        ['Aratinga', 'leucophthalma'],
        [
            'Psittacara',
            'leucophthalmus',
        ],
    ],
    [['Aratinga', 'euops'], ['Psittacara', 'euops']],
    [['Aratinga', 'chloroptera'], ['Psittacara', 'chloropterus']],
    // paridae revisions
    [['periparus', 'venustulus'], ['Pardaliparus', 'venustulus']],
    [['periparus', 'elegans'], ['Pardaliparus', 'elegans']],
    [['periparus', 'amabilis'], ['Pardaliparus', 'amabilis']],
    [['poecile', 'varius'], ['Sittiparus', 'varius']],
    [['parus', 'semilarvatus'], ['Sittiparus', 'semilarvatus']],
    [['parus', 'humilis'], ['Pseudopodoces', 'humilis']],
    [['parus', 'nuchalis'], ['Machlolophus', 'nuchalis']],
    [['parus', 'holsti'], ['Machlolophus', 'holsti']],
    [['parus', 'xanthogenys'], ['Machlolophus', 'xanthogenys']],
    [['parus', 'aplonotus'], ['Machlolophus', 'aplonotus']],
    [['parus', 'spilonotus'], ['Machlolophus', 'spilonotus']],
    [['parus', 'guineensis'], ['Melaniparus', 'guineensis']],
    [['parus', 'leucomelas'], ['Melaniparus', 'leucomelas']],
    [['parus', 'niger'], ['Melaniparus', 'niger']],
    [['parus', 'carpi'], ['Melaniparus', 'carpi']],
    [['parus', 'albiventris'], ['Melaniparus', 'albiventris']],
    [['parus', 'leuconotus'], ['Melaniparus', 'leuconotus']],
    [['parus', 'funereus'], ['Melaniparus', 'funereus']],
    [['parus', 'rufiventris'], ['Melaniparus', 'rufiventris']],
    [
        ['parus', 'pallidiventris'],
        [
            'Melaniparus',
            'pallidiventris',
        ],
    ],
    [['parus', 'fringillinus'], ['Melaniparus', 'fringillinus']],
    [['parus', 'fasciiventer'], ['Melaniparus', 'fasciiventer']],
    [['parus', 'thruppi'], ['Melaniparus', 'thruppi']],
    [['parus', 'griseiventris'], ['Melaniparus', 'griseiventris']],
    [['parus', 'cinerascens'], ['Melaniparus', 'cinerascens']],
    [['parus', 'afer'], ['Melaniparus', 'afer']],
    [['ptilogonys', 'cinereus'], ['Ptiliogonys', 'cinereus']],
    [['ptilogonys', 'caudatus'], ['Ptiliogonys', 'caudatus']],

    // 4.1 taxonomy
    [['Tetrao', 'parvirostris'], ['Tetrao', 'urogalloides']],
    [['Xenoperdix', 'obscurata'], ['Xenoperdix', 'obscuratus']],
    [['Gallus', 'lafayetii'], ['Gallus', 'lafayettii']],
    [['Vanellus', 'malabaricus'], ['Vanellus', 'malarbaricus']],
    [
        ['Macrodipteryx', 'longipennis'],
        [
            'Caprimulgus',
            'longipennis',
        ],
    ],
    [
        ['Macrodipteryx', 'vexillarius'],
        [
            'Caprimulgus',
            'vexillarius',
        ],
    ],
    [['Phaethornis', 'aethopyga'], ['Phaethornis', 'aethopygus']],
    [['Amazilia', 'saucerrottei'], ['Amazilia', 'saucerottei']],
    [['Sylvia', 'moltonii'], ['Sylvia', 'subalpina']],
    [['Dioptrornis', 'brunneus'], ['Melaenornis', 'brunneus']],
    [['Dioptrornis', 'fischeri'], ['Melaenornis', 'fischeri']],
    [
        ['Dioptrornis', 'chocolatinus'],
        [
            'Melaenornis',
            'chocolatinus',
        ],
    ],
    // muscicapidae revisions
    [
        ['Erythropygia', 'coryphoeus'],
        [
            'Cercotrichas',
            'coryphoeus',
        ],
    ],
    [
        ['Erythropygia', 'leucosticta'],
        [
            'Cercotrichas',
            'leucosticta',
        ],
    ],
    [
        ['Erythropygia', 'quadrivirgata'],
        [
            'Cercotrichas',
            'quadrivirgata',
        ],
    ],
    [['Erythropygia', 'barbata'], ['Cercotrichas', 'barbata']],
    [
        ['Erythropygia', 'galactotes'],
        [
            'Cercotrichas',
            'galactotes',
        ],
    ],
    [['Erythropygia', 'paena'], ['Cercotrichas', 'paena']],
    [['Erythropygia', 'hartlaubi'], ['Cercotrichas', 'hartlaubi']],
    [
        ['Erythropygia', 'leucophrys'],
        [
            'Cercotrichas',
            'leucophrys',
        ],
    ],
    [['Erythropygia', 'signata'], ['Cercotrichas', 'signata']],
    [['Saxicoloides', 'fulicatus'], ['Copsychus', 'fulicatus']],
    [['Trichixos', 'pyrropygus'], ['Copsychus', 'pyrropygus']],
    [['Bradornis', 'pallidus'], ['Melaenornis', 'pallidus']],
    [['Bradornis', 'infuscatus'], ['Melaenornis', 'infuscatus']],
    [
        ['Bradornis', 'microrhynchus'],
        [
            'Melaenornis',
            'microrhynchus',
        ],
    ],
    [['Bradornis', 'mariquensis'], ['Melaenornis', 'mariquensis']],
    [['Sigelus', 'silens'], ['Melaenornis', 'silens']],
    [['Rhinomyias', 'oscillans'], ['Cyornis', 'oscillans']],
    [['Rhinomyias', 'brunneatus'], ['Cyornis', 'brunneatus']],
    [['Rhinomyias', 'nicobaricus'], ['Cyornis', 'nicobaricus']],
    [['Rhinomyias', 'olivaceus'], ['Cyornis', 'olivaceus']],
    [['Rhinomyias', 'umbratilis'], ['Cyornis', 'umbratilis']],
    [['Rhinomyias', 'ruficauda'], ['Cyornis', 'ruficauda']],
    [['Rhinomyias', 'colonus'], ['Cyornis', 'colonus']],
    [['Rhinomyias', 'additus'], ['Eumyias', 'additus']],
    [['Xenocopsychus', 'ansorgei'], ['Cossypha', 'ansorgei']],
    [['Rhinomyias', 'gularis'], ['Vauriella', 'gularis']],
    [['Rhinomyias', 'albigularis'], ['Vauriella', 'albigularis']],
    [['Rhinomyias', 'insignis'], ['Vauriella', 'insignis']],
    [['Rhinomyias', 'goodfellowi'], ['Vauriella', 'goodfellowi']],
    [['Luscinia', 'brunnea'], ['Larvivora', 'brunnea']],
    [['Luscinia', 'cyane'], ['Larvivora', 'cyane']],
    [['Luscinia', 'sibilans'], ['Larvivora', 'sibilans']],
    [['Luscinia', 'ruficeps'], ['Larvivora', 'ruficeps']],
    [['Erithacus', 'komadori'], ['Larvivora', 'komadori']],
    [['Erithacus', 'akahige'], ['Larvivora', 'akahige']],
    [
        ['Hodgsonius', 'phoenicuroides'],
        [
            'Luscinia',
            'phoenicuroides',
        ],
    ],
    [['Luscinia', 'pectoralis'], ['Calliope', 'pectoralis']],
    [['Luscinia', 'calliope'], ['Calliope', 'calliope']],
    [['Luscinia', 'pectardens'], ['Calliope', 'pectardens']],
    [['Luscinia', 'obscura'], ['Calliope', 'obscura']],
    [['Rhyacornis', 'fuliginosa'], ['Phoenicurus', 'fuliginosus']],
    [['Rhyacornis', 'bicolor'], ['Phoenicurus', 'bicolor']],
    [['Oenanthe', 'monticola'], ['Myrmecocichla', 'monticola']],
    [['Pentholaea', 'arnotti'], ['Myrmecocichla', 'arnotti']],
    [['Pentholaea', 'collaris'], ['Myrmecocichla', 'collaris']],
    [['Pentholaea', 'albifrons'], ['Oenanthe', 'albifrons']],

    [['Carpodacus', 'eos'], ['Carpodacus', 'waltoni']],
];

$splits = [
    //array(array("genus", "species", "ssp"), array("genus", "species", "ssp or NULL")),

    // 3.4 changes
    [
        ['prioniturus', 'discurus', 'mindorensis'],
        [
            'prioniturus',
            'mindorensis',
            null,
        ],
    ],
    [
        ['tyto', 'tenebricosa', 'multipunctata'],
        [
            'tyto',
            'multipunctata',
            null,
        ],
    ],
    [
        ['glaucidium', 'gnoma', 'hoskinsii'],
        [
            'glaucidium',
            'hoskinsii',
            null,
        ],
    ],
    [
        ['glaucidium', 'gnoma', 'gnoma'],
        [
            'glaucidium',
            'gnoma',
            null,
        ],
    ],
    [
        ['eurostopodus', 'mystacalis', 'nigripennis'],
        [
            'eurostopodus',
            'nigripennis',
            null,
        ],
    ],
    [
        ['eurostopodus', 'mystacalis', 'exul'],
        [
            'eurostopodus',
            'exul',
            null,
        ],
    ],
    [
        ['eurostopodus', 'mystacalis', 'mystacalis'],
        [
            'eurostopodus',
            'mystacalis',
            null,
        ],
    ],
    [
        ['caprimulgus', 'jotaka', 'phalaena'],
        [
            'caprimulgus',
            'phalaena',
            null,
        ],
    ],
    [
        ['elaenia', 'pallatangae', 'olivina'],
        [
            'elaenia',
            'olivina',
            null,
        ],
    ],
    [
        ['elaenia', 'albiceps', 'chilensis'],
        [
            'elaenia',
            'chilensis',
            null,
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'pallidus'],
        [
            'pitohui',
            'cerviniventris',
            'pallidus',
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'cerviniventris'],
        [
            'pitohui',
            'cerviniventris',
            'cerviniventris',
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'uropygialis'],
        [
            'pitohui',
            'uropygialis',
            'uropygialis',
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'tibialis'],
        [
            'pitohui',
            'uropygialis',
            'tibialis',
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'brunneiceps'],
        [
            'pitohui',
            'uropygialis',
            'brunneiceps',
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'nigripectus'],
        [
            'pitohui',
            'uropygialis',
            'nigripectus',
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'aruensis'],
        [
            'pitohui',
            'uropygialis',
            'aruensis',
        ],
    ],
    [
        ['pitohui', 'kirhocephalus', 'meridonalis'],
        [
            'pitohui',
            'uropygialis',
            'meridonalis',
        ],
    ],
    [
        ['artemisiospiza', 'belli', 'nevadensis'],
        [
            'artemisiospiza',
            'nevadensis',
            null,
        ],
    ],

    // 3.5 changes
    [
        ['aerodramus', 'vanikorensis', 'amelis'],
        [
            'aerodramus',
            'amelis',
            null,
        ],
    ],
    [
        ['celeus', 'flavescens', 'ochraceus'],
        [
            'celeus',
            'ochraceus',
            null,
        ],
    ],
    [['picus', 'viridis', 'sharpei'], ['picus', 'sharpei', null]],
    [
        ['zimmerius', 'vilissimus', 'parvus'],
        [
            'zimmerius',
            'parvus',
            null,
        ],
    ],
    [
        ['zimmerius', 'improbus', 'petersi'],
        [
            'zimmerius',
            'petersi',
            null,
        ],
    ],
    [
        ['zimmerius', 'chrysops', 'minimus'],
        [
            'zimmerius',
            'minimus',
            'minimus',
        ],
    ],
    [
        ['zimmerius', 'chrysops', 'cumanensis'],
        [
            'zimmerius',
            'minimus',
            'cumanensis',
        ],
    ],
    [
        ['hypothymis', 'azurea', 'puella'],
        [
            'hypothymis',
            'puella',
            'puella',
        ],
    ],
    [
        ['hypothymis', 'azurea', 'blasii'],
        [
            'hypothymis',
            'puella',
            'blasii',
        ],
    ],
    [
        ['poecile', 'lugubris', 'hyrcanus'],
        [
            'poecile',
            'hyrcanus',
            null,
        ],
    ],
    [
        ['pnoepyga', 'albiventer', 'mutica'],
        [
            'pnoepyga',
            'mutica',
            null,
        ],
    ],
    [
        ['cyanoptila', 'cyanomelana', 'cumatilis'],
        [
            'cyanoptila',
            'cumatilis',
            null,
        ],
    ],
    [
        ['aethopyga', 'siparaja', 'magnifica'],
        [
            'aethopyga',
            'magnifica',
            null,
        ],
    ],
    [
        ['aethopyga', 'flagrans', 'guimarasensis'],
        [
            'aethopyga',
            'guimarasensis',
            null,
        ],
    ],
    [
        ['aethopyga', 'pulcherrima', 'decorosa'],
        [
            'aethopyga',
            'decorosa',
            null,
        ],
    ],
    [
        ['aethopyga', 'pulcherrima', 'jefferyi'],
        [
            'aethopyga',
            'jefferyi',
            null,
        ],
    ],
    [
        ['aethopyga', 'pulcherrima', 'pulcherrima'],
        [
            'aethopyga',
            'pulcherrima',
            '',
        ],
    ],
    [
        ['passer', 'simplex', 'zarudnyi'],
        [
            'passer',
            'zarudnyi',
            null,
        ],
    ],
    [
        ['carpodacus', 'vinaceus', 'formosanus'],
        [
            'carpodacus',
            'formosanus',
            '',
        ],
    ],
    [
        ['carpodacus', 'vinaceus', 'vinaceus'],
        [
            'carpodacus',
            'vinaceus',
            '',
        ],
    ],

    // 4.1 changes
    [
        ['otus', 'senegalensis', 'pamelae'],
        [
            'otus',
            'pamelae',
            null,
        ],
    ],
    [
        ['phaethornis', 'longirostris', 'mexicanus'],
        [
            'phaethornis',
            'mexicanus',
            'mexicanus',
        ],
    ],
    [
        ['phaethornis', 'longirostris', 'griseoventer'],
        [
            'phaethornis',
            'mexicanus',
            'griseoventer',
        ],
    ],
    [
        ['knipolegus', 'signatus', 'signatus'],
        [
            'knipolegus',
            'signatus',
            '',
        ],
    ],
    [
        ['knipolegus', 'signatus', 'cabanisi'],
        [
            'knipolegus',
            'cabanisi',
            '',
        ],
    ],
    [
        ['gymnopithys', 'leucaspis', 'bicolor'],
        [
            'gymnopithys',
            'bicolor',
            'bicolor',
        ],
    ],
    [
        ['gymnopithys', 'leucaspis', 'olivascens'],
        [
            'gymnopithys',
            'bicolor',
            'olivascens',
        ],
    ],
    [
        ['gymnopithys', 'leucaspis', 'daguae'],
        [
            'gymnopithys',
            'bicolor',
            'daguae',
        ],
    ],
    [
        ['gymnopithys', 'leucaspis', 'aequatorialis'],
        [
            'gymnopithys',
            'bicolor',
            'aequatorialis',
        ],
    ],
    [
        ['gymnopithys', 'leucaspis', 'ruficeps'],
        [
            'gymnopithys',
            'bicolor',
            'ruficeps',
        ],
    ],
    [
        ['pachycephala', 'orioloides', 'feminina'],
        [
            'pachycephala',
            'feminina',
            '',
        ],
    ],
    [
        ['pachycephala', 'caledonica', 'caledonica'],
        [
            'pachycephala',
            'caledonica',
            '',
        ],
    ],
    [
        ['pachycephala', 'caledonica', 'vanikorensis'],
        [
            'pachycephala',
            'chlorura',
            'vanikorensis',
        ],
    ],
    [
        ['pachycephala', 'caledonica', 'intacta'],
        [
            'pachycephala',
            'chlorura',
            'intacta',
        ],
    ],
    [
        ['pachycephala', 'caledonica', 'cucullata'],
        [
            'pachycephala',
            'chlorura',
            'cucullata',
        ],
    ],
    [
        ['pachycephala', 'caledonica', 'chorura'],
        [
            'pachycephala',
            'chlorura',
            'chorura',
        ],
    ],
    [
        ['pachycephala', 'caledonica', 'littayei'],
        [
            'pachycephala',
            'chlorura',
            'littayei',
        ],
    ],
    [
        ['enicurus', 'leschenaulti', 'borneensis'],
        [
            'enicurus',
            'borneensis',
            '',
        ],
    ],
    [
        ['carpodacus', 'synoicus', 'salimalii'],
        [
            'carpodacus',
            'stoliczkae',
            'salimalii',
        ],
    ],
    [
        ['carpodacus', 'synoicus', 'stoliczkae'],
        [
            'carpodacus',
            'stoliczkae',
            'stoliczkae',
        ],
    ],
    [
        ['carpodacus', 'synoicus', 'beicki'],
        [
            'carpodacus',
            'stoliczkae',
            'beicki',
        ],
    ],
    [
        ['carpodacus', 'synoicus', 'synoicus'],
        [
            'carpodacus',
            'synoicus',
            '',
        ],
    ],
    [
        ['emberiza', 'tahapisi', 'goslingi'],
        [
            'emberiza',
            'goslingi',
            '',
        ],
    ],

    // 3.4 ssp
    [
        ['threnetes', 'leucurus', 'loehkeni'],
        [
            'threnetes',
            'niger',
            'loehkeni',
        ],
    ],
    [
        ['aulacorhynchus', 'griseigularis', 'phaeolaemus'],
        [
            'aulacorhynchus',
            'albivitta',
            'phaeolaemus',
        ],
    ],
    [
        ['thamnomanes', 'caesius', 'intermedius'],
        [
            'thamnomanes',
            'schistogynus',
            'intermedius',
        ],
    ],
    [
        ['alcippe', 'peracensis', 'eremita'],
        [
            'alcippe',
            'grotei',
            'eremita',
        ],
    ],
    [
        ['ploceus', 'vitellinus', 'peixotoi'],
        [
            'ploceus',
            'velatus',
            'peixotoi',
        ],
    ],

    // 3.5 ssp
    [
        ['Pagodroma', 'nivea', 'confusa'],
        [
            'Pagodroma',
            'nivea',
            'major',
        ],
    ],
    [
        ['Pelagodroma', 'marina', 'eadesi'],
        [
            'Pelagodroma',
            'marina',
            'eadesorum',
        ],
    ],
    [
        ['Nycticorax', 'caledonicus', 'hilli'],
        [
            'Nycticorax',
            'caledonicus',
            'australasiae',
        ],
    ],
    [
        ['Nyctanassa', 'violacea', 'pauper'],
        [
            'Nyctanassa',
            'violacea',
            'paupera',
        ],
    ],
    [
        ['Rupornis', 'magnirostris', 'ruficaudus'],
        [
            'Rupornis',
            'magnirostris',
            'petulans',
        ],
    ],
    [
        ['Gallirallus', 'torquatus', 'saturatus'],
        [
            'Gallirallus',
            'torquatus',
            'limarius',
        ],
    ],
    [
        ['Porzana', 'albicollis', 'olivacea'],
        [
            'Porzana',
            'albicollis',
            'typhoeca',
        ],
    ],
    [
        ['Zenaida', 'auriculata', 'ruficauda'],
        [
            'Zenaida',
            'auriculata',
            'pentheria',
        ],
    ],
    [
        ['Zenaida', 'auriculata', 'rubripes'],
        [
            'Zenaida',
            'auriculata',
            'stenura',
        ],
    ],
    [
        ['Zenaida', 'auriculata', 'virgata'],
        [
            'Zenaida',
            'auriculata',
            'chrysauchenia',
        ],
    ],
    [
        ['Northiella', 'haematogaster', 'haematorrhous'],
        [
            'Northiella',
            'haematogaster',
            'haematorrhoa',
        ],
    ],
    [
        ['Eudynamys', 'orientalis', 'everetti'],
        [
            'Eudynamys',
            'orientalis',
            'picatus',
        ],
    ],
    [
        ['Megascops', 'choliba', 'uruguaiensis'],
        [
            'Megascops',
            'choliba',
            'uruguaii',
        ],
    ],
    [
        ['Megascops', 'albogularis', 'macabrum'],
        [
            'Megascops',
            'albogularis',
            'macabrus',
        ],
    ],
    [
        ['Ketupa', 'zeylonensis', 'leschenaultii'],
        [
            'Ketupa',
            'zeylonensis',
            'leschenaulti',
        ],
    ],
    [
        ['Glaucidium', 'siju', 'turquinensis'],
        [
            'Glaucidium',
            'siju',
            'turquinense',
        ],
    ],
    [
        ['Lurocalis', 'semitorquatus', 'nattereri'],
        [
            'Lurocalis',
            'semitorquatus',
            'nattererii',
        ],
    ],
    [
        ['Hydropsalis', 'cayennensis', 'apertus'],
        [
            'Hydropsalis',
            'cayennensis',
            'aperta',
        ],
    ],
    [
        ['Apus', 'pacificus', 'kurodae'],
        [
            'Apus',
            'pacificus',
            'kanoi',
        ],
    ],
    [
        ['Thalurania', 'colombica', 'fannyi'],
        [
            'Thalurania',
            'colombica',
            'fannyae',
        ],
    ],
    [
        ['Eriocnemis', 'vestita', 'arcosi'],
        [
            'Eriocnemis',
            'vestita',
            'arcosae',
        ],
    ],
    [
        ['Lesbia', 'nuna', 'eucharis'],
        [
            'Lesbia',
            'nuna',
            'huallagae',
        ],
    ],
    [
        ['Trogon', 'caligatus', 'braccatus'],
        [
            'Trogon',
            'caligatus',
            'sallaei',
        ],
    ],
    [
        ['Brachygalba', 'lugubris', 'naumburgi'],
        [
            'Brachygalba',
            'lugubris',
            'naumburgae',
        ],
    ],
    [
        ['Pteroglossus', 'aracari', 'vergens'],
        [
            'Pteroglossus',
            'aracari',
            'wiedii',
        ],
    ],
    [
        ['Aethopyga', 'flagrans', 'daphoenonota'],
        [
            'Aethopyga',
            'guimarasensis',
            'daphoenonota',
        ],
    ],

    // 4.1 ssp
    [
        ['Dromaius', 'novaehollandiae', 'ater'],
        [
            'Dromaius',
            'novaehollandiae',
            'minor',
        ],
    ],
    [
        ['Colinus', 'virginianus', 'coyolcos'],
        [
            'Colinus',
            'virginianus',
            'coyoleos',
        ],
    ],
    [
        ['Tetrastes', 'bonasia', 'griseonotus'],
        [
            'Tetrastes',
            'bonasia',
            'griseonota',
        ],
    ],
    [
        ['Tetrao', 'urogallus', 'major'],
        [
            'Tetrao',
            'urogallus',
            'crassirostris',
        ],
    ],
    [
        ['Lagopus', 'muta', 'capta'],
        [
            'Lagopus',
            'muta',
            'macruros',
        ],
    ],
    [
        ['Thalasseus', 'acuflavidus', 'eurygnatha'],
        [
            'Thalasseus',
            'acuflavidus',
            'eurygnathus',
        ],
    ],
    [
        ['Geoffroyus', 'geoffroyi', 'mysoriensis'],
        [
            'Geoffroyus',
            'geoffroyi',
            'mysorensis',
        ],
    ],
    [
        ['Momotus', 'momota', 'marcgravinianus'],
        [
            'Momotus',
            'momota',
            'marcgravianus',
        ],
    ],
    [
        ['Ceyx', 'argentatus', 'flumenicolus'],
        [
            'Ceyx',
            'argentatus',
            'flumenicola',
        ],
    ],
    [
        ['Eurystomus', 'orientalis', 'calonyx'],
        [
            'Eurystomus',
            'orientalis',
            'cyanicollis',
        ],
    ],
    [
        ['Capito', 'auratus', 'bolivianus'],
        [
            'Capito',
            'auratus',
            'insperatus',
        ],
    ],
    [
        ['Luscinia', 'megarhynchos', 'hafizi'],
        [
            'Luscinia',
            'megarhynchos',
            'golzii',
        ],
    ],
    [
        ['Saxicola', 'maurus', 'variegatus'],
        [
            'Saxicola',
            'maurus',
            'hemprichii',
        ],
    ],
    [
        ['Saxicola', 'maurus', 'armenicus'],
        [
            'Saxicola',
            'maurus',
            'variegatus',
        ],
    ],
    [
        ['Carpodacus', 'davidianus', 'argyrophrys'],
        [
            'Carpodacus',
            'pulcherrimus',
            'argyrophrys',
        ],
    ],
    [
        ['Carpodacus', 'davidianus', 'waltoni'],
        [
            'Carpodacus',
            'waltoni',
            'waltoni',
        ],
    ],
    [
        ['Carpodacus', 'eos', null],
        [
            'Carpodacus',
            'waltoni',
            'eos',
        ],
    ],

    // not truly 'splits', but a way to rename subspecies with gender changes
    [
        ['Aratinga', 'acuticaudata', 'acuticaudata'],
        [
            'Thectocercus',
            'acuticaudatus',
            'acuticaudatus',
        ],
    ],
    [
        ['Aratinga', 'acuticaudata', 'neoxena'],
        [
            'Thectocercus',
            'acuticaudatus',
            'neoxenus',
        ],
    ],
    [
        ['Aratinga', 'holochlora', 'holochlora'],
        [
            'Psittacara',
            'holochlorus',
            'holochlorus',
        ],
    ],
    [
        ['Aratinga', 'wagleri', 'frontata'],
        [
            'Psittacara',
            'wagleri',
            'frontatus',
        ],
    ],
    [
        ['Aratinga', 'mitrata', 'mitrata'],
        [
            'Psittacara',
            'mitratus',
            'mitratus',
        ],
    ],
    [
        ['Aratinga', 'mitrata', 'tucumana'],
        [
            'Psittacara',
            'mitratus',
            'tucumanus',
        ],
    ],
    [
        ['Aratinga', 'leucophthalma', 'leucophthalma'],
        [
            'Psittacara',
            'leucophthalmus',
            'leucophthalmus',
        ],
    ],
    [
        ['Aratinga', 'chloroptera', 'chloroptera'],
        [
            'Psittacara',
            'chloropterus',
            'chloropterus',
        ],
    ],
    [
        ['Lichenostomus', 'flavus', 'flavus'],
        [
            'Stomiopera',
            'flava',
            'flava',
        ],
    ],
    [
        ['Lichenostomus', 'flavus', 'addendus'],
        [
            'Stomiopera',
            'flava',
            'addenda',
        ],
    ],
    [
        ['Lichenostomus', 'flavescens', 'germanus'],
        [
            'Ptilotula',
            'flavescens',
            'germana',
        ],
    ],
    [
        ['lichenostomus', 'fuscus', 'fuscus'],
        [
            'ptilotula',
            'fusca',
            'fusca',
        ],
    ],
    [
        ['lichenostomus', 'fuscus', 'subgermanus'],
        [
            'ptilotula',
            'fusca',
            'subgermana',
        ],
    ],
    [
        ['lichenostomus', 'plumulus', 'plumulus'],
        [
            'ptilotula',
            'plumula',
            'plumula',
        ],
    ],
    [
        ['lichenostomus', 'penicillatus', 'penicillatus'],
        [
            'ptilotula',
            'penicillata',
            'penicillata',
        ],
    ],
    [
        ['Rhyacornis', 'fuliginosa', 'fuliginosa'],
        [
            'Phoenicurus',
            'fuliginosus',
            'fuliginosus',
        ],
    ],
];

$lumps = [
    //array(array("genus", "species", NULL), array("genus", "species", "ssp")),

    // 3.4 changes
    [
        ['thalurania', 'fannyi', 'hypochlora'],
        [
            'thalurania',
            'colombica',
            'hypochlora',
        ],
    ],
    [
        ['thalurania', 'fannyi', 'verticeps'],
        [
            'thalurania',
            'colombica',
            'verticeps',
        ],
    ],
    [
        ['thalurania', 'fannyi', ''],
        [
            'thalurania',
            'colombica',
            'fannyae?',
        ],
    ],
    [
        ['upucerthia', 'jelskii', 'pallida'],
        [
            'upucerthia',
            'validirostris',
            'pallida',
        ],
    ],
    [
        ['upucerthia', 'jelskii', ''],
        [
            'upucerthia',
            'validirostris',
            'jelskii?',
        ],
    ],

    // 3.5 changes
    [
        ['ortygospiza', 'fuscocrissa', 'digressa'],
        [
            'ortygospiza',
            'atricollis',
            'digressa',
        ],
    ],
    [
        ['ortygospiza', 'fuscocrissa', ''],
        [
            'ortygospiza',
            'atricollis',
            'fuscocrissa',
        ],
    ],
    [
        ['ortygospiza', 'gabonensis', null],
        [
            'ortygospiza',
            'atricollis',
            'gabonensis',
        ],
    ],

    // 4.1 changes
    [
        ['carpodacus', 'rubicilla', null],
        [
            'carpodacus',
            'rubicilla',
            'rubicilla',
        ],
    ],
    [
        ['carpodacus', 'severtzovi', 'diabolicus'],
        [
            'carpodacus',
            'rubicilla',
            'diabolicus',
        ],
    ],
    [
        ['carpodacus', 'severtzovi', 'kobdensis'],
        [
            'carpodacus',
            'rubicilla',
            'kobdensis',
        ],
    ],
    [
        ['carpodacus', 'severtzovi', 'severtzovi'],
        [
            'carpodacus',
            'rubicilla',
            'severtzovi',
        ],
    ],
];
