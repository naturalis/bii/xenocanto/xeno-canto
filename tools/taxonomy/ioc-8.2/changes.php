<?php

$renames =
    [
        [['Seicercus', 'whistleri'], ['Phylloscopus', 'whistleri']],
        [['Schoenicola', 'brevirostris'], ['Catriscus', 'brevirostris']],
        [['Seicercus', 'grammiceps'], ['Phylloscopus', 'grammiceps']],
        [['Coracina', 'tenuirostris'], ['Edolisoma', 'tenuirostre']],
        [['Coracina', 'melaschistos'], ['Lalage', 'melaschistos']],
        [['Locustella', 'pryeri'], ['Helopsaltes', 'pryeri']],
        [['Locustella', 'certhiola'], ['Helopsaltes', 'certhiola']],
        [['Megalurus', 'macrurus'], ['Cincloramphus', 'macrurus']],
        [['Seicercus', 'affinis'], ['Phylloscopus', 'intermedius']],
        [['Seicercus', 'valentini'], ['Phylloscopus', 'valentini']],
        [['Coracina', 'fimbriata'], ['Lalage', 'fimbriata']],
        [['Coracina', 'melas'], ['Edolisoma', 'melas']],
        [['Megalurus', 'timoriensis'], ['Cincloramphus', 'timoriensis']],
        [['Seicercus', 'montis'], ['Phylloscopus', 'montis']],
        [['Montecincla', 'meridionale'], ['Montecincla', 'meridionalis']],
        [['Megalurus', 'gramineus'], ['Poodytes', 'gramineus']],
        [['Madanga', 'ruficollis'], ['Anthus', 'ruficollis']],
        [['Amaurocichla', 'bocagii'], ['Motacilla', 'bocagii']],
        [['Seicercus', 'burkii'], ['Phylloscopus', 'burkii']],
        [
            ['Seicercus', 'tephrocephalus'],
            [
                'Phylloscopus',
                'tephrocephalus',
            ],
        ],
        [['Seicercus', 'omeiensis'], ['Phylloscopus', 'omeiensis']],
        [['Seicercus', 'soror'], ['Phylloscopus', 'soror']],
        [['Seicercus', 'poliogenys'], ['Phylloscopus', 'poliogenys']],
        [['Megalurulus', 'mariei'], ['Cincloramphus', 'mariae']],
        [['Locustella', 'pleskei'], ['Helopsaltes', 'pleskei']],
        [['Locustella', 'ochotensis'], ['Helopsaltes', 'ochotensis']],
        [['Locustella', 'amnicola'], ['Helopsaltes', 'amnicola']],
        [['Locustella', 'fasciolata'], ['Helopsaltes', 'fasciolatus']],
        [['Megalurus', 'albolimbatus'], ['Poodytes', 'albolimbatus']],
        [['Megalurus', 'carteri'], ['Poodytes', 'carteri']],
        [['Megalurus', 'rufescens'], ['Poodytes', 'rufescens']],
        [['Megalurus', 'cruralis'], ['Cincloramphus', 'cruralis']],
        [['Megalurulus', 'rubiginosus'], ['Cincloramphus', 'rubiginosus']],
        [['Megalurulus', 'grosvenori'], ['Cincloramphus', 'grosvenori']],
        [
            ['Buettikoferella', 'bivittata'],
            [
                'Cincloramphus',
                'bivittatus',
            ],
        ],
        [['Megalurus', 'mathewsi'], ['Cincloramphus', 'mathewsi']],
        [['Megalurulus', 'llaneae'], ['Cincloramphus', 'llaneae']],
        [['Chaetornis', 'striata'], ['Schoenicola', 'striatus']],
        [['Amphilais', 'seebohmi'], ['Bradypterus', 'seebohmi']],
        [['Pycnonotus', 'hualon'], ['Nok', 'hualon']],
        [['Psaltria', 'exilis'], ['Aegithalos', 'exilis']],
        [['Ninox', 'superciliaris'], ['Athene', 'superciliaris']],
        [['Heteroglaux', 'blewitti'], ['Athene', 'blewitti']],
        [['Coracina', 'graueri'], ['Ceblepyris', 'graueri']],
        [['Coracina', 'pectoralis'], ['Ceblepyris', 'pectoralis']],
        [['Coracina', 'mcgregori'], ['Malindangia', 'mcgregori']],
        [['Coracina', 'analis'], ['Edolisoma', 'anale']],
        [['Coracina', 'ostenta'], ['Edolisoma', 'ostentum']],
        [['Coracina', 'dohertyi'], ['Edolisoma', 'dohertyi']],
        [['Coracina', 'dispar'], ['Edolisoma', 'dispar']],
        [['Coracina', 'salomonis'], ['Edolisoma', 'salomonis']],
        [['Coracina', 'incerta'], ['Edolisoma', 'incertum']],
        [['Coracina', 'sula'], ['Edolisoma', 'sula']],
        [['Coracina', 'nesiotis'], ['Edolisoma', 'nesiotis']],
        [['Coracina', 'insperata'], ['Edolisoma', 'insperatum']],
        [['Coracina', 'parvula'], ['Edolisoma', 'parvulum']],
        [['Coracina', 'monacha'], ['Edolisoma', 'monacha']],
        [['Coracina', 'abbotti'], ['Celebesica', 'abbotti']],
        [['Coracina', 'azurea'], ['Cyanograucalus', 'azureus']],
        [['Coracina', 'typica'], ['Lalage', 'typica']],
        [['Coracina', 'newtoni'], ['Lalage', 'newtoni']],
        [['Megascops', 'sp.nov.Santa_Marta'], ['Megascops', 'gilesi']],
        [['Locustella', 'amnicola'], ['Helopsaltes', 'amnicola']],
        [['Locustella', 'fasciolata'], ['Helopsaltes', 'fasciolatus']],
    ];
$splits  =
    [
        [
            ['Phylloscopus', 'davisoni', 'davisoni'],
            [
                'Phylloscopus',
                'intensior',
                'muleyitensis',
            ],
        ],
        [
            ['Dendrocoptes', 'auriceps', 'auriceps'],
            [
                'Dendrocoptes',
                'auriceps',
            ],
        ],
        [
            ['Lophorina', 'superba', 'feminina'],
            [
                'Lophorina',
                'superba',
                'superba',
            ],
        ],
        [['Cyornis', 'colonus', 'subsolanus'], ['Cyornis', 'colonus', '']],
        [
            ['Riparia', 'diluta', 'gavrilovi'],
            [
                'Riparia',
                'diluta',
                'diluta',
            ],
        ],
        [
            ['Riparia', 'diluta', 'transbaykalica'],
            [
                'Riparia',
                'diluta',
                'diluta',
            ],
        ],
        [
            ['Riparia', 'riparia', 'innominata'],
            [
                'Riparia',
                'riparia',
                'riparia',
            ],
        ],
        [
            ['Riparia', 'riparia', 'eilata'],
            [
                'Riparia',
                'riparia',
                'shelleyi',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'koenigi'],
            [
                'Lanius',
                'excubitor',
                'koenigi',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'algeriensis'],
            [
                'Lanius',
                'excubitor',
                'algeriensis',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'elegans'],
            [
                'Lanius',
                'excubitor',
                'elegans',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'leucopygos'],
            [
                'Lanius',
                'excubitor',
                'leucopygos',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'aucheri'],
            [
                'Lanius',
                'excubitor',
                'aucheri',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'theresae'],
            [
                'Lanius',
                'excubitor',
                'theresae',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'buryi'],
            [
                'Lanius',
                'excubitor',
                'buryi',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'uncinatus'],
            [
                'Lanius',
                'excubitor',
                'uncinatus',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'lahtora'],
            [
                'Lanius',
                'excubitor',
                'lahtora',
            ],
        ],
        [
            ['Dendrocoptes', 'auriceps', 'incognitus'],
            [
                'Dendrocoptes',
                'auriceps',
                '',
            ],
        ],
        [
            ['Phylloscopus', 'ogilviegranti', 'intensior'],
            [
                'Phylloscopus',
                'intensior',
                'intensior',
            ],
        ],
        [
            ['Phylloscopus', 'borealis', 'borealis'],
            [
                'Phylloscopus',
                'borealis',
                '',
            ],
        ],
        [
            ['Phylloscopus', 'borealis', 'kennicotti'],
            [
                'Phylloscopus',
                'borealis',
                '',
            ],
        ],
        [
            ['Seicercus', 'affinis', 'affinis'],
            [
                'Phylloscopus',
                'intermedius',
                'zosterops',
            ],
        ],
        [
            ['Seicercus', 'affinis', 'intermedius'],
            [
                'Phylloscopus',
                'intermedius',
                'intermedius',
            ],
        ],
        [
            ['Seicercus', 'whistleri', 'whistleri'],
            [
                'Phylloscopus',
                'whistleri',
                'whistleri',
            ],
        ],
        [
            ['Seicercus', 'valentini', 'valentini'],
            [
                'Phylloscopus',
                'valentini',
                'valentini',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'castaniceps'],
            [
                'Phylloscopus',
                'castaniceps',
                'castaniceps',
            ],
        ],
        [
            ['Seicercus', 'montis', 'montis'],
            [
                'Phylloscopus',
                'montis',
                'montis',
            ],
        ],
        [
            ['Seicercus', 'grammiceps', 'grammiceps'],
            [
                'Phylloscopus',
                'grammiceps',
                'grammiceps',
            ],
        ],
        [
            ['Locustella', 'pryeri', 'pryeri'],
            [
                'Helopsaltes',
                'pryeri',
                'pryeri',
            ],
        ],
        [
            ['Locustella', 'certhiola', 'certhiola'],
            [
                'Helopsaltes',
                'certhiola',
                'certhiola',
            ],
        ],
        [
            ['Bradypterus', 'alfredi', 'alfredi'],
            [
                'Locustella',
                'alfredi',
                'alfredi',
            ],
        ],
        [
            ['Megalurus', 'punctatus', 'punctatus'],
            [
                'Poodytes',
                'punctatus',
                'punctatus',
            ],
        ],
        [
            ['Megalurus', 'gramineus', 'gramineus'],
            [
                'Poodytes',
                'gramineus',
                'gramineus',
            ],
        ],
        [
            ['Megalurus', 'macrurus', 'macrurus'],
            [
                'Cincloramphus',
                'macrurus',
                'macrurus',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'timoriensis'],
            [
                'Cincloramphus',
                'timoriensis',
                'timoriensis',
            ],
        ],
        [
            ['Megalurulus', 'whitneyi', 'whitneyi'],
            [
                'Cincloramphus',
                'whitneyi',
                'whitneyi',
            ],
        ],
        [
            ['Megalurulus', 'rufus', 'rufus'],
            [
                'Cincloramphus',
                'rufus',
                'rufus',
            ],
        ],
        [
            ['Schoenicola', 'brevirostris', 'brevirostris'],
            [
                'Catriscus',
                'brevirostris',
                'brevirostris',
            ],
        ],
        [
            ['Horornis', 'diphone', 'canturians'],
            [
                'Horornis',
                'canturians',
                'canturians',
            ],
        ],
        [
            ['Sporophila', 'torqueola', 'morelleti'],
            [
                'Sporophila',
                'morelleti',
                'morelleti',
            ],
        ],
        [['Pica', 'pica', 'bottanensis'], ['Pica', 'bottanensis', '']],
        [['Pica', 'pica', 'mauritanica'], ['Pica', 'mauritanica', '']],
        [['Pica', 'pica', 'asirensis'], ['Pica', 'asirensis', '']],
        [['Pica', 'pica', 'serica'], ['Pica', 'serica', 'serica']],
        [
            ['Rhipidura', 'teysmanni', 'sulaensis'],
            [
                'Rhipidura',
                'sulaensis',
            ],
        ],
        [
            ['Coracina', 'cinerea', 'cinerea'],
            [
                'Ceblepyris',
                'cinereus',
                'cinereus',
            ],
        ],
        [
            ['Coracina', 'cucullata', 'cucullata'],
            [
                'Ceblepyris',
                'cucullatus',
                'cucullatus',
            ],
        ],
        [
            ['Coracina', 'caesia', 'caesia'],
            [
                'Ceblepyris',
                'caesius',
                'caesius',
            ],
        ],
        [
            ['Coracina', 'coerulescens', 'coerulescens'],
            [
                'Edolisoma',
                'coerulescens',
                'coerulescens',
            ],
        ],
        [
            ['Coracina', 'montana', 'montana'],
            [
                'Edolisoma',
                'montanum',
                'montanum',
            ],
        ],
        [
            ['Coracina', 'schisticeps', 'schisticeps'],
            [
                'Edolisoma',
                'schisticeps',
                'schisticeps',
            ],
        ],
        [
            ['Coracina', 'ceramensis', 'ceramensis'],
            [
                'Edolisoma',
                'ceramense',
                'ceramense',
            ],
        ],
        [
            ['Coracina', 'mindanensis', 'mindanensis'],
            [
                'Edolisoma',
                'mindanense',
                'mindanense',
            ],
        ],
        [
            ['Coracina', 'holopolia', 'holopolia'],
            [
                'Edolisoma',
                'holopolium',
                'holopolium',
            ],
        ],
        [['Coracina', 'morio', 'morio'], ['Edolisoma', 'morio', 'morio']],
        [
            ['Coracina', 'remota', 'remota'],
            [
                'Edolisoma',
                'remotum',
                'remotum',
            ],
        ],
        [['Coracina', 'melas', 'melas'], ['Edolisoma', 'melas', 'melas']],
        [
            ['Coracina', 'tenuirostris', 'tenuirostris'],
            [
                'Edolisoma',
                'tenuirostre',
                'tenuirostre',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'admiralitatis'],
            [
                'Edolisoma',
                'admiralitatis',
                '',
            ],
        ],
        [
            ['Coracina', 'melaschistos', 'melaschistos'],
            [
                'Lalage',
                'melaschistos',
                'melaschistos',
            ],
        ],
        [
            ['Coracina', 'melanoptera', 'melanoptera'],
            [
                'Lalage',
                'melanoptera',
                'melanoptera',
            ],
        ],
        [
            ['Coracina', 'polioptera', 'polioptera'],
            [
                'Lalage',
                'polioptera',
                'polioptera',
            ],
        ],
        [
            ['Coracina', 'fimbriata', 'fimbriata'],
            [
                'Lalage',
                'fimbriata',
                'fimbriata',
            ],
        ],
        [
            ['Psophodes', 'nigrogularis', 'leucogaster'],
            [
                'Psophodes',
                'leucogaster',
                'leucogaster',
            ],
        ],
        [
            ['Cranioleuca', 'baroni', 'baroni'],
            [
                'Cranioleuca',
                'antisiensis',
                'baroni',
            ],
        ],
        [
            ['Thamnistes', 'anabatinus', 'rufescens'],
            [
                'Thamnistes',
                'rufescens',
                '',
            ],
        ],
        [
            ['Machaeropterus', 'striolatus', 'eckelberryi'],
            [
                'Machaeropterus',
                'eckelberryi',
                '',
            ],
        ],
        [
            ['Megascops', 'roraimae', 'roraimae'],
            [
                'Megascops',
                'roraimae',
                'roraimae',
            ],
        ],
        [
            ['Megascops', 'napensis', ''],
            [
                'Megascops',
                'roraimae',
                'napensis',
            ],
        ],
        [
            ['Lophorina', 'superba', 'niedda'],
            [
                'Lophorina',
                'niedda',
                'niedda',
            ],
        ],
        [['Lophorina', 'superba', 'minor'], ['Lophorina', 'minor', '']],
        [
            ['Seicercus', 'whistleri', 'nemoralis'],
            [
                'Phylloscopus',
                'whistleri',
                'nemoralis',
            ],
        ],
        [
            ['Seicercus', 'valentini', 'latouchei'],
            [
                'Phylloscopus',
                'valentini',
                'latouchei',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'laurentei'],
            [
                'Phylloscopus',
                'castaniceps',
                'laurentei',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'sinensis'],
            [
                'Phylloscopus',
                'castaniceps',
                'sinensis',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'collinsi'],
            [
                'Phylloscopus',
                'castaniceps',
                'collinsi',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'stresemanni'],
            [
                'Phylloscopus',
                'castaniceps',
                'stresemanni',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'annamensis'],
            [
                'Phylloscopus',
                'castaniceps',
                'annamensis',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'youngi'],
            [
                'Phylloscopus',
                'castaniceps',
                'youngi',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'butleri'],
            [
                'Phylloscopus',
                'castaniceps',
                'butleri',
            ],
        ],
        [
            ['Seicercus', 'castaniceps', 'muelleri'],
            [
                'Phylloscopus',
                'castaniceps',
                'muelleri',
            ],
        ],
        [
            ['Seicercus', 'montis', 'davisoni'],
            [
                'Phylloscopus',
                'montis',
                'davisoni',
            ],
        ],
        [
            ['Seicercus', 'montis', 'inornatus'],
            [
                'Phylloscopus',
                'montis',
                'barisanus',
            ],
        ],
        [
            ['Seicercus', 'montis', 'xanthopygius'],
            [
                'Phylloscopus',
                'montis',
                'xanthopygius',
            ],
        ],
        [
            ['Seicercus', 'montis', 'floris'],
            [
                'Phylloscopus',
                'montis',
                'floris',
            ],
        ],
        [
            ['Seicercus', 'montis', 'paulinae'],
            [
                'Phylloscopus',
                'montis',
                'paulinae',
            ],
        ],
        [
            ['Seicercus', 'grammiceps', 'sumatrensis'],
            [
                'Phylloscopus',
                'grammiceps',
                'sumatrensis',
            ],
        ],
        [
            ['Phylloscopus', 'presbytes', 'floris'],
            [
                'Phylloscopus',
                'presbytes',
                'floresianus',
            ],
        ],
        [
            ['Phylloscopus', 'borealis', 'kennicotti'],
            [
                'Phylloscopus',
                'borealis',
                '',
            ],
        ],
        [
            ['Locustella', 'pryeri', 'sinensis'],
            [
                'Helopsaltes',
                'pryeri',
                'sinensis',
            ],
        ],
        [
            ['Locustella', 'certhiola', 'sparsimstriata'],
            [
                'Helopsaltes',
                'certhiola',
                'sparsimstriatus',
            ],
        ],
        [
            ['Locustella', 'certhiola', 'centralasiae'],
            [
                'Helopsaltes',
                'certhiola',
                'centralasiae',
            ],
        ],
        [
            ['Locustella', 'certhiola', 'rubescens'],
            [
                'Helopsaltes',
                'certhiola',
                'rubescens',
            ],
        ],
        [
            ['Locustella', 'certhiola', 'minor'],
            [
                'Helopsaltes',
                'certhiola',
                'minor',
            ],
        ],
        [
            ['Bradypterus', 'alfredi', 'kungwensis'],
            [
                'Locustella',
                'alfredi',
                'kungwensis',
            ],
        ],
        [
            ['Megalurus', 'punctatus', 'vealeae'],
            [
                'Poodytes',
                'punctatus',
                'vealeae',
            ],
        ],
        [
            ['Megalurus', 'punctatus', 'stewartianus'],
            [
                'Poodytes',
                'punctatus',
                'stewartianus',
            ],
        ],
        [
            ['Megalurus', 'punctatus', 'wilsoni'],
            [
                'Poodytes',
                'punctatus',
                'wilsoni',
            ],
        ],
        [
            ['Megalurus', 'punctatus', 'caudatus'],
            [
                'Poodytes',
                'punctatus',
                'caudatus',
            ],
        ],
        [
            ['Megalurus', 'gramineus', 'papuensis'],
            [
                'Poodytes',
                'gramineus',
                'papuensis',
            ],
        ],
        [
            ['Megalurus', 'gramineus', 'goulburni'],
            [
                'Poodytes',
                'gramineus',
                'goulburni',
            ],
        ],
        [
            ['Megalurus', 'gramineus', 'thomasi'],
            [
                'Poodytes',
                'gramineus',
                'thomasi',
            ],
        ],
        [
            ['Megalurus', 'macrurus', 'stresemanni'],
            [
                'Cincloramphus',
                'macrurus',
                'stresemanni',
            ],
        ],
        [
            ['Megalurus', 'macrurus', 'harterti'],
            [
                'Cincloramphus',
                'macrurus',
                'harterti',
            ],
        ],
        [
            ['Megalurus', 'macrurus', 'alpinus'],
            [
                'Cincloramphus',
                'macrurus',
                'alpinus',
            ],
        ],
        [
            ['Megalurus', 'macrurus', 'interscapularis'],
            [
                'Cincloramphus',
                'macrurus',
                'interscapularis',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'tweeddalei'],
            [
                'Cincloramphus',
                'timoriensis',
                'tweeddalei',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'alopex'],
            [
                'Cincloramphus',
                'timoriensis',
                'alopex',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'amboinensis'],
            [
                'Cincloramphus',
                'timoriensis',
                'amboinensis',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'crex'],
            [
                'Cincloramphus',
                'timoriensis',
                'crex',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'mindorensis'],
            [
                'Cincloramphus',
                'timoriensis',
                'mindorensis',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'celebensis'],
            [
                'Cincloramphus',
                'timoriensis',
                'celebensis',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'inquirendus'],
            [
                'Cincloramphus',
                'timoriensis',
                'inquirendus',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'muscalis'],
            [
                'Cincloramphus',
                'timoriensis',
                'muscalis',
            ],
        ],
        [
            ['Megalurus', 'timoriensis', 'alisteri'],
            [
                'Cincloramphus',
                'timoriensis',
                'alisteri',
            ],
        ],
        [
            ['Megalurulus', 'whitneyi', 'turipavae'],
            [
                'Cincloramphus',
                'whitneyi',
                'turipavae',
            ],
        ],
        [
            ['Megalurulus', 'rufus', 'cluniei'],
            [
                'Cincloramphus',
                'rufus',
                'cluniei',
            ],
        ],
        [
            ['Schoenicola', 'brevirostris', 'alexinae'],
            [
                'Catriscus',
                'brevirostris',
                'alexinae',
            ],
        ],
        [
            ['Sporophila', 'torqueola', 'sharpei'],
            [
                'Sporophila',
                'morelleti',
                'sharpei',
            ],
        ],
        [
            ['Sporophila', 'torqueola', 'mutanda'],
            [
                'Sporophila',
                'morelleti',
                'mutanda',
            ],
        ],
        [['Pica', 'pica', 'anderssoni'], ['Pica', 'serica', 'anderssoni']],
        [
            ['Coracina', 'cinerea', 'pallida'],
            [
                'Ceblepyris',
                'cinereus',
                'pallidus',
            ],
        ],
        [
            ['Coracina', 'cucullata', 'moheliensis'],
            [
                'Ceblepyris',
                'cucullatus',
                'moheliensis',
            ],
        ],
        [['Coracina', 'caesia', 'pura'], ['Ceblepyris', 'caesius', 'purus']],
        [
            ['Coracina', 'coerulescens', 'deschauenseei'],
            [
                'Edolisoma',
                'coerulescens',
                'deschauenseei',
            ],
        ],
        [
            ['Coracina', 'coerulescens', 'altera'],
            [
                'Edolisoma',
                'coerulescens',
                'alterum',
            ],
        ],
        [
            ['Coracina', 'montana', 'bicinia'],
            [
                'Edolisoma',
                'montanum',
                'bicinia',
            ],
        ],
        [
            ['Coracina', 'schisticeps', 'reichenowi'],
            [
                'Edolisoma',
                'schisticeps',
                'reichenowi',
            ],
        ],
        [
            ['Coracina', 'schisticeps', 'poliopsa'],
            [
                'Edolisoma',
                'schisticeps',
                'poliopsa',
            ],
        ],
        [
            ['Coracina', 'schisticeps', 'vittata'],
            [
                'Edolisoma',
                'schisticeps',
                'vittatum',
            ],
        ],
        [
            ['Coracina', 'ceramensis', 'hoogerwerfi'],
            [
                'Edolisoma',
                'ceramense',
                'hoogerwerfi',
            ],
        ],
        [
            ['Coracina', 'mindanensis', 'lecroyae'],
            [
                'Edolisoma',
                'mindanense',
                'lecroyae',
            ],
        ],
        [
            ['Coracina', 'mindanensis', 'elusa'],
            [
                'Edolisoma',
                'mindanense',
                'elusum',
            ],
        ],
        [
            ['Coracina', 'mindanensis', 'ripleyi'],
            [
                'Edolisoma',
                'mindanense',
                'ripleyi',
            ],
        ],
        [
            ['Coracina', 'mindanensis', 'everetti'],
            [
                'Edolisoma',
                'mindanense',
                'everetti',
            ],
        ],
        [
            ['Coracina', 'holopolia', 'tricolor'],
            [
                'Edolisoma',
                'holopolium',
                'tricolor',
            ],
        ],
        [
            ['Coracina', 'holopolia', 'pygmaea'],
            [
                'Edolisoma',
                'holopolium',
                'pygmaeum',
            ],
        ],
        [
            ['Coracina', 'morio', 'salvadorii'],
            [
                'Edolisoma',
                'morio',
                'salvadorii',
            ],
        ],
        [
            ['Coracina', 'morio', 'talautensis'],
            [
                'Edolisoma',
                'morio',
                'talautense',
            ],
        ],
        [
            ['Coracina', 'remota', 'ultima'],
            [
                'Edolisoma',
                'remotum',
                'ultimum',
            ],
        ],
        [
            ['Coracina', 'remota', 'saturatior'],
            [
                'Edolisoma',
                'remotum',
                'saturatius',
            ],
        ],
        [
            ['Coracina', 'remota', 'erythropygia'],
            [
                'Edolisoma',
                'remotum',
                'erythropygium',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'edithae'],
            [
                'Edolisoma',
                'tenuirostre',
                'edithae',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'pererrata'],
            [
                'Edolisoma',
                'tenuirostre',
                'pererratum',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'kalaotuae'],
            [
                'Edolisoma',
                'tenuirostre',
                'kalaotuae',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'emancipata'],
            [
                'Edolisoma',
                'tenuirostre',
                'emancipatum',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'timoriensis'],
            [
                'Edolisoma',
                'tenuirostre',
                'timoriense',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'pelingi'],
            [
                'Edolisoma',
                'tenuirostre',
                'pelingi',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'grayi'],
            [
                'Edolisoma',
                'tenuirostre',
                'grayi',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'obiensis'],
            [
                'Edolisoma',
                'tenuirostre',
                'obiense',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'amboinensis'],
            [
                'Edolisoma',
                'tenuirostre',
                'amboinense',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'matthiae'],
            [
                'Edolisoma',
                'tenuirostre',
                'matthiae',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'heinrothi'],
            [
                'Edolisoma',
                'tenuirostre',
                'heinrothi',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'rooki'],
            [
                'Edolisoma',
                'tenuirostre',
                'rooki',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'nehrkorni'],
            [
                'Edolisoma',
                'tenuirostre',
                'nehrkorni',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'numforana'],
            [
                'Edolisoma',
                'tenuirostre',
                'numforanum',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'meyerii'],
            [
                'Edolisoma',
                'tenuirostre',
                'meyerii',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'aruensis'],
            [
                'Edolisoma',
                'tenuirostre',
                'aruense',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'muellerii'],
            [
                'Edolisoma',
                'tenuirostre',
                'muellerii',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'tagulana'],
            [
                'Edolisoma',
                'tenuirostre',
                'tagulanum',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'rostrata'],
            [
                'Edolisoma',
                'tenuirostre',
                'rostratum',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'nisoria'],
            [
                'Edolisoma',
                'tenuirostre',
                'nisorium',
            ],
        ],
        [
            ['Coracina', 'tenuirostris', 'melvillensis'],
            [
                'Edolisoma',
                'tenuirostre',
                'melvillense',
            ],
        ],
        [
            ['Coracina', 'melas', 'waigeuensis'],
            [
                'Edolisoma',
                'melas',
                'waigeuense',
            ],
        ],
        [
            ['Coracina', 'melas', 'tommasonis'],
            [
                'Edolisoma',
                'melas',
                'tommasonis',
            ],
        ],
        [
            ['Coracina', 'melas', 'batantae'],
            [
                'Edolisoma',
                'melas',
                'batantae',
            ],
        ],
        [
            ['Coracina', 'melaschistos', 'avensis'],
            [
                'Lalage',
                'melaschistos',
                'avensis',
            ],
        ],
        [
            ['Coracina', 'melaschistos', 'intermedia'],
            [
                'Lalage',
                'melaschistos',
                'intermedia',
            ],
        ],
        [
            ['Coracina', 'melaschistos', 'saturata'],
            [
                'Lalage',
                'melaschistos',
                'saturata',
            ],
        ],
        [
            ['Coracina', 'melanoptera', 'sykesi'],
            [
                'Lalage',
                'melanoptera',
                'sykesi',
            ],
        ],
        [
            ['Coracina', 'polioptera', 'jabouillei'],
            [
                'Lalage',
                'polioptera',
                'jabouillei',
            ],
        ],
        [
            ['Coracina', 'polioptera', 'indochinensis'],
            [
                'Lalage',
                'polioptera',
                'indochinensis',
            ],
        ],
        [
            ['Coracina', 'fimbriata', 'neglecta'],
            [
                'Lalage',
                'fimbriata',
                'neglecta',
            ],
        ],
        [
            ['Coracina', 'fimbriata', 'culminata'],
            [
                'Lalage',
                'fimbriata',
                'culminata',
            ],
        ],
        [
            ['Coracina', 'fimbriata', 'schierbrandi'],
            [
                'Lalage',
                'fimbriata',
                'schierbrandi',
            ],
        ],
        [
            ['Coracina', 'fimbriata', 'compta'],
            [
                'Lalage',
                'fimbriata',
                'compta',
            ],
        ],
        [
            ['Psophodes', 'nigrogularis', 'lashmari'],
            [
                'Psophodes',
                'leucogaster',
                'lashmari',
            ],
        ],
        [
            ['Cranioleuca', 'baroni', 'capitalis'],
            [
                'Cranioleuca',
                'antisiensis',
                'capitalis',
            ],
        ],
        [
            ['Cranioleuca', 'baroni', 'zaratensis'],
            [
                'Cranioleuca',
                'antisiensis',
                'zaratensis',
            ],
        ],
    ];
$lumps   =
    [
        [
            ['Megascops', 'colombianus', ''],
            [
                'Megascops',
                'ingens',
                'colombianus',
            ],
        ],
        [
            ['Horornis', 'borealis', ''],
            [
                'Horornis',
                'canturians',
                'borealis',
            ],
        ],
    ];
