<?php

$renames =
    [

        [['Picoides', 'fumigatus'], ['Leuconotopicus', 'fumigatus']],
        [['Picoides', 'scalaris'], ['Dryobates', 'scalaris']],
        [['Picoides', 'pubescens'], ['Dryobates', 'pubescens']],
        [['Picoides', 'villosus'], ['Leuconotopicus', 'villosus']], // overrule
        [['Dendropicos', 'namaquus'], ['Chloropicus', 'namaquus']],
        [['Dendrocopos', 'medius'], ['Dendrocoptes', 'medius']],
        [['Dendrocopos', 'minor'], ['Dryobates', 'minor']],
        [
            ['Picoides', 'albolarvatus'],
            [
                'Leuconotopicus',
                'albolarvatus',
            ],
        ],
        [['Dendrocopos', 'kizuki'], ['Yungipicus', 'kizuki']],
        [['Picoides', 'arizonae'], ['Leuconotopicus', 'arizonae']],
        [
            ['Dendrocopos', 'canicapillus'],
            [
                'Yungipicus',
                'canicapillus',
            ],
        ],
        [['Dendrocopos', 'auriceps'], ['Dendrocoptes', 'auriceps']],
        [['Dendrocopos', 'temminckii'], ['Yungipicus', 'temminckii']],
        [['Dendrocopos', 'ramsayi'], ['Yungipicus', 'ramsayi']],
        [['Dendropicos', 'xantholophus'], ['Chloropicus', 'xantholophus']],
        [['Dendropicos', 'pyrrhogaster'], ['Chloropicus', 'pyrrhogaster']],
        [['Picoides', 'nuttallii'], ['Dryobates', 'nuttallii']],
        [['Picoides', 'borealis'], ['Leuconotopicus', 'borealis']],
        [['Picoides', 'stricklandi'], ['Leuconotopicus', 'stricklandi']],
        [['Sapheopipo', 'noguchii'], ['Dendrocopos', 'noguchii']],
        [['Synallaxis', 'whitneyi'], ['Synallaxis', 'cinerea']],

    ];

$splits =
    [
        [
            ['Corvinella', 'corvina', 'caliginosa'],
            [
                'Corvinella',
                'corvina',
                'affinis',
            ],
        ],
        [
            ['Calendulauda', 'africanoides', 'austinrobertsi'],
            [
                'Calendulauda',
                'africanoides',
                'africanoides',
            ],
        ],
        [
            ['Drepanornis', 'albertisi', 'geisleri'],
            [
                'Drepanornis',
                'albertisi',
                'albertisi',
            ],
        ],
        [
            ['Macropygia', 'amboinensis', 'albicapilla'],
            [
                'Macropygia',
                'doreya',
                'albicapilla',
            ],
        ],
        [
            ['Macropygia', 'amboinensis', 'albiceps'],
            [
                'Macropygia',
                'doreya',
                'albiceps',
            ],
        ],
        [
            ['Dumetia', 'hyperythra', 'abuensis'],
            [
                'Dumetia',
                'hyperythra',
                'albogularis',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'amamii'],
            [
                'Yungipicus',
                'kizuki',
                'amamii',
            ],
        ],
        [
            ['Melocichla', 'mentalis', 'incana'],
            [
                'Melocichla',
                'mentalis',
                'amauroura',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'amurensis'],
            [
                'Dryobates',
                'minor',
                'amurensis',
            ],
        ],
        [
            ['Dendrocopos', 'medius', 'anatoliae'],
            [
                'Dendrocoptes',
                'medius',
                'anatoliae',
            ],
        ],
        [['Periparus', 'ater', 'abietum'], ['Periparus', 'ater', 'ater']],
        [
            ['Macropygia', 'amboinensis', 'atrata'],
            [
                'Macropygia',
                'doreya',
                'atrata',
            ],
        ],
        [
            ['Picoides', 'villosus', 'audubonii'],
            [
                'Leuconotopicus',
                'villosus',
                'audubonii',
            ],
        ],
        [
            ['Paradisaea', 'raggiana', 'granti'],
            [
                'Paradisaea',
                'raggiana',
                'augustaevictoriae',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'aurantiiventris'],
            [
                'Yungipicus',
                'canicapillus',
                'aurantiiventris',
            ],
        ],
        [
            ['Dendrocopos', 'auriceps', 'auriceps'],
            [
                'Dendrocoptes',
                'auriceps',
                'auriceps',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'auritus'],
            [
                'Yungipicus',
                'canicapillus',
                'auritus',
            ],
        ],
        [['Pica', 'pica', 'hemileucoptera'], ['Pica', 'pica', 'bactriana']],
        [
            ['Macropygia', 'amboinensis', 'balim'],
            [
                'Macropygia',
                'doreya',
                'balim',
            ],
        ],
        [
            ['Egretta', 'intermedia', 'brachyrhyncha'],
            [
                'Ardea',
                'intermedia',
                'brachyrhyncha',
            ],
        ],
        [
            ['Napothera', 'brevicaudata', 'venningi'],
            [
                'Napothera',
                'brevicaudata',
                'brevicaudata',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'buturlini'],
            [
                'Dryobates',
                'minor',
                'buturlini',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'cactophilus'],
            [
                'Dryobates',
                'scalaris',
                'cactophilus',
            ],
        ],
        [
            ['Dendrocopos', 'medius', 'caucasicus'],
            [
                'Dendrocoptes',
                'medius',
                'caucasicus',
            ],
        ],
        [
            ['Iole', 'olivacea', 'perplexa'],
            [
                'Iole',
                'olivacea',
                'charlottae',
            ],
        ],
        [
            ['Apalis', 'cinerea', 'funebris'],
            [
                'Apalis',
                'cinerea',
                'cinerea',
            ],
        ],
        [
            ['Dendrocopos', 'nanus', 'cinereigula'],
            [
                'Yungipicus',
                'nanus',
                'cinereigula',
            ],
        ],
        [
            ['Ailuroedus', 'buccoides', 'cinnamomeus'],
            [
                'Ailuroedus',
                'stonii',
                'cinnamomeus',
            ],
        ],
        [
            ['Dendropicos', 'namaquus', 'coalescens'],
            [
                'Chloropicus',
                'namaquus',
                'coalescens',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'colchicus'],
            [
                'Dryobates',
                'minor',
                'colchicus',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'comminutus'],
            [
                'Dryobates',
                'minor',
                'comminutus',
            ],
        ],
        [
            ['Garrulax', 'davidi', 'experrectus'],
            [
                'Garrulax',
                'davidi',
                'concolor',
            ],
        ],
        [
            ['Macronus', 'gularis', 'connecteus'],
            [
                'Macronus',
                'gularis',
                'connectens',
            ],
        ],
        [
            ['Macronus', 'gularis', 'chersonesophilus'],
            [
                'Macronus',
                'gularis',
                'connectens',
            ],
        ],
        [
            ['Sylvia', 'curruca', 'caucasica'],
            [
                'Sylvia',
                'curruca',
                'curruca',
            ],
        ],
        [
            ['Cyanopica', 'cyanus', 'stegmanni'],
            [
                'Cyanopica',
                'cyanus',
                'cyanus',
            ],
        ],
        [
            ['Cyanopica', 'cyanus', 'koreensis'],
            [
                'Cyanopica',
                'cyanus',
                'cyanus',
            ],
        ],
        [
            ['Cyanopica', 'cyanus', 'kansuensis'],
            [
                'Cyanopica',
                'cyanus',
                'cyanus',
            ],
        ],
        [
            ['Cyanopica', 'cyanus', 'interposita'],
            [
                'Cyanopica',
                'cyanus',
                'cyanus',
            ],
        ],
        [
            ['Cyanopica', 'cyanus', 'swinhoei'],
            [
                'Cyanopica',
                'cyanus',
                'cyanus',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'danfordi'],
            [
                'Dryobates',
                'minor',
                'danfordi',
            ],
        ],
        [
            ['Garrulax', 'davidi', 'chinganicus'],
            [
                'Garrulax',
                'davidi',
                'davidi',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'delacouri'],
            [
                'Yungipicus',
                'canicapillus',
                'delacouri',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'doerriesi'],
            [
                'Yungipicus',
                'canicapillus',
                'doerriesi',
            ],
        ],
        [
            ['Macropygia', 'emiliana', 'elassa'],
            [
                'Macropygia',
                'modiglianii',
                'elassa',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'eremicus'],
            [
                'Dryobates',
                'scalaris',
                'eremicus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'extimus'],
            [
                'Leuconotopicus',
                'villosus',
                'extimus',
            ],
        ],
        [
            ['Prinia', 'flavicans', 'ortleppi'],
            [
                'Prinia',
                'flavicans',
                'flavicans',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'trobriandi'],
            [
                'Colluricincla',
                'megarhyncha',
                'fortis',
            ],
        ],
        [
            ['Picoides', 'arizonae', 'fraterculus'],
            [
                'Leuconotopicus',
                'arizonae',
                'fraterculus',
            ],
        ],
        [
            ['Dendrocopos', 'maculatus', 'fulvifasciatus'],
            [
                'Yungipicus',
                'maculatus',
                'fulvifasciatus',
            ],
        ],
        [
            ['Picoides', 'pubescens', 'fumidus'],
            [
                'Dryobates',
                'pubescens',
                'fumidus',
            ],
        ],
        [
            ['Picoides', 'pubescens', 'gairdnerii'],
            [
                'Dryobates',
                'pubescens',
                'gairdnerii',
            ],
        ],
        [
            ['Picoides', 'pubescens', 'glacialis'],
            [
                'Dryobates',
                'pubescens',
                'glacialis',
            ],
        ],
        [
            ['Acrocephalus', 'australis', 'carterae'],
            [
                'Acrocephalus',
                'australis',
                'gouldi',
            ],
        ],
        [
            ['Dendrocopos', 'moluccensis', 'grandis'],
            [
                'Yungipicus',
                'moluccensis',
                'grandis',
            ],
        ],
        [
            ['Picoides', 'albolarvatus', 'gravirostris'],
            [
                'Leuconotopicus',
                'albolarvatus',
                'gravirostris',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'graysoni'],
            [
                'Dryobates',
                'scalaris',
                'graysoni',
            ],
        ],
        [
            ['Eremomela', 'gregalis', 'damarensis'],
            [
                'Eremomela',
                'gregalis',
                'gregalis',
            ],
        ],
        [
            ['Dendrocopos', 'nanus', 'gymnopthalmos'],
            [
                'Yungipicus',
                'nanus',
                'gymnopthalmos',
            ],
        ],
        [
            ['Tchagra', 'senegalus', 'warsangliensis'],
            [
                'Tchagra',
                'senegalus',
                'habessinicus',
            ],
        ],
        [
            ['Sylvia', 'curruca', 'telengitica'],
            [
                'Sylvia',
                'curruca',
                'halimodendri',
            ],
        ],
        [
            ['Dryoscopus', 'cubla', 'chapini'],
            [
                'Dryoscopus',
                'cubla',
                'hamatus',
            ],
        ],
        [
            ['Zosterops', 'ugiensis', 'hamlini'],
            [
                'Zosterops',
                'rendovae',
                'hamlini',
            ],
        ],
        [
            ['Dendrocopos', 'nanus', 'hardwickii'],
            [
                'Yungipicus',
                'nanus',
                'hardwickii',
            ],
        ],
        [
            ['Galerida', 'magnirostris', 'montivaga'],
            [
                'Galerida',
                'magnirostris',
                'harei',
            ],
        ],
        [
            ['Picoides', 'villosus', 'harrisi'],
            [
                'Leuconotopicus',
                'villosus',
                'harrisi',
            ],
        ],
        [
            ['Arses', 'telescopthalmus', 'lauterbachi'],
            [
                'Arses',
                'telescopthalmus',
                'henkei',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'hortorum'],
            [
                'Dryobates',
                'minor',
                'hortorum',
            ],
        ],
        [
            ['Picoides', 'villosus', 'hyloscopus'],
            [
                'Leuconotopicus',
                'villosus',
                'hyloscopus',
            ],
        ],
        [
            ['Macropygia', 'emiliana', 'hypocterna'],
            [
                'Macropygia',
                'modiglianii',
                'hypocterna',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'hyrcanus'],
            [
                'Dryobates',
                'minor',
                'hyrcanus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'icastus'],
            [
                'Leuconotopicus',
                'villosus',
                'icastus',
            ],
        ],
        [
            ['Eremomela', 'icteropygialis', 'perimacha'],
            [
                'Eremomela',
                'icteropygialis',
                'icteropygialis',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'hybridus'],
            [
                'Colluricincla',
                'megarhyncha',
                'idenburgi',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'immaculatus'],
            [
                'Dryobates',
                'minor',
                'immaculatus',
            ],
        ],
        [
            ['Dendrocopos', 'auriceps', 'incognitus'],
            [
                'Dendrocoptes',
                'auriceps',
                'incognitus',
            ],
        ],
        [
            ['Dendrocopos', 'cathpharius', 'innixus'],
            [
                'Dryobates',
                'cathpharius',
                'innixus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'intermedius'],
            [
                'Leuconotopicus',
                'villosus',
                'intermedius',
            ],
        ],
        [
            ['Ammomanes', 'deserti', 'darica'],
            [
                'Ammomanes',
                'deserti',
                'iranica',
            ],
        ],
        [
            ['Ammomanes', 'deserti', 'coxi'],
            [
                'Ammomanes',
                'deserti',
                'isabellina',
            ],
        ],
        [
            ['Garrulus', 'glandarius', 'hiugaensis'],
            [
                'Garrulus',
                'glandarius',
                'japonicus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'jardinii'],
            [
                'Leuconotopicus',
                'villosus',
                'jardinii',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'kaleensis'],
            [
                'Yungipicus',
                'canicapillus',
                'kaleensis',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'kamtschatkensis'],
            [
                'Dryobates',
                'minor',
                'kamtschatkensis',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'kotataki'],
            [
                'Yungipicus',
                'kizuki',
                'kotataki',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'ledouci'],
            [
                'Dryobates',
                'minor',
                'ledouci',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'leucoptilurus'],
            [
                'Dryobates',
                'scalaris',
                'leucoptilurus',
            ],
        ],
        [
            ['Lanius', 'meridionalis', 'jebelmarrae'],
            [
                'Lanius',
                'meridionalis',
                'leucopygos',
            ],
        ],
        [
            ['Picoides', 'villosus', 'leucothorectis'],
            [
                'Leuconotopicus',
                'villosus',
                'leucothorectis',
            ],
        ],
        [
            ['Picoides', 'pubescens', 'leucurus'],
            [
                'Dryobates',
                'pubescens',
                'leucurus',
            ],
        ],
        [
            ['Macropygia', 'magna', 'longa'],
            [
                'Macropygia',
                'macassariensis',
                'longa',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'lucasanus'],
            [
                'Dryobates',
                'scalaris',
                'lucasanus',
            ],
        ],
        [
            ['Dendrocopos', 'cathpharius', 'ludlowi'],
            [
                'Dryobates',
                'cathpharius',
                'ludlowi',
            ],
        ],
        [
            ['Lanius', 'ludovicianus', 'miamensis'],
            [
                'Lanius',
                'ludovicianus',
                'ludovicianus',
            ],
        ],
        [
            ['Galerida', 'cristata', 'iwanowi'],
            [
                'Galerida',
                'cristata',
                'magna',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'matsudairai'],
            [
                'Yungipicus',
                'kizuki',
                'matsudairai',
            ],
        ],
        [
            ['Picoides', 'villosus', 'maynardi'],
            [
                'Leuconotopicus',
                'villosus',
                'maynardi',
            ],
        ],
        [
            ['Picoides', 'pubescens', 'medianus'],
            [
                'Dryobates',
                'pubescens',
                'medianus',
            ],
        ],
        [
            ['Dendrocopos', 'medius', 'medius'],
            [
                'Dendrocoptes',
                'medius',
                'medius',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'aruensis'],
            [
                'Colluricincla',
                'megarhyncha',
                'megarhyncha',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'misoliensis'],
            [
                'Colluricincla',
                'megarhyncha',
                'megarhyncha',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'ferruginea'],
            [
                'Colluricincla',
                'megarhyncha',
                'megarhyncha',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'goodsoni'],
            [
                'Colluricincla',
                'megarhyncha',
                'megarhyncha',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'palmeri'],
            [
                'Colluricincla',
                'megarhyncha',
                'megarhyncha',
            ],
        ],
        [
            ['Manorina', 'melanocephala', 'lepidota'],
            [
                'Manorina',
                'melanocephala',
                'melanocephala',
            ],
        ],
        [
            ['Sylvia', 'melanocephala', 'pasiphae'],
            [
                'Sylvia',
                'melanocephala',
                'melanocephala',
            ],
        ],
        [
            ['Turdoides', 'melanops', 'angolensis'],
            [
                'Turdoides',
                'melanops',
                'melanops',
            ],
        ],
        [
            ['Zosterops', 'montanus', 'gilli'],
            [
                'Zosterops',
                'meyeni',
                'meyeni',
            ],
        ],
        [['Parus', 'minor', 'kagoshimae'], ['Parus', 'minor', 'minor']],
        [['Sylvia', 'minula', 'jaxartica'], ['Sylvia', 'minula', 'minula']],
        [
            ['Napothera', 'epilepidota', 'mendeni'],
            [
                'Napothera',
                'epilepidota',
                'minuta',
            ],
        ],
        [
            ['Ailuroedus', 'melanotis', 'misoliensis'],
            [
                'Ailuroedus',
                'arfakianus',
                'misoliensis',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'mitchellii'],
            [
                'Yungipicus',
                'canicapillus',
                'mitchellii',
            ],
        ],
        [
            ['Picoides', 'villosus', 'monticola'],
            [
                'Leuconotopicus',
                'villosus',
                'monticola',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'morgani'],
            [
                'Dryobates',
                'minor',
                'morgani',
            ],
        ],
        [
            ['Terpsiphone', 'mutata', 'singetra'],
            [
                'Terpsiphone',
                'mutata',
                'mutata',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'madaraszi'],
            [
                'Colluricincla',
                'megarhyncha',
                'neos',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'nigrescens'],
            [
                'Yungipicus',
                'kizuki',
                'nigrescens',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'nippon'],
            [
                'Yungipicus',
                'kizuki',
                'nippon',
            ],
        ],
        [
            ['Gerygone', 'chrysogaster', 'leucothorax'],
            [
                'Gerygone',
                'chrysogaster',
                'notata',
            ],
        ],
        [
            ['Gerygone', 'chrysogaster', 'dohertyi'],
            [
                'Gerygone',
                'chrysogaster',
                'notata',
            ],
        ],
        [
            ['Philemon', 'novaeguineae', 'brevipennis'],
            [
                'Philemon',
                'novaeguineae',
                'novaeguineae',
            ],
        ],
        [
            ['Prosthemadera', 'novaeseelandiae', 'kermadecensis'],
            [
                'Prosthemadera',
                'novaeseelandiae',
                'novaeseelandiae',
            ],
        ],
        [
            ['Zosterops', 'ugiensis', 'oblitus'],
            [
                'Zosterops',
                'rendovae',
                'oblitus',
            ],
        ],
        [
            ['Picoides', 'fumigatus', 'obscuratus'],
            [
                'Leuconotopicus',
                'fumigatus',
                'obscuratus',
            ],
        ],
        [
            ['Picoides', 'fumigatus', 'oleagineus'],
            [
                'Leuconotopicus',
                'fumigatus',
                'oleagineus',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'orii'],
            [
                'Yungipicus',
                'kizuki',
                'orii',
            ],
        ],
        [
            ['Picoides', 'villosus', 'orius'],
            [
                'Leuconotopicus',
                'villosus',
                'orius',
            ],
        ],
        [
            ['Pachycephala', 'vitiensis', 'ornata'],
            [
                'Pachycephala',
                'vanikorensis',
                'ornata',
            ],
        ],
        [
            ['Dendrocopos', 'mahrattensis', 'pallescens'],
            [
                'Leiopicus',
                'mahrattensis',
                'pallescens',
            ],
        ],
        [
            ['Coracina', 'papuensis', 'rothschildi'],
            [
                'Coracina',
                'papuensis',
                'papuensis',
            ],
        ],
        [
            ['Zosterops', 'kulambangrae', 'paradoxus'],
            [
                'Zosterops',
                'tetiparius',
                'paradoxus',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'parvus'],
            [
                'Dryobates',
                'scalaris',
                'parvus',
            ],
        ],
        [
            ['Garrulax', 'pectoralis', 'melanotis'],
            [
                'Garrulax',
                'pectoralis',
                'pectoralis',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'ripleyi'],
            [
                'Crateroscelis',
                'robusta',
                'peninsularis',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'permutatus'],
            [
                'Yungipicus',
                'kizuki',
                'permutatus',
            ],
        ],
        [
            ['Dendrocopos', 'cathpharius', 'pernyii'],
            [
                'Dryobates',
                'cathpharius',
                'pernyii',
            ],
        ],
        [
            ['Periparus', 'ater', 'gaddi'],
            [
                'Periparus',
                'ater',
                'phaeonotus',
            ],
        ],
        [
            ['Periparus', 'ater', 'chorassanicus'],
            [
                'Periparus',
                'ater',
                'phaeonotus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'picoideus'],
            [
                'Leuconotopicus',
                'villosus',
                'picoideus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'piger'],
            [
                'Leuconotopicus',
                'villosus',
                'piger',
            ],
        ],
        [
            ['Egretta', 'intermedia', 'plumifera'],
            [
                'Ardea',
                'intermedia',
                'plumifera',
            ],
        ],
        [
            ['Malacocincla', 'malaccensis', 'sordida'],
            [
                'Malacocincla',
                'malaccensis',
                'poliogenis',
            ],
        ],
        [
            ['Malacocincla', 'malaccensis', 'feriata'],
            [
                'Malacocincla',
                'malaccensis',
                'poliogenis',
            ],
        ],
        [
            ['Pycnopygius', 'ixoides', 'simplex'],
            [
                'Pycnopygius',
                'ixoides',
                'proximus',
            ],
        ],
        [
            ['Dendrocopos', 'cathpharius', 'pyrrhothorax'],
            [
                'Dryobates',
                'cathpharius',
                'pyrrhothorax',
            ],
        ],
        [
            ['Dendrocopos', 'minor', 'quadrifasciatus'],
            [
                'Dryobates',
                'minor',
                'quadrifasciatus',
            ],
        ],
        [
            ['Picoides', 'fumigatus', 'reichenbachi'],
            [
                'Leuconotopicus',
                'fumigatus',
                'reichenbachi',
            ],
        ],
        [
            ['Myzomela', 'rosenbergii', 'wahgiensis'],
            [
                'Myzomela',
                'rosenbergii',
                'rosenbergii',
            ],
        ],
        [
            ['Trichastoma', 'celebense', 'connectens'],
            [
                'Trichastoma',
                'celebense',
                'rufofuscum',
            ],
        ],
        [
            ['Dendrocopos', 'medius', 'sanctijohannis'],
            [
                'Dendrocoptes',
                'medius',
                'sanctijohannis',
            ],
        ],
        [
            ['Picoides', 'villosus', 'sanctorum'],
            [
                'Leuconotopicus',
                'villosus',
                'sanctorum',
            ],
        ],
        [
            ['Macropygia', 'amboinensis', 'sanghirensis'],
            [
                'Macropygia',
                'doreya',
                'sanghirensis',
            ],
        ],
        [
            ['Picoides', 'fumigatus', 'sanguinolentus'],
            [
                'Leuconotopicus',
                'fumigatus',
                'sanguinolentus',
            ],
        ],
        [
            ['Dendropicos', 'namaquus', 'schoensis'],
            [
                'Chloropicus',
                'namaquus',
                'schoensis',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'scintilliceps'],
            [
                'Yungipicus',
                'canicapillus',
                'scintilliceps',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'seebohmi'],
            [
                'Yungipicus',
                'kizuki',
                'seebohmi',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'semicoronatus'],
            [
                'Yungipicus',
                'canicapillus',
                'semicoronatus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'septentrionalis'],
            [
                'Leuconotopicus',
                'villosus',
                'septentrionalis',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'shikokuensis'],
            [
                'Yungipicus',
                'kizuki',
                'shikokuensis',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'sinaloensis'],
            [
                'Dryobates',
                'scalaris',
                'sinaloensis',
            ],
        ],
        [
            ['Gavicalis', 'versicolor', 'vulgaris'],
            [
                'Gavicalis',
                'versicolor',
                'sonoroides',
            ],
        ],
        [
            ['Gavicalis', 'versicolor', 'intermedius'],
            [
                'Gavicalis',
                'versicolor',
                'sonoroides',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'soulei'],
            [
                'Dryobates',
                'scalaris',
                'soulei',
            ],
        ],
        [
            ['Pomatorhinus', 'ferruginosus', 'namdapha'],
            [
                'Pomatorhinus',
                'ferruginosus',
                'stanfordi',
            ],
        ],
        [
            ['Locustella', 'naevia', 'mongolica'],
            [
                'Locustella',
                'naevia',
                'straminea',
            ],
        ],
        [
            ['Turdoides', 'striata', 'orissae'],
            [
                'Turdoides',
                'striata',
                'striata',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'swinhoei'],
            [
                'Yungipicus',
                'canicapillus',
                'swinhoei',
            ],
        ],
        [
            ['Pomatostomus', 'temporalis', 'strepitans'],
            [
                'Pomatostomus',
                'temporalis',
                'temporalis',
            ],
        ],
        [
            ['Dendrocopos', 'cathpharius', 'tenebrosus'],
            [
                'Dryobates',
                'cathpharius',
                'tenebrosus',
            ],
        ],
        [
            ['Picoides', 'villosus', 'terraenovae'],
            [
                'Leuconotopicus',
                'villosus',
                'terraenovae',
            ],
        ],
        [
            ['Gymnorhina', 'tibicen', 'terraereginae'],
            [
                'Gymnorhina',
                'tibicen',
                'tibicen',
            ],
        ],
        [
            ['Picoides', 'pubescens', 'turati'],
            [
                'Dryobates',
                'pubescens',
                'turati',
            ],
        ],
        [
            ['Pitohui', 'uropygialis', 'tibialis'],
            [
                'Pitohui',
                'uropygialis',
                'uropygialis',
            ],
        ],
        [
            ['Pachycephala', 'vitiensis', 'utupae'],
            [
                'Pachycephala',
                'vanikorensis',
                'utupae',
            ],
        ],
        [
            ['Dendrocopos', 'maculatus', 'validirostris'],
            [
                'Yungipicus',
                'maculatus',
                'validirostris',
            ],
        ],
        [
            ['Zosterops', 'lateralis', 'macmillani'],
            [
                'Zosterops',
                'lateralis',
                'vatensis',
            ],
        ],
        [
            ['Pericrocotus', 'cinnamomeus', 'separatus'],
            [
                'Pericrocotus',
                'cinnamomeus',
                'vividus',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'volzi'],
            [
                'Yungipicus',
                'canicapillus',
                'volzi',
            ],
        ],
        [
            ['Horornis', 'flavolivaceus', 'alexanderi'],
            [
                'Horornis',
                'flavolivaceus',
                'weberi',
            ],
        ],
        [
            ['Mirafra', 'javanica', 'beaulieui'],
            [
                'Mirafra',
                'javanica',
                'williamsoni',
            ],
        ],
        [
            ['Aphelocoma', 'wollweberi', 'gracilis'],
            [
                'Aphelocoma',
                'wollweberi',
                'wollweberi',
            ],
        ],
        [
            ['Bleda', 'syndactylus', 'nandensis'],
            [
                'Bleda',
                'syndactylus',
                'woosnami',
            ],
        ],
        [
            ['Minla', 'strigula', 'cinereigenae'],
            [
                'Minla',
                'strigula',
                'yunnanensis',
            ],
        ],
        [
            ['Macropygia', 'amboinensis', 'amboinensis'],
            [
                'Macropygia',
                'amboinensis',
                'amboinensis',
            ],
        ],
        [
            ['Macropygia', 'amboinensis', 'doreya'],
            [
                'Macropygia',
                'doreya',
                'doreya',
            ],
        ],
        [
            ['Macropygia', 'emiliana', 'cinnamomea'],
            [
                'Macropygia',
                'cinnamomea',
                null,
            ],
        ],
        [
            ['Macropygia', 'emiliana', 'modiglianii'],
            [
                'Macropygia',
                'modiglianii',
                'modiglianii',
            ],
        ],
        [
            ['Macropygia', 'magna', 'timorlaoensis'],
            [
                'Macropygia',
                'timorlaoensis',
                null,
            ],
        ],
        [['Macropygia', 'magna', 'magna'], ['Macropygia', 'magna', null]],
        [
            ['Macropygia', 'magna', 'macassariensis'],
            [
                'Macropygia',
                'macassariensis',
                'macassariensis',
            ],
        ],
        [
            ['Ptilinopus', 'porphyraceus', 'ponapensis'],
            [
                'Ptilinopus',
                'ponapensis',
                null,
            ],
        ],
        [
            ['Ptilinopus', 'porphyraceus', 'hernsheimi'],
            [
                'Ptilinopus',
                'hernsheimi',
                null,
            ],
        ],
        [
            ['Psilopogon', 'asiaticus', 'chersonesus'],
            [
                'Psilopogon',
                'chersonesus',
                null,
            ],
        ],
        [
            ['Psilopogon', 'franklinii', 'auricularis'],
            [
                'Psilopogon',
                'auricularis',
                null,
            ],
        ],
        [
            ['Elaenia', 'chiriquensis', 'brachyptera'],
            [
                'Elaenia',
                'brachyptera',
                null,
            ],
        ],
        [
            ['Ailuroedus', 'buccoides', 'geislerorum'],
            [
                'Ailuroedus',
                'geislerorum',
                'geislerorum',
            ],
        ],
        [
            ['Ailuroedus', 'buccoides', 'stonii'],
            [
                'Ailuroedus',
                'stonii',
                'stonii',
            ],
        ],
        [
            ['Ailuroedus', 'melanotis', 'maculosus'],
            [
                'Ailuroedus',
                'maculosus',
                null,
            ],
        ],
        [
            ['Ailuroedus', 'melanotis', 'melanocephalus'],
            [
                'Ailuroedus',
                'melanocephalus',
                null,
            ],
        ],
        [
            ['Ailuroedus', 'melanotis', 'astigmaticus'],
            [
                'Ailuroedus',
                'astigmaticus',
                null,
            ],
        ],
        [
            ['Ailuroedus', 'melanotis', 'arfakianus'],
            [
                'Ailuroedus',
                'arfakianus',
                'arfakianus',
            ],
        ],
        [
            ['Ailuroedus', 'melanotis', 'jobiensis'],
            [
                'Ailuroedus',
                'jobiensis',
                null,
            ],
        ],
        [
            ['Pachycephala', 'chlorura', 'vanikorensis'],
            [
                'Pachycephala',
                'vanikorensis',
                'vanikorensis',
            ],
        ],
        [
            ['Zosterops', 'cinereus', 'ponapensis'],
            [
                'Zosterops',
                'ponapensis',
                null,
            ],
        ],
        [
            ['Zosterops', 'cinereus', 'cinereus'],
            [
                'Zosterops',
                'cinereus',
                null,
            ],
        ],
        [
            ['Zosterops', 'kulambangrae', 'kulambangrae'],
            [
                'Zosterops',
                'kulambangrae',
                null,
            ],
        ],
        [
            ['Zosterops', 'kulambangrae', 'tetiparius'],
            [
                'Zosterops',
                'tetiparius',
                'tetiparius',
            ],
        ],
        [
            ['Zosterops', 'senegalensis', 'stenocricotus'],
            [
                'Zosterops',
                'stenocricotus',
                null,
            ],
        ],
        [
            ['Myzomela', 'cineracea', 'cineracea'],
            [
                'Myzomela',
                'cineracea',
                null,
            ],
        ],
        [
            ['Ptiloprora', 'perstriata', 'perstriata'],
            [
                'Ptiloprora',
                'perstriata',
                null,
            ],
        ],
        [
            ['Meliphaga', 'mimikae', 'mimikae'],
            [
                'Meliphaga',
                'mimikae',
                null,
            ],
        ],
        [
            ['Meliphaga', 'cinereifrons', 'cinereifrons'],
            [
                'Meliphaga',
                'cinereifrons',
                null,
            ],
        ],
        [['Batis', 'mixta', 'mixta'], ['Batis', 'mixta', null]],
        [['Lanius', 'collurio', 'collurio'], ['Lanius', 'collurio', null]],
        [
            ['Vireolanius', 'eximius', 'eximius'],
            [
                'Vireolanius',
                'eximius',
                null,
            ],
        ],
        [
            ['Pteruthius', 'rufiventer', 'rufiventer'],
            [
                'Pteruthius',
                'rufiventer',
                null,
            ],
        ],
        [
            ['Pteridophora', 'alberti', 'alberti'],
            [
                'Pteridophora',
                'alberti',
                null,
            ],
        ],
        [
            ['Mirafra', 'erythroptera', 'erythroptera'],
            [
                'Mirafra',
                'erythroptera',
                null,
            ],
        ],
        [
            ['Melanocorypha', 'bimaculata', 'bimaculata'],
            [
                'Melanocorypha',
                'bimaculata',
                null,
            ],
        ],
        [
            ['Melanocorypha', 'maxima', 'maxima'],
            [
                'Melanocorypha',
                'maxima',
                null,
            ],
        ],
        [
            ['Euryptila', 'subcinnamomea', 'subcinnamomea'],
            [
                'Euryptila',
                'subcinnamomea',
                null,
            ],
        ],
        [
            ['Alcippe', 'brunneicauda', 'brunneicauda'],
            [
                'Alcippe',
                'brunneicauda',
                null,
            ],
        ],
        [
            ['Illadopsis', 'albipectus', 'albipectus'],
            [
                'Illadopsis',
                'albipectus',
                null,
            ],
        ],
        [['Garrulax', 'maesi', 'maesi'], ['Garrulax', 'maesi', null]],
        [
            ['Garrulax', 'berthemyi', 'berthemyi'],
            [
                'Garrulax',
                'berthemyi',
                null,
            ],
        ],
        [
            ['Trochalopteron', 'elliotii ', 'elliotii '],
            [
                'Trochalopteron',
                'elliotii ',
                null,
            ],
        ],
        [
            ['Trochalopteron', 'henrici ', 'henrici '],
            [
                'Trochalopteron',
                'henrici ',
                null,
            ],
        ],
        [['Sylvia', 'althaea', 'althaea'], ['Sylvia', 'althaea', null]],
        [
            ['Yuhina', 'nigrimenta', 'nigrimenta'],
            [
                'Yuhina',
                'nigrimenta',
                null,
            ],
        ],
        [
            ['Egretta', 'intermedia', 'intermedia'],
            [
                'Ardea',
                'intermedia',
                'intermedia',
            ],
        ],
        [
            ['Dendrocopos', 'nanus', 'nanus'],
            [
                'Yungipicus',
                'nanus',
                'nanus',
            ],
        ],
        [
            ['Dendrocopos', 'canicapillus', 'canicapillus'],
            [
                'Yungipicus',
                'canicapillus',
                'canicapillus',
            ],
        ],
        [
            ['Dendrocopos', 'maculatus', 'maculatus'],
            [
                'Yungipicus',
                'maculatus',
                'maculatus',
            ],
        ],
        [
            ['Dendrocopos', 'moluccensis', 'moluccensis'],
            [
                'Yungipicus',
                'moluccensis',
                'moluccensis',
            ],
        ],
        [
            ['Dendrocopos', 'kizuki', 'kizuki'],
            [
                'Yungipicus',
                'kizuki',
                'kizuki',
            ],
        ],
        [
            ['Dendrocopos', 'mahrattensis', 'mahrattensis'],
            [
                'Leiopicus',
                'mahrattensis',
                'mahrattensis',
            ],
        ],
        [
            ['Dendropicos', 'namaquus', 'namaquus'],
            [
                'Chloropicus',
                'namaquus',
                'namaquus',
            ],
        ],
        [
            ['Picoides', 'scalaris', 'scalaris'],
            [
                'Dryobates',
                'scalaris',
                'scalaris',
            ],
        ],
        [
            ['Picoides', 'pubescens', 'pubescens'],
            [
                'Dryobates',
                'pubescens',
                'pubescens',
            ],
        ],
        [
            ['Dendrocopos', 'cathpharius', 'cathpharius'],
            [
                'Dryobates',
                'cathpharius',
                'cathpharius',
            ],
        ],
        [['Dendrocopos', 'minor', 'minor'], ['Dryobates', 'minor', 'minor']],
        [
            ['Picoides', 'fumigatus', 'fumigatus'],
            [
                'Leuconotopicus',
                'fumigatus',
                'fumigatus',
            ],
        ],
        [
            ['Picoides', 'arizonae', 'arizonae'],
            [
                'Leuconotopicus',
                'arizonae',
                'arizonae',
            ],
        ],
        [
            ['Picoides', 'villosus', 'villosus'],
            [
                'Leuconotopicus',
                'villosus',
                'villosus',
            ],
        ],
        [
            ['Picoides', 'albolarvatus', 'albolarvatus'],
            [
                'Leuconotopicus',
                'albolarvatus',
                'albolarvatus',
            ],
        ],
        [
            ['Zosterops ', 'ugiensis', 'ugiensis'],
            [
                'Zosterops',
                'rendovae',
                'rendovae',
            ],
        ],
    ];
$lumps  =
    [

        [
            ['Ailuroedus', 'melanotis', 'guttaticollis'],
            [
                'Ailuroedus',
                'jobiensis',
                null,
            ],
        ]
        ,
        [
            ['Myzomela', 'cineracea', 'rooki'],
            [
                'Myzomela',
                'cineracea',
                null,
            ],
        ]
        ,
        [
            ['Ptiloprora', 'perstriata', 'incerta'],
            [
                'Ptiloprora',
                'perstriata',
                null,
            ],
        ]
        ,
        [
            ['Ptiloprora', 'perstriata', 'praedicta'],
            [
                'Ptiloprora',
                'perstriata',
                null,
            ],
        ]
        ,
        [['Meliphaga', 'mimikae', 'rara'], ['Meliphaga', 'mimikae', null]]
        ,
        [
            ['Meliphaga', 'mimikae', 'bastille'],
            [
                'Meliphaga',
                'mimikae',
                null,
            ],
        ]
        ,
        [['Meliphaga', 'mimikae', 'granti'], ['Meliphaga', 'mimikae', null]]
        ,
        [
            ['Meliphaga', 'cinereifrons', 'stevensi'],
            [
                'Meliphaga',
                'cinereifrons',
                null,
            ],
        ]
        ,
        [['Batis', 'mixta', 'ultima'], ['Batis', 'mixta', null]]
        ,
        [['Lanius', 'collurio', 'kobylini'], ['Lanius', 'collurio', null]]
        ,
        [
            ['Vireolanius', 'eximius', 'mutabilis'],
            [
                'Vireolanius',
                'eximius',
                null,
            ],
        ]
        ,
        [
            ['Pteruthius', 'rufiventer', 'delacouri'],
            [
                'Pteruthius',
                'rufiventer',
                null,
            ],
        ]
        ,
        [
            ['Pteridophora', 'alberti', 'buergersi'],
            [
                'Pteridophora',
                'alberti',
                null,
            ],
        ]
        ,
        [['Remiz', 'macronyx', 'altaicus'], ['Remiz', 'macronyx', null]]
        ,
        [
            ['Mirafra', 'erythroptera', 'sindiana'],
            [
                'Mirafra',
                'erythroptera',
                null,
            ],
        ]
        ,
        [
            ['Melanocorypha', 'bimaculata', 'rufescens'],
            [
                'Melanocorypha',
                'bimaculata',
                null,
            ],
        ]
        ,
        [
            ['Melanocorypha', 'bimaculata', 'torquata'],
            [
                'Melanocorypha',
                'bimaculata',
                null,
            ],
        ]
        ,
        [
            ['Melanocorypha', 'maxima', 'flavescens'],
            [
                'Melanocorypha',
                'maxima',
                null,
            ],
        ]
        ,
        [
            ['Melanocorypha', 'maxima', 'holdereri'],
            [
                'Melanocorypha',
                'maxima',
                null,
            ],
        ]
        ,
        [
            ['Euryptila', 'subcinnamomea', 'petrophila'],
            [
                'Euryptila',
                'subcinnamomea',
                null,
            ],
        ]
        ,
        [
            ['Alcippe', 'brunneicauda', 'eriphaea'],
            [
                'Alcippe',
                'brunneicauda',
                null,
            ],
        ]
        ,
        [
            ['Illadopsis', 'albipectus', 'barakae'],
            [
                'Illadopsis',
                'albipectus',
                null,
            ],
        ]
        ,
        [
            ['Illadopsis', 'albipectus', 'trensei'],
            [
                'Illadopsis',
                'albipectus',
                null,
            ],
        ]
        ,
        [['Garrulax', 'maesi', 'grahami'], ['Garrulax', 'maesi', null]]
        ,
        [
            ['Garrulax', 'pectoralis', 'pingi'],
            [
                'Garrulax',
                'pectoralis',
                null,
            ],
        ]
        ,
        [
            ['Garrulax', 'berthemyi', 'ricinus'],
            [
                'Garrulax',
                'berthemyi',
                null,
            ],
        ]
        ,
        [
            ['Trochalopteron', 'elliotii ', 'prjevalskii'],
            [
                'Trochalopteron',
                'elliotii ',
                null,
            ],
        ]
        ,
        [
            ['Trochalopteron', 'henrici ', 'gucenense'],
            [
                'Trochalopteron',
                'henrici ',
                null,
            ],
        ]
        ,
        [['Sylvia', 'althaea', 'monticola'], ['Sylvia', 'althaea', null]]
        ,
        [
            ['Sylvia', 'deserticola', 'ticehursti'],
            [
                'Sylvia',
                'deserticola',
                null,
            ],
        ]
        ,
        [
            ['Yuhina', 'nigrimenta', 'intermedia'],
            [
                'Yuhina',
                'nigrimenta',
                null,
            ],
        ]
        ,
        [
            ['Yuhina', 'nigrimenta', 'pallida'],
            [
                'Yuhina',
                'nigrimenta',
                null,
            ],
        ],
    ];
