<?php

$renames =
    [
        [['Cracticus', 'quoyi'], ['Melloria', 'quoyi']],
        [['Sylvia', 'subcaerulea'], ['Sylvia', 'subcoerulea']],
        [['Cholornis', 'paradoxa'], ['Cholornis', 'paradoxus']],
        [['Salpornis', 'spilonotus'], ['Salpornis', 'spilonota']],
        [['Saroglossa', 'spiloptera'], ['Saroglossa', 'spilopterus']],
        [['Luscinia', 'phoenicuroides'], ['Luscinia', 'phaenicuroides']],
        [['Monticola', 'cinclorhynchus'], ['Monticola', 'cinclorhyncha']],
        [['Muscicapa', 'ruficauda'], ['Ficedula', 'ruficauda']],
        [['Saltator', 'rufiventris'], ['Pseudosaltator', 'rufiventris']],
    ];
$splits  =
    [
        [
            ['Chlorospingus', 'flavopectus', 'persimilis'],
            [
                'Chlorospingus',
                'flavopectus',
                'albifrons',
            ],
        ],
        [
            ['Crithagra', 'albogularis', 'hewitti'],
            [
                'Crithagra',
                'albogularis',
                'albogularis',
            ],
        ],
        [
            ['Artemisiospiza', 'belli', 'clementeae'],
            [
                'Artemisiospiza',
                'belli',
                'belli',
            ],
        ],
        [
            ['Peucaea', 'botterii', 'mexicana'],
            [
                'Peucaea',
                'botterii',
                'botterii',
            ],
        ],
        [
            ['Atlapetes', 'albinucha', 'coloratus'],
            [
                'Atlapetes',
                'albinucha',
                'brunnescens',
            ],
        ],
        [
            ['Aramides', 'cajaneus', 'latens'],
            [
                'Aramides',
                'cajaneus',
                'cajaneus',
            ],
        ],
        [
            ['Aramides', 'cajaneus', 'morrisoni'],
            [
                'Aramides',
                'cajaneus',
                'cajaneus',
            ],
        ],
        [
            ['Piculus', 'chrysochloros', 'guianensis'],
            [
                'Piculus',
                'chrysochloros',
                'capistratus',
            ],
        ],
        [
            ['Fringilla', 'coelebs', 'balearica'],
            [
                'Fringilla',
                'coelebs',
                'coelebs',
            ],
        ],
        [
            ['Fringilla', 'coelebs', 'tyrrhenica'],
            [
                'Fringilla',
                'coelebs',
                'coelebs',
            ],
        ],
        [
            ['Fringilla', 'coelebs', 'schiebeli'],
            [
                'Fringilla',
                'coelebs',
                'coelebs',
            ],
        ],
        [
            ['Fringilla', 'coelebs', 'caucasica'],
            [
                'Fringilla',
                'coelebs',
                'coelebs',
            ],
        ],
        [
            ['Zonotrichia', 'capensis', 'orestera'],
            [
                'Zonotrichia',
                'capensis',
                'costaricensis',
            ],
        ],
        [
            ['Melozone', 'crissalis', 'bullata'],
            [
                'Melozone',
                'crissalis',
                'crissalis',
            ],
        ],
        [
            ['Melozone', 'crissalis', 'carolae'],
            [
                'Melozone',
                'crissalis',
                'crissalis',
            ],
        ],
        [
            ['Melozone', 'crissalis', 'eremophila'],
            [
                'Melozone',
                'crissalis',
                'crissalis',
            ],
        ],
        [
            ['Haemorhous', 'mexicanus', 'clementis'],
            [
                'Haemorhous',
                'mexicanus',
                'frontalis',
            ],
        ],
        [
            ['Icterus', 'chrysater', 'hondae'],
            [
                'Icterus',
                'chrysater',
                'giraudii',
            ],
        ],
        [['Icterus', 'gularis', 'gigas'], ['Icterus', 'gularis', 'gularis']],
        [
            ['Leucosticte', 'brandti', 'audreyana'],
            [
                'Leucosticte',
                'brandti',
                'haematopygia',
            ],
        ],
        [
            ['Emberiza', 'impetuani', 'eremica'],
            [
                'Emberiza',
                'impetuani',
                'impetuani',
            ],
        ],
        [
            ['Setophaga', 'pitiayumi', 'speciosa'],
            [
                'Setophaga',
                'pitiayumi',
                'inornata',
            ],
        ],
        [
            ['Setophaga', 'pitiayumi', 'nana'],
            [
                'Setophaga',
                'pitiayumi',
                'inornata',
            ],
        ],
        [
            ['Piculus', 'chrysochloros', 'hypochryseus'],
            [
                'Piculus',
                'chrysochloros',
                'laemostictus',
            ],
        ],
        [
            ['Pinicola', 'enucleator', 'alascensis'],
            [
                'Pinicola',
                'enucleator',
                'leucura',
            ],
        ],
        [
            ['Pinicola', 'enucleator', 'eschatosa'],
            [
                'Pinicola',
                'enucleator',
                'leucura',
            ],
        ],
        [
            ['Ammodramus', 'maritimus', 'pelonotus'],
            [
                'Ammodramus',
                'maritimus',
                'macgillivraii',
            ],
        ],
        [
            ['Orthotomus', 'atrogularis', 'anambensis'],
            [
                'Orthotomus',
                'atrogularis',
                'major',
            ],
        ],
        [
            ['Turdus', 'flavipes', 'melanopleurus'],
            [
                'Turdus',
                'flavipes',
                'melanopleura',
            ],
        ],
        [
            ['Icterus', 'gularis', 'tamaulipensis'],
            [
                'Icterus',
                'gularis',
                'mentalis',
            ],
        ],
        [
            ['Icterus', 'gularis', 'yucatanensis'],
            [
                'Icterus',
                'gularis',
                'mentalis',
            ],
        ],
        [
            ['Icterus', 'gularis', 'troglodytes'],
            [
                'Icterus',
                'gularis',
                'mentalis',
            ],
        ],
        [
            ['Turdus', 'abyssinicus', 'milanjensis'],
            [
                'Turdus',
                'olivaceus',
                'milanjensis',
            ],
        ],
        [
            ['Linaria', 'flavirostris', 'pamirensis'],
            [
                'Linaria',
                'flavirostris',
                'montanella',
            ],
        ],
        [
            ['Pseudocolaptes', 'boissonneautii', 'orientalis'],
            [
                'Pseudocolaptes',
                'boissonneautii',
                'oberholseri',
            ],
        ],
        [
            ['Nyctanassa', 'violacea', 'paupera'],
            [
                'Nyctanassa',
                'violacea',
                'paupera',
            ],
        ],
        [
            ['Pomatorhinus', 'ferruginosus', 'formosus'],
            [
                'Pomatorhinus',
                'ferruginosus',
                'phayrei',
            ],
        ],
        [
            ['Asthenes', 'wyatti', 'perijana'],
            [
                'Asthenes',
                'wyatti',
                'phelpsi',
            ],
        ],
        [
            ['Linaria', 'flavirostris', 'bensonorum'],
            [
                'Linaria',
                'flavirostris',
                'pipilans',
            ],
        ],
        [
            ['Setophaga', 'pitiayumi', 'elegans'],
            [
                'Setophaga',
                'pitiayumi',
                'pitiayumi',
            ],
        ],
        [
            ['Setophaga', 'pitiayumi', 'roraimae'],
            [
                'Setophaga',
                'pitiayumi',
                'pitiayumi',
            ],
        ],
        [
            ['Setophaga', 'pitiayumi', 'melanogenys'],
            [
                'Setophaga',
                'pitiayumi',
                'pitiayumi',
            ],
        ],
        [
            ['Geothlypis', 'poliocephala', 'pontilis'],
            [
                'Geothlypis',
                'poliocephala',
                'poliocephala',
            ],
        ],
        [
            ['Petrochelidon', 'pyrrhonota', 'hypopolia'],
            [
                'Petrochelidon',
                'pyrrhonota',
                'pyrrhonota',
            ],
        ],
        [
            ['Pyrrhula', 'pyrrhula', 'paphlagoniae'],
            [
                'Pyrrhula',
                'pyrrhula',
                'pyrrhula',
            ],
        ],
        [
            ['Agelaius', 'phoeniceus', 'brevirostris'],
            [
                'Agelaius',
                'phoeniceus',
                'richmondi',
            ],
        ],
        [
            ['Oenanthe', 'leucura', 'syenitica'],
            [
                'Oenanthe',
                'leucura',
                'riggenbachi',
            ],
        ],
        [
            ['Peucaea', 'ruficauda', 'ibarrorum'],
            [
                'Peucaea',
                'ruficauda',
                'ruficauda',
            ],
        ],
        [
            ['Arremonops', 'rufivirgatus', 'ridgwayi'],
            [
                'Arremonops',
                'rufivirgatus',
                'rufivirgatus',
            ],
        ],
        [
            ['Melospiza', 'melodia', 'samuelis'],
            [
                'Melospiza',
                'melodia',
                'samuelsis',
            ],
        ],
        [
            ['Sporophila', 'schistacea', 'subconcolor'],
            [
                'Sporophila',
                'schistacea',
                'schistacea',
            ],
        ],
        [
            ['Aimophila', 'ruficeps', 'rupicola'],
            [
                'Aimophila',
                'ruficeps',
                'scotti',
            ],
        ],
        [
            ['Coccothraustes', 'coccothraustes', 'schulpini'],
            [
                'Coccothraustes',
                'coccothraustes',
                'shulpini',
            ],
        ],
        [
            ['Atlapetes', 'seebohmi', 'celicae'],
            [
                'Atlapetes',
                'seebohmi',
                'simonsi',
            ],
        ],
        [
            ['Gracupica', 'contra', 'sordidus'],
            [
                'Gracupica',
                'contra',
                'sordida',
            ],
        ],
        [
            ['Spinus', 'spinescens', 'capitaneus'],
            [
                'Spinus',
                'spinescens',
                'spinescens',
            ],
        ],
        [
            ['Atlapetes', 'latinuchus', 'simplex'],
            [
                'Atlapetes',
                'latinuchus',
                'spodionotus',
            ],
        ],
        [
            ['Arremonops', 'conirostris', 'richmondi'],
            [
                'Arremonops',
                'conirostris',
                'striaticeps',
            ],
        ],
        [
            ['Emberiza', 'tahapisi', 'insularis'],
            [
                'Emberiza',
                'tahapisi',
                'tahapisi',
            ],
        ],
        [
            ['Leucosticte', 'tephrocotis', 'irvingi'],
            [
                'Leucosticte',
                'tephrocotis',
                'tephrocotis',
            ],
        ],
        [
            ['Sicalis', 'uropigyalis', 'connectens'],
            [
                'Sicalis',
                'uropigyalis',
                'uropigyalis',
            ],
        ],
        [
            ['Myiagra', 'azureocapilla', 'whitneyi'],
            [
                'Myiagra',
                'castaneigularis',
                'whitneyi',
            ],
        ],
        [
            ['Piculus', 'chrysochloros', 'aurosus'],
            [
                'Piculus',
                'chrysochloros',
                'xanthochlorus',
            ],
        ],
        [
            ['Aulacorhynchus', 'cognatus', ''],
            [
                'Aulacorhynchus',
                'caeruleogularis',
                'cognatus',
            ],
        ],
        [
            ['Aulacorhynchus', 'caeruleogularis', ''],
            [
                'Aulacorhynchus',
                'caeruleogularis',
                'caeruleogularis',
            ],
        ],
        [
            ['Aulacorhynchus', 'lautus', ''],
            [
                'Aulacorhynchus',
                'albivitta',
                'lautus',
            ],
        ],
        [
            ['Aulacorhynchus', 'griseigularis', ''],
            [
                'Aulacorhynchus',
                'albivitta',
                'griseigularis',
            ],
        ],
        [
            ['Pachycephala', 'macrorhyncha', 'balim'],
            [
                'Pachycephala',
                'balim',
                '',
            ],
        ],
        [
            ['Myiagra', 'azureocapilla', 'castaneigularis'],
            [
                'Myiagra',
                'castaneigularis',
                'castaneigularis',
            ],
        ],
        [
            ['Calandrella', 'brachydactyla', 'dukhunensis'],
            [
                'Calandrella',
                'dukhunensis',
                '',
            ],
        ],
        [
            ['Pycnonotus', 'blanfordi', 'blanfordi'],
            [
                'Pycnonotus',
                'blanfordi',
                '',
            ],
        ],
        [
            ['Pycnonotus', 'blanfordi', 'conradi'],
            [
                'Pycnonotus',
                'conradi',
                '',
            ],
        ],
        [
            ['Hypocnemis', 'cantator', 'cantator'],
            [
                'Hypocnemis',
                'cantator',
                '',
            ],
        ],
        [
            ['Zoothera', 'mollissima', 'mollissima'],
            [
                'Zoothera',
                'mollissima',
                '',
            ],
        ],
        [['Icterus', 'spurius', 'spurius'], ['Icterus', 'spurius', '']],
        [
            ['Agelaius', 'assimilis', 'assimilis'],
            [
                'Agelaius',
                'assimilis',
                '',
            ],
        ],
        [['Spizella', 'pusilla', 'pusilla'], ['Spizella', 'pusilla', '']],
        [
            ['Chlorospingus', 'pileatus', 'pileatus'],
            [
                'Chlorospingus',
                'pileatus',
                '',
            ],
        ],
        [['Paroaria', 'capitata', 'capitata'], ['Paroaria', 'capitata', '']],
        [['Tangara', 'florida', 'florida'], ['Tangara', 'florida', '']],
        [['Diglossa', 'plumbea', 'plumbea'], ['Diglossa', 'plumbea', '']],
        [
            ['Sporophila', 'bouvronides', 'bouvronides'],
            [
                'Sporophila',
                'bouvronides',
                '',
            ],
        ],
        [
            ['Sporophila', 'bouvreuil', 'bouvreuil'],
            [
                'Sporophila',
                'bouvreuil',
                '',
            ],
        ],
        [
            ['Tiaris', 'fuliginosus', 'fuliginosus'],
            [
                'Tiaris',
                'fuliginosus',
                '',
            ],
        ],
        [
            ['Saltator', 'atripennis', 'atripennis'],
            [
                'Saltator',
                'atripennis',
                '',
            ],
        ],
    ];
$lumps   =
    [

        [
            ['Hypocnemis', 'cantator', 'notaea'],
            [
                'Hypocnemis',
                'cantator',
                '',
            ],
        ]
        ,
        [
            ['Acrocephalus', 'baeticatus', 'guiersi'],
            [
                'Acrocephalus',
                'baeticatus',
                '',
            ],
        ]
        ,
        [
            ['Zoothera', 'mollissima', 'whiteheadi'],
            [
                'Zoothera',
                'mollissima',
                '',
            ],
        ]
        ,
        [
            ['Agelaius', 'assimilis', 'subniger'],
            [
                'Agelaius',
                'assimilis',
                '',
            ],
        ]
        ,
        [['Spizella', 'pusilla', 'arenacea'], ['Spizella', 'pusilla', '']]
        ,
        [
            ['Chlorospingus', 'flavopectus', 'novicius'],
            [
                'Chlorospingus',
                'flavopectus',
                '',
            ],
        ]
        ,
        [
            ['Chlorospingus', 'pileatus', 'diversus'],
            [
                'Chlorospingus',
                'pileatus',
                '',
            ],
        ]
        ,
        [['Paroaria', 'capitata', 'fuscipes'], ['Paroaria', 'capitata', '']]
        ,
        [['Tangara', 'florida', 'auriceps'], ['Tangara', 'florida', '']]
        ,
        [
            ['Diglossa', 'plumbea', 'veraguensis'],
            [
                'Diglossa',
                'plumbea',
                '',
            ],
        ]
        ,
        [
            ['Sporophila', 'bouvronides', 'restricta'],
            [
                'Sporophila',
                'bouvronides',
                '',
            ],
        ]
        ,
        [
            ['Sporophila', 'bouvreuil', 'saturata'],
            [
                'Sporophila',
                'bouvreuil',
                '',
            ],
        ]
        ,
        [
            ['Sporophila', 'bouvreuil', 'crypta'],
            [
                'Sporophila',
                'bouvreuil',
                '',
            ],
        ]
        ,
        [['Tiaris', 'fuliginosus', 'zulia'], ['Tiaris', 'fuliginosus', '']]
        ,
        [
            ['Tiaris', 'fuliginosus', 'fumosus'],
            [
                'Tiaris',
                'fuliginosus',
                '',
            ],
        ]
        ,
        [
            ['Saltator', 'atripennis', 'caniceps'],
            [
                'Saltator',
                'atripennis',
                '',
            ],
        ],
    ];
