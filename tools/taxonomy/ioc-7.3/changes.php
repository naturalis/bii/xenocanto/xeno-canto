<?php

$renames =
    [
        [['Procelsterna', 'albivitta'], ['Anous', 'albivittus']],
        [['Procelsterna', 'cerulea'], ['Anous', 'ceruleus']],
        [['Euchrepornis', 'sharpei'], ['Euchrepomis', 'sharpei']],
        [['Euchrepornis', 'humeralis'], ['Euchrepomis', 'humeralis']],
        [['Euchrepornis', 'spodioptila'], ['Euchrepomis', 'spodioptila']],
        [['Euchrepornis', 'callinota'], ['Euchrepomis', 'callinota']],
        [['Anas', 'falcata'], ['Mareca', 'falcata']],
        [['Anas', 'strepera'], ['Mareca', 'strepera']],
        [['Anas', 'penelope'], ['Mareca', 'penelope']],
        [['Anas', 'sibilatrix'], ['Mareca', 'sibilatrix']],
        [['Anas', 'americana'], ['Mareca', 'americana']],
        [['Anas', 'marecula'], ['Mareca', 'marecula']],
        [['Anas', 'querquedula'], ['Spatula', 'querquedula']],
        [['Anas', 'hottentota'], ['Spatula', 'hottentota']],
        [['Anas', 'puna'], ['Spatula', 'puna']],
        [['Anas', 'platalea'], ['Spatula', 'platalea']],
        [['Anas', 'discors'], ['Spatula', 'discors']],
        [['Anas', 'smithii'], ['Spatula', 'smithii']],
        [['Anas', 'rhynchotis'], ['Spatula', 'rhynchotis']],
        [['Anas', 'clypeata'], ['Spatula', 'clypeata']],
        [['Anas', 'formosa'], ['Sibirionetta', 'formosa']],
        [['Anas', 'versicolor'], ['Spatula', 'versicolor']],
        [['Anas', 'cyanoptera'], ['Spatula', 'cyanoptera']],
    ];
$splits  =
    [
        [
            ['Lanius', 'excubitor', 'sibiricus'],
            [
                'Lanius',
                'borealis',
                'sibiricus',
            ],
        ],
        [
            ['Lanius', 'excubitor', 'bianchii'],
            [
                'Lanius',
                'borealis',
                'bianchii',
            ],
        ],
        [
            ['Lanius', 'excubitor', 'mollis'],
            [
                'Lanius',
                'borealis',
                'mollis',
            ],
        ],
        [
            ['Lanius', 'excubitor', 'funereus'],
            [
                'Lanius',
                'borealis',
                'funereus',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'albidior'],
            [
                'Collocalia',
                'uropygialis',
                'albidior',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'perneglecta'],
            [
                'Collocalia',
                'neglecta',
                'perneglecta',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'sumbae'],
            [
                'Collocalia',
                'sumbawae',
                'sumbae',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'bagobo'],
            [
                'Collocalia',
                'isonota',
                'bagobo',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'septentrionalis'],
            [
                'Collocalia',
                'marginata',
                'septentrionalis',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'elachyptera'],
            [
                'Collocalia',
                'affinis',
                'elachyptera',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'vanderbilti'],
            [
                'Collocalia',
                'affinis',
                'vanderbilti',
            ],
        ],
        [
            ['Catharus', 'guttatus', 'munroi'],
            [
                'Catharus',
                'guttatus',
                'auduboni',
            ],
        ],
        [
            ['Catharus', 'guttatus', 'jewetti'],
            [
                'Catharus',
                'guttatus',
                'slevini',
            ],
        ],
        [['Sialia', 'sialis', 'grata'], ['Sialia', 'sialis', 'sialis']],
        [
            ['Lamprotornis', 'nitens', 'phoenicopterus'],
            [
                'Lamprotornis',
                'nitens',
                '',
            ],
        ],
        [
            ['Lamprotornis', 'nitens', 'culminator'],
            [
                'Lamprotornis',
                'nitens',
                '',
            ],
        ],
        [
            ['Lamprotornis', 'nitens', 'nitens'],
            [
                'Lamprotornis',
                'nitens',
                '',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'erwini'],
            [
                'Collocalia',
                'esculenta',
                '',
            ],
        ],
        [
            ['Sipodotus', 'wallacii', 'coronatus'],
            [
                'Sipodotus',
                'wallacii',
                '',
            ],
        ],
        [
            ['Surniculus', 'dicruroides', 'barussarum'],
            [
                'Surniculus',
                'lugubris',
                'barussarum',
            ],
        ],
        [
            ['Procelsterna', 'albivitta', 'skottsbergii'],
            [
                'Anous',
                'albivittus',
                'skottsbergii',
            ],
        ],
        [
            ['Procelsterna', 'albivitta', 'imitatrix'],
            [
                'Anous',
                'albivittus',
                'imitatrix',
            ],
        ],
        [
            ['Procelsterna', 'cerulea', 'saxatilis'],
            [
                'Anous',
                'ceruleus',
                'saxatilis',
            ],
        ],
        [
            ['Procelsterna', 'cerulea', 'nebouxi'],
            [
                'Anous',
                'ceruleus',
                'nebouxi',
            ],
        ],
        [
            ['Procelsterna', 'cerulea', 'teretirostris'],
            [
                'Anous',
                'ceruleus',
                'teretirostris',
            ],
        ],
        [
            ['Procelsterna', 'cerulea', 'murphyi'],
            [
                'Anous',
                'ceruleus',
                'murphyi',
            ],
        ],
        [['Anas', 'strepera', 'couesi'], ['Mareca', 'strepera', 'couesi']],
        [
            ['Anas', 'versicolor', 'fretensis'],
            [
                'Spatula',
                'versicolor',
                'fretensis',
            ],
        ],
        [
            ['Anas', 'cyanoptera', 'septentrionalium'],
            [
                'Spatula',
                'cyanoptera',
                'septentrionalium',
            ],
        ],
        [
            ['Anas', 'cyanoptera', 'tropica'],
            [
                'Spatula',
                'cyanoptera',
                'tropica',
            ],
        ],
        [
            ['Anas', 'cyanoptera', 'borreroi'],
            [
                'Spatula',
                'cyanoptera',
                'borreroi',
            ],
        ],
        [
            ['Anas', 'cyanoptera', 'orinoma'],
            [
                'Spatula',
                'cyanoptera',
                'orinoma',
            ],
        ],
        [['Junco', 'phaeonotus', 'bairdi'], ['Junco', 'bairdi', '']],
        [
            ['Montecincla', 'fairbanki', 'meridionale'],
            [
                'Montecincla',
                'meridionale',
                '',
            ],
        ],
        [
            ['Montecincla', 'fairbanki', 'fairbanki'],
            [
                'Montecincla',
                'fairbanki',
                '',
            ],
        ],
        [
            ['Montecincla', 'cachinnans', 'jerdoni'],
            [
                'Montecincla',
                'jerdoni',
                '',
            ],
        ],
        [
            ['Montecincla', 'cachinnans', 'cachinnans'],
            [
                'Montecincla',
                'cachinnans',
                '',
            ],
        ],
        [
            ['Lamprolia', 'victoriae', 'victoriae'],
            [
                'Lamprolia',
                'victoriae',
                '',
            ],
        ],
        [
            ['Lamprolia', 'victoriae', 'klinesmithi'],
            [
                'Lamprolia',
                'klinesmithi',
                '',
            ],
        ],
        [
            ['Lanius', 'excubitor', 'borealis'],
            [
                'Lanius',
                'borealis',
                'borealis',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'natalis'],
            [
                'Collocalia',
                'natalis',
                '',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'uropygialis'],
            [
                'Collocalia',
                'uropygialis',
                '',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'neglecta'],
            [
                'Collocalia',
                'neglecta',
                'neglecta',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'sumbawae'],
            [
                'Collocalia',
                'sumbawae',
                'sumbawae',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'isonota'],
            [
                'Collocalia',
                'isonota',
                'isonota',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'marginata'],
            [
                'Collocalia',
                'marginata',
                'marginata',
            ],
        ],
        [
            ['Collocalia', 'esculenta', 'affinis'],
            [
                'Collocalia',
                'affinis',
                'affinis',
            ],
        ],
        [
            ['Lamprotornis', 'nitens', 'nitens'],
            [
                'Lamprotornis',
                'nitens',
                '',
            ],
        ],
        [
            ['Sipodotus', 'wallacii', 'wallacii'],
            [
                'Sipodotus',
                'wallacii',
                '',
            ],
        ],
        [
            ['Procelsterna', 'albivitta', 'albivitta'],
            [
                'Anous',
                'albivittus',
                'albivittus',
            ],
        ],
        [
            ['Procelsterna', 'cerulea', 'cerulea'],
            [
                'Anous',
                'ceruleus',
                'ceruleus',
            ],
        ],
        [['Anas', 'strepera', ''], ['Mareca', 'strepera', 'strepera']],
        [
            ['Anas', 'versicolor', 'versicolor'],
            [
                'Spatula',
                'versicolor',
                'versicolor',
            ],
        ],
        [
            ['Anas', 'cyanoptera', 'cyanoptera'],
            [
                'Spatula',
                'cyanoptera',
                'cyanoptera',
            ],
        ],
    ];
$lumps   =
    [

        [['Larus', 'thayeri', ''], ['Larus', 'glaucoides', 'thayeri']],
    ];
