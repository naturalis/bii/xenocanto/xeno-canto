<?php

$renames =
    [
        [['Myiomela', 'major'], ['Sholicola', 'major']],
        [['Iole', 'virescens'], ['Iole', 'viridescens']],
        [['Iole', 'olivacea'], ['Iole', 'crypta']],
        [['Trochalopteron', 'cachinnans'], ['Montecincla', 'cachinnans']],
        [['Limicola', 'falcinellus'], ['Calidris', 'falcinellus']],
        [['Terenura', 'sharpei'], ['Euchrepornis', 'sharpei']],
        [['Terenura', 'humeralis'], ['Euchrepornis', 'humeralis']],
        [['Terenura', 'spodioptila'], ['Euchrepornis', 'spodioptila']],
        [['Terenura', 'callinota'], ['Euchrepornis', 'callinota']],
        [['Tryngites', 'subruficollis'], ['Calidris', 'subruficollis']],
        [['Eurynorhynchus', 'pygmeus'], ['Calidris', 'pygmea']],
        [['Philomachus', 'pugnax'], ['Calidris', 'pugnax']],
        [['Aphriza', 'virgata'], ['Calidris', 'virgata']],
        [['Grus', 'vipio'], ['Antigone', 'vipio']],
        [['Grus', 'canadensis'], ['Antigone', 'canadensis']],
        [['Grus', 'antigone'], ['Antigone', 'antigone']],
        [['Grus', 'rubicunda'], ['Antigone', 'rubicunda']],
        [['Grus', 'leucogeranus'], ['Leucogeranus', 'leucogeranus']],
    ];
$splits  =
    [
        [
            ['Cinnyris', 'ludovicensis', 'skye'],
            [
                'Cinnyris',
                'whytei',
                'skye',
            ],
        ],
        [
            ['Iole', 'virescens', 'myitkyinensis'],
            [
                'Iole',
                'viridescens',
                'myitkyinensis',
            ],
        ],
        [
            ['Psittacara', 'wagleri', 'minor'],
            [
                'Psittacara',
                'frontatus',
                'minor',
            ],
        ],
        [
            ['Ocreatus', 'underwoodii', 'annae'],
            [
                'Ocreatus',
                'addae',
                'annae',
            ],
        ],
        [['Tyto', 'alba', 'pratincola'], ['Tyto', 'furcata', 'pratincola']],
        [['Tyto', 'alba', 'guatemalae'], ['Tyto', 'furcata', 'guatemalae']],
        [['Tyto', 'alba', 'bondi'], ['Tyto', 'furcata', 'bondi']],
        [['Tyto', 'alba', 'niveicauda'], ['Tyto', 'furcata', 'niveicauda']],
        [['Tyto', 'alba', 'nigrescens'], ['Tyto', 'furcata', 'nigrescens']],
        [['Tyto', 'alba', 'insularis'], ['Tyto', 'furcata', 'insularis']],
        [['Tyto', 'alba', 'bargei'], ['Tyto', 'furcata', 'bargei']],
        [['Tyto', 'alba', 'contempta'], ['Tyto', 'furcata', 'contempta']],
        [['Tyto', 'alba', 'hellmayri'], ['Tyto', 'furcata', 'hellmayri']],
        [['Tyto', 'alba', 'tuidara'], ['Tyto', 'furcata', 'tuidara']],
        [
            ['Tyto', 'alba', 'punctatissima'],
            [
                'Tyto',
                'furcata',
                'punctatissima',
            ],
        ],
        [
            ['Anthus', 'gutturalis', 'rhododendri'],
            [
                'Anthus',
                'gutturalis',
                'wollastoni',
            ],
        ],
        [
            ['Lonchura', 'grandis', 'heurni'],
            [
                'Lonchura',
                'grandis',
                'destructa',
            ],
        ],
        [
            ['Lonchura', 'grandis', 'ernesti'],
            [
                'Lonchura',
                'grandis',
                'grandis',
            ],
        ],
        [
            ['Lonchura', 'leucosticta', 'moresbyae'],
            [
                'Lonchura',
                'leucosticta',
                '',
            ],
        ],
        [
            ['Oreostruthus', 'fuliginosus', 'pallidus'],
            [
                'Oreostruthus',
                'fuliginosus',
                '',
            ],
        ],
        [
            ['Oreostruthus', 'fuliginosus', 'hagenensis'],
            [
                'Oreostruthus',
                'fuliginosus',
                '',
            ],
        ],
        [
            ['Leptocoma', 'aspasia', 'salvadorii'],
            [
                'Leptocoma',
                'aspasia',
                'aspasia',
            ],
        ],
        [
            ['Dicaeum', 'geelvinkianum', 'setekwa'],
            [
                'Dicaeum',
                'geelvinkianum',
                'diversum',
            ],
        ],
        [
            ['Dicaeum', 'geelvinkianum', 'centrale'],
            [
                'Dicaeum',
                'geelvinkianum',
                'diversum',
            ],
        ],
        [
            ['Turdus', 'poliocephalus', 'erebus'],
            [
                'Turdus',
                'poliocephalus',
                'papuensis',
            ],
        ],
        [
            ['Turdus', 'poliocephalus', 'keysseri'],
            [
                'Turdus',
                'poliocephalus',
                'papuensis',
            ],
        ],
        [
            ['Aplonis', 'mysolensis', 'sulaensis'],
            [
                'Aplonis',
                'mysolensis',
                '',
            ],
        ],
        [
            ['Zosterops', 'griseotinctus', 'aignani'],
            [
                'Zosterops',
                'griseotinctus',
                'griseotinctus',
            ],
        ],
        [
            ['Zosterops', 'novaeguineae', 'aruensis'],
            [
                'Zosterops',
                'novaeguineae',
                'wuroi',
            ],
        ],
        [
            ['Megalurus', 'macrurus', 'mayri'],
            [
                'Megalurus',
                'macrurus',
                'macrurus',
            ],
        ],
        [
            ['Megalurus', 'macrurus', 'wahgiensis'],
            [
                'Megalurus',
                'macrurus',
                'macrurus',
            ],
        ],
        [
            ['Phylloscopus', 'maforensis', 'paniaiae'],
            [
                'Phylloscopus',
                'maforensis',
                'albigularis',
            ],
        ],
        [
            ['Iole', 'propinqua', 'lekhakuni'],
            [
                'Iole',
                'viridescens',
                'lekhakuni',
            ],
        ],
        [
            ['Iole', 'propinqua', 'cinnamomeoventris'],
            [
                'Iole',
                'viridescens',
                'cinnamomeoventris',
            ],
        ],
        [
            ['Iole', 'virescens', 'myitkyinensis'],
            [
                'Iole',
                'propinqua',
                'myitkyinensis',
            ],
        ],
        [
            ['Amalocichla', 'incerta', 'olivascentior'],
            [
                'Amalocichla',
                'incerta',
                'brevicauda',
            ],
        ],
        [
            ['Drymodes', 'beccarii', 'brevirostris'],
            [
                'Drymodes',
                'beccarii',
                'beccarii',
            ],
        ],
        [['Eugerygone', 'rubra', 'saturatior'], ['Eugerygone', 'rubra', '']],
        [
            ['Peneothello', 'sigillata', 'hagensis'],
            [
                'Peneothello',
                'sigillata',
                'sigillata',
            ],
        ],
        [
            ['Microeca', 'flavovirescens', 'cuicui'],
            [
                'Microeca',
                'flavovirescens',
                '',
            ],
        ],
        [
            ['Pachycephalopsis', 'poliosoma', 'albigularis'],
            [
                'Pachycephalopsis',
                'poliosoma',
                'approximans',
            ],
        ],
        [
            ['Pachycephalopsis', 'poliosoma', 'balim'],
            [
                'Pachycephalopsis',
                'poliosoma',
                'approximans',
            ],
        ],
        [
            ['Pachycephalopsis', 'poliosoma', 'idenburgi'],
            [
                'Pachycephalopsis',
                'poliosoma',
                'hunsteini',
            ],
        ],
        [
            ['Pachycephalopsis', 'hattamensis', 'axillaris'],
            [
                'Pachycephalopsis',
                'hattamensis',
                'hattamensis',
            ],
        ],
        [
            ['Heteromyias', 'albispecularis', 'centralis'],
            [
                'Heteromyias',
                'albispecularis',
                'rothschildi',
            ],
        ],
        [
            ['Heteromyias', 'albispecularis', 'albicapilla'],
            [
                'Heteromyias',
                'albispecularis',
                'armiti',
            ],
        ],
        [
            ['Paradisaea', 'minor', 'finschi'],
            [
                'Paradisaea',
                'minor',
                'minor',
            ],
        ],
        [
            ['Seleucidis', 'melanoleucus', 'auripennis'],
            [
                'Seleucidis',
                'melanoleucus',
                '',
            ],
        ],
        [['Epimachus', 'meyeri', 'albicans'], ['Epimachus', 'meyeri', '']],
        [['Epimachus', 'meyeri', 'bloodi'], ['Epimachus', 'meyeri', '']],
        [
            ['Epimachus', 'fastosus', 'ultimus'],
            [
                'Epimachus',
                'fastosus',
                '',
            ],
        ],
        [
            ['Epimachus', 'fastosus', 'atratus'],
            [
                'Epimachus',
                'fastosus',
                '',
            ],
        ],
        [
            ['Parotia', 'carolae', 'clelandiae'],
            [
                'Parotia',
                'carolae',
                'chrysenia',
            ],
        ],
        [
            ['Phonygammus', 'keraudrenii', 'diamondi'],
            [
                'Phonygammus',
                'keraudrenii',
                'jamesii',
            ],
        ],
        [
            ['Phonygammus', 'keraudrenii', 'aruensis'],
            [
                'Phonygammus',
                'keraudrenii',
                'jamesii',
            ],
        ],
        [
            ['Phonygammus', 'keraudrenii', 'adelberti'],
            [
                'Phonygammus',
                'keraudrenii',
                'neumanni',
            ],
        ],
        [['Manucodia', 'comrii', 'trobriandi'], ['Manucodia', 'comrii', '']],
        [['Ifrita', 'kowaldi', 'brunnea'], ['Ifrita', 'kowaldi', '']],
        [
            ['Melampitta', 'lugubris', 'rostrata'],
            [
                'Melampitta',
                'lugubris',
                '',
            ],
        ],
        [
            ['Melampitta', 'lugubris', 'longicauda'],
            [
                'Melampitta',
                'lugubris',
                '',
            ],
        ],
        [
            ['Corvus', 'fuscicapillus', 'megarhynchus'],
            [
                'Corvus',
                'fuscicapillus',
                '',
            ],
        ],
        [
            ['Symposiachrus', 'trivirgatus', 'bernsteinii'],
            [
                'Symposiachrus',
                'trivirgatus',
                'nigrimentum',
            ],
        ],
        [
            ['Rhipidura', 'brachyrhyncha', 'devisi'],
            [
                'Rhipidura',
                'brachyrhyncha',
                '',
            ],
        ],
        [
            ['Rhipidura', 'albolimbata', 'lorentzi'],
            [
                'Rhipidura',
                'albolimbata',
                '',
            ],
        ],
        [
            ['Rhipidura', 'hyperythra', 'muelleri'],
            [
                'Rhipidura',
                'hyperythra',
                'hyperythra',
            ],
        ],
        [
            ['Rhipidura', 'leucothorax', 'clamosa'],
            [
                'Rhipidura',
                'leucothorax',
                'leucothorax',
            ],
        ],
        [
            ['Pitohui', 'kirhocephalus', 'adiensis'],
            [
                'Pitohui',
                'kirhocephalus',
                'rubiensis',
            ],
        ],
        [
            ['Pitohui', 'kirhocephalus', 'carolinae'],
            [
                'Pitohui',
                'kirhocephalus',
                'rubiensis',
            ],
        ],
        [
            ['Lanius', 'senator', 'rutilans'],
            [
                'Lanius',
                'senator',
                'senator',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'neos'],
            [
                'Colluricincla',
                'megarhyncha',
                'madaraszi',
            ],
        ],
        [
            ['Colluricincla', 'megarhyncha', 'idenburgi'],
            [
                'Colluricincla',
                'megarhyncha',
                'hybridus',
            ],
        ],
        [
            ['Colluricincla', 'tenebrosa', 'atra'],
            [
                'Colluricincla',
                'tenebrosa',
                '',
            ],
        ],
        [
            ['Pseudorectes', 'ferrugineus', 'fuscus'],
            [
                'Pseudorectes',
                'ferrugineus',
                'leucorhynchus',
            ],
        ],
        [
            ['Pachycephala', 'schlegelii', 'cyclopum'],
            [
                'Pachycephala',
                'schlegelii',
                'obscurior',
            ],
        ],
        [
            ['Pachycephala', 'simplex', 'gagiensis'],
            [
                'Pachycephala',
                'simplex',
                'griseiceps',
            ],
        ],
        [
            ['Pachycephala', 'simplex', 'waigeuensis'],
            [
                'Pachycephala',
                'simplex',
                'griseiceps',
            ],
        ],
        [
            ['Pachycephala', 'simplex', 'perneglecta'],
            [
                'Pachycephala',
                'simplex',
                'griseiceps',
            ],
        ],
        [
            ['Pachycephala', 'simplex', 'miosnomensis'],
            [
                'Pachycephala',
                'simplex',
                'jobiensis',
            ],
        ],
        [
            ['Pachycephala', 'modesta', 'hypoleuca'],
            [
                'Pachycephala',
                'modesta',
                'modesta',
            ],
        ],
        [
            ['Melanorectes', 'nigrescens', 'buergersi'],
            [
                'Melanorectes',
                'nigrescens',
                'meeki',
            ],
        ],
        [
            ['Ornorectes', 'cristatus', 'arthuri'],
            [
                'Ornorectes',
                'cristatus',
                '',
            ],
        ],
        [
            ['Ornorectes', 'cristatus', 'kodonophonos'],
            [
                'Ornorectes',
                'cristatus',
                '',
            ],
        ],
        [
            ['Aleadryas', 'rufinucha', 'lochmia'],
            [
                'Aleadryas',
                'rufinucha',
                'gamblei',
            ],
        ],
        [
            ['Daphoenositta', 'miranda', 'frontalis'],
            [
                'Daphoenositta',
                'miranda',
                '',
            ],
        ],
        [
            ['Daphoenositta', 'miranda', 'kuboriensis'],
            [
                'Daphoenositta',
                'miranda',
                '',
            ],
        ],
        [
            ['Daphoenositta', 'papuensis', 'intermedia'],
            [
                'Daphoenositta',
                'papuensis',
                'alba',
            ],
        ],
        [
            ['Daphoenositta', 'papuensis', 'wahgiensis'],
            [
                'Daphoenositta',
                'papuensis',
                'toxopeusi',
            ],
        ],
        [
            ['Lalage', 'leucomela', 'trobriandi'],
            [
                'Lalage',
                'leucomela',
                'obscurior',
            ],
        ],
        [['Coracina', 'melas', 'meeki'], ['Coracina', 'melas', 'melas']],
        [['Coracina', 'melas', 'goodsoni'], ['Coracina', 'melas', 'melas']],
        [
            ['Coracina', 'caeruleogrisea', 'strenua'],
            [
                'Coracina',
                'caeruleogrisea',
                '',
            ],
        ],
        [
            ['Coracina', 'caeruleogrisea', 'adamsoni'],
            [
                'Coracina',
                'caeruleogrisea',
                '',
            ],
        ],
        [
            ['Rhagologus', 'leucostigma', 'novus'],
            [
                'Rhagologus',
                'leucostigma',
                'leucostigma',
            ],
        ],
        [['Cinclosoma', 'ajax', 'muscale'], ['Cinclosoma', 'ajax', 'alare']],
        [
            ['Paramythia', 'montium', 'brevicauda'],
            [
                'Paramythia',
                'montium',
                'montium',
            ],
        ],
        [
            ['Toxorhamphus', 'poliopterus', 'maximus'],
            [
                'Toxorhamphus',
                'poliopterus',
                '',
            ],
        ],
        [
            ['Oedistoma', 'pygmaeum', 'waigeuense'],
            [
                'Oedistoma',
                'pygmaeum',
                'pygmaeum',
            ],
        ],
        [
            ['Oedistoma', 'iliolophus', 'affine'],
            [
                'Oedistoma',
                'iliolophus',
                'iliolophus',
            ],
        ],
        [
            ['Oedistoma', 'iliolophus', 'flavum'],
            [
                'Oedistoma',
                'iliolophus',
                'iliolophus',
            ],
        ],
        [
            ['Rhamphocharis', 'crassirostris', 'viridescens'],
            [
                'Rhamphocharis',
                'crassirostris',
                'piperata',
            ],
        ],
        [
            ['Melanocharis', 'striativentris', 'chrysocome'],
            [
                'Melanocharis',
                'striativentris',
                'striativentris',
            ],
        ],
        [
            ['Melanocharis', 'versteri', 'virago'],
            [
                'Melanocharis',
                'versteri',
                'maculiceps',
            ],
        ],
        [
            ['Melanocharis', 'longicauda', 'chloris'],
            [
                'Melanocharis',
                'longicauda',
                'longicauda',
            ],
        ],
        [
            ['Melanocharis', 'longicauda', 'umbrosa'],
            [
                'Melanocharis',
                'longicauda',
                'longicauda',
            ],
        ],
        [
            ['Cnemophilus', 'loriae', 'inexpectatus'],
            [
                'Cnemophilus',
                'loriae',
                '',
            ],
        ],
        [
            ['Cnemophilus', 'loriae', 'amethystinus'],
            [
                'Cnemophilus',
                'loriae',
                '',
            ],
        ],
        [
            ['Gerygone', 'palpebrosa', 'inconspicua'],
            [
                'Gerygone',
                'palpebrosa',
                'palpebrosa',
            ],
        ],
        [
            ['Gerygone', 'chloronota', 'cinereiceps'],
            [
                'Gerygone',
                'chloronota',
                'chloronota',
            ],
        ],
        [
            ['Gerygone', 'chloronota', 'aruensis'],
            [
                'Gerygone',
                'chloronota',
                'chloronota',
            ],
        ],
        [
            ['Gerygone', 'magnirostris', 'cobana'],
            [
                'Gerygone',
                'magnirostris',
                'conspicillata',
            ],
        ],
        [
            ['Gerygone', 'magnirostris', 'occasa'],
            [
                'Gerygone',
                'magnirostris',
                'conspicillata',
            ],
        ],
        [
            ['Gerygone', 'magnirostris', 'proxima'],
            [
                'Gerygone',
                'magnirostris',
                'rosseliana',
            ],
        ],
        [
            ['Gerygone', 'magnirostris', 'onerosa'],
            [
                'Gerygone',
                'magnirostris',
                'rosseliana',
            ],
        ],
        [
            ['Gerygone', 'magnirostris', 'tagulana'],
            [
                'Gerygone',
                'magnirostris',
                'rosseliana',
            ],
        ],
        [
            ['Gerygone', 'ruficollis', 'insperata'],
            [
                'Gerygone',
                'ruficollis',
                '',
            ],
        ],
        [
            ['Sericornis', 'spilodera', 'batantae'],
            [
                'Sericornis',
                'spilodera',
                'ferrugineus',
            ],
        ],
        [
            ['Sericornis', 'spilodera', 'wuroi'],
            [
                'Sericornis',
                'spilodera',
                'guttatus',
            ],
        ],
        [
            ['Crateroscelis', 'robusta', 'pratti'],
            [
                'Crateroscelis',
                'robusta',
                'robusta',
            ],
        ],
        [
            ['Crateroscelis', 'nigrorufa', 'blissi'],
            [
                'Crateroscelis',
                'nigrorufa',
                '',
            ],
        ],
        [
            ['Crateroscelis', 'murina', 'fumosa'],
            [
                'Crateroscelis',
                'murina',
                'capitalis',
            ],
        ],
        [
            ['Pachycare', 'flavogriseum', 'randi'],
            [
                'Pachycare',
                'flavogriseum',
                'subaurantium',
            ],
        ],
        [
            ['Meliphaga', 'aruensis', 'sharpei'],
            [
                'Meliphaga',
                'aruensis',
                '',
            ],
        ],
        [
            ['Meliphaga', 'analoga', 'longirostris'],
            [
                'Meliphaga',
                'analoga',
                '',
            ],
        ],
        [['Meliphaga', 'analoga', 'flavida'], ['Meliphaga', 'analoga', '']],
        [
            ['Meliphaga', 'orientalis', 'citreola'],
            [
                'Meliphaga',
                'orientalis',
                'becki',
            ],
        ],
        [
            ['Meliphaga', 'montana', 'margaretae'],
            [
                'Meliphaga',
                'montana',
                'montana',
            ],
        ],
        [
            ['Meliphaga', 'montana', 'sepik'],
            [
                'Meliphaga',
                'montana',
                'montana',
            ],
        ],
        [
            ['Meliphaga', 'montana', 'germanorum'],
            [
                'Meliphaga',
                'montana',
                'montana',
            ],
        ],
        [
            ['Meliphaga', 'montana', 'huonensis'],
            [
                'Meliphaga',
                'montana',
                'montana',
            ],
        ],
        [
            ['Meliphaga', 'montana', 'aicora'],
            [
                'Meliphaga',
                'montana',
                'montana',
            ],
        ],
        [
            ['Ptilotula', 'flavescens', 'germana'],
            [
                'Ptilotula',
                'flavescens',
                'flavescens',
            ],
        ],
        [
            ['Melidectes', 'torquatus', 'nuchalis'],
            [
                'Melidectes',
                'torquatus',
                'torquatus',
            ],
        ],
        [
            ['Melidectes', 'torquatus', 'mixtus'],
            [
                'Melidectes',
                'torquatus',
                'torquatus',
            ],
        ],
        [
            ['Melidectes', 'belfordi', 'kinneari'],
            [
                'Melidectes',
                'belfordi',
                'joiceyi',
            ],
        ],
        [
            ['Melidectes', 'ochromelas', 'batesi'],
            [
                'Melidectes',
                'ochromelas',
                '',
            ],
        ],
        [
            ['Melidectes', 'ochromelas', 'lucifer'],
            [
                'Melidectes',
                'ochromelas',
                '',
            ],
        ],
        [
            ['Melidectes', 'fuscus', 'occidentalis'],
            [
                'Melidectes',
                'fuscus',
                '',
            ],
        ],
        [
            ['Caligavis', 'obscura', 'viridifrons'],
            [
                'Caligavis',
                'obscura',
                '',
            ],
        ],
        [
            ['Caligavis', 'subfrenata', 'melanolaema'],
            [
                'Caligavis',
                'subfrenata',
                '',
            ],
        ],
        [
            ['Caligavis', 'subfrenata', 'utakwensis'],
            [
                'Caligavis',
                'subfrenata',
                '',
            ],
        ],
        [
            ['Caligavis', 'subfrenata', 'salvadorii'],
            [
                'Caligavis',
                'subfrenata',
                '',
            ],
        ],
        [
            ['Timeliopsis', 'fulvigula', 'fuscicapilla'],
            [
                'Timeliopsis',
                'fulvigula',
                'meyeri',
            ],
        ],
        [
            ['Pycnopygius', 'cinereus', 'dorsalis'],
            [
                'Pycnopygius',
                'cinereus',
                'cinereus',
            ],
        ],
        [
            ['Pycnopygius', 'ixoides', 'cinereifrons'],
            [
                'Pycnopygius',
                'ixoides',
                'ixoides',
            ],
        ],
        [['Ptiloprora', 'guisei', 'umbrosa'], ['Ptiloprora', 'guisei', '']],
        [['Ptiloprora', 'plumbea', 'granti'], ['Ptiloprora', 'plumbea', '']],
        [
            ['Glycichaera', 'fallax', 'poliocephala'],
            [
                'Glycichaera',
                'fallax',
                'fallax',
            ],
        ],
        [
            ['Melilestes', 'megarhynchus', 'stresemanni'],
            [
                'Melilestes',
                'megarhynchus',
                'vagans',
            ],
        ],
        [
            ['Philemon', 'novaeguineae', 'trivialis'],
            [
                'Philemon',
                'novaeguineae',
                'novaeguineae',
            ],
        ],
        [
            ['Xanthotis', 'flaviventer', 'philemon'],
            [
                'Xanthotis',
                'flaviventer',
                'meyeri',
            ],
        ],
        [
            ['Xanthotis', 'polygrammus', 'kuehni'],
            [
                'Xanthotis',
                'polygrammus',
                'poikilosternos',
            ],
        ],
        [
            ['Xanthotis', 'polygrammus', 'candidior'],
            [
                'Xanthotis',
                'polygrammus',
                'lophotis',
            ],
        ],
        [
            ['Myzomela', 'nigrita', 'meyeri'],
            [
                'Myzomela',
                'nigrita',
                'nigrita',
            ],
        ],
        [
            ['Myzomela', 'nigrita', 'louisiadensis'],
            [
                'Myzomela',
                'nigrita',
                'nigrita',
            ],
        ],
        [
            ['Myzomela', 'obscura', 'aruensis'],
            [
                'Myzomela',
                'obscura',
                'fumata',
            ],
        ],
        [
            ['Cormobates', 'placens', 'kombok'],
            [
                'Cormobates',
                'placens',
                'placens',
            ],
        ],
        [
            ['Cormobates', 'placens', 'amati'],
            [
                'Cormobates',
                'placens',
                'placens',
            ],
        ],
        [
            ['Cormobates', 'placens', 'nubicola'],
            [
                'Cormobates',
                'placens',
                'placens',
            ],
        ],
        [
            ['Cormobates', 'placens', 'lecroyae'],
            [
                'Cormobates',
                'placens',
                'placens',
            ],
        ],
        [
            ['Micropsitta', 'keiensis', 'viridipectus'],
            [
                'Micropsitta',
                'keiensis',
                'keiensis',
            ],
        ],
        [
            ['Geoffroyus', 'geoffroyi', 'orientalis'],
            [
                'Geoffroyus',
                'geoffroyi',
                'aruensis',
            ],
        ],
        [
            ['Eclectus', 'roratus', 'aruensis'],
            [
                'Eclectus',
                'roratus',
                'polychloros',
            ],
        ],
        [
            ['Eclectus', 'roratus', 'biaki'],
            [
                'Eclectus',
                'roratus',
                'polychloros',
            ],
        ],
        [
            ['Aprosmictus', 'erythropterus', 'papua'],
            [
                'Aprosmictus',
                'erythropterus',
                'coccineopterus',
            ],
        ],
        [
            ['Loriculus', 'aurantiifrons', 'batavorum'],
            [
                'Loriculus',
                'aurantiifrons',
                'meeki',
            ],
        ],
        [
            ['Cyclopsitta', 'diophthalma', 'coccineifrons'],
            [
                'Cyclopsitta',
                'diophthalma',
                'diophthalma',
            ],
        ],
        [
            ['Cyclopsitta', 'gulielmitertii', 'ramuensis'],
            [
                'Cyclopsitta',
                'gulielmitertii',
                'amabilis',
            ],
        ],
        [
            ['Psittaculirostris', 'desmarestii', 'intermedius'],
            [
                'Psittaculirostris',
                'desmarestii',
                'occidentalis',
            ],
        ],
        [
            ['Chalcopsitta', 'duivenbodei', 'syringanuchalis'],
            [
                'Chalcopsitta',
                'duivenbodei',
                '',
            ],
        ],
        [
            ['Trichoglossus', 'haematodus', 'micropteryx'],
            [
                'Trichoglossus',
                'haematodus',
                'massena',
            ],
        ],
        [
            ['Trichoglossus', 'haematodus', 'caeruleiceps'],
            [
                'Trichoglossus',
                'haematodus',
                'nigrogularis',
            ],
        ],
        [
            ['Neopsittacus', 'pullicauda', 'alpinus'],
            [
                'Neopsittacus',
                'pullicauda',
                '',
            ],
        ],
        [
            ['Neopsittacus', 'pullicauda', 'socialis'],
            [
                'Neopsittacus',
                'pullicauda',
                '',
            ],
        ],
        [
            ['Neopsittacus', 'musschenbroekii', 'major'],
            [
                'Neopsittacus',
                'musschenbroekii',
                '',
            ],
        ],
        [
            ['Psittacella', 'madaraszi', 'major'],
            [
                'Psittacella',
                'madaraszi',
                'madaraszi',
            ],
        ],
        [
            ['Psittacella', 'madaraszi', 'hallstromi'],
            [
                'Psittacella',
                'madaraszi',
                'madaraszi',
            ],
        ],
        [
            ['Psittacella', 'modesta', 'subcollaris'],
            [
                'Psittacella',
                'modesta',
                'collaris',
            ],
        ],
        [['Falco', 'severus', 'papuanus'], ['Falco', 'severus', '']],
        [
            ['Rhyticeros', 'plicatus', 'ruficollis'],
            [
                'Rhyticeros',
                'plicatus',
                '',
            ],
        ],
        [
            ['Rhyticeros', 'plicatus', 'jungei'],
            [
                'Rhyticeros',
                'plicatus',
                '',
            ],
        ],
        [
            ['Rhyticeros', 'plicatus', 'dampieri'],
            [
                'Rhyticeros',
                'plicatus',
                '',
            ],
        ],
        [
            ['Rhyticeros', 'plicatus', 'harterti'],
            [
                'Rhyticeros',
                'plicatus',
                '',
            ],
        ],
        [
            ['Rhyticeros', 'plicatus', 'mendanae'],
            [
                'Rhyticeros',
                'plicatus',
                '',
            ],
        ],
        [
            ['Merops', 'philippinus', 'javanicus'],
            [
                'Merops',
                'philippinus',
                '',
            ],
        ],
        [
            ['Merops', 'philippinus', 'celebensis'],
            [
                'Merops',
                'philippinus',
                '',
            ],
        ],
        [
            ['Merops', 'philippinus', 'salvadorii'],
            [
                'Merops',
                'philippinus',
                '',
            ],
        ],
        [
            ['Syma', 'megarhyncha', 'wellsi'],
            [
                'Syma',
                'megarhyncha',
                'megarhyncha',
            ],
        ],
        [['Syma', 'torotoro', 'meeki'], ['Syma', 'torotoro', 'torotoro']],
        [['Clytoceyx', 'rex', 'imperator'], ['Clytoceyx', 'rex', '']],
        [
            ['Aerodramus', 'hirundinaceus', 'excelsus'],
            [
                'Aerodramus',
                'hirundinaceus',
                'hirundinaceus',
            ],
        ],
        [
            ['Aegotheles', 'albertisi', 'wondiwoi'],
            [
                'Aegotheles',
                'albertisi',
                '',
            ],
        ],
        [
            ['Aegotheles', 'albertisi', 'salvadorii'],
            [
                'Aegotheles',
                'albertisi',
                '',
            ],
        ],
        [
            ['Aegotheles', 'wallacii', 'manni'],
            [
                'Aegotheles',
                'wallacii',
                '',
            ],
        ],
        [
            ['Aegotheles', 'wallacii', 'gigas'],
            [
                'Aegotheles',
                'wallacii',
                '',
            ],
        ],
        [
            ['Podargus', 'papuensis', 'rogersi'],
            [
                'Podargus',
                'papuensis',
                '',
            ],
        ],
        [
            ['Podargus', 'papuensis', 'baileyi'],
            [
                'Podargus',
                'papuensis',
                '',
            ],
        ],
        [
            ['Ninox', 'theomacha', 'hoedtii'],
            [
                'Ninox',
                'theomacha',
                'theomacha',
            ],
        ],
        [
            ['Cacomantis', 'castaneiventris', 'arfakianus'],
            [
                'Cacomantis',
                'castaneiventris',
                '',
            ],
        ],
        [
            ['Cacomantis', 'castaneiventris', 'weiskei'],
            [
                'Cacomantis',
                'castaneiventris',
                '',
            ],
        ],
        [
            ['Eudynamys', 'orientalis', 'minimus'],
            [
                'Eudynamys',
                'orientalis',
                'rufiventer',
            ],
        ],
        [
            ['Porzana', 'tabuensis', 'richardsoni'],
            [
                'Porzana',
                'tabuensis',
                '',
            ],
        ],
        [['Porzana', 'tabuensis', 'edwardi'], ['Porzana', 'tabuensis', '']],
        [
            ['Gymnocrex', 'plumbeiventris', 'hoveni'],
            [
                'Gymnocrex',
                'plumbeiventris',
                'plumbeiventris',
            ],
        ],
        [
            ['Gallirallus', 'philippensis', 'reductus'],
            [
                'Gallirallus',
                'philippensis',
                'lacustris',
            ],
        ],
        [
            ['Gallirallus', 'philippensis', 'randi'],
            [
                'Gallirallus',
                'philippensis',
                'mellori',
            ],
        ],
        [
            ['Lewinia', 'pectoralis', 'capta'],
            [
                'Lewinia',
                'pectoralis',
                'mayri',
            ],
        ],
        [
            ['Lewinia', 'pectoralis', 'insulsa'],
            [
                'Lewinia',
                'pectoralis',
                'alberti',
            ],
        ],
        [
            ['Rallicula', 'forbesi', 'steini'],
            [
                'Rallicula',
                'forbesi',
                'forbesi',
            ],
        ],
        [
            ['Rallicula', 'forbesi', 'dryas'],
            [
                'Rallicula',
                'forbesi',
                'forbesi',
            ],
        ],
        [
            ['Rallicula', 'rubra', 'telefolminensis'],
            [
                'Rallicula',
                'rubra',
                'klossi',
            ],
        ],
        [['Ducula', 'mullerii', 'aurantia'], ['Ducula', 'mullerii', '']],
        [
            ['Ptilinopus', 'pulchellus', 'decorus'],
            [
                'Ptilinopus',
                'pulchellus',
                '',
            ],
        ],
        [
            ['Ptilinopus', 'iozonus', 'pseudohumeralis'],
            [
                'Ptilinopus',
                'iozonus',
                'humeralis',
            ],
        ],
        [['Goura', 'cristata', 'minor'], ['Goura', 'cristata', '']],
        [['Goura', 'cristata', 'pygmaea'], ['Goura', 'cristata', '']],
        [
            ['Geopelia', 'placida', 'papua'],
            [
                'Geopelia',
                'placida',
                'placida',
            ],
        ],
        [
            ['Gallicolumba', 'rufigula', 'septentrionalis'],
            [
                'Gallicolumba',
                'rufigula',
                'rufigula',
            ],
        ],
        [
            ['Gallicolumba', 'rufigula', 'orientalis.'],
            [
                'Gallicolumba',
                'rufigula',
                'rufigula',
            ],
        ],
        [
            ['Accipiter', 'melanochlamys', 'schistacinus'],
            [
                'Accipiter',
                'melanochlamys',
                '',
            ],
        ],
        [
            ['Accipiter', 'cirrocephalus', 'rosselianus'],
            [
                'Accipiter',
                'fasciatus',
                'rosselianus',
            ],
        ],
        [
            ['Accipiter', 'toussenelii', 'macrocelides'],
            [
                'Accipiter',
                'toussenelii',
                'macroscelides',
            ],
        ],
        [
            ['Excalfactoria', 'chinensis', 'novaeguineae'],
            [
                'Excalfactoria',
                'chinensis',
                'lepida',
            ],
        ],
        [
            ['Excalfactoria', 'chinensis', 'papuensis'],
            [
                'Excalfactoria',
                'chinensis',
                'lepida',
            ],
        ],
        [
            ['Talegalla', 'fuscirostris', 'occidentis'],
            [
                'Talegalla',
                'fuscirostris',
                '',
            ],
        ],
        [
            ['Talegalla', 'fuscirostris', 'aruensis'],
            [
                'Talegalla',
                'fuscirostris',
                '',
            ],
        ],
        [
            ['Talegalla', 'fuscirostris', 'meyeri'],
            [
                'Talegalla',
                'fuscirostris',
                '',
            ],
        ],
        [['Talegalla', 'cuvieri', 'granti'], ['Talegalla', 'cuvieri', '']],
        [
            ['Myiomela', 'albiventris', 'ashambuensis'],
            [
                'Sholicola',
                'albiventris',
                'ashambuensis',
            ],
        ],
        [
            ['Trochalopteron', 'fairbanki', 'meridionale'],
            [
                'Montecincla',
                'fairbanki',
                'meridionale',
            ],
        ],
        [
            ['Trochalopteron', 'cachinnans', 'jerdoni'],
            [
                'Montecincla',
                'cachinnans',
                'jerdoni',
            ],
        ],
        [
            ['Terenura', 'spodioptila', 'signata'],
            [
                'Euchrepornis',
                'spodioptila',
                'signata',
            ],
        ],
        [
            ['Terenura', 'spodioptila', 'meridionalis'],
            [
                'Euchrepornis',
                'spodioptila',
                'meridionalis',
            ],
        ],
        [
            ['Terenura', 'callinota', 'peruviana'],
            [
                'Euchrepornis',
                'callinota',
                'peruviana',
            ],
        ],
        [
            ['Terenura', 'callinota', 'venezuelana'],
            [
                'Euchrepornis',
                'callinota',
                'venezuelana',
            ],
        ],
        [
            ['Terenura', 'callinota', 'guianensis'],
            [
                'Euchrepornis',
                'callinota',
                'guianensis',
            ],
        ],
        [
            ['Limicola', 'falcinellus', 'sibirica'],
            [
                'Calidris',
                'falcinellus',
                'sibirica',
            ],
        ],
        [
            ['Grus', 'canadensis', 'tabida'],
            [
                'Antigone',
                'canadensis',
                'tabida',
            ],
        ],
        [
            ['Grus', 'canadensis', 'pratensis'],
            [
                'Antigone',
                'canadensis',
                'pratensis',
            ],
        ],
        [
            ['Grus', 'canadensis', 'pulla'],
            [
                'Antigone',
                'canadensis',
                'pulla',
            ],
        ],
        [
            ['Grus', 'canadensis', 'nesiotes'],
            [
                'Antigone',
                'canadensis',
                'nesiotes',
            ],
        ],
        [
            ['Grus', 'antigone', 'sharpii'],
            [
                'Antigone',
                'antigone',
                'sharpii',
            ],
        ],
        [['Grus', 'antigone', 'gillae'], ['Antigone', 'antigone', 'gillae']],
        [
            ['Cinnyris', 'ludovicensis', 'whytei'],
            [
                'Cinnyris',
                'whytei',
                'whytei',
            ],
        ],
        [['Iole', 'olivacea', 'olivacea'], ['Iole', 'crypta', '']],
        [['Iole', 'olivacea', 'charlottae'], ['Iole', 'charlottae', '']],
        [
            ['Iole', 'virescens', 'virescens'],
            [
                'Iole',
                'viridescens',
                'viridescens',
            ],
        ],
        [['Iole', 'virescens', 'cacharensis'], ['Iole', 'cacharensis', '']],
        [
            ['Psittacara', 'wagleri', 'frontatus'],
            [
                'Psittacara',
                'frontatus',
                'frontatus',
            ],
        ],
        [
            ['Ocreatus', 'underwoodii', 'addae'],
            [
                'Ocreatus',
                'addae',
                'addae',
            ],
        ],
        [
            ['Ocreatus', 'underwoodii', 'peruanus'],
            [
                'Ocreatus',
                'peruanus',
                '',
            ],
        ],
        [
            ['Ocreatus', 'underwoodii', 'underwoodii'],
            [
                'Ocreatus',
                'underwoodii',
                'underwoodii',
            ],
        ],
        [
            ['Amazilia', 'saucerottei', 'hoffmanni'],
            [
                'Amazilia',
                'hoffmanni',
                '',
            ],
        ],
        [['Tyto', 'alba', 'furcata'], ['Tyto', 'furcata', 'furcata']],
        [
            ['Pternistis', 'castaneicollis', 'atrifrons'],
            [
                'Pternistis',
                'atrifrons',
                '',
            ],
        ],
        [
            ['Lonchura', 'leucosticta', 'leucosticta'],
            [
                'Lonchura',
                'leucosticta',
                '',
            ],
        ],
        [
            ['Oreostruthus', 'fuliginosus', 'fuliginosus'],
            [
                'Oreostruthus',
                'fuliginosus',
                '',
            ],
        ],
        [
            ['Aplonis', 'mysolensis', 'mysolensis'],
            [
                'Aplonis',
                'mysolensis',
                '',
            ],
        ],
        [['Eugerygone', 'rubra', 'rubra'], ['Eugerygone', 'rubra', '']],
        [
            ['Microeca', 'flavovirescens', 'flavovirescens'],
            [
                'Microeca',
                'flavovirescens',
                '',
            ],
        ],
        [
            ['Seleucidis', 'melanoleucus', 'melanoleucus'],
            [
                'Seleucidis',
                'melanoleucus',
                '',
            ],
        ],
        [['Epimachus', 'meyeri', 'meyeri'], ['Epimachus', 'meyeri', '']],
        [
            ['Epimachus', 'fastosus', 'fastosus'],
            [
                'Epimachus',
                'fastosus',
                '',
            ],
        ],
        [['Ifrita', 'kowaldi', 'kowaldi'], ['Ifrita', 'kowaldi', '']],
        [['Manucodia', 'comrii', 'comrii'], ['Manucodia', 'comrii', '']],
        [
            ['Melampitta', 'lugubris', 'lugubris'],
            [
                'Melampitta',
                'lugubris',
                '',
            ],
        ],
        [
            ['Corvus', 'fuscicapillus', 'fuscicapillus'],
            [
                'Corvus',
                'fuscicapillus',
                '',
            ],
        ],
        [
            ['Rhipidura', 'brachyrhyncha', 'brachyrhyncha'],
            [
                'Rhipidura',
                'brachyrhyncha',
                '',
            ],
        ],
        [
            ['Rhipidura', 'albolimbata', 'albolimbata'],
            [
                'Rhipidura',
                'albolimbata',
                '',
            ],
        ],
        [
            ['Colluricincla', 'tenebrosa', 'tenebrosa'],
            [
                'Colluricincla',
                'tenebrosa',
                '',
            ],
        ],
        [
            ['Ornorectes', 'cristatus', 'cristatus'],
            [
                'Ornorectes',
                'cristatus',
                '',
            ],
        ],
        [
            ['Daphoenositta', 'miranda', 'miranda'],
            [
                'Daphoenositta',
                'miranda',
                '',
            ],
        ],
        [
            ['Coracina', 'caeruleogrisea', 'caeruleogrisea'],
            [
                'Coracina',
                'caeruleogrisea',
                '',
            ],
        ],
        [
            ['Toxorhamphus', 'poliopterus', 'poliopterus'],
            [
                'Toxorhamphus',
                'poliopterus',
                '',
            ],
        ],
        [['Cnemophilus', 'loriae', 'loriae'], ['Cnemophilus', 'loriae', '']],
        [
            ['Gerygone', 'ruficollis', 'ruficollis'],
            [
                'Gerygone',
                'ruficollis',
                '',
            ],
        ],
        [
            ['Crateroscelis', 'nigrorufa', 'nigrorufa'],
            [
                'Crateroscelis',
                'nigrorufa',
                '',
            ],
        ],
        [
            ['Meliphaga', 'aruensis', 'aruensis'],
            [
                'Meliphaga',
                'aruensis',
                '',
            ],
        ],
        [['Meliphaga', 'analoga', 'analoga'], ['Meliphaga', 'analoga', '']],
        [
            ['Melidectes', 'ochromelas', 'ochromelas'],
            [
                'Melidectes',
                'ochromelas',
                '',
            ],
        ],
        [['Melidectes', 'fuscus', 'fuscus'], ['Melidectes', 'fuscus', '']],
        [['Caligavis', 'obscura', 'obscura'], ['Caligavis', 'obscura', '']],
        [
            ['Caligavis', 'subfrenata', 'subfrenata'],
            [
                'Caligavis',
                'subfrenata',
                '',
            ],
        ],
        [['Ptiloprora', 'guisei', 'guisei'], ['Ptiloprora', 'guisei', '']],
        [
            ['Ptiloprora', 'plumbea', 'plumbea'],
            [
                'Ptiloprora',
                'plumbea',
                '',
            ],
        ],
        [
            ['Chalcopsitta', 'duivenbodei', 'duivenbodei'],
            [
                'Chalcopsitta',
                'duivenbodei',
                '',
            ],
        ],
        [
            ['Neopsittacus', 'pullicauda', 'pullicauda'],
            [
                'Neopsittacus',
                'pullicauda',
                '',
            ],
        ],
        [
            ['Neopsittacus', 'musschenbroekii', 'musschenbroekii'],
            [
                'Neopsittacus',
                'musschenbroekii',
                '',
            ],
        ],
        [['Falco', 'severus', 'severus'], ['Falco', 'severus', '']],
        [
            ['Rhyticeros', 'plicatus', 'plicatus'],
            [
                'Rhyticeros',
                'plicatus',
                '',
            ],
        ],
        [
            ['Merops', 'philippinus', 'philippinus'],
            [
                'Merops',
                'philippinus',
                '',
            ],
        ],
        [['Clytoceyx', 'rex', 'rex'], ['Clytoceyx', 'rex', '']],
        [
            ['Aegotheles', 'albertisi', 'albertisi'],
            [
                'Aegotheles',
                'albertisi',
                '',
            ],
        ],
        [
            ['Aegotheles', 'wallacii', 'wallaci'],
            [
                'Aegotheles',
                'wallacii',
                '',
            ],
        ],
        [
            ['Podargus', 'papuensis', 'papuensis'],
            [
                'Podargus',
                'papuensis',
                '',
            ],
        ],
        [
            ['Cacomantis', 'castaneiventris', 'castaneiventris'],
            [
                'Cacomantis',
                'castaneiventris',
                '',
            ],
        ],
        [
            ['Porzana', 'tabuensis', 'tabuensis'],
            [
                'Porzana',
                'tabuensis',
                '',
            ],
        ],
        [['Ducula', 'mullerii', 'mullerii'], ['Ducula', 'mullerii', '']],
        [
            ['Ptilinopus', 'pulchellus', 'pulchellus'],
            [
                'Ptilinopus',
                'pulchellus',
                '',
            ],
        ],
        [['Goura', 'cristata', 'cristata'], ['Goura', 'cristata', '']],
        [
            ['Accipiter', 'melanochlamys', 'melanochlamys'],
            [
                'Accipiter',
                'melanochlamys',
                '',
            ],
        ],
        [
            ['Talegalla', 'fuscirostris', 'fuscirostris'],
            [
                'Talegalla',
                'fuscirostris',
                '',
            ],
        ],
        [['Talegalla', 'cuvieri', 'cuvieri'], ['Talegalla', 'cuvieri', '']],
        [
            ['Casuarius', 'bennetti', 'papuanus'],
            [
                'Casuarius',
                'bennetti',
                'westermanni',
            ],
        ],
        [
            ['Myiomela', 'albiventris', 'albiventris'],
            [
                'Sholicola',
                'albiventris',
                'albiventris',
            ],
        ],
        [
            ['Trochalopteron', 'fairbanki', 'fairbanki'],
            [
                'Montecincla',
                'fairbanki',
                'fairbanki',
            ],
        ],
        [
            ['Terenura', 'spodioptila', 'spodioptila'],
            [
                'Euchrepornis',
                'spodioptila',
                'spodioptila',
            ],
        ],
        [
            ['Terenura', 'callinota', 'callinota'],
            [
                'Euchrepornis',
                'callinota',
                'callinota',
            ],
        ],
        [
            ['Limicola', 'falcinellus', 'falcinellus'],
            [
                'Calidris',
                'falcinellus',
                'falcinellus',
            ],
        ],
        [
            ['Grus', 'canadensis', 'canadensis'],
            [
                'Antigone',
                'canadensis',
                'canadensis',
            ],
        ],
        [
            ['Grus', 'antigone', 'antigone'],
            [
                'Antigone',
                'antigone',
                'antigone',
            ],
        ],
    ];
$lumps   =
    [
        [['Aegotheles', 'archboldi'], ['Aegotheles', 'albertisi']],
    ];
