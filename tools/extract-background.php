<?php

require_once 'vendor/autoload.php';
require_once 'xc/Library.php';

use xc\Library;
use xc\Task;

class ExtractBackground extends Task
{
    private $speciesNames = [];

    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        $this->getSpeciesNames();
        $inserted = 0;

        $res = $this->query("
            SELECT snd_nr, back_extra 
            FROM birdsounds 
            WHERE back_extra != '' AND back_extra IS NOT NULL"
        );
        while ($row = $res->fetch_object()) {
            $back = $row->back_extra;
            foreach ($this->speciesNames as $speciesNr => $speciesName) {
                $scientific = $speciesName['scientific'];
                if (str_contains(strtolower($back), strtolower($scientific)) && !str_contains($back, '?')) {
                    echo "Matched $scientific in XC$row->snd_nr (-> $row->back_extra)\n";
                    $english = Library::escape($speciesName['english']);
                    $family = Library::escape($speciesName['family']);

                    $this->query("
                        INSERT IGNORE 
                        INTO birdsounds_background (`snd_nr`, `species_nr`, `scientific`, `english`, `family`)
                        VALUES 
                        ($row->snd_nr, '$speciesNr', '" . Library::escape($scientific) . "', '$english', '$family')"
                    );
                    $inserted += Library::db()->getConnection()->affected_rows;
                }
            }
        }

        echo "\n\nReady! Extracted $inserted background species.\n\n";
    }

    private function getSpeciesNames()
    {
        $res = $this->query("SELECT species_nr, genus, species, family, eng_name FROM taxonomy");
        while ($row = $res->fetch_object()) {
            $this->speciesNames[$row->species_nr]['scientific'] = $row->genus . ' ' . $row->species;
            $this->speciesNames[$row->species_nr]['english'] = $row->eng_name;
            $this->speciesNames[$row->species_nr]['family'] = $row->family;
        }
    }

    private function query($sql)
    {
        return $this->mysqli()->query($sql);
    }

    private function mysqli()
    {
        return Library::db()->getConnection();
    }
}


function app()
{
    static $task = null;
    if (!$task) {
        $task = new ExtractBackground();
    }
    return $task;
}

app()->run();
