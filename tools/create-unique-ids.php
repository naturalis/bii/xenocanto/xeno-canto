<?php

/*
    insert into _taxonomy

    select 'ANURA', t1.family, null, SUBSTRING_INDEX(t1.species, ' ', 1), SUBSTRING(t1.species, LOCATE(' ', t1.species)+1), trim(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(t2.common_name, ';', 1), '(', 1), ',', 1)), ROUND((RAND() * (81000-80100))+80100, 2), concat(char(round(rand()*25)+97), char(round(rand()*25)+97), char(round(rand()*25)+97), char(round(rand()*25)+97), char(round(rand()*25)+97), char(round(rand()*25)+97)), 4, null, t1.authorities, 0, 0, 0, 0, 0, 0, 0, 0, 0
    from _aws t1
    left join _aweb as t2 on t1.aweb_genus = t2.genus and t1.aweb_species = t2.species
    where t1.species != ''
 */

use xc\Library;

require_once 'xc/Library.php';



class CreateUniqueIds extends xc\Task
{
    private string $updateTable = '_taxonomy';
    private float $minIocOrderNr = 0;
    private float $maxIocOrderNr = 99999;

    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        // Drop existing primary key in the temporary table, so we can use that to check if we have unique values
        $this->mysqli()->query("ALTER TABLE $this->updateTable DROP INDEX IF EXISTS `PRIMARY`");

        /* First create unique IOC_order_nrs (primary key). These matter only for birds, the other groups
        are sorted alphabetically. Pick random numbers higher than the highest bird id */
        $res = $this->mysqli()->query("SELECT genus, species, IOC_order_nr AS ioc_nr FROM $this->updateTable");
        while ($row = $res->fetch_object()) {
            if ($this->iocOrderNrExists($row->ioc_nr)) {
                $newIocNr = $this->generateIocOrderNr();
                $this->mysqli()->query(
                    "UPDATE $this->updateTable SET IOC_order_nr = $newIocNr WHERE genus = '$row->genus' AND species = '$row->species'"
                );
                echo "Updating IOC order nr for $row->genus $row->species\n";
            }
        }
        // Add primary key to check if we have unique values
        $this->mysqli()->query("ALTER TABLE $this->updateTable ADD PRIMARY KEY (`IOC_order_nr`)");

        // Next create unique species numbers
        $res = $this->mysqli()->query(
            "SELECT genus, species, IOC_order_nr AS ioc_nr, species_nr FROM $this->updateTable"
        );
        while ($row = $res->fetch_object()) {
            if ($this->speciesNrExists($row->species_nr)) {
                $newSpeciesNr = $this->generateSpeciesNr();
                $this->mysqli()->query(
                    "UPDATE $this->updateTable SET species_nr = '$newSpeciesNr' WHERE genus = '$row->genus' AND species = '$row->species'"
                );
            }
            echo "Updating species nr for $row->genus $row->species\n";
        }

        // Add primary key to check if we have unique values
        $this->mysqli()->query("ALTER TABLE $this->updateTable DROP INDEX IF EXISTS `PRIMARY`");
        $this->mysqli()->query("ALTER TABLE $this->updateTable ADD PRIMARY KEY (`species_nr`)");
        $this->mysqli()->query("ALTER TABLE $this->updateTable DROP INDEX `PRIMARY`");
    }

    private function mysqli()
    {
        return Library::db()->getConnection();
    }

    private function iocOrderNrExists($iocNr): bool
    {
        $res = $this->mysqli()->query(
            "SELECT IOC_order_nr FROM taxonomy WHERE IOC_order_nr = $iocNr
            UNION
            SELECT IOC_order_nr FROM $this->updateTable WHERE IOC_order_nr = $iocNr"
        );
        return $res->num_rows > 0;
    }

    private function generateIocOrderNr()
    {
        do {
            $iocNr = rand($this->minIocOrderNr() * 100, $this->maxIocOrderNr * 100) / 100;
        } while ($this->iocOrderNrExists($iocNr));
        return $iocNr;
    }

    private function minIocOrderNr()
    {
        // Take highest bird id (and add 1000 just in case); ids are decimals (really, so there plenty of
        // additional room here :)
        if ($this->minIocOrderNr == 0) {
            $res = $this->mysqli()->query("SELECT MAX(IOC_order_nr) AS max_nr FROM taxonomy WHERE group_id = 1");
            $row = $res->fetch_object();
            $this->minIocOrderNr = $row->max_nr + 1000;
        }
        return $this->minIocOrderNr;
    }

    private function speciesNrExists($speciesNr): bool
    {
        $res = $this->mysqli()->query(
            "SELECT species_nr FROM taxonomy WHERE species_nr = '$speciesNr'
            UNION
            SELECT species_nr FROM $this->updateTable WHERE species_nr = '$speciesNr'"
        );
        return $res->num_rows > 0;
    }

    private function generateSpeciesNr(): string
    {
        do {
            $letters = range('a', 'z');
            $speciesNr = '';
            for ($c = 0; $c < 6; $c++) {
                $speciesNr .= $letters[mt_rand(0, count($letters) - 1)];
            }
        } while ($this->speciesNrExists($speciesNr));
        return $speciesNr;
    }

}


function app()
{
    static $task = null;
    if (!$task) {
        $task = new CreateUniqueIds();
    }
    return $task;
}

app()->run();
