<?php

require_once 'vendor/autoload.php';
require_once 'xc/Library.php';

use xc\AudioUtil;
use xc\Library;
use xc\Task;

class GenerateFullSono extends Task
{
    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        $lockfile = '/tmp/.generate-full-sonos';
        $res = touch($lockfile);
        if (!$res) {
            Library::logger()->logError("[Generate full sonos] Creating $lockfile failed");
            exit(1);
        }
        $handle = fopen($lockfile, 'r+');
        if (flock($handle, LOCK_EX | LOCK_NB)) {
            while ($res = $this->db()->query('SELECT snd_nr from sonogram_queue LIMIT 10')) {
                if ($res->num_rows == 0) {
                    Library::logger()->logInfo("[Generate full sonos] Queue is empty");
                    break;
                }
                while ($row = $res->fetch_object()) {
                    Library::logger()->logInfo("[Generate full sonos] Generating sono for $row->snd_nr");
                    if (!AudioUtil::generateFullLengthSonogram($row->snd_nr)) {
                        Library::logger()->logError('Could not create sono for ' . $row->snd_nr);
                    }
                    Library::logger()->logInfo("[Generate full sonos] Generating mp3s for $row->snd_nr if necessary");
                    if (!AudioUtil::createMp3($row->snd_nr)) {
                        Library::logger()->logError('Could not create mp3(s) for ' . $row->snd_nr);
                    }
                }

                sleep(1);
            }
            flock($handle, LOCK_UN);
        } else {
            Library::logger()->logWarn("[Generate full sonos] Not generating full sonos because lock is held");
        }

        fclose($handle);
    }

}

function app()
{
    static $task = null;
    if ($task == null) {
        $task = new GenerateFullSono();
    }
    return $task;
}

app()->run();
