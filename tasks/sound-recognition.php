<?php

require_once 'vendor/autoload.php';
require_once 'xc/Library.php';

use xc\Library;
use xc\SoundRecognitionUtil;
use xc\Task;

class SoundRecognition extends Task
{
    private $lockFile = '/tmp/.sound-recognition';

    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        $res = touch($this->lockFile);
        if (!$res) {
            Library::logger()->logError("[Sound recognition] Creating $this->lockFile failed");
            exit(1);
        }

        $handle = fopen($this->lockFile, 'r+');
        if (flock($handle, LOCK_EX | LOCK_NB)) {
            while ($res = $this->db()->query('SELECT * FROM ai_queue LIMIT 10')) {
                if ($res->num_rows == 0) {
                    Library::logger()->logInfo("[Sound recognition] Queue is empty");
                    break;
                }
                while ($row = $res->fetch_object()) {
                    Library::logger()->logInfo("[Sound recognition] Starting sound recognition for XC$row->snd_nr");
                    // Remove from queue regardless of success
                    SoundRecognitionUtil::removeFromQueue($row->snd_nr);
                    if (!SoundRecognitionUtil::getIdentification($row->snd_nr)) {
                        Library::logger()->logError("[Sound recognition] ID failed for XC$row->snd_nr");
                        continue;
                    }
                    // Post reply to forum if ID yielded results
                    $summary = SoundRecognitionUtil::getIdentifiedSpecies($row->snd_nr);
                    if (!empty($summary)) {
                        SoundRecognitionUtil::postReplyToForum($row->snd_nr, $summary);
                        Library::logger()->logInfo("[Sound recognition] ID completed successfully, summary posted on forum");
                    } else {
                        Library::logger()->logInfo("[Sound recognition] ID completed successfully, but no species identified");
                    }
                }
            }
            flock($handle, LOCK_UN);
        } else {
            Library::logger()->logWarn("[Sound recognition] Not starting recognition because lock is held");
        }

        fclose($handle);
    }

}

function app()
{
    static $task = null;
    if ($task == null) {
        $task = new SoundRecognition();
    }
    return $task;
}

app()->run();
