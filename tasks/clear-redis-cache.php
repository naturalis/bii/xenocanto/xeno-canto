<?php

require_once 'vendor/autoload.php';
require_once 'xc/Library.php';

use xc\Library;
use xc\XCRedis;

class ClearRedisCache extends xc\Task
{
    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        Library::logger()->logInfo('[Clear cache] Clearing all keys...');

        $redis = new XCRedis();
        $redis->clearAllCache();

        Library::logger()->logInfo('[Clear cache] Ready!');
    }
}

function app()
{
    static $task = null;
    if (!$task) {
        $task = new ClearRedisCache();
    }
    return $task;
}

app()->run();
