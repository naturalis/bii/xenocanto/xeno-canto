<?php

require_once 'vendor/autoload.php';
require_once 'xc/Library.php';
require_once 'tasks/update-stats-common.php';

use xc\Library;
use xc\Query;
use xc\Species;
use xc\XCRedis;

class UpdateStatsHourlyTask extends UpdateStatsCommon
{
    public function __construct()
    {
        parent::__construct('.');
    }

    public function __destruct()
    {
        // Clear Redis cache when done
        Library::logger()->logInfo('[Update stats] Clearing Redis cache');
        $this->redis = new XCRedis();
        $this->redis->clearHourlyStatsCache();
    }

    public function run()
    {
        $this->updateSpeciesTotals();
        $this->updatesForFrontPage();
    }

    private function updateSpeciesTotals()
    {
        Library::logger()->logInfo(
            '[Update stats] Update species totals - species'
        );
        $this->query(
            '
            UPDATE taxonomy A
            LEFT JOIN
            (SELECT count( snd_nr ) AS nr, species_nr
            FROM birdsounds
            GROUP BY species_nr) B USING(species_nr)
            SET recordings = IF(B.nr IS NULL, 0, B.nr)'
        );

        Library::logger()->logInfo(
            '[Update stats] Update species totals - background species'
        );
        // background recording numbers
        $this->query(
            '
            UPDATE taxonomy A
            LEFT JOIN
            (SELECT count(snd_nr) AS nr, species_nr FROM birdsounds_background GROUP BY species_nr) B USING(species_nr)
            SET back_recordings = IF(B.nr IS NULL, 0, B.nr)'
        );
    }

    private function updatesForFrontPage()
    {
        $this->createNextTable('latest_statistics');
        $this->createNextTable('latest_species');
        $this->createNextTable('latest_recordists');

        foreach ($this->getGroups() as $id => $group) {
            Library::logger()->logInfo("[Update stats] Updates for front page - $group");
            $sql = "
                SELECT COUNT(snd_nr) as nr_recs, COUNT(DISTINCT(dir)) AS nr_recordists 
                FROM birdsounds 
                WHERE group_id = $id";
            $row = $this->query($sql)->fetch_object();
            $nrRecordings = $row->nr_recs;
            $nrRecordists = $row->nr_recordists;

            $sql = "
                SELECT SUM(t1.length) AS duration 
                FROM audio_info AS t1
                LEFT JOIN birdsounds AS t2 ON t1.snd_nr = t2.snd_nr
                WHERE t2.group_id = $id";
            $row = $this->query($sql)->fetch_object();
            $totalDuration = intval($row->duration);

            $sql = "
                SELECT COUNT(species_nr) AS nr_species 
                FROM taxonomy 
                WHERE (recordings > 0 OR back_recordings > 0) 
                AND " . Species::excludePseudoSpeciesSQL('taxonomy') . " 
                AND group_id = $id";
            $row = $this->query($sql)->fetch_object();
            $nrSpecies = $row->nr_species;

            $sql = "
                SELECT count(ssp) AS nr_ssp 
                FROM (
                    SELECT species_nr, ssp 
                    FROM taxonomy_ssp 
                    INNER JOIN birdsounds 
                    USING (species_nr, ssp)
                    WHERE birdsounds.group_id = $id 
                    GROUP BY species_nr, ssp
                ) AS A";
            $row = $this->query($sql)->fetch_object();
            $nrSsp = $row->nr_ssp;

            $sql = "
                SELECT COUNT(*) AS nr_gbif
                FROM birdsounds AS t1
                LEFT JOIN `users` AS t2 ON t1.dir = t2.dir
                LEFT JOIN taxonomy AS t3 ON t1.species_nr = t3.species_nr
                WHERE t2.third_party_license = 1 AND t3.restricted = 0 AND t1.group_id = $id";
            $row = $this->query($sql)->fetch_object();
            $nrGbif = intval($row->nr_gbif);

            $sql = "
                SELECT COUNT(*) AS nr_ids 
                FROM birdsounds 
                WHERE " . Query::ignoreMysteryAndPseudoSpeciesSql() . " AND group_id = $id";
            $row = $this->query($sql)->fetch_object();
            $nrIds = intval($row->nr_ids);

            $sql = "
                SELECT COUNT(*) as nr_backids 
                FROM birdsounds_background AS t1
                LEFT JOIN birdsounds AS t2 on t1.snd_nr = t2.snd_nr
                WHERE t2.group_id = $id";
            $row = $this->query($sql)->fetch_object();
            $nrBackids = intval($row->nr_backids);

            $sql = "
                INSERT INTO latest_statistics_next 
                VALUES
                ($nrRecordings, $totalDuration, $nrRecordists, $nrSpecies, $nrSsp, $nrGbif, $id, $nrIds, $nrBackids)";
            $this->query($sql);

            // update the latest species table
            $sql = "
                INSERT INTO latest_species_next 
                    SELECT species_nr, min(snd_nr) as nr, $id 
                    FROM birdsounds 
                    WHERE " . Query::ignoreMysteryAndPseudoSpeciesSql() . " AND group_id = $id
                    GROUP BY species_nr 
                    ORDER BY nr DESC";
            $this->query($sql);

            // update the latest recordists table (add group-specific stats, even though
            // they aren't used right now)
            $sql = "
                INSERT INTO latest_recordists_next 
                    SELECT dir, min(snd_nr) AS nr, $id 
                    FROM birdsounds 
                    WHERE group_id = $id
                    GROUP BY dir 
                    ORDER BY nr DESC
                    LIMIT 25";
            $this->query($sql);
        }

        $sql = "
            INSERT INTO latest_recordists_next 
                SELECT dir, min(snd_nr) AS nr, 0 
                FROM birdsounds 
                GROUP BY dir 
                ORDER BY nr DESC
                LIMIT 25";
        $this->query($sql);

        $sql = "
            INSERT INTO latest_statistics_next VALUES (
                (SELECT SUM(nr_recordings) FROM latest_statistics_next AS t1),
                (SELECT SUM(total_duration) FROM latest_statistics_next AS t2),
                (SELECT COUNT(DISTINCT(dir)) FROM birdsounds), 
                (SELECT SUM(nr_species) FROM latest_statistics_next AS t3), 
                (SELECT SUM(nr_ssp) FROM latest_statistics_next AS t4), 
                (SELECT SUM(nr_gbif) FROM latest_statistics_next AS t5),
                0,
                (SELECT SUM(nr_ids) FROM latest_statistics_next AS t6),
                (SELECT COUNT(*) FROM birdsounds_background)
            )
        ";
        $this->query($sql);

        $this->replaceTable('latest_statistics', 'latest_statistics_next');
        $this->replaceTable('latest_species', 'latest_species_next');
        $this->replaceTable('latest_recordists', 'latest_recordists_next');
    }
}

$task = new UpdateStatsHourlyTask();
$task->run();
